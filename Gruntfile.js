module.exports = function(grunt) {

	// Project configuration
	grunt.initConfig({

		concat: {
			options: {
				separator: ';'
			},
			js_frontend: {
				src: ['bower_components/jquery/dist/jquery.js',
							'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/affix.js',
							'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/alert.js',
							'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/button.js',
							'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/carousel.js',
							'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/collapse.js',
							'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/dropdown.js',
							'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/tab.js',
							'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/transition.js',
							'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/scrollspy.js',
							'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/modal.js',
							'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/tooltip.js',
							'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/popover.js',
							'bower_components/jquery-listnav/jquery-listnav.js',
							'app/assets/javascript/frontend.js'],
				dest: 'public/assets/javascript/frontend.min.js'
			},
			js_backend: {
				src: ['bower_components/jquery/dist/jquery.js',
							'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/affix.js',
							'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/alert.js',
							'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/button.js',
							'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/carousel.js',
							'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/collapse.js',
							'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/dropdown.js',
							'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/tab.js',
							'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/transition.js',
							'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/scrollspy.js',
							'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/modal.js',
							'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/tooltip.js',
							'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/popover.js',
							'bower_components/datatables/media/js/jquery.dataTables.js',
							'bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.js',
							'bower_components/datatables-responsive/js/dataTables.responsive.js',
							'bower_components/bootstrap-file-input/bootstrap.file-input.js',
							'bower_components/bootstrap-confirmation2/bootstrap-confirmation.js',
							'app/assets/javascript/backend.js'],
				dest: 'public/assets/javascript/backend.min.js'
			}
		},
		uglify: {
			options: {
        //mangle: false // Keep the names of functions and variables unchanged
      },
			js_frontend: {
				files: {
					'public/assets/javascript/frontend.min.js': 'public/assets/javascript/frontend.min.js'
				}
			},
			js_backend: {
				files: {
					'public/assets/javascript/backend.min.js': 'public/assets/javascript/backend.min.js'
				}
			}
		},
		sass: {
			options: {
				sourcemap: 'auto',
				style: 'compressed',
				precision: 10
			},
			frontend: {
				files: {
					'public/assets/stylesheets/frontend.min.css': 'app/assets/stylesheets/frontend.scss'
				}
			}
		},
		autoprefixer: {
			options: {
				browsers: ['last 2 versions', 'ie 8', 'ie 9']
			},
			frontend: {
				src: 'public/assets/stylesheets/frontend.min.css',
				dest: 'public/assets/stylesheets/frontend.min.css'
			}
		},
		watch: {
			js_frontend: {
        files: [
          'app/assets/javascript/frontend.js'
        ],
        tasks: ['concat:js_frontend', 'uglify:js_frontend'],
        options: {
          livereload: true
        }
      },
      js_backend: {
        files: [
          'app/assets/javascript/backend.js'
        ],
        tasks: ['concat:js_backend', 'uglify:js_backend'],
        options: {
          livereload: true
        }
      },
      sass: {
        files: ['app/assets/stylesheets/*.scss'],
        tasks: ['sass'],
        options: {
          livereload: true
        }
      },
      php: {
        files: ['app/views/**/*.php'],
        options: {
          livereload: true
        }
      }
		}
		
	});

	// Plugin loading
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-autoprefixer');

  // Task definition
  grunt.registerTask('build', ['concat', 'uglify', 'sass', 'autoprefixer']);
	grunt.registerTask('default', ['build', 'watch']);
	
}