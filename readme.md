## JUARDE
[juarde.com.mx](http://www.juarde.com.mx)

### Requerimientos para trabajar con el proyecto:

*  XAMPP/MAMP
*  Composer
*  Ruby
*  Node.js
*  Grunt
*  Bower

### Instalar el framework Laravel

```
#!bash

composer install
```

### Instalar las dependencias del front-end

```
#!bash

bower install
```

### Compilar y observar cambios en archivos sass y javascript

```
#!bash

grunt
```
