@extends('english.layouts.master')

@section('content')
		<section class="container">

			<div class="row">
				<div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
					<ul class="list-group" id="pages-listnav">
					@if (!empty($records))
						@foreach($records as $record)
							<li class="list-group-item">
								<a href="#modal-pages" class="show" id="{{$record->id}}" data-toggle="modal" data-target="#modal-pages">
									{{ $record->business_name }}
								</a>
							</li>
						@endforeach
					
					@else
					<h4>There are no businesses in database<h4>
					@endif

					</ul>				
				</div>
			</div><!-- /.row -->
		</section><!-- container -->

		<div class="modal fade" id="modal-pages">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title"></h4>
					</div>
					<div class="modal-body table-responsive">
						<table class="table table-striped">
							<tbody>
							</tbody>
						</table>
						<img src="" alt="" class="img-responsive" id="pages-image">
					</div><!-- /.modal-body -->
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal #modal-show-->

@stop
