@extends('english.layouts.master')

@section('content')
		<section class="container">
			<div class="row">
				<div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
					<ul class="list-group" id="categories-listnav">

					@if (!empty($categories))
					@foreach($categories as $lista)
						<li class="list-group-item">
							<a href="{{URL::to('advertisements?search='),$lista->category}}">{{ $lista->category }}</a>
						</li>
					@endforeach

					@else
					<h3>There are no categories<h3>
					@endif

					</ul>					
				</div>
			</div>
		</section>
@stop
