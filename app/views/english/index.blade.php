@extends('english.layouts.master')

@section('content')
		<section class="content container" id="landing">
			<div class="row">
				<div class="col-xs-10 col-sm-6  col-xs-offset-1 col-sm-offset-3">
					<img src="assets/images/logo.png" class="img-responsive" alt="Juarde Logo">
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-sm-8 col-sm-offset-2">
					<form action="advertisements" method="get">
						<div class="input-group input-group-lg">
							<input type="text" class="form-control" placeholder="Example: Hotels in San Miguel" name="search">
							<span class="input-group-btn"><button class="btn btn-search" type="submit">Search</button></span>
						</div>
					</form>
				</div>
			</div>
		</section>
@stop