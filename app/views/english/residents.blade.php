@extends('english.layouts.master')

@section('content')
		<section class="container">
			<div class="row">
				<div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
					<ul class="list-group" id="residents-listnav">
					@if (!empty($residents))
						@foreach($residents as $lista)
							<li class="list-group-item">
								<a href="#modal-show" class="show" id="{{$lista->id}}" data-toggle="modal" data-target="#modal-show">
									{{ $lista->last_name1 }}
									{{ $lista->first_name1 }}
									{{ $lista->last_name2 }}
									{{ $lista->first_name2 }}
								</a>
							</li>
						@endforeach
					
					@else
					<h4>There are no residents in database<h4>
					@endif

					</ul>				
				</div>
			</div><!-- /.row -->
		</section>

		<div class="modal fade" id="modal-show">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title"><i class="fa fa-user fa-fw"></i><span id="residents-name"></span></h4>
					</div>
					<div class="modal-body">
						<table class="table table-striped table-responsive">
							<tbody>
							</tbody>
						</table>
					</div><!-- /.modal-body -->
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal #modal-show-->

@stop
