@extends('english.layouts.master')

<?php
	$months=GlobalController::dates();
	$temporal_month=null;
?>

@section('content')
		<section class="container">
			<div class="row">

				<div class="col-sm-8 col-sm-offset-2">
					
					@if (!empty($events))
						@foreach($events as $lista)

    					@if($temporal_month!=date('m',strtotime($lista->date)))
    						<hr>
    						<h3>{{$months[date('m',strtotime($lista->date))]}}</h3>
    						<?php $temporal_month=date('m',strtotime($lista->date)); ?>
    					@endif
							
							<blockquote>
								<p>
									<span class="fa-stack">
										<i class="fa fa-calendar-o fa-stack-2x"></i>
										<strong class="fa-stack-1x calendar-text">{{ date('d',strtotime($lista->date)) }}</strong>
									</span>
									{{ $lista->description }}
								</p>
							</blockquote>
					
						@endforeach
					
					@else
						<h3>No existen Eventos<h3>
					@endif					
				
				</div>
			</div>
		</section>
@stop
