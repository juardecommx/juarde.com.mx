@extends('spanish.layouts.master')

<?php 
	$meses=GlobalController::fechas();
	$mes_temporal=null;
?>

@section('content')
		<section class="container">
			<div class="row">

				<div class="col-sm-8 col-sm-offset-2">
					
					@if (!empty($eventos))
						@foreach($eventos as $lista)
    				
    					@if($mes_temporal!=date('m',strtotime($lista->fecha)))
    						<hr>
    						<h3>{{$meses[date('m',strtotime($lista->fecha))]}}</h3>
    						<?php $mes_temporal=date('m',strtotime($lista->fecha)); ?>
    					@endif
						
							<blockquote>
								<p>
									<span class="fa-stack">
										<i class="fa fa-calendar-o fa-stack-2x"></i>
										<strong class="fa-stack-1x calendar-text">{{ date('d',strtotime($lista->fecha)) }}</strong>
									</span>
									{{ $lista->descripcion }}
								</p>
							</blockquote>
							
						@endforeach
					
					@else
						<h3>No existen Eventos<h3>
					@endif					
				
				</div>
			</div>
		</section>
@stop
