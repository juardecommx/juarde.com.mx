<!DOCTYPE html>
<html lang="">
	<head>
		@section('meta')
			<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1">
		@show

		<title>
			@section('title')
	  			Juarde
	  	@show
		</title>

		@section('css')
  			{{ HTML::style('assets/stylesheets/frontend.min.css')}}
  	@show

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<?php $links=GlobalController::englishLinks(); ?>
		@section('navigator')
		<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
			<div class="container">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <!-- <a class="navbar-brand" href="#">Brand</a> -->
		    </div>

		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="row nav nav-pills">
		        <li class="col-xs-4 col-sm-2 col-md-2 col-lg-2 {{ (Request::is('inicio') ? 'active' : '') }}">
		        	<a href="{{URL::to('inicio')}}">
		        		<i class="fa fa-home fa-3x"></i>
								<p>Inicio</p>
		        	</a>
		        </li>
		        <li class="col-xs-4 col-sm-2 col-md-2 col-lg-2 {{ (Request::is('categorias') ? 'active' : '') }}">
		        	<a href="{{URL::to('categorias')}}">
								<i class="fa fa-th-large fa-3x"></i>
								<p>Categorías</p>
							</a>
		        </li>
		        <li class="col-xs-4 col-sm-2 col-md-2 col-lg-2 {{ (Request::is('residentes') ? 'active' : '') }}">
		        	<a href="{{URL::to('residentes')}}">
								<i class="fa fa-users fa-3x"></i>
								<p>Residentes</p>
							</a>
		        </li>
		        <li class="col-xs-4 col-sm-2 col-md-2 col-lg-2 {{ (Request::is('paginas') ? 'active' : '') }}">
		        	<a href="{{URL::to('paginas')}}">
								<i class="fa fa-book fa-3x"></i>
								<p>Páginas</p>
							</a>
		        </li>
		        <li class="col-xs-4 col-sm-2 col-md-2 col-lg-2 {{ (Request::is('eventos') ? 'active' : '') }}">
		        	<a href="{{URL::to('eventos')}}">
								<i class="fa fa-calendar fa-3x"></i>
								<p>Eventos</p>
							</a>
		        </li>
		        <li class="col-xs-4 col-sm-2 col-md-2 col-lg-2 {{ (Request::is('contacto') ? 'active' : '') }}">
		        	<a href="{{URL::to('contacto')}}">
								<i class="fa fa-envelope fa-3x"></i>
								<p>Contacto</p>
							</a>
		        </li>
		      </ul>
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
		<section class="container" id="menu">
			<div class="row">
				<div class="col-xs-8 col-sm-6"id="menu-language">

					<?php $link=GlobalController::saveGetSpanish($links); ?>

					<ul class="nav nav-pills">
						<li><a href="{{URL::to($link)}}">Inglés</a></li>
						<li><a href="#">Español</a></li>
					</ul>
				</div>
				<div class="col-xs-4 col-sm-6" id="menu-login">
					<ul class="nav nav-pills pull-right">
						<!-- <li><a href="{{URL::to('admin')}}">Entrar</a></li> -->
						<li><a href="#" tabindex="0" id="login">Entrar</a></li>
					</ul>
				</div>
			</div>
		</section>
		@show

		@yield('content')

		@section('footer')
		<section class="footer">
			<div class="container">
				<div class="hidden-xs hidden-sm col-md-4" id="site-map">
					<ul>
						<li><a href="{{URL::to('inicio')}}">Inicio</a></li>
						<li><a href="{{URL::to('categorias')}}">Categorías</a></li>
						<li><a href="#">Residentes</a></li><br>
						<li><a href="#">Anuncios</a></li>
						<li><a href="{{URL::to('eventos')}}">Eventos</a></li>
						<li><a href="#">Contacto</a></li>
					</ul>
				</div>
				<div class="col-xs-12 col-md-4 col-md-push-4" id="social-icons">
					<a href="#">
						<span class="fa-stack fa-lg">
							<i class="fa fa-circle fa-stack-2x fa-inverse"></i><i class="fa fa-linkedin fa-stack-1x"></i>
						</span>
					</a>
					<a href="#">
						<span class="fa-stack fa-lg">
							<i class="fa fa-circle fa-stack-2x fa-inverse"></i><i class="fa fa-facebook fa-stack-1x"></i>
						</span>
					</a>
					<a href="#">
						<span class="fa-stack fa-lg">
							<i class="fa fa-circle fa-stack-2x fa-inverse"></i><i class="fa fa-twitter fa-stack-1x"></i>
						</span>
					</a>
					<a href="#">
						<span class="fa-stack fa-lg">
							<i class="fa fa-circle fa-stack-2x fa-inverse"></i><i class="fa fa-pinterest fa-stack-1x"></i>
						</span>
					</a>
					<a href="#">
						<span class="fa-stack fa-lg">
							<i class="fa fa-circle fa-stack-2x fa-inverse"></i><i class="fa fa-google-plus fa-stack-1x"></i>
						</span>
					</a>
				</div>
				<div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-0 col-md-pull-4" id="copyright">
					<p>&copy; {{date('Y')}} Copyright by JUARDE. All rights reserved.</p>
				</div>
			</div><!-- .container -->
		</section><!-- .footer -->
		@show
		
				@section('login')
		<div id="popover-login" class="hide">
			{{ Form::open(array('url'=>'login'), array('role'=>'form')) }}
	       <div class="form-group">
	        <label for="username" class="sr-only">Usuario</label>
	        {{ Form::text('name',Input::old('name'), array('id'=>'username','class'=>'form-control input-sm', 'placeholder'=>'Usuario')) }}
	        </div>
	        <div class="form-group">
	        	<label for="password" class="sr-only">Contraseña</label>
	        	{{ Form::password('password', array('id'=>'password', 'class'=>'form-control input-sm', 'placeholder'=>'Contraseña')) }}
	        </div>
	        <button id="submit" type="submit" class="btn btn-sm btn-primary btn-block" value="Enviar">Entrar</button>
				</div>
			{{ Form::close() }}
		</div>
		@show

		@section('js')
		<script>
			var baseURL = "<?php echo URL::to('/'); ?>";
		</script>
	    {{ HTML::script('assets/javascript/frontend.min.js') }}
		@show
	</body>
</html>