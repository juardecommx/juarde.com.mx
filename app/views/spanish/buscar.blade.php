@extends('spanish.layouts.master')

@section('content')
		<section class="container">
			<div class="row">
				<div class="col-sm-3 col-md-2" id="search-logo">
					<img src="assets/images/logo.png" alt="Juarde Logo" class="img-responsive" id="search-logo-img">
				</div>
				<div class="col-sm-6 col-md-5" id="search-input-group">
					<form action="buscar" method="get">
						<div class="input-group">
							<input type="text" class="form-control" name="search" value="{{$query['search']}}">
							<span class="input-group-btn"><button class="btn btn-search" type="submit">Buscar</button></span>
						</div>
					</form>
				</div>
			</div><!-- row -->
			<div class="row">
				<div class="col-sm-12">
					@if (!empty($query['advertisements']) and count($query['results'])>0)
					<h5 id="result-counter">{{count($query['results'])}} 

						@if(count($query['results'])>1)
						Resultados encontrados de 
						@else
						Resultado encontrado de 
						@endif

						"{{$query['search']}}"</h5>

						<div class="panel-group" id="buscar-accordion" role="tablist" aria-multiselectable="true">
							<?php $i = 0 ?>
						@foreach($query['advertisements'] as $query['advertisements'])
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="heading{{$query['advertisements']->advertiser_id}}">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#buscar-accordion" href="#collapse{{$query['advertisements']->advertiser_id}}" aria-expanded="{{($i == 0) ? 'true' : 'false'}}" aria-controls="collapse{{$query['advertisements']->advertiser_id}}">
											{{ $query['advertisements']->business_name }}
										</a>
									</h4>
								</div>
								<div id="collapse{{$query['advertisements']->advertiser_id}}" class="panel-collapse collapse {{($i == 0) ? 'in' : ''}}" role="tabpanel" aria-labelledby="heading{{$query['advertisements']->advertiser_id}}">
										
									<table class="table table-condensed">
										<tbody>
									@if(file_exists(public_path().'/assets/thumbs/'.$query['advertisements']->advertiser_id.'.jpg'))
										<tr>
											<td colspan="2"><a href="#modal-buscar" class="show" id="{{$query['advertisements']->advertiser_id}}" data-toggle="modal" data-target="#modal-buscar"> 
									  	{{ HTML::image('/assets/thumbs/'.$query['advertisements']->advertiser_id.'.jpg', $query['advertisements']->business_name) }}
									  	</a></td>
										</tr>
									@endif
									@if (!empty($query['advertisements']->address))
										<tr>
											<td><strong>Dirección:</strong></td>
											<td><i class="fa fa-map-marker fa-lg fa-fw"></i>{{ $query['advertisements']->address}}</td>
										</tr>
									@endif
									@if (!empty($query['advertisements']->phone))
										<tr>
											<td><strong>Teléfono:</strong></td>
											<td><i class="fa fa-phone fa-lg fa-fw"></i><a href="tel:{{$query['advertisements']->phone}}">{{ $query['advertisements']->phone}}</a></td>
										</tr>
									@endif
									@if (!empty($query['advertisements']->phone2))
										<tr>
											<td><strong>Teléfono 2:</strong></td>
											<td><i class="fa fa-phone fa-lg fa-fw"></i><a href="tel:{{$query['advertisements']->phone2}}">{{ $query['advertisements']->phone2}}</a></td>
										</tr>
									@endif
									@if (!empty($query['advertisements']->mobile_phone))
										<tr>
											<td><strong>Celular:</strong></td>
											<td><i class="fa fa-mobile fa-lg fa-fw"></i><a href="tel:{{$query['advertisements']->mobile_phone}}">{{ $query['advertisements']->mobile_phone}}</a></td>
										</tr>
									@endif
									@if (!empty($query['advertisements']->mobile_phone2))
										<tr>
											<td><strong>Celular2:</strong></td>
											<td><i class="fa fa-mobile fa-lg fa-fw"></i><a href="tel:{{$query['advertisements']->mobile_phone2}}">{{ $query['advertisements']->mobile_phone2}}</a></td>
										</tr>
									@endif
									@if (!empty($query['advertisements']->nextel))
										<tr>
											<td><strong>Nextel:</strong></td>
											<td><i class="fa fa-mobile fa-lg fa-fw"></i><a href="tel:{{$query['advertisements']->nextel}}">{{ $query['advertisements']->nextel}}</a></td>
										</tr>
									@endif
									@if (!empty($query['advertisements']->email))
										<tr>
											<td><strong>Email:</strong></td>
											<td><i class="fa fa-envelope fa-lg fa-fw"></i><a href="mailto:{{ $query['advertisements']->email}}">{{ $query['advertisements']->email}}</a></td>
										</tr>
									@endif
									@if (!empty($query['advertisements']->email2))
										<tr>
											<td><strong>Email 2:</strong></td>
											<td><i class="fa fa-envelope fa-lg fa-fw"></i><a href="mailto:{{ $query['advertisements']->email2}}">{{ $query['advertisements']->email2}}</a></td>
										</tr>
									@endif
									@if (!empty($query['advertisements']->web_page))
										<tr>
											<td><strong>Sitio Web:</strong></td>
											<td><i class="fa fa-external-link-square fa-lg fa-fw"></i><a href="http://{{$query['advertisements']->web_page}}" target="_blank">{{ $query['advertisements']->web_page }}</a></td>
										</tr>
									@endif
									</tbody>
									</table>
							</div><!-- panel-collapse -->
							</div><!-- panel -->
							<?php $i++ ?>
						@endforeach
						</div><!-- /.panel-group -->
						{{ $query['pagination']->appends(array('search' => $query['search']))->links() }}
					@else
						<p>La búsqueda "{{$query['search']}}" no obtuvo ningun resultado.<p>
						<p>Sugerencias:</p>
						<ul>
							<li>Comprueba que todas las palabras están escritas correctamente.</li>
							<li>Intenta usar otras palabras.</li>
							<li>Intenta usar palabras más generales.</li>
						</ul>
					@endif		
				</div>
			</div><!-- row -->
		</section><!-- container -->

		<div class="modal fade" id="modal-buscar">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title"></h4>
					</div>
					<div class="modal-body table-responsive">
						<table class="table table-striped">
							<tbody>
							</tbody>
						</table>
						<img src="" alt="" class="img-responsive" id="buscar-image">
					</div><!-- /.modal-body -->
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal #modal-show-->

@stop
