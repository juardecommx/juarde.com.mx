@extends('spanish.layouts.master')

@section('content')
		<section class="container">
			<div class="row">
				<div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
					<ul class="list-group" id="categorias-listnav">

					@if (!empty($categorias))
					@foreach($categorias as $lista)
						<li class="list-group-item">
							<a href="{{URL::to('buscar?search='),$lista->categoria}}">{{ $lista->categoria }}</a>
						</li>
					@endforeach
					
					@else
					<h3>No existen Categorias<h3>
					@endif	
					
					</ul>				
				</div>
			</div>
		</section>
@stop
