@extends('spanish.layouts.master')

@section('content')
		<section class="container" id="contact">
			<div class="row">
				<div class="col-sm-5 col-sm-offset-1" id="contact-address">
					<div class="row">
						<div class="col-xs-10 col-sm-8 col-xs-offset-1">
							<img src="assets/images/logo.png" alt="Juarde Logo" class="img-responsive">
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-sm-12">
							<address>
								<strong>Dirección:</strong>
								{{$contact->address}}<br>
								<strong>Teléfonos:</strong> {{$contact->phones}}<br>
								<strong>Email:</strong> {{$contact->email}}
							</address>
						</div>
					</div>
				</div><!-- /#contact-address -->
				<div class="col-sm-5" id="contact-form">
					
					{{ Form::open(array(
						'url'=>'enviar',
						'method'=>'POST',
						'role'=>'form',
						)) }}
						<div class="row">
							<label class="col-sm-4" for="name">Nombre</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" id="name" name="name">
							</div>
						</div>
						<div class="row">
							<label class="col-sm-4" for="email">Email</label>
							<div class="col-sm-8">
								<input type="email" class="form-control" id="email" name="email">
							</div>
						</div>
						<div class="row">
							<label class="col-sm-4" for="message">Comentarios</label>
							<div class="col-sm-8">
								<textarea class="form-control" rows="5" id="message" name="content"></textarea>
							</div>
						</div>
						<div class="row">
							<label class="col-sm-4 formValidator" for="formValidator">No llenar este campo</label>
							<div class="col-sm-8 formValidator">
								<input type="text" class="form-control" id="formValidator" name="formValidator">
							</div>
						</div>
						<div class="row">
							<div class="col-sm-8 col-sm-offset-4">
								<button type="submit" class="btn btn-default btn-block">Enviar</button>
							</div>
						</div>
					{{ Form::close() }}
				</div><!-- /#contact-form -->
			</div>
		</section>
@stop
