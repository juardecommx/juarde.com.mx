<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Juarde</title>

		<link href="assets/stylesheets/frontend.css" rel="stylesheet">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>

		<nav class="navbar navbar-default" role="navigation">
			<div class="container">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <!-- <a class="navbar-brand" href="#">Brand</a> -->
		    </div>

		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="row nav nav-pills">
		        <li class="col-xs-2 col-sm-2 col-md-2 col-lg-2 active">
		        	<a href="#">
		        		<i class="fa fa-home fa-3x"></i>
								<p>Home</p>
		        	</a>
		        </li>
		        <li class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
		        	<a href="#">
								<i class="fa fa-th-large fa-3x"></i>
								<p>Categories</p>
							</a>
		        </li>
		        <li class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
		        	<a href="#">
								<i class="fa fa-users fa-3x"></i>
								<p>Residents</p>
							</a>
		        </li>
		        <li class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
		        	<a href="#">
								<i class="fa fa-book fa-3x"></i>
								<p>Yellow Pages</p>
							</a>
		        </li>
		        <li class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
		        	<a href="#">
								<i class="fa fa-calendar fa-3x"></i>
								<p>Events</p>
							</a>
		        </li>
		        <li class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
		        	<a href="#">
								<i class="fa fa-envelope fa-3x"></i>
								<p>Contact</p>
							</a>
		        </li>
		      </ul>
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>

		<section class="content container" id="main">
			<div class="row">
				<div class="col-sm-6 col-sm-offset-3">
					<img src="assets/images/logo.png" class="img-responsive" alt="Juarde Logo">
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-sm-8 col-sm-offset-2">
					<form action="advertisements" method="get">
					<div class="input-group input-group-lg">
						<input type="text" class="form-control" placeholder="Example: Hotels in San Miguel" name="search">
						<span class="input-group-btn"><button class="btn btn-search" type="submit">Search</button></span>
					</div>
					</form>
				</div>
			</div>
		</section>

		<section class="footer">
			<div class="container">
				<div class="row">
					<div class="col-md-4" id="site-map">
						<ul>
							<li><a href="#">Home</a></li>
							<li><a href="#">Categories</a></li>
							<li><a href="#">Residents</a></li><br>
							<li><a href="#">Yellow Pages</a></li>
							<li><a href="#">Events</a></li>
							<li><a href="#">Contact</a></li>
						</ul>
					</div>
					<div class="col-md-4" id="copyright">
						<p>&copy; 2014 Copyright by GIDEA. All rights reserved.</p>
					</div>
					<div class="col-md-4" id="social-icons">
						<a href="#">
							<span class="fa-stack fa-lg">
								<i class="fa fa-circle fa-stack-2x fa-inverse"></i><i class="fa fa-linkedin fa-stack-1x"></i>
							</span>
						</a>
						<a href="#">
							<span class="fa-stack fa-lg">
								<i class="fa fa-circle fa-stack-2x fa-inverse"></i><i class="fa fa-facebook fa-stack-1x"></i>
							</span>
						</a>
						<a href="#">
							<span class="fa-stack fa-lg">
								<i class="fa fa-circle fa-stack-2x fa-inverse"></i><i class="fa fa-twitter fa-stack-1x"></i>
							</span>
						</a>
						<a href="#">
							<span class="fa-stack fa-lg">
								<i class="fa fa-circle fa-stack-2x fa-inverse"></i><i class="fa fa-pinterest fa-stack-1x"></i>
							</span>
						</a>
						<a href="#">
							<span class="fa-stack fa-lg">
								<i class="fa fa-circle fa-stack-2x fa-inverse"></i><i class="fa fa-google-plus fa-stack-1x"></i>
							</span>
						</a>
					</div>
				</div>
			</div>
		</section>

		<script src="assets/javascript/frontend.js"></script>
	</body>
</html>