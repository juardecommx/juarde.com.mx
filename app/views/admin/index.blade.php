@extends('admin.layouts.master')

@section('content')
		<section class="content container" id="landing">
			<div class="row">
				<div class="col-xs-10 col-sm-6  col-xs-offset-1 col-sm-offset-3">
					<img src="assets/images/logo.png" class="img-responsive" alt="Juarde Logo">
					<h2 class="text-center"><small>Welcome to the Admin Area</small></h2>
				</div>
			</div>
		</section>
@stop