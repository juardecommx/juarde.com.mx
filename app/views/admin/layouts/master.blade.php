<!DOCTYPE html>
<html lang="">
	<head>
		@section('meta')
			<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1">
		@show

		<title>
			@section('title')
	  			Juarde
	  	@show
		</title>

		@section('css')
  			{{ HTML::style('assets/stylesheets/frontend.min.css')}}
  	@show

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		@section('navigator')
		<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
			<div class="container">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <!-- <a class="navbar-brand" href="#">Brand</a> -->
		    </div>

		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="row nav nav-pills">
		        <li class="col-xs-4 col-sm-2 col-md-2 col-lg-2 {{ (Request::is('admin') ? 'active' : '') }}">
		        	<a href="{{URL::to('admin')}}">
		        		<i class="fa fa-home fa-3x"></i>
								<p>Home</p>
		        	</a>
		        </li>
		        <li class="col-xs-4 col-sm-2 col-md-2 col-lg-2 {{ (Request::is('admin/categories/list') ? 'active' : '') }}">
		        	<a href="{{URL::to('admin/categories/list')}}">
								<i class="fa fa-th-large fa-3x"></i>
								<p>Categories</p>
							</a>
		        </li>
		        <li class="col-xs-4 col-sm-2 col-md-2 col-lg-2 {{ (Request::is('admin/residents/list') ? 'active' : '') }}">
		        	<a href="{{URL::to('admin/residents/list')}}">
								<i class="fa fa-users fa-3x"></i>
								<p>Residents</p>
							</a>
		        </li>
		        <li class="col-xs-4 col-sm-2 col-md-2 col-lg-2 {{ (Request::is('admin/pages/list') ? 'active' : '') }}">
		        	<a href="{{URL::to('admin/pages/list')}}">
								<i class="fa fa-book fa-3x"></i>
								<p>Yellow Pages</p>
							</a>
		        </li>
		        <li class="col-xs-4 col-sm-2 col-md-2 col-lg-2 {{ (Request::is('admin/events/list') ? 'active' : '') }}">
		        	<a href="{{URL::to('admin/events/list')}}">
								<i class="fa fa-calendar fa-3x"></i>
								<p>Events</p>
							</a>
		        </li>
		        <li class="col-xs-4 col-sm-2 col-md-2 col-lg-2 {{ (Request::is('admin/contact/list') ? 'active' : '') }}">
		        	<a href="{{URL::to('admin/contact/list')}}">
								<i class="fa fa-envelope fa-3x"></i>
								<p>Contact</p>
							</a>
		        </li>
		      </ul>
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
		<section class="container" id="menu">
			<div class="row">
<!-- 				<div class="col-xs-8 col-sm-6"id="menu-language">
					<ul class="nav nav-pills">
						<li><a href="#">English</a></li>
						<li><a href="#">Spanish</a></li>
					</ul>
				</div> -->
				<div class="col-xs-6 col-xs-offset-6" id="menu-login">
					<ul class="nav nav-pills pull-right">
						<li><a href="{{ URL::to('logout') }}">Log Out</a></li>
					</ul>
				</div>
			</div>
		</section>
		@show

		@yield('content')

		@section('footer')
		<section class="footer">
			<div class="container">
				<div class="hidden-xs hidden-sm col-md-4" id="site-map">
					<ul>
						<li><a href="{{URL::to('home')}}">Home</a></li>
						<li><a href="{{URL::to('categories')}}">Categories</a></li>
						<li><a href="#">Residents</a></li><br>
						<li><a href="#">Yellow Pages</a></li>
						<li><a href="#">Events</a></li>
						<li><a href="#">Contact</a></li>
					</ul>
				</div>
				<div class="col-xs-12 col-md-4 col-md-push-4" id="social-icons">
					<a href="#">
						<span class="fa-stack fa-lg">
							<i class="fa fa-circle fa-stack-2x fa-inverse"></i><i class="fa fa-linkedin fa-stack-1x"></i>
						</span>
					</a>
					<a href="#">
						<span class="fa-stack fa-lg">
							<i class="fa fa-circle fa-stack-2x fa-inverse"></i><i class="fa fa-facebook fa-stack-1x"></i>
						</span>
					</a>
					<a href="#">
						<span class="fa-stack fa-lg">
							<i class="fa fa-circle fa-stack-2x fa-inverse"></i><i class="fa fa-twitter fa-stack-1x"></i>
						</span>
					</a>
					<a href="#">
						<span class="fa-stack fa-lg">
							<i class="fa fa-circle fa-stack-2x fa-inverse"></i><i class="fa fa-pinterest fa-stack-1x"></i>
						</span>
					</a>
					<a href="#">
						<span class="fa-stack fa-lg">
							<i class="fa fa-circle fa-stack-2x fa-inverse"></i><i class="fa fa-google-plus fa-stack-1x"></i>
						</span>
					</a>
				</div>
				<div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-0 col-md-pull-4" id="copyright">
					<p>&copy; {{date('Y')}} Copyright by JUARDE. All rights reserved.</p>
				</div>
			</div>
		</section>
		@show
		
		@section('js')
			<script>var baseURL = "<?php echo URL::to('/'); ?>"; </script>
	    {{ HTML::script('assets/javascript/backend.min.js') }}
		@show
	</body>
</html>