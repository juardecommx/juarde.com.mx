@extends('admin.layouts.master')

@section('content')
	<section class="content container">
		<div class="row">
			<div class="col-sm-offset-3 col-sm-7">
				<h3>Contact Information</h3>
			</div>
		</div>
		
		{{ Form::open(array('url' => 'admin/contact/update', 'class'=>'form-horizontal', 'role'=>'form')) }}
			<div class="form-group">
				{{ Form::label('address', 'Address', array('class'=>'col-sm-offset-1 col-sm-2 control-label')) }}
			  	<div class="col-sm-7">
			  		{{ Form::text('address', $contact->address, array('class'=>'form-control')) }}
					</div>
			</div>
			<div class="form-group">
				{{ Form::label('phones', 'Phones', array('class'=>'col-sm-offset-1 col-sm-2 control-label')) }}
				<div class="col-sm-7">
					{{ Form::text('phones', $contact->phones, array('class'=>'form-control')) }}
				</div>
			</div>
			<div class="form-group">
				{{ Form::label('email', 'E-Mail Address', array('class'=>'col-sm-offset-1 col-sm-2 control-label')) }}
				<div class="col-sm-7">
					{{ Form::text('email', $contact->email, array('class'=>'form-control')) }}
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-3 col-sm-7">
					{{ Form::submit('Save', array('class'=>'btn btn-primary'))}}
				</div>
			</div>
		{{ Form::close() }}

		</section>
@stop