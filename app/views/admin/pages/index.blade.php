@extends('admin.layouts.master')

@section('content')
		<section class="content container">
		@if(Session::has('message'))
				<?php $class =  Session::get('class'); ?>
				<div class="alert alert-{{$class}} alert-dismissable fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					@if($class=='success')
						<i class="fa fa-check-circle"></i>
					@endif
					@if($class=='warning')
						<i class="fa fa-exclamation-circle"></i>
					@endif
					@if($class=='danger')
						<i class="fa fa-exclamation-circle"></i>
					@endif
					{{ Session::get('message') }}
				</div>
			@endif

			<table class="table table-striped table-hover table-condensed" id="table-pages">
				<thead>
					<tr>
						<th>Name</th>
						<th class="all">Actions</th>
					</tr>
				</thead>
				<tbody>
				@foreach($query as $query)
					<tr>
						<td>{{ $query->business_name }}</td>
						<td class="nowrap">
							<a href="#modal-edit" data-toggle="modal" data-target="#modal-edit" class="btn btn-primary btn-xs edit" id="{{$query->id}}"><i class="fa fa-pencil-square fa-fw"></i>Edit</a>
							<a href="{{URL::to('admin/pages/destroy',$query->id)}}" class="btn btn-danger btn-xs" data-toggle="confirmation"><i class="fa fa-minus-square fa-fw"></i>Delete</a>
						</td>
					</tr>
			@endforeach
				</tbody>
			</table>
		</section>

		<div class="modal fade" id="modal-new">
			<div class="modal-dialog">
				<div class="modal-content">
					{{ Form::open(array('url' => 'admin/pages/store', 'files' => true, 'class'=>'form-horizontal', 'id'=>'form-new', 'role'=>'form')) }}
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title"><i class="fa fa-plus-square fa-fw"></i>New</h4>
					</div>
					<div class="modal-body">
						
						<div class="form-group">
							<label for="business_name" class="col-sm-3 control-label">Business Name</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="business_name" name="business_name" required>
							</div>
						</div>

						<div class="form-group">
							<label for="address" class="col-sm-3 control-label">Address</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="address" name="address">
							</div>
						</div>

						<div class="form-group">
							<label for="email" class="col-sm-3 control-label">Email</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="email" name="email">
							</div>
						</div>

						<div class="form-group">
							<label for="email2" class="col-sm-3 control-label">Email 2</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="email2" name="email2">
							</div>
						</div>

						<div class="form-group">
							<label for="phone" class="col-sm-3 control-label">Phone</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="phone" name="phone">
							</div>
						</div>

						<div class="form-group">
							<label for="phone2" class="col-sm-3 control-label">Phone 2</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="phone2" name="phone2">
							</div>
						</div>
						
						<div class="form-group">
							<label for="mobile_phone" class="col-sm-3 control-label">Mobile Phone</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="mobile_phone" name="mobile_phone">
							</div>
						</div>

						<div class="form-group">
							<label for="mobile_phone2" class="col-sm-3 control-label">Mobile Phone 2</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="mobile_phone2" name="mobile_phone2">
							</div>
						</div>
						
						<div class="form-group">
							<label for="nextel" class="col-sm-3 control-label">Nextel</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="nextel" name="nextel">
							</div>
						</div>

						<div class="form-group">
							<label for="web_page" class="col-sm-3 control-label">Web Page</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="web_page" name="web_page">
							</div>
						</div>

						<div class="form-group">
							<label for="facebook" class="col-sm-3 control-label">Facebook</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="facebook" name="facebook">
							</div>
						</div>
						
						<div class="form-group">
							<label for="twitter" class="col-sm-3 control-label">Twitter</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="twitter" name="twitter">
							</div>
						</div>

						<div class="form-group">
							<label for="other" class="col-sm-3 control-label">Other</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="other" name="other">
							</div>
						</div>
						
				    <div class="form-group">
				    	<label for="image-new" class="col-sm-3 control-label">Image</label>
				    	<div class="col-sm-9">
				    		<input type="file" name="image" accept="image/jpeg" id="image-new">
				    	</div>
				    </div>

						<div class="form-group">
							<label for="category1" class="col-sm-3 control-label">Category 1</label>
							<div class="col-sm-9">
								{{ Form::select('categoria1',array(null => 'Please Select') + $categoryOptions , Input::old('category'),array('id' => 'category1', 'class'=>'form-control'))}}
							</div>
						</div>

						<div class="form-group">
							<label for="category2" class="col-sm-3 control-label">Category 2</label>
							<div class="col-sm-9">
								{{ Form::select('categoria2',array(null => 'Please Select') + $categoryOptions , Input::old('category'),array('id' => 'category2', 'class'=>'form-control'))}}
							</div>
						</div>

						<div class="form-group">
							<label for="category3" class="col-sm-3 control-label">Category 3</label>
							<div class="col-sm-9">
								{{ Form::select('categoria3',array(null => 'Please Select') + $categoryOptions , Input::old('category'),array('id' => 'category3', 'class'=>'form-control'))}}
							</div>
						</div>

						<div class="form-group">
							<label for="category4" class="col-sm-3 control-label">Category 4</label>
							<div class="col-sm-9">
								{{ Form::select('categoria4',array(null => 'Please Select') + $categoryOptions , Input::old('category'),array('id' => 'category4', 'class'=>'form-control'))}}
							</div>
						</div>

						<div class="form-group">
							<label for="category5" class="col-sm-3 control-label">Category 5</label>
							<div class="col-sm-9">
								{{ Form::select('categoria5',array(null => 'Please Select') + $categoryOptions , Input::old('category'),array('id' => 'category5', 'class'=>'form-control'))}}
							</div>
						</div>

					</div><!-- /.modal-body -->
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary" value="Save">Save</button>
					</div>
					{{ Form::close() }}
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal #modal-new-->

		<div class="modal fade" id="modal-edit">
			<div class="modal-dialog">
				<div class="modal-content">
					{{ Form::open(array('url' => 'admin/pages/update', 'files' => true, 'class'=>'form-horizontal', 'id'=>'form-edit', 'role'=>'form')) }}
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title"><i class="fa fa-pencil-square fa-fw"></i>Edit</h4>
					</div>
					<div class="modal-body">
						
						<div class="form-group">
							<label for="business_name" class="col-sm-3 control-label">Business Name</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="business_name" name="business_name" required>
							</div>
						</div>

						<div class="form-group">
							<label for="address" class="col-sm-3 control-label">Address</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="address" name="address">
							</div>
						</div>

						<div class="form-group">
							<label for="email" class="col-sm-3 control-label">Email</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="email" name="email">
							</div>
						</div>

						<div class="form-group">
							<label for="email2" class="col-sm-3 control-label">Email 2</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="email2" name="email2">
							</div>
						</div>

						<div class="form-group">
							<label for="phone" class="col-sm-3 control-label">Phone</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="phone" name="phone">
							</div>
						</div>

						<div class="form-group">
							<label for="phone2" class="col-sm-3 control-label">Phone 2</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="phone2" name="phone2">
							</div>
						</div>
						
						<div class="form-group">
							<label for="mobile_phone" class="col-sm-3 control-label">Mobile Phone</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="mobile_phone" name="mobile_phone">
							</div>
						</div>

						<div class="form-group">
							<label for="mobile_phone2" class="col-sm-3 control-label">Mobile Phone 2</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="mobile_phone2" name="mobile_phone2">
							</div>
						</div>
						
						<div class="form-group">
							<label for="nextel" class="col-sm-3 control-label">Nextel</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="nextel" name="nextel">
							</div>
						</div>

						<div class="form-group">
							<label for="web_page" class="col-sm-3 control-label">Web Page</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="web_page" name="web_page">
							</div>
						</div>

						<div class="form-group">
							<label for="facebook" class="col-sm-3 control-label">Facebook</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="facebook" name="facebook">
							</div>
						</div>
						
						<div class="form-group">
							<label for="twitter" class="col-sm-3 control-label">Twitter</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="twitter" name="twitter">
							</div>
						</div>

						<div class="form-group">
							<label for="other" class="col-sm-3 control-label">Other</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="other" name="other">
							</div>
						</div>
						
				    <div class="form-group">
				    	<label for="image-edit" class="col-sm-3 control-label">Image</label>
				    	<div class="col-sm-9">
				    		<input type="file" name="image" accept="image/jpeg" id="image-edit">
				    	</div>
				    </div>

						<div class="form-group">
							<label for="category1" class="col-sm-3 control-label">Category 1</label>
							<div class="col-sm-9">
								{{ Form::select('categoria1',array(null => 'Please Select') + $categoryOptions , Input::old('category'),array('id' => 'category1', 'class'=>'form-control'))}}
								{{ Form::hidden('hide1', null) }}
							</div>
						</div>

						<div class="form-group">
							<label for="category2" class="col-sm-3 control-label">Category 2</label>
							<div class="col-sm-9">
								{{ Form::select('categoria2',array(null => 'Please Select') + $categoryOptions , Input::old('category'),array('id' => 'category2', 'class'=>'form-control'))}}
								{{ Form::hidden('hide2', null) }}
							</div>
						</div>

						<div class="form-group">
							<label for="category3" class="col-sm-3 control-label">Category 3</label>
							<div class="col-sm-9">
								{{ Form::select('categoria3',array(null => 'Please Select') + $categoryOptions , Input::old('category'),array('id' => 'category3', 'class'=>'form-control'))}}
								{{ Form::hidden('hide3', null) }}
							</div>
						</div>

						<div class="form-group">
							<label for="category4" class="col-sm-3 control-label">Category 4</label>
							<div class="col-sm-9">
								{{ Form::select('categoria4',array(null => 'Please Select') + $categoryOptions , Input::old('category'),array('id' => 'category4', 'class'=>'form-control'))}}
								{{ Form::hidden('hide4', null) }}
							</div>
						</div>

						<div class="form-group">
							<label for="category5" class="col-sm-3 control-label">Category 5</label>
							<div class="col-sm-9">
								{{ Form::select('categoria5',array(null => 'Please Select') + $categoryOptions , Input::old('category'),array('id' => 'category5', 'class'=>'form-control'))}}
								{{ Form::hidden('hide5', null) }}
							</div>
						</div>

					</div><!-- /.modal-body -->
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary" value="Save">Save changes</button>
					</div>
					{{ Form::close() }}
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal #modal-edit-->

@stop