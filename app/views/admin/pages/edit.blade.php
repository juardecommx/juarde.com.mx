@extends('admin.layouts.master')

@section('content')
		<section class="content container">
			<h1>Pages Edit</h1>

			{{ Form::open(array('url' => 'admin/pages/update/'.$advertiser->id, 'files' => true,'id'=>'form', 'class'=>'form-horizontal', 'role'=>'form')) }}
			
			<div class="form-group">
				<label for="business_name" class="col-sm-2 control-label">Business Name:</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="business_name" name="business_name" value="{{$advertiser->business_name}}">
				</div>
			</div>

			<div class="form-group">
				<label for="address" class="col-sm-2 control-label">Address:</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="address" name="address" value="{{$advertiser->address}}">
				</div>
			</div>

			<div class="form-group">
				<label for="email" class="col-sm-2 control-label">Email:</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="email" name="email" value="{{$advertiser->email}}">
				</div>
			</div>

			<div class="form-group">
				<label for="email2" class="col-sm-2 control-label">Email 2:</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="email2" name="email2" value="{{$advertiser->email2}}">
				</div>
			</div>

			<div class="form-group">
				<label for="phone" class="col-sm-2 control-label">Phone:</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="phone" name="phone" value="{{$advertiser->phone}}">
				</div>
			</div>

			<div class="form-group">
				<label for="phone2" class="col-sm-2 control-label">Phone 2:</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="phone2" name="phone2" value="{{$advertiser->phone2}}">
				</div>
			</div>
			
			<div class="form-group">
				<label for="mobile_phone" class="col-sm-2 control-label">Mobile Phone:</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="mobile_phone" name="mobile_phone" value="{{$advertiser->mobile_phone}}">
				</div>
			</div>

			<div class="form-group">
				<label for="mobile_phone2" class="col-sm-2 control-label">Mobile Phone 2:</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="mobile_phone2" name="mobile_phone2" value="{{$advertiser->mobile_phone2}}">
				</div>
			</div>
			
			<div class="form-group">
				<label for="nextel" class="col-sm-2 control-label">Nextel:</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="nextel" name="nextel" value="{{$advertiser->nextel}}">
				</div>
			</div>

			<div class="form-group">
				<label for="web_page" class="col-sm-2 control-label">Web Page:</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="web_page" name="web_page" value="{{$advertiser->web_page}}">
				</div>
			</div>

			<div class="form-group">
				<label for="facebook" class="col-sm-2 control-label">Facebook:</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="facebook" name="facebook" value="{{$advertiser->facebook}}">
				</div>
			</div>
			
			<div class="form-group">
				<label for="twitter" class="col-sm-2 control-label">Twitter:</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="twitter" name="twitter" value="{{$advertiser->twitter}}">
				</div>
			</div>

			<div class="form-group">
				<label for="other" class="col-sm-2 control-label">Other:</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="other" name="other" value="{{$advertiser->other}}">
				</div>
			</div>
			
	    <div class="form-group">
	    	<label for="image" class="col-sm-2 control-label">Image:</label>
	    	<div class="col-sm-10">
	    		<input type="file" name="image" accept="image/jpeg" id="file" class="filestyle" data-buttonBefore="true" data-icon="false">
	    	</div>
	    </div>

			<div class="form-group">
				<label for="category1" class="col-sm-2 control-label">category 1: </label>
				<div class="col-sm-10">
	        @if(isset($category[0]->category_id))
		        {{ Form::select('categoria1',array(null => 'Please Select')+ $categoryOptions , $category[0]->category_id,['id' => 'category1', 'class'=>'form-control'])}}
		        {{ Form::hidden('hide1', $category[0]->id) }}
	    		@else
		    		{{ Form::select('categoria1',array(null => 'Please Select')+ $categoryOptions,['id' => 'category1', 'class'=>'form-control'])}}
		    		{{ Form::hidden('hide1', null) }}
	    		@endif
	    	</div>
			</div>

			<div class="form-group">
				<label for="category2" class="col-sm-2 control-label">category 2: </label>
				<div class="col-sm-10">
	        @if(isset($category[1]->category_id))
		        {{ Form::select('categoria2',array(null => 'Please Select')+ $categoryOptions , $category[1]->category_id,['id' => 'category2', 'class'=>'form-control'])}}
		        {{ Form::hidden('hide2', $category[1]->id) }}
	    		@else
		    		{{ Form::select('categoria2',array(null => 'Please Select')+ $categoryOptions,['id' => 'category2', 'class'=>'form-control'])}}
		    		{{ Form::hidden('hide2', null) }}
	    		@endif
	    	</div>
			</div>

			<div class="form-group">
				<label for="category3" class="col-sm-2 control-label">category 3: </label>
	      <div class="col-sm-10">
					@if(isset($category[2]->category_id))
		        {{ Form::select('categoria3',array(null => 'Please Select')+ $categoryOptions , $category[2]->category_id,['id' => 'category3', 'class'=>'form-control'])}}
		        {{ Form::hidden('hide3', $category[2]->id) }}
	        @else
		        {{ Form::select('categoria3',array(null => 'Please Select')+ $categoryOptions ,['id' => 'category3', 'class'=>'form-control'])}}
		        {{ Form::hidden('hide3', null) }}
	        @endif
				</div>
	    </div>

			<div class="form-group">
				<label for="category4" class="col-sm-2 control-label">category 4: </label>
				<div class="col-sm-10">
					@if(isset($category[3]->category_id))
		        {{ Form::select('categoria4',array(null => 'Please Select')+ $categoryOptions , $category[3]->category_id,['id' => 'category4', 'class'=>'form-control'])}}
		        {{ Form::hidden('hide4', $category[3]->id) }}
	        @else
		        {{ Form::select('categoria4',array(null => 'Please Select')+ $categoryOptions ,['id' => 'category4', 'class'=>'form-control'])}}
		        {{ Form::hidden('hide4', null) }}
	        @endif
	    	</div>
	    </div>

			<div class="form-group">
	      <label for="category5" class="col-sm-2 control-label">category 5: </label>
				<div class="col-sm-10">
				@if(isset($category[4]->category_id))
	        {{ Form::select('categoria5',array(null => 'Please Select')+ $categoryOptions , $category[4]->category_id,['id' => 'category5', 'class'=>'form-control'])}}
	        {{ Form::hidden('hide5', $category[4]->id) }}
	      @else
	        {{ Form::select('categoria5',array(null => 'Please Select')+ $categoryOptions ,['id' => 'category5', 'class'=>'form-control'])}}
	        {{ Form::hidden('hide5', null) }}
	        @endif
	      </div>
	    </div>
	    <div class="col-sm-2">
	    	<input type="submit" style="font-size: 20px" class="btn btn-primary btn-large btn-block" value="Save">
	    </div>
	    {{ Form::close() }}
		</section>
@stop