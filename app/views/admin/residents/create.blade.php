@extends('admin.layouts.master')

@section('content')
		<section class="content container">
			<h1>Create de residents</h1>
			
			{{ Form::open(array('url' => 'admin/residents/store', 'files' => true,'id'=>'form', 'class'=>'form-horizontal', 'role'=>'form')) }}

				<label for="last_name1">Last Name 1:</label>
					<input type="text" name="last_name1" id="last_name1"></input><br>

				<label for="first_name1">First Name 1:</label>
					<input type="text" name="first_name1" id="first_name1"></input><br>

				<label for="last_name2">Last Name 2:</label>
					<input type="text" name="last_name2" id="last_name2"></input><br>

				<label for="first_name2">First Name 2:</label>
					<input type="text" name="first_name2" id="first_name2"></input><br>

				<label for="street_address">Street Address:</label>
					<input type="text" name="street_address" id="street_address"></input><br>

				<label for="apdo">Apdo:</label>
					<input type="text" name="apdo" id="apdo"></input><br>

				<label for="tel">Phone:</label>
					<input type="text" name="tel" id="tel"></input><br>

				<label for="tel2">Phone2:</label>
					<input type="text" name="tel2" id="tel2"></input><br>

				<label for="fax">Fax:</label>
					<input type="fax" name="fax" id="fax"></input><br>

				<label for="voip_server">Voip Server:</label>
					<input type="text" name="voip_server" id="voip_server"></input><br>

				<label for="email">Email:</label>
					<input type="text" name="email" id="email"></input><br>

				<label for="facebok">Facebook:</label>
					<input type="text" name="facebook" id="facebook"></input><br>

				<label for="twitter">Twitter:</label>
					<input type="text" name="twitter" id="twitter"></input><br>
				
	        	<input type="submit" style="font-size: 20px"  value="Save">
	        {{ Form::close() }}

			</form>
		</section>
@stop