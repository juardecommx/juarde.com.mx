@extends('admin.layouts.master')

@section('content')
		<section class="content container">

			@if(Session::has('message'))
				<?php $class =  Session::get('class'); ?>
				<div class="alert alert-{{$class}} alert-dismissable fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					@if($class=='success')
						<i class="fa fa-check-circle"></i>
					@endif
					@if($class=='warning')
						<i class="fa fa-exclamation-circle"></i>
					@endif
					@if($class=='danger')
						<i class="fa fa-exclamation-circle"></i>
					@endif
					{{ Session::get('message') }}
				</div>
			@endif

			<table class="table table-striped table-hover table-condensed" id="table-residents">
				<thead>
					<tr>
						<th>Name</th>
						<th class="all">Actions</th>
					</tr>
				</thead>
				<tbody>
			@foreach($residents as $residents)
				<tr>
					<td>
						{{$residents->last_name1." ".$residents->first_name1}}
						@if (!empty($residents->last_name2))
							{{" ".$residents->last_name2}}
							@if (!empty($residents->first_name2))
								{{" ".$residents->first_name2}}
							@endif
						@endif
					</td>
					<td class="nowrap">
						<a href="#modal-edit" data-toggle="modal" data-target="#modal-edit" id="{{$residents->id}}" class="btn btn-primary btn-xs edit"><i class="fa fa-pencil-square fa-fw"></i>Edit</a>
						<a href="{{URL::to('admin/residents/destroy',$residents->id)}}" class="btn btn-danger btn-xs" data-toggle="confirmation"><i class="fa fa-minus-square fa-fw"></i>Delete</a>
					</td>
				</tr>
			@endforeach
				</tbody>
			</table>
		</section>

		<div class="modal fade" id="modal-new">
			<div class="modal-dialog">
				<div class="modal-content">
					{{ Form::open(array('url' => 'admin/residents/store', 'files' => true, 'class'=>'form-horizontal', 'id'=>'form-new', 'role'=>'form')) }}
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title"><i class="fa fa-plus-square fa-fw"></i>New</h4>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<label for="last_name1" class="col-sm-3 control-label">Last Name</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" name="last_name1" id="last_name1" required>
							</div>
						</div>
						<div class="form-group">
							<label for="first_name1" class="col-sm-3 control-label">First Name</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" name="first_name1" id="first_name1" required>
							</div>
						</div>
						<div class="form-group">
							<label for="last_name2" class="col-sm-3 control-label">Last Name 2</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" name="last_name2" id="last_name2">
							</div>
						</div>
						<div class="form-group">
							<label for="first_name2" class="col-sm-3 control-label">First Name 2</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" name="first_name2" id="first_name2">
							</div>
						</div>
						<div class="form-group">
							<label for="address" class="col-sm-3 control-label">Address</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" name="street_address" id="street_address">
							</div>
						</div>
						<div class="form-group">
							<label for="apdo" class="col-sm-3 control-label">PO Box</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" name="apdo" id="apdo">
							</div>
						</div>
						<div class="form-group">
							<label for="tel" class="col-sm-3 control-label">Phone</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" name="tel" id="tel">
							</div>
						</div>
						<div class="form-group">
							<label for="tel2" class="col-sm-3 control-label">Phone 2</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" name="tel2" id="tel2">
							</div>
						</div>
						<div class="form-group">
							<label for="fax" class="col-sm-3 control-label">Fax</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" name="fax" id="fax">
							</div>
						</div>
						<div class="form-group">
							<label for="voip_server" class="col-sm-3 control-label">VoIP Server</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" name="voip_server" id="voip_server">
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-3 control-label">Email</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" name="email" id="email">
							</div>
						</div>
						<div class="form-group">
							<label for="facebook" class="col-sm-3 control-label">Facebook</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" name="facebook" id="facebook">
							</div>
						</div>
						<div class="form-group">
							<label for="twitter" class="col-sm-3 control-label">Twitter</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" name="twitter" id="twitter">
							</div>
						</div>
					</div><!-- /.modal-body -->
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary" value="Save">Save</button>
					</div>
					{{ Form::close() }}
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal #modal-new-->

		<div class="modal fade" id="modal-edit">
			<div class="modal-dialog">
				<div class="modal-content">
					{{ Form::open(array('url' => 'admin/residents/update', 'files' => true, 'class'=>'form-horizontal', 'id'=>'form-edit', 'role'=>'form')) }}
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title"><i class="fa fa-pencil-square fa-fw"></i>Edit</h4>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<label for="last_name1" class="col-sm-3 control-label">Last Name</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" name="last_name1" id="last_name1" required>
							</div>
						</div>
						<div class="form-group">
							<label for="first_name1" class="col-sm-3 control-label">First Name</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" name="first_name1" id="first_name1" required>
							</div>
						</div>
						<div class="form-group">
							<label for="last_name2" class="col-sm-3 control-label">Last Name 2</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" name="last_name2" id="last_name2">
							</div>
						</div>
						<div class="form-group">
							<label for="first_name2" class="col-sm-3 control-label">First Name 2</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" name="first_name2" id="first_name2">
							</div>
						</div>
						<div class="form-group">
							<label for="address" class="col-sm-3 control-label">Address</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" name="street_address" id="street_address">
							</div>
						</div>
						<div class="form-group">
							<label for="apdo" class="col-sm-3 control-label">PO Box</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" name="apdo" id="apdo">
							</div>
						</div>
						<div class="form-group">
							<label for="tel" class="col-sm-3 control-label">Phone</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" name="tel" id="tel">
							</div>
						</div>
						<div class="form-group">
							<label for="tel2" class="col-sm-3 control-label">Phone 2</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" name="tel2" id="tel2">
							</div>
						</div>
						<div class="form-group">
							<label for="fax" class="col-sm-3 control-label">Fax</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" name="fax" id="fax">
							</div>
						</div>
						<div class="form-group">
							<label for="voip_server" class="col-sm-3 control-label">VoIP Server</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" name="voip_server" id="voip_server">
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-3 control-label">Email</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" name="email" id="email">
							</div>
						</div>
						<div class="form-group">
							<label for="facebook" class="col-sm-3 control-label">Facebook</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" name="facebook" id="facebook">
							</div>
						</div>
						<div class="form-group">
							<label for="twitter" class="col-sm-3 control-label">Twitter</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" name="twitter" id="twitter">
							</div>
						</div>
					</div><!-- /.modal-body -->
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary" value="Save">Save changes</button>
					</div>
					{{ Form::close() }}
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal #modal-edit-->

@stop