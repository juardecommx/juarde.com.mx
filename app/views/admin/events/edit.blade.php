@extends('admin.layouts.master')

@section('content')
		<section class="content container">
			<h1>Edit de events</h1>

			{{ Form::open(array('url' => 'admin/events/update/'.$event->id, 'files' => true,'id'=>'form', 'class'=>'form-horizontal', 'role'=>'form')) }}

				<label for="event">Event:</label>
					<input type="text" name="event" id="event" required value="{{$event->description}}"></input><br>

				<label for="evento">Evento:</label>
					<input type="text" name="evento" id="evento" required value="{{$evento->descripcion}}"></input><br>

				<label for="date">Fecha:</label>
					<input type="text" name="date" id="date" required value="{{$event->date}}"></input><br>
				
	        	<input type="submit" style="font-size: 20px"  value="Save">
	        {{ Form::close() }}

		</section>
@stop