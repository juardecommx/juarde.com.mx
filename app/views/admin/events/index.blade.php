@extends('admin.layouts.master')

@section('content')
		<section class="content container">
		@if(Session::has('message'))
				<?php $class =  Session::get('class'); ?>
				<div class="alert alert-{{$class}} alert-dismissable fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					@if($class=='success')
						<i class="fa fa-check-circle"></i>
					@endif
					@if($class=='warning')
						<i class="fa fa-exclamation-circle"></i>
					@endif
					@if($class=='danger')
						<i class="fa fa-exclamation-circle"></i>
					@endif
					{{ Session::get('message') }}
				</div>
			@endif

			<table class="table table-striped table-hover table-condensed" id="table-events">
				<thead>
					<tr>
						<th>English</th>
						<th>Spanish</th>
						<th>Date</th>
						<th class="all">Actions</th>
					</tr>
				</thead>
				<tbody>
			@foreach($events as $events)
				<tr>
					<td>
						{{$events->description}}
					</td>
					<td>
						{{$events->descripcion}}
					</td>
					<td class="nowrap">
						{{$events->date}}
					</td>
					<td class="nowrap">
						<a href="#modal-edit" data-toggle="modal" data-target="#modal-edit" class="btn btn-primary btn-xs edit" id="{{$events->id}}"><i class="fa fa-pencil-square fa-fw"></i>Edit</a>
						<a href="{{URL::to('admin/events/destroy',$events->id)}}" class="btn btn-danger btn-xs" data-toggle="confirmation"><i class="fa fa-minus-square fa-fw"></i>Delete</a>
					</td>
				</tr>
			@endforeach
			</tbody>
			</table>
		</section>

		<div class="modal fade" id="modal-new">
			<div class="modal-dialog">
				<div class="modal-content">
					{{ Form::open(array('url' => 'admin/events/store', 'files' => true, 'class'=>'form-horizontal', 'id'=>'form-new', 'role'=>'form')) }}
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title"><i class="fa fa-plus-square fa-fw"></i>New</h4>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<label for="event" class="col-sm-2 control-label">Event</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="event" id="event" required>
							</div>
						</div>
						<div class="form-group">
							<label for="evento" class="col-sm-2 control-label">Evento</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="evento" id="evento" required>
							</div>
						</div>
						<div class="form-group">
							<label for="date" class="col-sm-2 control-label">Date</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="date" id="date" required>
							</div>
						</div>
					</div><!-- /.modal-body -->
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary" value="Save">Save</button>
					</div>
					{{ Form::close() }}
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal #modal-edit-->

		<div class="modal fade" id="modal-edit">
			<div class="modal-dialog">
				<div class="modal-content">
					{{ Form::open(array('url' => 'admin/events/update', 'files' => true, 'class'=>'form-horizontal', 'id'=>'form-edit', 'role'=>'form')) }}
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title"><i class="fa fa-pencil-square fa-fw"></i>Edit</h4>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<label for="event" class="col-sm-2 control-label">Event</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="event" id="event" required>
							</div>
						</div>
						<div class="form-group">
							<label for="evento" class="col-sm-2 control-label">Evento</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="evento" id="evento" required>
							</div>
						</div>
						<div class="form-group">
							<label for="date" class="col-sm-2 control-label">Date</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="date" id="date" required>
							</div>
						</div>
					</div><!-- /.modal-body -->
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary" value="Save">Save changes</button>
					</div>
					{{ Form::close() }}
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal #modal-edit-->

@stop