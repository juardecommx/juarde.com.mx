<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvertisementsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//Creamos el esquema de la tabla
		Schema::table('advertisements',function($table)
		{
		//Creamos la tabla
		$table->create();

		//Definimos el tipo de almacenamiento
		//$table->engine = 'InnoDB';
		$table->engine = 'MyISAM';
		
		//Definimos el indice pirimario autoincrementable
		$table->increments('id');

		//Creamos los indices que seran relacionados
      	$table->integer('category_id')->unsigned()->index();
      	$table->integer('advertiser_id')->unsigned()->index();

      	//Establecemos las relaciones
		$table->foreign('category_id')
      		->references('id')->on('categories')
      		->onDelete('cascade')
      		->onUpdate('cascade');

		$table->foreign('advertiser_id')
      		->references('id')->on('advertisers')
      		->onDelete('cascade')
      		->onUpdate('cascade');

      	//Creamos los demas campos
		$table->timestamps();
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//Eliminamos la tabla
		Schema::drop('advertisements');
	}

}
