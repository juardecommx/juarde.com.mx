<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResidentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//definimos el esquema de la tabla
		Schema::table('residents',function($table)
		{
		//Creamos la tabla
		$table->create();

		//Definimos la clave primaria auto incrementable
		$table->increments('id');

		//Definimos el tipo de almacenamiento
		//$table->engine = 'InnoDB';
		$table->engine = 'MyISAM';

		//Definimos los demas campos
		$table->string('last_name1',32);
		$table->string('first_name1',32);
		$table->string('last_name2',32);
		$table->string('first_name2',32);
		$table->string('street_address',64);
		$table->string('apdo',32);
		$table->string('tel',32);
		$table->string('tel2',32);
		$table->string('fax',32);
		$table->string('voip_server',32);
		$table->string('email',32);
		$table->string('facebook',32);
		$table->string('twitter',32);
		$table->timestamps();
		});

		//Declaramos un indice fulltext para la busqueda de campos
		DB::statement('ALTER TABLE residents ADD FULLTEXT search(last_name1, first_name1,last_name2, first_name2,tel,fax,email)');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{	
		//Eliminamos el indice de busqueda antes de eliminar la tabla
		Schema::table('residents', function($table) {
            $table->dropIndex('search');
        });

		//Eliminamos la tabla
		Schema::drop('residents');
	}

}
