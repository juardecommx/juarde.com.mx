<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//Definimos el esquema de la tabla
		Schema::table('categorias',function($table)
		{
		//Creamos la tabla
		$table->create();

		//Definimos el tipo de almacenamiento
		//$table->engine = 'InnoDB';
		$table->engine = 'MyISAM';

		//Definimos un indice 
      	$table->increments('id')->unsigned()->index();

      	//Establecemos la relacion
		$table->foreign('id')
      		->references('id')->on('categories')
      		->onDelete('cascade')
      		->onUpdate('cascade');

		$table->string('categoria',256);
		$table->timestamps();
		});

		//Creamos el indice de busqueda
		DB::statement('ALTER TABLE categorias ADD FULLTEXT search(categoria)');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//Eliminamos el indice de busqueda antes de eliminar la tabla
		Schema::table('categorias', function($table) {
          $table->dropIndex('search');
        });

		//Eliminamos la tabla
		Schema::drop('categorias');
	}

}
