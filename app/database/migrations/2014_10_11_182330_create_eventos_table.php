<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//Creamos el esquema de la tabla
		Schema::table('eventos',function($table)
		{
		//Creamos la tabla
		$table->create();

		//Definimos el tipo de almacenamiento
		//$table->engine = 'InnoDB';
		$table->engine = 'MyISAM';

		//Definimos los indices
      	$table->increments('id')->unsigned()->index();

      	//Establecemos la relacion de las tablas
		$table->foreign('id')
      		->references('id')->on('events')
      		->onDelete('cascade')
      		->onUpdate('cascade');

      	//Creamos los demas campos
		$table->date('fecha');
		$table->string('descripcion',150);
		$table->timestamps();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//Eliminamos la tabla
		Schema::drop('eventos');
	}

}
