<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvertisersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//Creamos el esquema de la tabla
		Schema::table('advertisers',function($table)
		{
		//Creamos la tabla
		$table->create();

		//Definimos el tipo de almacenamiento
		//$table->engine = 'InnoDB';
		$table->engine = 'MyISAM';

		//Definimos nuestra clave primaria autoincrementable
		$table->increments('id');

		//Definimso los demas campos
		$table->string('business_name',255);
		$table->string('address',128);
		$table->string('email',255);
		$table->string('email2',255);
		$table->string('phone',64);
		$table->string('phone2',64);
		$table->string('mobile_phone',64);
		$table->string('mobile_phone2',64);
		$table->string('nextel',64);
		$table->string('web_page',64);
		$table->string('facebook',32);
		$table->string('twitter',32);
		$table->string('other',32);
		$table->timestamps();
		});

		//Creamos el indice de busqueda
		DB::statement('ALTER TABLE advertisers ADD FULLTEXT search(business_name,web_page,address,email,phone)');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//Eliminamos el indice de busqueda antes de eliminar la tabla
		Schema::table('advertisers', function($table) {
            $table->dropIndex('search');
        });

        //Eliminamos la tabla
		Schema::drop('advertisers');
	}

}
