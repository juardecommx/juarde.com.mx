<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//Creamos el esquema de la tabla
		Schema::table('events',function($table)
		{
		//Creamos la tabla
		$table->create();

		//Definimos el tipo de almacenamiento
		//$table->engine = 'InnoDB';
		$table->engine = 'MyISAM';

		//Creamos el indice primario autoincrementable
		$table->increments('id');

		//Creamos los demas campos
		$table->date('date');
		$table->string('description',150);
		$table->timestamps();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//Eliminamos la tabla
		Schema::drop('events');
	}

}
