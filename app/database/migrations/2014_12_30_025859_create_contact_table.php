<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//Creamos el esquema de la tabla
		Schema::table('contact',function($table)
		{
		//Creamos la tabla
		$table->create();

		//Definimos los indices
      	$table->increments('id');

      	//Creamos los demas campos
		$table->string('address',200);
		$table->string('phones',150);
		$table->string('email',150);

		$table->timestamps();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//Eliminamos la tabla
		Schema::drop('contact');
	}

}
