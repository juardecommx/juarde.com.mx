<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//Definimos el esquema de la tabla
		Schema::table('categories',function($table)
		{
		//Creamos la tabla
		$table->create();

		//Definimos el tipo de almacenamiento
		//$table->engine = 'InnoDB';
		$table->engine = 'MyISAM';

		//Creamos el indice primario autoincrementable
		$table->increments('id');
		
		//Definimos los demas campos
		$table->string('category',255);
		$table->timestamps();
		});

		//Creamos un Indice de busqueda
		DB::statement('ALTER TABLE categories ADD FULLTEXT search(category)');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//Eliminamos el indice de busqueda antes de eliminar l tabla
		Schema::table('categories', function($table) {
            $table->dropIndex('search');
        });
        //Eliminamos la tabla
		Schema::drop('categories');
	}

}
