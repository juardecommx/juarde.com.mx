<?php
class EventsSeeder extends Seeder{

	public function run(){

		DB::table('events')->delete();

Ev::create(array(
							'id'=>'1',
							'date'=>'2015-01-01',
							'description'=>'New Year’s Day', 
					)); 
Ev::create(array(
							'id'=>'2',
							'date'=>'2015-01-06',
							'description'=>'Three Kings Day: Blessing of children’s presents', 
					)); 
Ev::create(array(
							'id'=>'3',
							'date'=>'2015-01-12',
							'description'=>'Click Fest  - Wedding-Photographer Conference', 
					)); 
Ev::create(array(
							'id'=>'4',
							'date'=>'2015-01-19',
							'description'=>'M. Luther kin, Jr. Day', 
					)); 
Ev::create(array(
							'id'=>'5',
							'date'=>'2015-01-17',
							'description'=>'St. Anthony’s Day: Blessing of animals in front of Parroquia, San Juan de Dios, San Antonio & La Salud', 
					)); 
Ev::create(array(
							'id'=>'6',
							'date'=>'2015-01-21',
							'description'=>'General Allende’s Birthday: National Independence Hero', 
					)); 
Ev::create(array(
							'id'=>'7',
							'date'=>'2015-01-22',
							'description'=>'Departure of Pilgrims to San Juan de los Lagos', 
					)); 
Ev::create(array(
							'id'=>'8',
							'date'=>'2015-02-02',
							'description'=>'Candlemass Day: Seed and Plant Sale at Parque Juarez to begin planting season', 
					)); 
Ev::create(array(
							'id'=>'9',
							'date'=>'2015-02-05',
							'description'=>'Constitution Day', 
					)); 
Ev::create(array(
							'id'=>'10',
							'date'=>'2015-02-11',
							'description'=>'10th annual San Miguel Writers’ Conference', 
					)); 
Ev::create(array(
							'id'=>'11',
							'date'=>'2015-02-12',
							'description'=>'Lincoln´s Birthday', 
					)); 
Ev::create(array(
							'id'=>'12',
							'date'=>'2015-02-14',
							'description'=>'Valentine’s Day', 
					)); 
Ev::create(array(
							'id'=>'13',
							'date'=>'2015-02-18',
							'description'=>'Ash Wednesday. Lent begins', 
					)); 
Ev::create(array(
							'id'=>'14',
							'date'=>'2015-02-20',
							'description'=>'Fringe, Festival of Performing Arts', 
					)); 
Ev::create(array(
							'id'=>'15',
							'date'=>'2015-02-24',
							'description'=>'Flag Day', 
					)); 
Ev::create(array(
							'id'=>'16',
							'date'=>'2015-03-05',
							'description'=>'CUBAFEST 2014 Cuban Music (12th edition)', 
					)); 
Ev::create(array(
							'id'=>'17',
							'date'=>'2015-03-17',
							'description'=>'St. Patrick’s Day', 
					)); 
Ev::create(array(
							'id'=>'18',
							'date'=>'2015-03-19',
							'description'=>'Feast day of St. Joseph', 
					)); 
Ev::create(array(
							'id'=>'19',
							'date'=>'2015-03-21',
							'description'=>'Birthday of Benito Juarez', 
					)); 
Ev::create(array(
							'id'=>'20',
							'date'=>'2015-03-21',
							'description'=>'Spring begins', 
					)); 
Ev::create(array(
							'id'=>'21',
							'date'=>'2015-03-27',
							'description'=>'Viernes de Dolores (Friday of Pain)', 
					)); 
Ev::create(array(
							'id'=>'22',
							'date'=>'2015-03-29',
							'description'=>'Daylight Savings Time begins', 
					)); 
Ev::create(array(
							'id'=>'23',
							'date'=>'2015-03-29',
							'description'=>'Palm Sunday', 
					)); 
Ev::create(array(
							'id'=>'24',
							'date'=>'2015-03-30',
							'description'=>'Easter Monday', 
					)); 
Ev::create(array(
							'id'=>'25',
							'date'=>'2015-03-31',
							'description'=>'Holy Tuesday', 
					)); 
Ev::create(array(
							'id'=>'26',
							'date'=>'2015-04-01',
							'description'=>'Holy Wednesday', 
					)); 
Ev::create(array(
							'id'=>'27',
							'date'=>'2015-04-02',
							'description'=>'Holy Thursday', 
					)); 
Ev::create(array(
							'id'=>'28',
							'date'=>'2015-04-03',
							'description'=>'Good Friday', 
					)); 
Ev::create(array(
							'id'=>'29',
							'date'=>'2015-04-04',
							'description'=>'Easter Eve', 
					)); 
Ev::create(array(
							'id'=>'30',
							'date'=>'2015-04-05',
							'description'=>'Easter Sunday', 
					)); 
Ev::create(array(
							'id'=>'31',
							'date'=>'2015-04-30',
							'description'=>'Children’s Day', 
					)); 
Ev::create(array(
							'id'=>'32',
							'date'=>'2015-05-01',
							'description'=>'Labor Day', 
					)); 
Ev::create(array(
							'id'=>'33',
							'date'=>'2015-05-03',
							'description'=>'Feast day of Santa Cruz: Day of masons & builders', 
					)); 
Ev::create(array(
							'id'=>'34',
							'date'=>'2015-05-05',
							'description'=>'Anniversary of the Battle of Puebla. Mexican Army defeats the French in 1862', 
					)); 
Ev::create(array(
							'id'=>'35',
							'date'=>'2015-05-10',
							'description'=>'Mother’s Day', 
					)); 
Ev::create(array(
							'id'=>'36',
							'date'=>'2015-05-15',
							'description'=>'San Isidro Labrador’s Day: Patron of Rain & Agriculture; Village priests bless livestock decorated with garlands', 
					)); 
Ev::create(array(
							'id'=>'37',
							'date'=>'2015-05-15',
							'description'=>'Teacher’s Day', 
					)); 
Ev::create(array(
							'id'=>'38',
							'date'=>'2015-05-23',
							'description'=>'Students Day', 
					)); 
Ev::create(array(
							'id'=>'39',
							'date'=>'2015-05-25',
							'description'=>'Memorial Day (EUA)', 
					)); 
Ev::create(array(
							'id'=>'40',
							'date'=>'2015-06-04',
							'description'=>'Corpus Christi', 
					)); 
Ev::create(array(
							'id'=>'41',
							'date'=>'2015-06-14',
							'description'=>'Flag Day', 
					)); 
Ev::create(array(
							'id'=>'42',
							'date'=>'2015-06-14',
							'description'=>'Feast day of St. Anthony of Padua, Locos Parade', 
					)); 
Ev::create(array(
							'id'=>'43',
							'date'=>'2015-06-21',
							'description'=>'Father’s Day', 
					)); 
Ev::create(array(
							'id'=>'44',
							'date'=>'2015-07-03',
							'description'=>'3rd National Congress of World Heritage', 
					)); 
Ev::create(array(
							'id'=>'45',
							'date'=>'2015-07-04',
							'description'=>'Independence Day', 
					)); 
Ev::create(array(
							'id'=>'46',
							'date'=>'2015-07-13',
							'description'=>'Puppet Festival', 
					)); 
Ev::create(array(
							'id'=>'47',
							'date'=>'2015-07-25',
							'description'=>'Guanajuato International Film Festival begins', 
					)); 
Ev::create(array(
							'id'=>'48',
							'date'=>'2015-07-30',
							'description'=>'Anniversary of  the death of Father Hidalgo', 
					)); 
Ev::create(array(
							'id'=>'49',
							'date'=>'2015-08-01',
							'description'=>'Chamber Music Festival', 
					)); 
Ev::create(array(
							'id'=>'50',
							'date'=>'2015-08-03',
							'description'=>'Guanajuato International Film Festival ends', 
					)); 
Ev::create(array(
							'id'=>'51',
							'date'=>'2015-09-07',
							'description'=>'Labor Day', 
					)); 
Ev::create(array(
							'id'=>'52',
							'date'=>'2015-09-13',
							'description'=>'Child Heroes of Chapultepec Memorial', 
					)); 
Ev::create(array(
							'id'=>'53',
							'date'=>'2015-09-14',
							'description'=>'Historical Cavalcade arrival', 
					)); 
Ev::create(array(
							'id'=>'54',
							'date'=>'2015-09-15',
							'description'=>'Independence Celebration', 
					)); 
Ev::create(array(
							'id'=>'55',
							'date'=>'2015-09-16',
							'description'=>'Independence Day Parade', 
					)); 
Ev::create(array(
							'id'=>'56',
							'date'=>'2015-09-16',
							'description'=>'Insurgent Traditional arrival', 
					)); 
Ev::create(array(
							'id'=>'57',
							'date'=>'2015-10-01',
							'description'=>'Hummingbird Festival . (done between the months of September and October)', 
					)); 
Ev::create(array(
							'id'=>'58',
							'date'=>'2015-10-02',
							'description'=>'Traditional Alborada (Celebration of  Dawn)', 
					)); 
Ev::create(array(
							'id'=>'59',
							'date'=>'2015-10-03',
							'description'=>'Feast day of Archangel Saint Michael', 
					)); 
Ev::create(array(
							'id'=>'60',
							'date'=>'2015-10-03',
							'description'=>'Arrival of dancers with offerings and Suchiles to San Miguel de Allende', 
					)); 
Ev::create(array(
							'id'=>'61',
							'date'=>'2015-10-04',
							'description'=>'Feast day of St. Francis of Assisi', 
					)); 
Ev::create(array(
							'id'=>'62',
							'date'=>'2015-10-13',
							'description'=>'International Cervantino Festival (FIC) in Guanajuato', 
					)); 
Ev::create(array(
							'id'=>'63',
							'date'=>'2015-10-12',
							'description'=>'Columbus Day', 
					)); 
Ev::create(array(
							'id'=>'64',
							'date'=>'2015-10-24',
							'description'=>'Daylight Savings Time ends', 
					)); 
Ev::create(array(
							'id'=>'65',
							'date'=>'2015-10-30',
							'description'=>'Calaca Festival (Start)', 
					)); 
Ev::create(array(
							'id'=>'66',
							'date'=>'2015-11-01',
							'description'=>'All Saints Day', 
					)); 
Ev::create(array(
							'id'=>'67',
							'date'=>'2015-11-02',
							'description'=>'Day of the Dead', 
					)); 
Ev::create(array(
							'id'=>'68',
							'date'=>'2015-11-11',
							'description'=>'Veterans Day', 
					)); 
Ev::create(array(
							'id'=>'69',
							'date'=>'2015-11-11',
							'description'=>'International San Miguel Jazz & Blues Festival', 
					)); 
Ev::create(array(
							'id'=>'70',
							'date'=>'2015-11-20',
							'description'=>'Mexican Revolution Day', 
					)); 
Ev::create(array(
							'id'=>'71',
							'date'=>'2015-11-26',
							'description'=>'Thanksgiving Day U.S.', 
					)); 
Ev::create(array(
							'id'=>'72',
							'date'=>'2015-12-12',
							'description'=>'Feast of the Patroness of Mexico: Our Lady of Guadalupe', 
					)); 
Ev::create(array(
							'id'=>'73',
							'date'=>'2015-12-16',
							'description'=>'Christmas Posadas Celebration', 
					)); 
Ev::create(array(
							'id'=>'74',
							'date'=>'2015-12-24',
							'description'=>'Christmas Eve Celebration', 
					)); 
Ev::create(array(
							'id'=>'75',
							'date'=>'2015-12-25',
							'description'=>'Christmas', 
					)); 
Ev::create(array(
							'id'=>'76',
							'date'=>'2015-12-31',
							'description'=>'Traditional New Year’s Dinner', 
					)); 


	}
}
