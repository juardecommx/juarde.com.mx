<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();
		$this->call('CategoriesSeeder');
		$this->call('CategoriasSeeder');
		$this->call('EventsSeeder');
		$this->call('EventosSeeder');
		$this->call('AdvertisersSeeder');
		$this->call('AdvertisementsSeeder');
		$this->call('Residents');
		$this->call('ContactSeeder');
		$this->call('UserSeeder');
	}

}
