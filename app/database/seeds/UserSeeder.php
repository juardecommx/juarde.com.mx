<?php
class UserSeeder extends Seeder{

	public function run(){

		DB::table('users')->delete();
		DB::table('users')->truncate();

		User::create(array(
			'email'=>'info@juarde.com.mx',
			'name'=>'AdminJuarde',
			'password'=> Hash::make('Juarde2015'),
			));
}}

?>