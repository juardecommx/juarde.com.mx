<?php
class EventosSeeder extends Seeder{

	public function run(){

		DB::table('eventos')->delete();

Evento::create(array(
							'id'=>'1',
							'fecha'=>'2015-01-01',
							'descripcion'=>'Año Nuevo', 
					)); 
Evento::create(array(
							'id'=>'2',
							'fecha'=>'2015-01-06',
							'descripcion'=>'Día de los Reyes Magos, Caravana de Reyes (Parroquia de San Antonio)', 
					)); 
Evento::create(array(
							'id'=>'3',
							'fecha'=>'2015-01-12',
							'descripcion'=>'Click Fest  - congreso dirigido a fotógrafos de boda', 
					)); 
Evento::create(array(
							'id'=>'4',
							'fecha'=>'2015-01-19',
							'descripcion'=>'Dia de M. Luther kin, Jr.', 
					)); 
Evento::create(array(
							'id'=>'5',
							'fecha'=>'2015-01-17',
							'descripcion'=>'Día de San Antonio Abad, Bendición de los animales (Parroquia de San Antonio y otros templos)', 
					)); 
Evento::create(array(
							'id'=>'6',
							'fecha'=>'2015-01-21',
							'descripcion'=>'Natalicio del Generalísimo Ignacio de Allende y Unzaga (Desfile cívico)', 
					)); 
Evento::create(array(
							'id'=>'7',
							'fecha'=>'2015-01-22',
							'descripcion'=>'Salida de los peregrinos para visitar a la Virgen de San Juan de los Lagos', 
					)); 
Evento::create(array(
							'id'=>'8',
							'fecha'=>'2015-02-02',
							'descripcion'=>'Día de la Candelaria, inicio temporada de la siembra, venta de semillas y plantas en el parque Benito Juárez', 
					)); 
Evento::create(array(
							'id'=>'9',
							'fecha'=>'2015-02-05',
							'descripcion'=>'Aniversario de la promulgación de la Constitución Mexicana', 
					)); 
Evento::create(array(
							'id'=>'10',
							'fecha'=>'2015-02-11',
							'descripcion'=>'X Festival Internacional de Escritores y Literatura en San Miguel.', 
					)); 
Evento::create(array(
							'id'=>'11',
							'fecha'=>'2015-02-12',
							'descripcion'=>'Natalicio de Abraham Lincoln', 
					)); 
Evento::create(array(
							'id'=>'12',
							'fecha'=>'2015-02-14',
							'descripcion'=>'Día de San Valentín', 
					)); 
Evento::create(array(
							'id'=>'13',
							'fecha'=>'2015-02-18',
							'descripcion'=>'Miércoles de Ceniza', 
					)); 
Evento::create(array(
							'id'=>'14',
							'fecha'=>'2015-02-20',
							'descripcion'=>'Fringe, San Miguel, Festival de artes escénicas', 
					)); 
Evento::create(array(
							'id'=>'15',
							'fecha'=>'2015-02-24',
							'descripcion'=>'Día de la Bandera', 
					)); 
Evento::create(array(
							'id'=>'16',
							'fecha'=>'2015-03-05',
							'descripcion'=>'CUBAFEST 2014 Música Cubana (12ª. Edición)', 
					)); 
Evento::create(array(
							'id'=>'17',
							'fecha'=>'2015-03-17',
							'descripcion'=>'Día de San Patricio', 
					)); 
Evento::create(array(
							'id'=>'18',
							'fecha'=>'2015-03-19',
							'descripcion'=>'Fiesta de San José (se recorre al dom. 22)', 
					)); 
Evento::create(array(
							'id'=>'19',
							'fecha'=>'2015-03-21',
							'descripcion'=>'Natalicio de Benito Juárez', 
					)); 
Evento::create(array(
							'id'=>'20',
							'fecha'=>'2015-03-21',
							'descripcion'=>'Inicio de la primavera', 
					)); 
Evento::create(array(
							'id'=>'21',
							'fecha'=>'2015-03-27',
							'descripcion'=>'Viernes de Dolores', 
					)); 
Evento::create(array(
							'id'=>'22',
							'fecha'=>'2015-03-29',
							'descripcion'=>'Cambio de Horario de Verano', 
					)); 
Evento::create(array(
							'id'=>'23',
							'fecha'=>'2015-03-29',
							'descripcion'=>'Domingo de Ramos', 
					)); 
Evento::create(array(
							'id'=>'24',
							'fecha'=>'2015-03-30',
							'descripcion'=>'Lunes Santo', 
					)); 
Evento::create(array(
							'id'=>'25',
							'fecha'=>'2015-03-31',
							'descripcion'=>'Martes Santo', 
					)); 
Evento::create(array(
							'id'=>'26',
							'fecha'=>'2015-04-01',
							'descripcion'=>'Miércoles Santo', 
					)); 
Evento::create(array(
							'id'=>'27',
							'fecha'=>'2015-04-02',
							'descripcion'=>'Jueves Santo', 
					)); 
Evento::create(array(
							'id'=>'28',
							'fecha'=>'2015-04-03',
							'descripcion'=>'Viernes Santo', 
					)); 
Evento::create(array(
							'id'=>'29',
							'fecha'=>'2015-04-04',
							'descripcion'=>'Sábado Santo', 
					)); 
Evento::create(array(
							'id'=>'30',
							'fecha'=>'2015-04-05',
							'descripcion'=>'Domingo de Resurrección', 
					)); 
Evento::create(array(
							'id'=>'31',
							'fecha'=>'2015-04-30',
							'descripcion'=>'Día del niño', 
					)); 
Evento::create(array(
							'id'=>'32',
							'fecha'=>'2015-05-01',
							'descripcion'=>'Día del trabajo', 
					)); 
Evento::create(array(
							'id'=>'33',
							'fecha'=>'2015-05-03',
							'descripcion'=>'Día de la Santa Cruz, albañiles, constructores', 
					)); 
Evento::create(array(
							'id'=>'34',
							'fecha'=>'2015-05-05',
							'descripcion'=>'Aniversario de la Batalla de Puebla, el ejército mexicano derrota al francés en 1862', 
					)); 
Evento::create(array(
							'id'=>'35',
							'fecha'=>'2015-05-10',
							'descripcion'=>'Día de las Madres', 
					)); 
Evento::create(array(
							'id'=>'36',
							'fecha'=>'2015-05-15',
							'descripcion'=>'Fiesta de San Isidro Labrador, Patrono de la lluvia y la agricultura, bendición del ganado', 
					)); 
Evento::create(array(
							'id'=>'37',
							'fecha'=>'2015-05-15',
							'descripcion'=>'Día del Maestro', 
					)); 
Evento::create(array(
							'id'=>'38',
							'fecha'=>'2015-05-23',
							'descripcion'=>'Día del Estudiante', 
					)); 
Evento::create(array(
							'id'=>'39',
							'fecha'=>'2015-05-25',
							'descripcion'=>'Dia de los caidos (EUA)', 
					)); 
Evento::create(array(
							'id'=>'40',
							'fecha'=>'2015-06-04',
							'descripcion'=>'Corpus Christi', 
					)); 
Evento::create(array(
							'id'=>'41',
							'fecha'=>'2015-06-14',
							'descripcion'=>'Día de la bandera E.U.A', 
					)); 
Evento::create(array(
							'id'=>'42',
							'fecha'=>'2015-06-14',
							'descripcion'=>'Fiesta de San Antonio Desfile de los locos', 
					)); 
Evento::create(array(
							'id'=>'43',
							'fecha'=>'2015-06-21',
							'descripcion'=>'Día del Padre', 
					)); 
Evento::create(array(
							'id'=>'44',
							'fecha'=>'2015-07-03',
							'descripcion'=>'3er Congreso Nacional de Patrimonio Mundial', 
					)); 
Evento::create(array(
							'id'=>'45',
							'fecha'=>'2015-07-04',
							'descripcion'=>'Día de la Independencia, U.S.A', 
					)); 
Evento::create(array(
							'id'=>'46',
							'fecha'=>'2015-07-13',
							'descripcion'=>'Festival de Títeres', 
					)); 
Evento::create(array(
							'id'=>'47',
							'fecha'=>'2015-07-25',
							'descripcion'=>'Inicio, Festival Internacional de Cine de Guanajuato  GIFF', 
					)); 
Evento::create(array(
							'id'=>'48',
							'fecha'=>'2015-07-30',
							'descripcion'=>'Aniversario de la muerte del Padre Hidalgo', 
					)); 
Evento::create(array(
							'id'=>'49',
							'fecha'=>'2015-08-01',
							'descripcion'=>'Festival de Música de Cámara', 
					)); 
Evento::create(array(
							'id'=>'50',
							'fecha'=>'2015-08-03',
							'descripcion'=>'Termino, Festival Internacional de Cine de Guanajuato  GIFF', 
					)); 
Evento::create(array(
							'id'=>'51',
							'fecha'=>'2015-09-07',
							'descripcion'=>'Día del Trabajo, U.S.A', 
					)); 
Evento::create(array(
							'id'=>'52',
							'fecha'=>'2015-09-13',
							'descripcion'=>'Conmemoración de la muerte de los niños Héroes', 
					)); 
Evento::create(array(
							'id'=>'53',
							'fecha'=>'2015-09-14',
							'descripcion'=>'Llegada de Cabalgatas históricas', 
					)); 
Evento::create(array(
							'id'=>'54',
							'fecha'=>'2015-09-15',
							'descripcion'=>'Grito de la Independencia', 
					)); 
Evento::create(array(
							'id'=>'55',
							'fecha'=>'2015-09-16',
							'descripcion'=>'Desfile Cívico Mili­tar del día de la Independencia de México.', 
					)); 
Evento::create(array(
							'id'=>'56',
							'fecha'=>'2015-09-16',
							'descripcion'=>'Tradicional entrada de los Insurgentes.', 
					)); 
Evento::create(array(
							'id'=>'57',
							'fecha'=>'2015-10-01',
							'descripcion'=>'Festival Internacional del Colibrí.  (se realiza entre los meses de septiembre y octubre)', 
					)); 
Evento::create(array(
							'id'=>'58',
							'fecha'=>'2015-10-02',
							'descripcion'=>'Tradicional Alborada', 
					)); 
Evento::create(array(
							'id'=>'59',
							'fecha'=>'2015-10-03',
							'descripcion'=>'Fiesta de San Miguel Arcángel', 
					)); 
Evento::create(array(
							'id'=>'60',
							'fecha'=>'2015-10-03',
							'descripcion'=>'Llegada de los danzantes con ofrendas y súchiles a San Miguel de Allende', 
					)); 
Evento::create(array(
							'id'=>'61',
							'fecha'=>'2015-10-04',
							'descripcion'=>'Fiesta de San Francisco de Asís', 
					)); 
Evento::create(array(
							'id'=>'62',
							'fecha'=>'2015-10-13',
							'descripcion'=>'Festival Internacional Cervantino (FIC) en Guanajuato', 
					)); 
Evento::create(array(
							'id'=>'63',
							'fecha'=>'2015-10-12',
							'descripcion'=>'Conmemoración del Día de la Raza', 
					)); 
Evento::create(array(
							'id'=>'64',
							'fecha'=>'2015-10-24',
							'descripcion'=>'Cambio de horario de invierno', 
					)); 
Evento::create(array(
							'id'=>'65',
							'fecha'=>'2015-10-30',
							'descripcion'=>'Festival de la Calaca (inicia)', 
					)); 
Evento::create(array(
							'id'=>'66',
							'fecha'=>'2015-11-01',
							'descripcion'=>'Día de Todos Los Santos', 
					)); 
Evento::create(array(
							'id'=>'67',
							'fecha'=>'2015-11-02',
							'descripcion'=>'Día de los fieles difuntos / Festival de la Calaca (finaliza)', 
					)); 
Evento::create(array(
							'id'=>'68',
							'fecha'=>'2015-11-11',
							'descripcion'=>'Día de los Veteranos', 
					)); 
Evento::create(array(
							'id'=>'69',
							'fecha'=>'2015-11-11',
							'descripcion'=>'Festival Internacional de Jazz & Blues de San Miguel de Allende', 
					)); 
Evento::create(array(
							'id'=>'70',
							'fecha'=>'2015-11-20',
							'descripcion'=>'Día de la revolución Mexicana', 
					)); 
Evento::create(array(
							'id'=>'71',
							'fecha'=>'2015-11-26',
							'descripcion'=>'Día de Acción de Gracias en U.S.A.', 
					)); 
Evento::create(array(
							'id'=>'72',
							'fecha'=>'2015-12-12',
							'descripcion'=>'Fiesta de la patrona de México: La virgen de Guadalupe', 
					)); 
Evento::create(array(
							'id'=>'73',
							'fecha'=>'2015-12-16',
							'descripcion'=>'Celebración de Posadas Navideñas', 
					)); 
Evento::create(array(
							'id'=>'74',
							'fecha'=>'2015-12-24',
							'descripcion'=>'Celebración de Noche Buena', 
					)); 
Evento::create(array(
							'id'=>'75',
							'fecha'=>'2015-12-25',
							'descripcion'=>'Navidad', 
					)); 
Evento::create(array(
							'id'=>'76',
							'fecha'=>'2015-12-31',
							'descripcion'=>'Cenas Tradicionales de Año Nuevo, Día de gracias en diferentes templos.', 
					)); 
	}
}
