<?php
class CategoriasSeeder extends Seeder{

	public function run(){

		DB::table('categorias')->delete();

Categoria::create(array(
					'id'=>'1',
					'categoria'=>'Accesorios de Moda', 
					));
Categoria::create(array(
					'id'=>'2',
					'categoria'=>'Contadores Públicos', 
					));
Categoria::create(array(
					'id'=>'3',
					'categoria'=>'Auditores (Contable y Fiscal)', 
					));
Categoria::create(array(
					'id'=>'4',
					'categoria'=>'Contadores Públicos (Bilingües)', 
					)); 
Categoria::create(array(
					'id'=>'5',
					'categoria'=>'Acupuntura', 
					)); 
Categoria::create(array(
					'id'=>'6',
					'categoria'=>'Tiendas para Adultos', 
					)); 
Categoria::create(array(
					'id'=>'7',
					'categoria'=>'Actividades en Motocicleta Paseos y Renta [véase también  Motocicletas]', 
					)); 
Categoria::create(array(
					'id'=>'8',
					'categoria'=>'Publicidad', 
					)); 
Categoria::create(array(
					'id'=>'9',
					'categoria'=>'Fotografía Aérea', 
					)); 
Categoria::create(array(
					'id'=>'10',
					'categoria'=>'Aires Acondicionados', 
					)); 
Categoria::create(array(
					'id'=>'11',
					'categoria'=>'Transporte Aeropuerto [véase también Choferes Particulares]', 
					)); 
Categoria::create(array(
					'id'=>'12',
					'categoria'=>'Sistemas de Alarma', 
					)); 
Categoria::create(array(
					'id'=>'13',
					'categoria'=>'Tratamientos Alcoholismo', 
					)); 
Categoria::create(array(
					'id'=>'14',
					'categoria'=>'Medicina Alternativa', 
					)); 
Categoria::create(array(
					'id'=>'15',
					'categoria'=>'Puertas y Ventanas de Aluminio', 
					)); 
Categoria::create(array(
					'id'=>'16',
					'categoria'=>'Ambulancias', 
					)); 
Categoria::create(array(
					'id'=>'17',
					'categoria'=>'Hospitales de Animales', 
					)); 
Categoria::create(array(
					'id'=>'18',
					'categoria'=>'Antigüedades', 
					)); 
Categoria::create(array(
					'id'=>'19',
					'categoria'=>'Avalúos-Tasaciones', 
					)); 
Categoria::create(array(
					'id'=>'20',
					'categoria'=>'Acuarios', 
					)); 
Categoria::create(array(
					'id'=>'21',
					'categoria'=>'Arquitectos y Diseñadores', 
					)); 
Categoria::create(array(
					'id'=>'22',
					'categoria'=>'Restauración Arquitectónica', 
					)); 
Categoria::create(array(
					'id'=>'23',
					'categoria'=>'Aromaterapia', 
					)); 
Categoria::create(array(
					'id'=>'24',
					'categoria'=>'Arte', 
					)); 
Categoria::create(array(
					'id'=>'25',
					'categoria'=>'Arte y Decoración', 
					)); 
Categoria::create(array(
					'id'=>'26',
					'categoria'=>'Arte (Contemporáneo)', 
					)); 
Categoria::create(array(
					'id'=>'27',
					'categoria'=>'Arte (Folklórico)', 
					)); 
Categoria::create(array(
					'id'=>'28',
					'categoria'=>'Arte (Pinturas)', 
					)); 
Categoria::create(array(
					'id'=>'29',
					'categoria'=>'Arte (Popular)', 
					)); 
Categoria::create(array(
					'id'=>'30',
					'categoria'=>'Clases de Arte', 
					)); 
Categoria::create(array(
					'id'=>'31',
					'categoria'=>'Galerías de Arte [véase también Arte]', 
					)); 
Categoria::create(array(
					'id'=>'32',
					'categoria'=>'Estudios de Arte [véase también Galerías de Arte]', 
					)); 
Categoria::create(array(
					'id'=>'33',
					'categoria'=>'Artículos de Arte', 
					)); 
Categoria::create(array(
					'id'=>'34',
					'categoria'=>'Asilos de Ancianos', 
					)); 
Categoria::create(array(
					'id'=>'35',
					'categoria'=>'Abogados', 
					)); 
Categoria::create(array(
					'id'=>'36',
					'categoria'=>'Sistemas de Audio', 
					)); 
Categoria::create(array(
					'id'=>'37',
					'categoria'=>'Hojalatería y Pintura', 
					)); 
Categoria::create(array(
					'id'=>'38',
					'categoria'=>'Eléctrico Automotriz', 
					)); 
Categoria::create(array(
					'id'=>'39',
					'categoria'=>'Refacciones para Autos', 
					)); 
Categoria::create(array(
					'id'=>'40',
					'categoria'=>'Seguros de Automóviles', 
					)); 
Categoria::create(array(
					'id'=>'41',
					'categoria'=>'Mecánica Automotriz', 
					)); 
Categoria::create(array(
					'id'=>'42',
					'categoria'=>'Mantenimiento para Autos', 
					)); 
Categoria::create(array(
					'id'=>'43',
					'categoria'=>'Bebé (Artículos)', 
					)); 
Categoria::create(array(
					'id'=>'44',
					'categoria'=>'Panaderías', 
					)); 
Categoria::create(array(
					'id'=>'45',
					'categoria'=>'Pastelerías', 
					)); 
Categoria::create(array(
					'id'=>'46',
					'categoria'=>'Bancos', 
					)); 
Categoria::create(array(
					'id'=>'47',
					'categoria'=>'Salones y Recintos para Eventos', 
					)); 
Categoria::create(array(
					'id'=>'48',
					'categoria'=>'Peluquerías / Barberías [véase también Estéticas]', 
					)); 
Categoria::create(array(
					'id'=>'49',
					'categoria'=>'Bares', 
					)); 
Categoria::create(array(
					'id'=>'50',
					'categoria'=>'Bares (Tapas y Vino)', 
					)); 
Categoria::create(array(
					'id'=>'51',
					'categoria'=>'Baño y Cuidado del Cuerpo', 
					)); 
Categoria::create(array(
					'id'=>'52',
					'categoria'=>'Baños (Accesorios)', 
					)); 
Categoria::create(array(
					'id'=>'53',
					'categoria'=>'Bazares', 
					)); 
Categoria::create(array(
					'id'=>'54',
					'categoria'=>'Productos de Belleza', 
					)); 
Categoria::create(array(
					'id'=>'55',
					'categoria'=>'Salones de Belleza [véase también Estilistas]', 
					)); 
Categoria::create(array(
					'id'=>'56',
					'categoria'=>'Bed & Breakfast', 
					)); 
Categoria::create(array(
					'id'=>'57',
					'categoria'=>'Blancos', 
					)); 
Categoria::create(array(
					'id'=>'58',
					'categoria'=>'Cervecerías', 
					)); 
Categoria::create(array(
					'id'=>'59',
					'categoria'=>'Accesorios Bicicletas', 
					)); 
Categoria::create(array(
					'id'=>'60',
					'categoria'=>'Bicicletas (Renta y Venta)', 
					)); 
Categoria::create(array(
					'id'=>'61',
					'categoria'=>'Observación de Aves', 
					)); 
Categoria::create(array(
					'id'=>'62',
					'categoria'=>'Bancos de Sangre', 
					)); 
Categoria::create(array(
					'id'=>'63',
					'categoria'=>'Libros', 
					)); 
Categoria::create(array(
					'id'=>'64',
					'categoria'=>'Botas', 
					)); 
Categoria::create(array(
					'id'=>'65',
					'categoria'=>'Boutiques', 
					)); 
Categoria::create(array(
					'id'=>'66',
					'categoria'=>'Frenos y Suspensión', 
					)); 
Categoria::create(array(
					'id'=>'67',
					'categoria'=>'Inhumaciones', 
					)); 
Categoria::create(array(
					'id'=>'68',
					'categoria'=>'Autobuses', 
					)); 
Categoria::create(array(
					'id'=>'69',
					'categoria'=>'Carnicerías', 
					)); 
Categoria::create(array(
					'id'=>'70',
					'categoria'=>'Televisión por Cable', 
					)); 
Categoria::create(array(
					'id'=>'71',
					'categoria'=>'Cafeterías [véase también Restaurantes Cafetería]', 
					)); 
Categoria::create(array(
					'id'=>'72',
					'categoria'=>'Campamentos', 
					)); 
Categoria::create(array(
					'id'=>'73',
					'categoria'=>'Velas', 
					)); 
Categoria::create(array(
					'id'=>'74',
					'categoria'=>'Dulcerías', 
					)); 
Categoria::create(array(
					'id'=>'75',
					'categoria'=>'Agencias de Autos', 
					)); 
Categoria::create(array(
					'id'=>'76',
					'categoria'=>'Autolavados', 
					)); 
Categoria::create(array(
					'id'=>'77',
					'categoria'=>'Carpinterías y Ebanisterías', 
					)); 
Categoria::create(array(
					'id'=>'78',
					'categoria'=>'Lavado de Alfombras', 
					)); 
Categoria::create(array(
					'id'=>'79',
					'categoria'=>'Alfombras y Tapetes', 
					)); 
Categoria::create(array(
					'id'=>'80',
					'categoria'=>'Banquetes', 
					)); 
Categoria::create(array(
					'id'=>'81',
					'categoria'=>'Azulejos de Cerámica', 
					)); 
Categoria::create(array(
					'id'=>'82',
					'categoria'=>'Cerámica', 
					)); 
Categoria::create(array(
					'id'=>'83',
					'categoria'=>'Organizaciones Benéficas', 
					)); 
Categoria::create(array(
					'id'=>'84',
					'categoria'=>'Queserías', 
					)); 
Categoria::create(array(
					'id'=>'85',
					'categoria'=>'Quiroprácticos', 
					)); 
Categoria::create(array(
					'id'=>'86',
					'categoria'=>'Chocolaterías', 
					)); 
Categoria::create(array(
					'id'=>'87',
					'categoria'=>'Iglesias', 
					)); 
Categoria::create(array(
					'id'=>'88',
					'categoria'=>'Clases (Escuelas)', 
					)); 
Categoria::create(array(
					'id'=>'89',
					'categoria'=>'Clases (Talleres)', 
					)); 
Categoria::create(array(
					'id'=>'90',
					'categoria'=>'Productos de Limpieza', 
					)); 
Categoria::create(array(
					'id'=>'91',
					'categoria'=>'Ropa', 
					)); 
Categoria::create(array(
					'id'=>'92',
					'categoria'=>'Computadoras (Venta y Mantenimiento)', 
					)); 
Categoria::create(array(
					'id'=>'93',
					'categoria'=>'Salas de Conferencias', 
					)); 
Categoria::create(array(
					'id'=>'94',
					'categoria'=>'Salón de Conferencias', 
					)); 
Categoria::create(array(
					'id'=>'95',
					'categoria'=>'Tiendas de Consignación', 
					)); 
Categoria::create(array(
					'id'=>'96',
					'categoria'=>'Constructores / Contratistas', 
					)); 
Categoria::create(array(
					'id'=>'97',
					'categoria'=>'Acabados', 
					)); 
Categoria::create(array(
					'id'=>'98',
					'categoria'=>'Materiales para la Construcción', 
					)); 
Categoria::create(array(
					'id'=>'99',
					'categoria'=>'Cerámica para Construcción', 
					)); 
Categoria::create(array(
					'id'=>'100',
					'categoria'=>'Tiendas de Autoservicio', 
					)); 
Categoria::create(array(
					'id'=>'101',
					'categoria'=>'Clases de Cocina', 
					)); 
Categoria::create(array(
					'id'=>'102',
					'categoria'=>'Servicios de Mensajería', 
					)); 
Categoria::create(array(
					'id'=>'103',
					'categoria'=>'Materiales para Artesanías y Manualidades', 
					)); 
Categoria::create(array(
					'id'=>'104',
					'categoria'=>'Cremaciones', 
					)); 
Categoria::create(array(
					'id'=>'105',
					'categoria'=>'Casas de Cambio', 
					)); 
Categoria::create(array(
					'id'=>'106',
					'categoria'=>'Cortinas', 
					)); 
Categoria::create(array(
					'id'=>'107',
					'categoria'=>'Productos Lácteos', 
					)); 
Categoria::create(array(
					'id'=>'108',
					'categoria'=>'Clases de Baile / Danza', 
					)); 
Categoria::create(array(
					'id'=>'109',
					'categoria'=>'Delicatessen', 
					)); 
Categoria::create(array(
					'id'=>'110',
					'categoria'=>'Servicio de Entrega a Domicilio', 
					)); 
Categoria::create(array(
					'id'=>'111',
					'categoria'=>'Blanqueado Dental', 
					)); 
Categoria::create(array(
					'id'=>'112',
					'categoria'=>'Limpieza Dental', 
					)); 
Categoria::create(array(
					'id'=>'113',
					'categoria'=>'Implantes Dentales', 
					)); 
Categoria::create(array(
					'id'=>'114',
					'categoria'=>'Dentistas', 
					)); 
Categoria::create(array(
					'id'=>'115',
					'categoria'=>'Tiendas Departamentales', 
					)); 
Categoria::create(array(
					'id'=>'116',
					'categoria'=>'Depilación', 
					)); 
Categoria::create(array(
					'id'=>'117',
					'categoria'=>'Farmacias Dermatológicas', 
					)); 
Categoria::create(array(
					'id'=>'118',
					'categoria'=>'Dermatólogos', 
					)); 
Categoria::create(array(
					'id'=>'119',
					'categoria'=>'Decoración', 
					)); 
Categoria::create(array(
					'id'=>'120',
					'categoria'=>'Estéticas Caninas', 
					)); 
Categoria::create(array(
					'id'=>'121',
					'categoria'=>'Adiestramiento Canino', 
					)); 
Categoria::create(array(
					'id'=>'122',
					'categoria'=>'Choferes Particulares', 
					)); 
Categoria::create(array(
					'id'=>'123',
					'categoria'=>'Farmacias', 
					)); 
Categoria::create(array(
					'id'=>'124',
					'categoria'=>'Farmacias (Servicio a Domicilio)', 
					)); 
Categoria::create(array(
					'id'=>'125',
					'categoria'=>'Tintorerías', 
					)); 
Categoria::create(array(
					'id'=>'126',
					'categoria'=>'Tablaroca', 
					)); 
Categoria::create(array(
					'id'=>'127',
					'categoria'=>'Ecotecnología para Vivienda', 
					)); 
Categoria::create(array(
					'id'=>'128',
					'categoria'=>'Puertas Eléctricas', 
					)); 
Categoria::create(array(
					'id'=>'129',
					'categoria'=>'Reparación de Aparatos Eléctricos', 
					)); 
Categoria::create(array(
					'id'=>'130',
					'categoria'=>'Aparatos Eléctricos', 
					)); 
Categoria::create(array(
					'id'=>'131',
					'categoria'=>'Accesorios Eléctricos', 
					)); 
Categoria::create(array(
					'id'=>'132',
					'categoria'=>'Electricistas', 
					)); 
Categoria::create(array(
					'id'=>'133',
					'categoria'=>'Cercas Eléctrificadas', 
					)); 
Categoria::create(array(
					'id'=>'134',
					'categoria'=>'Aparatos Electrónicos', 
					)); 
Categoria::create(array(
					'id'=>'135',
					'categoria'=>'Sistemas de Emergencia', 
					)); 
Categoria::create(array(
					'id'=>'136',
					'categoria'=>'Centros de Verificación Automotriz', 
					)); 
Categoria::create(array(
					'id'=>'137',
					'categoria'=>'Endocrinólogos', 
					)); 
Categoria::create(array(
					'id'=>'138',
					'categoria'=>'Endodoncistas', 
					)); 
Categoria::create(array(
					'id'=>'139',
					'categoria'=>'Salones para Fiestas [véase también Salones y Recintos para Eventos]', 
					)); 
Categoria::create(array(
					'id'=>'140',
					'categoria'=>'Organización de Eventos', 
					)); 
Categoria::create(array(
					'id'=>'141',
					'categoria'=>'Excursiones', 
					)); 
Categoria::create(array(
					'id'=>'142',
					'categoria'=>'Telas y Textiles', 
					)); 
Categoria::create(array(
					'id'=>'143',
					'categoria'=>'Faciales', 
					)); 
Categoria::create(array(
					'id'=>'144',
					'categoria'=>'Ventiladores (de Techo)', 
					)); 
Categoria::create(array(
					'id'=>'145',
					'categoria'=>'Granjas Ecológicas', 
					)); 
Categoria::create(array(
					'id'=>'146',
					'categoria'=>'Moda', 
					)); 
Categoria::create(array(
					'id'=>'147',
					'categoria'=>'Comida Rápida', 
					)); 
Categoria::create(array(
					'id'=>'148',
					'categoria'=>'Cercas y Mallas', 
					)); 
Categoria::create(array(
					'id'=>'149',
					'categoria'=>'Fertilización', 
					)); 
Categoria::create(array(
					'id'=>'150',
					'categoria'=>'Planeación Financiera', 
					)); 
Categoria::create(array(
					'id'=>'151',
					'categoria'=>'Servicios Financieros [véase también Inversiones, Bancos]', 
					)); 
Categoria::create(array(
					'id'=>'152',
					'categoria'=>'Pescados y Mariscos [véase también Restaurantes Mariscos]', 
					)); 
Categoria::create(array(
					'id'=>'153',
					'categoria'=>'Fitness', 
					)); 
Categoria::create(array(
					'id'=>'154',
					'categoria'=>'Pisos', 
					)); 
Categoria::create(array(
					'id'=>'155',
					'categoria'=>'Florerías', 
					)); 
Categoria::create(array(
					'id'=>'156',
					'categoria'=>'Forrajerías', 
					)); 
Categoria::create(array(
					'id'=>'157',
					'categoria'=>'Marcos', 
					)); 
Categoria::create(array(
					'id'=>'158',
					'categoria'=>'Frutas y Verduras', 
					)); 
Categoria::create(array(
					'id'=>'159',
					'categoria'=>'Funerarias [véase también Servicios Funerarios]', 
					)); 
Categoria::create(array(
					'id'=>'160',
					'categoria'=>'Seguros Funerarios', 
					)); 
Categoria::create(array(
					'id'=>'161',
					'categoria'=>'Servicios Funerarios', 
					)); 
Categoria::create(array(
					'id'=>'162',
					'categoria'=>'Muebles', 
					)); 
Categoria::create(array(
					'id'=>'163',
					'categoria'=>'Muebles para Exteriores', 
					)); 
Categoria::create(array(
					'id'=>'164',
					'categoria'=>'Fabricación de Muebles', 
					)); 
Categoria::create(array(
					'id'=>'165',
					'categoria'=>'Renta de Mobiliario [véase también Alquileres para Fiesta]', 
					)); 
Categoria::create(array(
					'id'=>'166',
					'categoria'=>'Decoración para Jardines', 
					)); 
Categoria::create(array(
					'id'=>'167',
					'categoria'=>'Artículos para Jardines [véase también Decoración para Jardines]', 
					)); 
Categoria::create(array(
					'id'=>'168',
					'categoria'=>'Gas LP', 
					)); 
Categoria::create(array(
					'id'=>'169',
					'categoria'=>'Gasolinerías', 
					)); 
Categoria::create(array(
					'id'=>'170',
					'categoria'=>'Regalos', 
					)); 
Categoria::create(array(
					'id'=>'171',
					'categoria'=>'Vidrio Soplado', 
					)); 
Categoria::create(array(
					'id'=>'172',
					'categoria'=>'Vidrierías', 
					)); 
Categoria::create(array(
					'id'=>'173',
					'categoria'=>'Orfebres', 
					)); 
Categoria::create(array(
					'id'=>'174',
					'categoria'=>'Golf', 
					)); 
Categoria::create(array(
					'id'=>'175',
					'categoria'=>'Mantenimiento para Carros de Golf', 
					)); 
Categoria::create(array(
					'id'=>'176',
					'categoria'=>'Tiendas Gourmet', 
					)); 
Categoria::create(array(
					'id'=>'177',
					'categoria'=>'Diseño y Construcción de Jardines', 
					)); 
Categoria::create(array(
					'id'=>'178',
					'categoria'=>'Diseño Gráfico', 
					)); 
Categoria::create(array(
					'id'=>'179',
					'categoria'=>'Abarrotes', 
					)); 
Categoria::create(array(
					'id'=>'180',
					'categoria'=>'Casas de Huéspedes', 
					)); 
Categoria::create(array(
					'id'=>'181',
					'categoria'=>'Gimnasios', 
					)); 
Categoria::create(array(
					'id'=>'182',
					'categoria'=>'Estilistas', 
					)); 
Categoria::create(array(
					'id'=>'183',
					'categoria'=>'Artesanías', 
					)); 
Categoria::create(array(
					'id'=>'184',
					'categoria'=>'Ferreterías', 
					)); 
Categoria::create(array(
					'id'=>'185',
					'categoria'=>'Tlapalerías', 
					)); 
Categoria::create(array(
					'id'=>'186',
					'categoria'=>'Salud', 
					)); 
Categoria::create(array(
					'id'=>'187',
					'categoria'=>'Centros de Salud', 
					)); 
Categoria::create(array(
					'id'=>'188',
					'categoria'=>'Aparatos Auditivos', 
					)); 
Categoria::create(array(
					'id'=>'189',
					'categoria'=>'Calefacción', 
					)); 
Categoria::create(array(
					'id'=>'190',
					'categoria'=>'Accesorios para el Hogar', 
					)); 
Categoria::create(array(
					'id'=>'191',
					'categoria'=>'Construcción y Remodelación de Casas', 
					)); 
Categoria::create(array(
					'id'=>'192',
					'categoria'=>'Decoración para el Hogar', 
					)); 
Categoria::create(array(
					'id'=>'193',
					'categoria'=>'Mantenimiento para el Hogar', 
					)); 
Categoria::create(array(
					'id'=>'194',
					'categoria'=>'Alojamiento para Caballos', 
					)); 
Categoria::create(array(
					'id'=>'195',
					'categoria'=>'Entrenamiento para Caballos (Adiestramiento)', 
					)); 
Categoria::create(array(
					'id'=>'196',
					'categoria'=>'Clases de Equitación', 
					)); 
Categoria::create(array(
					'id'=>'197',
					'categoria'=>'Consultorios Clínicos', 
					)); 
Categoria::create(array(
					'id'=>'198',
					'categoria'=>'Hospitales y Clínicas', 
					)); 
Categoria::create(array(
					'id'=>'199',
					'categoria'=>'Posadas', 
					)); 
Categoria::create(array(
					'id'=>'200',
					'categoria'=>'Hoteles', 
					)); 
Categoria::create(array(
					'id'=>'201',
					'categoria'=>'Hoteles Boutique', 
					)); 
Categoria::create(array(
					'id'=>'202',
					'categoria'=>'Helados y Nieves', 
					)); 
Categoria::create(array(
					'id'=>'203',
					'categoria'=>'Servicios de Migración', 
					)); 
Categoria::create(array(
					'id'=>'204',
					'categoria'=>'Importaciones y Exportaciones', 
					)); 
Categoria::create(array(
					'id'=>'205',
					'categoria'=>'Seguros', 
					)); 
Categoria::create(array(
					'id'=>'206',
					'categoria'=>'Seguros de Hogar', 
					)); 
Categoria::create(array(
					'id'=>'207',
					'categoria'=>'Seguros de Vida', 
					)); 
Categoria::create(array(
					'id'=>'208',
					'categoria'=>'Seguros de Gastos Médicos', 
					)); 
Categoria::create(array(
					'id'=>'209',
					'categoria'=>'Diseño de Interiores', 
					)); 
Categoria::create(array(
					'id'=>'210',
					'categoria'=>'Médicos (Internistas)', 
					)); 
Categoria::create(array(
					'id'=>'211',
					'categoria'=>'Internet (Proveedores)', 
					)); 
Categoria::create(array(
					'id'=>'212',
					'categoria'=>'Internet / Cyber', 
					)); 
Categoria::create(array(
					'id'=>'213',
					'categoria'=>'Inversiones [véase también Bancos]', 
					)); 
Categoria::create(array(
					'id'=>'214',
					'categoria'=>'Herrerías', 
					)); 
Categoria::create(array(
					'id'=>'215',
					'categoria'=>'Sistemas de Riego', 
					)); 
Categoria::create(array(
					'id'=>'216',
					'categoria'=>'Joyerías', 
					)); 
Categoria::create(array(
					'id'=>'217',
					'categoria'=>'Joyería (Reparación)', 
					)); 
Categoria::create(array(
					'id'=>'218',
					'categoria'=>'Cocinas (Accesorios)', 
					)); 
Categoria::create(array(
					'id'=>'219',
					'categoria'=>'Cocinas', 
					)); 
Categoria::create(array(
					'id'=>'220',
					'categoria'=>'Laboratorios', 
					)); 
Categoria::create(array(
					'id'=>'221',
					'categoria'=>'Lámparas', 
					)); 
Categoria::create(array(
					'id'=>'222',
					'categoria'=>'Paisajismo', 
					)); 
Categoria::create(array(
					'id'=>'223',
					'categoria'=>'Escuelas de Idiomas', 
					)); 
Categoria::create(array(
					'id'=>'224',
					'categoria'=>'Láser (Dental)', 
					)); 
Categoria::create(array(
					'id'=>'225',
					'categoria'=>'Lavanderías', 
					)); 
Categoria::create(array(
					'id'=>'226',
					'categoria'=>'Artículos de Piel', 
					)); 
Categoria::create(array(
					'id'=>'227',
					'categoria'=>'Asesoría Legal', 
					)); 
Categoria::create(array(
					'id'=>'228',
					'categoria'=>'Bibliotecas', 
					)); 
Categoria::create(array(
					'id'=>'229',
					'categoria'=>'Iluminación', 
					)); 
Categoria::create(array(
					'id'=>'230',
					'categoria'=>'Música en Vivo', 
					)); 
Categoria::create(array(
					'id'=>'231',
					'categoria'=>'Cerrajerías', 
					)); 
Categoria::create(array(
					'id'=>'232',
					'categoria'=>'Madererías', 
					)); 
Categoria::create(array(
					'id'=>'233',
					'categoria'=>'Revistas', 
					)); 
Categoria::create(array(
					'id'=>'234',
					'categoria'=>'Correo / Centro de Mensajes', 
					)); 
Categoria::create(array(
					'id'=>'235',
					'categoria'=>'Manicura y Pedicura', 
					)); 
Categoria::create(array(
					'id'=>'236',
					'categoria'=>'Mercados', 
					)); 
Categoria::create(array(
					'id'=>'237',
					'categoria'=>'Masajes', 
					)); 
Categoria::create(array(
					'id'=>'238',
					'categoria'=>'Cirujía Maxilofacial', 
					)); 
Categoria::create(array(
					'id'=>'239',
					'categoria'=>'Emergencias Médicas', 
					)); 
Categoria::create(array(
					'id'=>'240',
					'categoria'=>'Meditación', 
					)); 
Categoria::create(array(
					'id'=>'241',
					'categoria'=>'Hipotecas', 
					)); 
Categoria::create(array(
					'id'=>'242',
					'categoria'=>'Motocicletas (Renta)', 
					)); 
Categoria::create(array(
					'id'=>'243',
					'categoria'=>'Motocicletas (Venta)', 
					)); 
Categoria::create(array(
					'id'=>'244',
					'categoria'=>'Motocicletas (Servicio y Refacciones)', 
					)); 
Categoria::create(array(
					'id'=>'245',
					'categoria'=>'Cines', 
					)); 
Categoria::create(array(
					'id'=>'246',
					'categoria'=>'Mudanzas', 
					)); 
Categoria::create(array(
					'id'=>'247',
					'categoria'=>'Servicios Municipales', 
					)); 
Categoria::create(array(
					'id'=>'248',
					'categoria'=>'Museos', 
					)); 
Categoria::create(array(
					'id'=>'249',
					'categoria'=>'Músicos', 
					)); 
Categoria::create(array(
					'id'=>'250',
					'categoria'=>'Niñeras', 
					)); 
Categoria::create(array(
					'id'=>'251',
					'categoria'=>'Tiendas Naturistas', 
					)); 
Categoria::create(array(
					'id'=>'252',
					'categoria'=>'Productos Naturales [véase también Tiendas Naturistas]', 
					)); 
Categoria::create(array(
					'id'=>'253',
					'categoria'=>'Periódicos', 
					)); 
Categoria::create(array(
					'id'=>'254',
					'categoria'=>'Clubes Nocturnos', 
					)); 
Categoria::create(array(
					'id'=>'255',
					'categoria'=>'Notarios Públicos', 
					)); 
Categoria::create(array(
					'id'=>'256',
					'categoria'=>'Viveros', 
					)); 
Categoria::create(array(
					'id'=>'257',
					'categoria'=>'Enfermeros', 
					)); 
Categoria::create(array(
					'id'=>'258',
					'categoria'=>'Papeleras', 
					)); 
Categoria::create(array(
					'id'=>'259',
					'categoria'=>'Administración en Línea', 
					)); 
Categoria::create(array(
					'id'=>'260',
					'categoria'=>'Ópticas', 
					)); 
Categoria::create(array(
					'id'=>'261',
					'categoria'=>'Cirugía Dental', 
					)); 
Categoria::create(array(
					'id'=>'262',
					'categoria'=>'Productos Orgánicos', 
					)); 
Categoria::create(array(
					'id'=>'263',
					'categoria'=>'Ortodoncistas', 
					)); 
Categoria::create(array(
					'id'=>'264',
					'categoria'=>'Oxígeno', 
					)); 
Categoria::create(array(
					'id'=>'265',
					'categoria'=>'Material de Empaque', 
					)); 
Categoria::create(array(
					'id'=>'266',
					'categoria'=>'Paquetería y Mensajería', 
					)); 
Categoria::create(array(
					'id'=>'267',
					'categoria'=>'Pinturas', 
					)); 
Categoria::create(array(
					'id'=>'268',
					'categoria'=>'Pensiones', 
					)); 
Categoria::create(array(
					'id'=>'269',
					'categoria'=>'Estacionamientos / Pensiones de Autos', 
					)); 
Categoria::create(array(
					'id'=>'270',
					'categoria'=>'Planificación de Fiestas', 
					)); 
Categoria::create(array(
					'id'=>'271',
					'categoria'=>'Fiestas para Niños', 
					)); 
Categoria::create(array(
					'id'=>'272',
					'categoria'=>'Alquileres para Fiestas', 
					)); 
Categoria::create(array(
					'id'=>'273',
					'categoria'=>'Artículos para Fiesta', 
					)); 
Categoria::create(array(
					'id'=>'274',
					'categoria'=>'Reposterías [véase también Pastelerías]', 
					)); 
Categoria::create(array(
					'id'=>'275',
					'categoria'=>'Casas de Empeño', 
					)); 
Categoria::create(array(
					'id'=>'276',
					'categoria'=>'Perfumes', 
					)); 
Categoria::create(array(
					'id'=>'277',
					'categoria'=>'Entrenamiento Personal', 
					)); 
Categoria::create(array(
					'id'=>'278',
					'categoria'=>'Control de Plagas', 
					)); 
Categoria::create(array(
					'id'=>'279',
					'categoria'=>'Alojamiento para Mascotas', 
					)); 
Categoria::create(array(
					'id'=>'280',
					'categoria'=>'Mascotas (Cuidado)', 
					)); 
Categoria::create(array(
					'id'=>'281',
					'categoria'=>'Alimento para Mascotas', 
					)); 
Categoria::create(array(
					'id'=>'282',
					'categoria'=>'Mascotas (Comida)', 
					)); 
Categoria::create(array(
					'id'=>'283',
					'categoria'=>'Mascotas (Estética)', 
					)); 
Categoria::create(array(
					'id'=>'284',
					'categoria'=>'Mascotas (Tiendas)', 
					)); 
Categoria::create(array(
					'id'=>'285',
					'categoria'=>'Mascotas (Accesorios)', 
					)); 
Categoria::create(array(
					'id'=>'286',
					'categoria'=>'Mascotas', 
					)); 
Categoria::create(array(
					'id'=>'287',
					'categoria'=>'Estudios Fotográficos', 
					)); 
Categoria::create(array(
					'id'=>'288',
					'categoria'=>'Fotógrafos', 
					)); 
Categoria::create(array(
					'id'=>'289',
					'categoria'=>'Clases de Fotografía', 
					)); 
Categoria::create(array(
					'id'=>'290',
					'categoria'=>'Terapia Física', 
					)); 
Categoria::create(array(
					'id'=>'291',
					'categoria'=>'Médicos', 
					)); 
Categoria::create(array(
					'id'=>'292',
					'categoria'=>'Médicos (Cardiopulmonares)', 
					)); 
Categoria::create(array(
					'id'=>'293',
					'categoria'=>'Médicos (Endocrinólogos Ginecológicos)', 
					)); 
Categoria::create(array(
					'id'=>'294',
					'categoria'=>'Médicos (Familiares)', 
					)); 
Categoria::create(array(
					'id'=>'295',
					'categoria'=>'Médicos (Ginecólogos)', 
					)); 
Categoria::create(array(
					'id'=>'296',
					'categoria'=>'Médicos (Cirujanos Plásticos)', 
					)); 
Categoria::create(array(
					'id'=>'297',
					'categoria'=>'Médicos (Psiquiatras)', 
					)); 
Categoria::create(array(
					'id'=>'298',
					'categoria'=>'Médicos (Urólogos Oncólogos)', 
					)); 
Categoria::create(array(
					'id'=>'299',
					'categoria'=>'Pilates', 
					)); 
Categoria::create(array(
					'id'=>'300',
					'categoria'=>'Pizzerías [véase también Restaurantes Pizza]', 
					)); 
Categoria::create(array(
					'id'=>'301',
					'categoria'=>'Cirugía Plástica', 
					)); 
Categoria::create(array(
					'id'=>'302',
					'categoria'=>'Plomeros', 
					)); 
Categoria::create(array(
					'id'=>'303',
					'categoria'=>'Accesorios de Plomería', 
					)); 
Categoria::create(array(
					'id'=>'304',
					'categoria'=>'Albercas (Accesorios)', 
					)); 
Categoria::create(array(
					'id'=>'305',
					'categoria'=>'Calderas para Albercas', 
					)); 
Categoria::create(array(
					'id'=>'306',
					'categoria'=>'Albercas  (Mantenimiento)', 
					)); 
Categoria::create(array(
					'id'=>'307',
					'categoria'=>'Imprentas', 
					)); 
Categoria::create(array(
					'id'=>'308',
					'categoria'=>'Seguridad Privada', 
					)); 
Categoria::create(array(
					'id'=>'309',
					'categoria'=>'Sistemas de Seguridad [véase también Alarmas, Seguridad Privada]', 
					)); 
Categoria::create(array(
					'id'=>'310',
					'categoria'=>'Psicoterapeutas', 
					)); 
Categoria::create(array(
					'id'=>'311',
					'categoria'=>'Sistemas de Purificación', 
					)); 
Categoria::create(array(
					'id'=>'312',
					'categoria'=>'Agua Purificada', 
					)); 
Categoria::create(array(
					'id'=>'313',
					'categoria'=>'Cantera', 
					)); 
Categoria::create(array(
					'id'=>'314',
					'categoria'=>'Estaciones de Radio', 
					)); 
Categoria::create(array(
					'id'=>'315',
					'categoria'=>'Bienes Raíces (Desarrollos)', 
					)); 
Categoria::create(array(
					'id'=>'316',
					'categoria'=>'Bienes Raíces (Administración)', 
					)); 
Categoria::create(array(
					'id'=>'317',
					'categoria'=>'Bienes Raíces (Renta)', 
					)); 
Categoria::create(array(
					'id'=>'318',
					'categoria'=>'Bienes Raíces (Venta)', 
					)); 
Categoria::create(array(
					'id'=>'319',
					'categoria'=>'Balnearios [veáse también Clubes Deportivos]', 
					)); 
Categoria::create(array(
					'id'=>'320',
					'categoria'=>'Rehabilitación (Asilos)', 
					)); 
Categoria::create(array(
					'id'=>'321',
					'categoria'=>'Renta de Tranvía', 
					)); 
Categoria::create(array(
					'id'=>'322',
					'categoria'=>'Renta de Videos', 
					)); 
Categoria::create(array(
					'id'=>'323',
					'categoria'=>'Restaurantes Bares', 
					)); 
Categoria::create(array(
					'id'=>'324',
					'categoria'=>'Restaurantes', 
					)); 
Categoria::create(array(
					'id'=>'325',
					'categoria'=>'Restaurantes Argentinos', 
					)); 
Categoria::create(array(
					'id'=>'326',
					'categoria'=>'Restaurantes Hamburguesas', 
					)); 
Categoria::create(array(
					'id'=>'327',
					'categoria'=>'Restaurantes Cafetería', 
					)); 
Categoria::create(array(
					'id'=>'328',
					'categoria'=>'Restaurantes Crepas', 
					)); 
Categoria::create(array(
					'id'=>'329',
					'categoria'=>'Restaurantes Cenadurías', 
					)); 
Categoria::create(array(
					'id'=>'330',
					'categoria'=>'Restaurantes Comida Rápida', 
					)); 
Categoria::create(array(
					'id'=>'331',
					'categoria'=>'Restaurantes Franceses', 
					)); 
Categoria::create(array(
					'id'=>'332',
					'categoria'=>'Restaurantes Internacionales', 
					)); 
Categoria::create(array(
					'id'=>'333',
					'categoria'=>'Restaurantes Italianos', 
					)); 
Categoria::create(array(
					'id'=>'334',
					'categoria'=>'Restaurantes Comida Mexicana', 
					)); 
Categoria::create(array(
					'id'=>'335',
					'categoria'=>'Restaurantes Orgánicos', 
					)); 
Categoria::create(array(
					'id'=>'336',
					'categoria'=>'Restaurantes Pizza', 
					)); 
Categoria::create(array(
					'id'=>'337',
					'categoria'=>'Restaurantes Rosticerías', 
					)); 
Categoria::create(array(
					'id'=>'338',
					'categoria'=>'Restaurantes Mariscos', 
					)); 
Categoria::create(array(
					'id'=>'339',
					'categoria'=>'Restaurantes de Comida Sueca', 
					)); 
Categoria::create(array(
					'id'=>'340',
					'categoria'=>'Restaurantes Tacos', 
					)); 
Categoria::create(array(
					'id'=>'341',
					'categoria'=>'Restaurantes Comida Yucateca', 
					)); 
Categoria::create(array(
					'id'=>'342',
					'categoria'=>'Clubes Hípicos', 
					)); 
Categoria::create(array(
					'id'=>'343',
					'categoria'=>'Rosticerías', 
					)); 
Categoria::create(array(
					'id'=>'344',
					'categoria'=>'Sistemas Satelitales', 
					)); 
Categoria::create(array(
					'id'=>'345',
					'categoria'=>'Televisión Satelital', 
					)); 
Categoria::create(array(
					'id'=>'346',
					'categoria'=>'Cajas Populares', 
					)); 
Categoria::create(array(
					'id'=>'347',
					'categoria'=>'Escuelas', 
					)); 
Categoria::create(array(
					'id'=>'348',
					'categoria'=>'Escuelas-Primaria', 
					)); 
Categoria::create(array(
					'id'=>'349',
					'categoria'=>'Escuelas-Preparatoria', 
					)); 
Categoria::create(array(
					'id'=>'350',
					'categoria'=>'Escuelas-Secundaria', 
					)); 
Categoria::create(array(
					'id'=>'351',
					'categoria'=>'Escuelas-Kínder', 
					)); 
Categoria::create(array(
					'id'=>'352',
					'categoria'=>'Escultura', 
					)); 
Categoria::create(array(
					'id'=>'353',
					'categoria'=>'Reparación y Limpieza de Calzado', 
					)); 
Categoria::create(array(
					'id'=>'354',
					'categoria'=>'Zapaterías', 
					)); 
Categoria::create(array(
					'id'=>'355',
					'categoria'=>'Compras', 
					)); 
Categoria::create(array(
					'id'=>'356',
					'categoria'=>'Centros Comerciales', 
					)); 
Categoria::create(array(
					'id'=>'357',
					'categoria'=>'Platerías', 
					)); 
Categoria::create(array(
					'id'=>'358',
					'categoria'=>'Medios Sociales', 
					)); 
Categoria::create(array(
					'id'=>'359',
					'categoria'=>'Calefacción Solar [véase también Sistemas Solares]', 
					)); 
Categoria::create(array(
					'id'=>'360',
					'categoria'=>'Sistemas Solares', 
					)); 
Categoria::create(array(
					'id'=>'361',
					'categoria'=>'Equipos de Sonido (Renta y Venta)', 
					)); 
Categoria::create(array(
					'id'=>'362',
					'categoria'=>'Clases de Español', 
					)); 
Categoria::create(array(
					'id'=>'363',
					'categoria'=>'Spas', 
					)); 
Categoria::create(array(
					'id'=>'364',
					'categoria'=>'Deportes (Accesorios)', 
					)); 
Categoria::create(array(
					'id'=>'365',
					'categoria'=>'Deportes', 
					)); 
Categoria::create(array(
					'id'=>'366',
					'categoria'=>'Bares (Deportes)', 
					)); 
Categoria::create(array(
					'id'=>'367',
					'categoria'=>'Clubes Deportivos', 
					)); 
Categoria::create(array(
					'id'=>'368',
					'categoria'=>'Papelerías', 
					)); 
Categoria::create(array(
					'id'=>'369',
					'categoria'=>'Casas de Bolsa', 
					)); 
Categoria::create(array(
					'id'=>'370',
					'categoria'=>'Bodegas', 
					)); 
Categoria::create(array(
					'id'=>'371',
					'categoria'=>'Arquitectura Sustentable', 
					)); 
Categoria::create(array(
					'id'=>'372',
					'categoria'=>'Albercas', 
					)); 
Categoria::create(array(
					'id'=>'373',
					'categoria'=>'Modistas y Sastres', 
					)); 
Categoria::create(array(
					'id'=>'374',
					'categoria'=>'Talavera', 
					)); 
Categoria::create(array(
					'id'=>'375',
					'categoria'=>'Taquerías [véase también Restaurantes Tacos]', 
					)); 
Categoria::create(array(
					'id'=>'376',
					'categoria'=>'Asesoría Fiscal', 
					)); 
Categoria::create(array(
					'id'=>'377',
					'categoria'=>'Preparación de Impuestos', 
					)); 
Categoria::create(array(
					'id'=>'378',
					'categoria'=>'Taxis', 
					)); 
Categoria::create(array(
					'id'=>'379',
					'categoria'=>'Empresas de Telefonía', 
					)); 
Categoria::create(array(
					'id'=>'380',
					'categoria'=>'Telefonía (Servicios)', 
					)); 
Categoria::create(array(
					'id'=>'381',
					'categoria'=>'Tenis', 
					)); 
Categoria::create(array(
					'id'=>'382',
					'categoria'=>'Teatros', 
					)); 
Categoria::create(array(
					'id'=>'383',
					'categoria'=>'Terapias', 
					)); 
Categoria::create(array(
					'id'=>'384',
					'categoria'=>'Aguas Termales', 
					)); 
Categoria::create(array(
					'id'=>'385',
					'categoria'=>'Llanteras', 
					)); 
Categoria::create(array(
					'id'=>'386',
					'categoria'=>'Escrituración', 
					)); 
Categoria::create(array(
					'id'=>'387',
					'categoria'=>'Tortillerías', 
					)); 
Categoria::create(array(
					'id'=>'388',
					'categoria'=>'Recorridos Turísticos', 
					)); 
Categoria::create(array(
					'id'=>'389',
					'categoria'=>'Grúas', 
					)); 
Categoria::create(array(
					'id'=>'390',
					'categoria'=>'Juguetes', 
					)); 
Categoria::create(array(
					'id'=>'391',
					'categoria'=>'Traductores', 
					)); 
Categoria::create(array(
					'id'=>'392',
					'categoria'=>'Transporte', 
					)); 
Categoria::create(array(
					'id'=>'393',
					'categoria'=>'Transportes (Renta)', 
					)); 
Categoria::create(array(
					'id'=>'394',
					'categoria'=>'Agencias de Viaje', 
					)); 
Categoria::create(array(
					'id'=>'395',
					'categoria'=>'Canales de Televisión', 
					)); 
Categoria::create(array(
					'id'=>'396',
					'categoria'=>'Universidades', 
					)); 
Categoria::create(array(
					'id'=>'397',
					'categoria'=>'Tapicerías', 
					)); 
Categoria::create(array(
					'id'=>'398',
					'categoria'=>'Agencias de Autos Usados', 
					)); 
Categoria::create(array(
					'id'=>'399',
					'categoria'=>'Paquetes Vacacionales', 
					)); 
Categoria::create(array(
					'id'=>'400',
					'categoria'=>'Rentas Vacacionales', 
					)); 
Categoria::create(array(
					'id'=>'401',
					'categoria'=>'Veterinarios', 
					)); 
Categoria::create(array(
					'id'=>'402',
					'categoria'=>'Servicios de Transferencia de Audio y Video', 
					)); 
Categoria::create(array(
					'id'=>'403',
					'categoria'=>'Videojuegos', 
					)); 
Categoria::create(array(
					'id'=>'404',
					'categoria'=>'Salas de Velación', 
					)); 
Categoria::create(array(
					'id'=>'405',
					'categoria'=>'Vitaminas', 
					)); 
Categoria::create(array(
					'id'=>'406',
					'categoria'=>'Voluntariado', 
					)); 
Categoria::create(array(
					'id'=>'407',
					'categoria'=>'Relojes (Venta y Reparación)', 
					)); 
Categoria::create(array(
					'id'=>'408',
					'categoria'=>'Calentadores de Agua', 
					)); 
Categoria::create(array(
					'id'=>'409',
					'categoria'=>'Purificación de Aguas (Sistemas)', 
					)); 
Categoria::create(array(
					'id'=>'410',
					'categoria'=>'Impermeabilizantes', 
					)); 
Categoria::create(array(
					'id'=>'411',
					'categoria'=>'Diseño de Sitios Web', 
					)); 
Categoria::create(array(
					'id'=>'412',
					'categoria'=>'Bodas (Planeación y Servicios)', 
					)); 
Categoria::create(array(
					'id'=>'413',
					'categoria'=>'Organización de Bodas', 
					)); 
Categoria::create(array(
					'id'=>'414',
					'categoria'=>'Cortinas y Persianas', 
					)); 
Categoria::create(array(
					'id'=>'415',
					'categoria'=>'Tratamiento para Ventanas', 
					)); 
Categoria::create(array(
					'id'=>'416',
					'categoria'=>'Parabrisas', 
					)); 
Categoria::create(array(
					'id'=>'417',
					'categoria'=>'Vinos y Licores', 
					)); 
Categoria::create(array(
					'id'=>'418',
					'categoria'=>'Clases de Yoga', 
					)); 
	}
}