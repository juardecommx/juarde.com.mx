<?php
class AdvertisersSeeder extends Seeder{

	public function run(){

		DB::table('advertisers')->delete();
		DB::table('advertisements')->delete();
		
		//Seeds Reales

			Advertiser::create(array(
					'id'=>'9',
					'business_name'=>'Ablu',   
					'phone'=>'152 5870',  
					)); 

			Advertiser::create(array(
					'id'=>'10',
					'business_name'=>'Abraham Cadena Bienes Raíces', 
					'address'=>'Ancha de San Antonio 47, Centro', 
					'email'=>'abrahamcadenarealestate@yahoo.com', 
					'phone'=>'152 3210',
					'phone2'=>'152 1638',
					'mobile_phone'=>'F 152 2313',  
					'web_page'=>'www.abrahamcadena.com', 
					)); 

			Advertiser::create(array(
					'id'=>'11',
					'business_name'=>'Academia Hispanoamericana',   
					'phone'=>'152 0349',  
					)); 

			Advertiser::create(array(
					'id'=>'12',
					'business_name'=>'ACDM Living & Learning',   
					'phone'=>'152 6064',  
					)); 

			Advertiser::create(array(
					'id'=>'13',
					'business_name'=>'Actinver Casa de Bolsa',   
					'phone'=>'152 1832',  
					)); 

			Advertiser::create(array(
					'id'=>'14',
					'business_name'=>'Acuario',   
					'phone'=>'152 8585',  
					)); 

			Advertiser::create(array(
					'id'=>'15',
					'business_name'=>'AD & C Design & Construction',   
					'phone'=>'152 5894',  
					)); 

			Advertiser::create(array(
					'id'=>'16',
					'business_name'=>'Agave Sotheby´s International Realty', 
					'address'=>'Hospicio 23 y Aldama 16. Centro', 
					'phone'=>'152 5220',
					'phone2'=>'152 2180',
					'mobile_phone'=>'USA 415 366 7020',  
					'web_page'=>'www.agavesothebysrealty.com', 
					)); 

			Advertiser::create(array(
					'id'=>'17',
					'business_name'=>'Agropecuaria Quiroz',   
					'phone'=>'152 3412',  
					)); 

			Advertiser::create(array(
					'id'=>'18',
					'business_name'=>'Agua Ciel',   
					'phone'=>'154 5060',  
					)); 

			Advertiser::create(array(
					'id'=>'19',
					'business_name'=>'Agua de Coco Boutique',   
					'phone'=>'154 9132',  
					)); 

			Advertiser::create(array(
					'id'=>'20',
					'business_name'=>'Agua Santorini',   
					'phone'=>'01 800 237 3734',  
					)); 

			Advertiser::create(array(
					'id'=>'21',
					'business_name'=>'Alas de San Miguel',   
					'phone'=>'152 3778',  
					)); 

			Advertiser::create(array(
					'id'=>'22',
					'business_name'=>'Alfonso y Santiago Mecánicos',   
					'phone'=>'152 1602',  
					)); 

			Advertiser::create(array(
					'id'=>'23',
					'business_name'=>'Allende Properties', 
					'address'=>'Cuna de Allende  15, Centro', 
					'email'=>'info@allendeproperties.com', 
					'phone'=>'154 5000',
					'phone2'=>'US 214 432 7558',
					'mobile_phone'=>'C 415 103 3311',  
					'mobile_phone2'=>'C 415 153 3167', 
					'web_page'=>'www.allendeproperties.com', 
					)); 

			Advertiser::create(array(
					'id'=>'24',
					'business_name'=>'Asilo ALMA, A.C.',   
					'phone'=>'152 7210',  
					)); 

			Advertiser::create(array(
					'id'=>'25',
					'business_name'=>'Alquileres Acuario',   
					'phone'=>'152 8585',  
					)); 

			Advertiser::create(array(
					'id'=>'26',
					'business_name'=>'Andrea',   
					'phone'=>'120 4368',  
					)); 

			Advertiser::create(array(
					'id'=>'27',
					'business_name'=>'Animal Care', 
					'address'=>'Río Grijalva 10 Fracc. Nuevo Mexiquito', 
					'email'=>'dr_r_lopez@hotmail.com', 
					'phone'=>'152 6977',
					'phone2'=>'154 5554',  
					)); 

			Advertiser::create(array(
					'id'=>'28',
					'business_name'=>'Animal Hospital', 
					'address'=>'Mesones 5, Locales F & B', 
					'email'=>'clinicadeanimales@hotmail.com', 
					'phone'=>'152 6273',
					'phone2'=>'F 154 4785',  
					)); 

			Advertiser::create(array(
					'id'=>'29',
					'business_name'=>'Animal Medical Center', 
					'address'=>'Salida a Celaya 67', 
					'email'=>'animalmedicalcentersma@gmail.com', 
					'phone'=>'185 8185',
					'phone2'=>'C 415 109 9957',  
					)); 

			Advertiser::create(array(
					'id'=>'30',
					'business_name'=>'Araiza',   
					'phone'=>'154 5934',  
					)); 

			Advertiser::create(array(
					'id'=>'31',
					'business_name'=>'Arq. Gabor Goded',   
					'phone'=>'',
					'mobile_phone'=>'(442) 265 1976',  
					)); 

			Advertiser::create(array(
					'id'=>'32',
					'business_name'=>'Arquitectura Sustentable Simple Co',   
					'phone'=>'150 7229',  
					)); 

			Advertiser::create(array(
					'id'=>'33',
					'business_name'=>'Arte y Antigüedades La Buhardilla',   
					'phone'=>'154 9911',  
					)); 

			Advertiser::create(array(
					'id'=>'34',
					'business_name'=>'Artesa',   
					'phone'=>'152 2477',  
					)); 

			Advertiser::create(array(
					'id'=>'35',
					'business_name'=>'Artesanías Búfalo',   
					'phone'=>'152 2271',  
					)); 

			Advertiser::create(array(
					'id'=>'36',
					'business_name'=>'Arthur Murray / Baile Café',   
					'phone'=>'152 0095',  
					)); 

			Advertiser::create(array(
					'id'=>'37',
					'business_name'=>'Atención',   
					'phone'=>'152 3770',  
					)); 

			Advertiser::create(array(
					'id'=>'38',
					'business_name'=>'Atenea Gallery', 
					'address'=>'Jesús 2, Centro', 
					'email'=>'info@atenearealty.com', 
					'phone'=>'152 0785',
					'phone2'=>'152 1337',
					'mobile_phone'=>'C 415 101 0194',  
					'mobile_phone2'=>'C 415 151 0101', 
					'web_page'=>'www.ateneagallery.com', 
					)); 

			Advertiser::create(array(
					'id'=>'39',
					'business_name'=>'Atenea Realty', 
					'address'=>'Jesús 2, Centro', 
					'email'=>'info@atenearealty.com', 
					'email2'=>'carlos@atenearealty.com', 
					'phone'=>'152 1337',
					'phone2'=>'152 0785',
					'mobile_phone'=>'C 415 151 0101',  
					'web_page'=>'www.atenearealty.com', 
					)); 

			Advertiser::create(array(
					'id'=>'40',
					'business_name'=>'Atotonilco El Viejo',   
					'phone'=>'185 2132',  
					)); 

			Advertiser::create(array(
					'id'=>'41',
					'business_name'=>'Atotonilco Gallery',   
					'phone'=>'185 2225',  
					)); 

			Advertiser::create(array(
					'id'=>'42',
					'business_name'=>'Avalúos fiscales y comerciales',   
					'phone'=>'154 7530',  
					)); 

			Advertiser::create(array(
					'id'=>'43',
					'business_name'=>'Bacco Ristorante Bar', 
					'address'=>'Hernández Macías 59,Centro Inside Hotel Sautto',  
					'phone'=>'154 5513',  
					)); 

			Advertiser::create(array(
					'id'=>'44',
					'business_name'=>'Balneario Xoté',   
					'phone'=>'155 8330',  
					)); 

			Advertiser::create(array(
					'id'=>'45',
					'business_name'=>'Banamex',   
					'phone'=>'152 1040',  
					)); 

			Advertiser::create(array(
					'id'=>'46',
					'business_name'=>'BanBajío',   
					'phone'=>'185 8313',  
					'web_page'=>'www.bb.com.mx', 
					)); 

			Advertiser::create(array(
					'id'=>'47',
					'business_name'=>'Banco Azteca',   
					'phone'=>'153 1906',  
					)); 

			Advertiser::create(array(
					'id'=>'48',
					'business_name'=>'Bancomer',   
					'phone'=>'152 2124',  
					)); 

			Advertiser::create(array(
					'id'=>'49',
					'business_name'=>'Banorte',   
					'phone'=>'152 0019',  
					)); 

			Advertiser::create(array(
					'id'=>'50',
					'business_name'=>'Banquetes La Finca',   
					'phone'=>'154 9465',  
					)); 

			Advertiser::create(array(
					'id'=>'51',
					'business_name'=>'Banquetes Licea',   
					'phone'=>'152 5868',  
					)); 

			Advertiser::create(array(
					'id'=>'52',
					'business_name'=>'Barajas Plomería',   
					'phone'=>'',
					'mobile_phone'=>'C 415 151 1205',  
					)); 

			Advertiser::create(array(
					'id'=>'53',
					'business_name'=>'Barbosa Arquitectos',   
					'phone'=>'152 2845',  
					)); 

			Advertiser::create(array(
					'id'=>'54',
					'business_name'=>'Barroso Rodríguez José Cruz',   
					'phone'=>'',
					'mobile_phone'=>'(462) 625 1308',  
					)); 

			Advertiser::create(array(
					'id'=>'55',
					'business_name'=>'Bella Piel',   
					'phone'=>'122 0125',  
					)); 

			Advertiser::create(array(
					'id'=>'56',
					'business_name'=>'Bellas Artes',   
					'phone'=>'152 0289',  
					)); 

			Advertiser::create(array(
					'id'=>'57',
					'business_name'=>'Biblioteca de C.A.S.A',   
					'phone'=>'154 6060',  
					)); 

			Advertiser::create(array(
					'id'=>'58',
					'business_name'=>'Biblioteca Municipal',   
					'phone'=>'152 7855',  
					)); 

			Advertiser::create(array(
					'id'=>'59',
					'business_name'=>'Bici Burro',   
					'phone'=>'152 1526',  
					)); 

			Advertiser::create(array(
					'id'=>'60',
					'business_name'=>'Bicicletas Escarabajo',   
					'phone'=>'154 6083',  
					)); 

			Advertiser::create(array(
					'id'=>'61',
					'business_name'=>'Bio Builders',   
					'phone'=>'',
					'mobile_phone'=>'C 415 107 0187',  
					)); 

			Advertiser::create(array(
					'id'=>'62',
					'business_name'=>'Bios Lab',   
					'phone'=>'154 7322',  
					)); 

			Advertiser::create(array(
					'id'=>'63',
					'business_name'=>'Blockbuster',   
					'phone'=>'152 7580',  
					)); 

			Advertiser::create(array(
					'id'=>'64',
					'business_name'=>'OCA (Operadora Turística Colonial de Allende)', 
					'address'=>'Cuna de Allende 17 Int. 3, Centro', 
					'email'=>'ocaviajes@yahoo.com.mx', 
					'email2'=>'ocaviajes@yahoo.com.mx', 
					'phone'=>'152 8011',
					'phone2'=>'154 9777',  
					'web_page'=>'www.allendecolonial.econsolid.com', 
					)); 

			Advertiser::create(array(
					'id'=>'65',
					'business_name'=>'Bodega Aurrera',   
					'phone'=>'150 7463',  
					)); 

			Advertiser::create(array(
					'id'=>'66',
					'business_name'=>'Bonanza',   
					'phone'=>'152 1260',  
					)); 

			Advertiser::create(array(
					'id'=>'67',
					'business_name'=>'Botica Agundis', 
					'address'=>'Canal 26, Centro', 
					'email'=>'boticaagundisqyahoo.com.mx', 
					'phone'=>'152 1198',  
					)); 

			Advertiser::create(array(
					'id'=>'68',
					'business_name'=>'Bridgestone Firestone',   
					'phone'=>'150 7189',  
					)); 

			Advertiser::create(array(
					'id'=>'69',
					'business_name'=>'Britt Zaist-Henry Vermillion',   
					'phone'=>'154 5409',  
					)); 

			Advertiser::create(array(
					'id'=>'70',
					'business_name'=>'Caja Popular Alianza',   
					'phone'=>'152 1545',  
					)); 

			Advertiser::create(array(
					'id'=>'71',
					'business_name'=>'Caja Popular Mexicana',   
					'phone'=>'154 4040',  
					)); 

			Advertiser::create(array(
					'id'=>'72',
					'business_name'=>'Canal 3',   
					'phone'=>'121 1308',  
					)); 

			Advertiser::create(array(
					'id'=>'73',
					'business_name'=>'Canastaquiz',   
					'phone'=>'',
					'mobile_phone'=>'C 415 153 9131',  
					)); 

			Advertiser::create(array(
					'id'=>'74',
					'business_name'=>'Canek',   
					'phone'=>'152 4520',  
					)); 

			Advertiser::create(array(
					'id'=>'75',
					'business_name'=>'Carmen Rivera Río', 
					'address'=>'Canal 3 A, Centro', 
					'email'=>'carmen@rrrseguros.com', 
					'phone'=>'152 1086',
					'mobile_phone'=>'C 415 113 9774',  
					)); 

			Advertiser::create(array(
					'id'=>'76',
					'business_name'=>'Carnicería La Nueva Aurora', 
					'address'=>'Infonavit La Luz, Durazno 24', 
					'email'=>'jarteaga804@gmail.com', 
					'phone'=>'152 4004',
					'phone2'=>'12 05 021',  
					)); 

			Advertiser::create(array(
					'id'=>'77',
					'business_name'=>'Carnicería La Lonja',   
					'phone'=>'152 0471',  
					)); 

			Advertiser::create(array(
					'id'=>'78',
					'business_name'=>'Carpintería Caballero',   
					'phone'=>'152 5338',  
					)); 

			Advertiser::create(array(
					'id'=>'79',
					'business_name'=>'Carrusel',   
					'phone'=>'120 0600',  
					)); 

			Advertiser::create(array(
					'id'=>'80',
					'business_name'=>'Casa Carly', 
					'address'=>'Calzada de la Aurora 48', 
					'email'=>'carly@gomexart.com', 
					'phone'=>'152 8900',  
					'web_page'=>'www.casacarly.com', 
					)); 

			Advertiser::create(array(
					'id'=>'81',
					'business_name'=>'Casa Schuck',   
					'phone'=>'152 0657',  
					)); 

			Advertiser::create(array(
					'id'=>'82',
					'business_name'=>'Casa del Inquisidor', 
					'address'=>'Aldama 1, Centro',  
					'email2'=>'aldamanumber1@gmail.com', 
					'phone'=>'154 6868',
					'mobile_phone'=>'C 415 109 3023',  
					'web_page'=>'www.lacasadelinquisidor.com', 
					)); 

			Advertiser::create(array(
					'id'=>'83',
					'business_name'=>'Casa Roberto', 
					'address'=>'Stirling Dickinson 28 Local 1', 
					'email'=>'info@casa-roberto.com', 
					'email2'=>'casaroberto1@gmail.com', 
					'phone'=>'152 8620',  
					'web_page'=>'www.casa-roberto.com', 
					)); 

			Advertiser::create(array(
					'id'=>'84',
					'business_name'=>'Casa Rosada Hotel',   
					'phone'=>'152 8123',  
					)); 

			Advertiser::create(array(
					'id'=>'85',
					'business_name'=>'CEDECOM',   
					'phone'=>'110 3180',  
					)); 

			Advertiser::create(array(
					'id'=>'86',
					'business_name'=>'Celebrations San Miguel',   
					'phone'=>'154 8364',  
					)); 

			Advertiser::create(array(
					'id'=>'87',
					'business_name'=>'Centro Cultural Ignacio Ramírez El Nigromante',   
					'phone'=>'152 0289',  
					)); 

			Advertiser::create(array(
					'id'=>'88',
					'business_name'=>'Centro de Crecimiento, A.C.',   
					'phone'=>'152 0318',  
					)); 

			Advertiser::create(array(
					'id'=>'89',
					'business_name'=>'Centro de Estudios Superiores Allende',   
					'phone'=>'154 7648',  
					)); 

			Advertiser::create(array(
					'id'=>'90',
					'business_name'=>'Centro Naturista San Miguel',   
					'phone'=>'154 9277',  
					)); 

			Advertiser::create(array(
					'id'=>'91',
					'business_name'=>'Centro Shakti',   
					'phone'=>'',
					'mobile_phone'=>'C 415 113 7061',  
					)); 

			Advertiser::create(array(
					'id'=>'92',
					'business_name'=>'Cercas y Mallas San Miguel',   
					'phone'=>'152 6167',  
					)); 

			Advertiser::create(array(
					'id'=>'93',
					'business_name'=>'Cerrajería Ceballos',   
					'phone'=>'154 7181',  
					)); 

			Advertiser::create(array(
					'id'=>'94',
					'business_name'=>'Cerrajería Flores',   
					'phone'=>'120 2152',  
					)); 

			Advertiser::create(array(
					'id'=>'95',
					'business_name'=>'Cerrajería Vizcaíno',   
					'phone'=>'154 9333',  
					)); 

			Advertiser::create(array(
					'id'=>'96',
					'business_name'=>'Charco del Ingenio',   
					'phone'=>'154 4715',  
					)); 

			Advertiser::create(array(
					'id'=>'97',
					'business_name'=>'Chevrolet',   
					'phone'=>'152 2598',  
					)); 

			Advertiser::create(array(
					'id'=>'98',
					'business_name'=>'Chocolates Johfrej C & V', 
					'address'=>'Jesús 2 A, Centro', 
					'email'=>'rpatino@johfrej.com', 
					'phone'=>'152 3191',  
					'web_page'=>'www.johfrej.com', 
					)); 

			Advertiser::create(array(
					'id'=>'99',
					'business_name'=>'Classes Unlimited',   
					'phone'=>'152 2483',  
					)); 

			Advertiser::create(array(
					'id'=>'100',
					'business_name'=>'Clínica Automotriz San Miguel',   
					'phone'=>'152 1974',  
					)); 

			Advertiser::create(array(
					'id'=>'101',
					'business_name'=>'Clínica del Dolor',   
					'phone'=>'152 8105',  
					)); 

			Advertiser::create(array(
					'id'=>'102',
					'business_name'=>'Club de Golf Malanquín', 
					'address'=>'Km 3 Carr. San Miguel - Celaya', 
					'email'=>'ventas@malanquin.com.mx', 
					'phone'=>'152 6721',
					'phone2'=>'152 0516',  
					'web_page'=>'www.malanquin.com.mx', 
					)); 

			Advertiser::create(array(
					'id'=>'103',
					'business_name'=>'Coldwell Banker SMART Real Estate',   
					'phone'=>'122 3010',  
					'web_page'=>'wwww.cbpremier-realestate.com', 
					)); 

			Advertiser::create(array(
					'id'=>'104',
					'business_name'=>'Colegio Siglo XXI',   
					'phone'=>'152 7500',  
					)); 

			Advertiser::create(array(
					'id'=>'105',
					'business_name'=>'Mega Comercial Mexicana',   
					'phone'=>'120 2051',  
					)); 

			Advertiser::create(array(
					'id'=>'106',
					'business_name'=>'Comex',   
					'phone'=>'150 0130',  
					)); 

			Advertiser::create(array(
					'id'=>'107',
					'business_name'=>'Construrama',   
					'phone'=>'01 800 122 2121',  
					)); 

			Advertiser::create(array(
					'id'=>'108',
					'business_name'=>'C. Roberto Guzmán V.',   
					'phone'=>'154 5531',  
					)); 

			Advertiser::create(array(
					'id'=>'109',
					'business_name'=>'Counter Cultures', 
					'address'=>'Zacateros 21, Centro', 
					'email'=>'roger@countercultures.com.mx', 
					'phone'=>'154 8375',
					'mobile_phone'=>'C 415 114 0949',  
					'web_page'=>'www.countercultures.com.mx', 
					)); 

			Advertiser::create(array(
					'id'=>'110',
					'business_name'=>'Coyote Canyon',   
					'phone'=>'154 4193',  
					)); 

			Advertiser::create(array(
					'id'=>'111',
					'business_name'=>'Cris Car',   
					'phone'=>'154 6737',  
					)); 

			Advertiser::create(array(
					'id'=>'112',
					'business_name'=>'Cruz Roja',   
					'phone'=>'152 4121',  
					)); 

			Advertiser::create(array(
					'id'=>'113',
					'business_name'=>'Cuauhtémoc',   
					'phone'=>'155 8234',  
					)); 

			Advertiser::create(array(
					'id'=>'114',
					'business_name'=>'Dental Associates', 
					'address'=>'San Jorge 12, San Antonio', 
					'email'=>'lauraelu@prodigy.net.mx', 
					'phone'=>'152 4330',
					'phone2'=>'152 4262',  
					)); 

			Advertiser::create(array(
					'id'=>'115',
					'business_name'=>'DesMex Solar',   
					'phone'=>'120 0945',  
					)); 

			Advertiser::create(array(
					'id'=>'116',
					'business_name'=>'Devlyn',   
					'phone'=>'110 3228',  
					)); 

			Advertiser::create(array(
					'id'=>'117',
					'business_name'=>'DHL',   
					'phone'=>'152 3564',  
					)); 

			Advertiser::create(array(
					'id'=>'118',
					'business_name'=>'Distribuidora Comercial Castañeda',   
					'phone'=>'152 0480',  
					)); 

			Advertiser::create(array(
					'id'=>'119',
					'business_name'=>'Domino‘s',   
					'phone'=>'154 5300',  
					)); 

			Advertiser::create(array(
					'id'=>'120',
					'business_name'=>'Dr. Alejandro Méndez Gelacio',   
					'phone'=>'154 7259',  
					)); 

			Advertiser::create(array(
					'id'=>'121',
					'business_name'=>'Dr. Carlos Ramírez', 
					'address'=>'Ancha de San Antonio 20 Int 2',  
					'phone'=>'154 8330',
					'phone2'=>'152 2905',
					'mobile_phone'=>'C 415 1010 249',  
					)); 

			Advertiser::create(array(
					'id'=>'122',
					'business_name'=>'Dr. Hugo Rosas Hernández',   
					'phone'=>'152 2403',  
					)); 

			Advertiser::create(array(
					'id'=>'123',
					'business_name'=>'Dr. Javier Retana Santana', 
					'address'=>'Nuñez 19 A, Centro', 
					'email'=>'javier_retanas@hotmail.com', 
					'phone'=>'152 2319',
					'phone2'=>'150 0087',
					'mobile_phone'=>'C 415 113 9628',  
					)); 

			Advertiser::create(array(
					'id'=>'124',
					'business_name'=>'Dr. Jorge Álvarez de la Cadena',   
					'phone'=>'154 6333',  
					)); 

			Advertiser::create(array(
					'id'=>'125',
					'business_name'=>'Dr. Roberto Maxwell FCCP',   
					'phone'=>'152 2365',  
					)); 

			Advertiser::create(array(
					'id'=>'126',
					'business_name'=>'Dr. Roberto Miranda', 
					'address'=>'Pila Seca 53-1, Centro', 
					'email'=>'robertomiranda67@hotmail.com', 
					'phone'=>'154 6969',
					'phone2'=>'152 1111',  
					)); 

			Advertiser::create(array(
					'id'=>'127',
					'business_name'=>'Dr. Salvador Noyola Fuentes',   
					'phone'=>'152 2329',  
					)); 

			Advertiser::create(array(
					'id'=>'128',
					'business_name'=>'Dr. Salvador Quiroz M.D', 
					'address'=>'Libramiento Dolores Hidalgo 43',  
					'phone'=>'154 4020',
					'phone2'=>'152 2233',  
					)); 

			Advertiser::create(array(
					'id'=>'129',
					'business_name'=>'Dra. Alma Godínez', 
					'address'=>'Hidalgo 1 Int. 5, Centro', 
					'email'=>'dralmadentista@gmail.com', 
					'phone'=>'152 3082',
					'mobile_phone'=>'C 415 153 5270',  
					)); 

			Advertiser::create(array(
					'id'=>'130',
					'business_name'=>'Dra. Blanca Elena Farías Hernández', 
					'address'=>'Hernández Macías 85 A, Centro',  
					'phone'=>'152 2321',
					'phone2'=>'152 4082',  
					)); 

			Advertiser::create(array(
					'id'=>'131',
					'business_name'=>'Dra. Maribel Licea Tovar',   
					'phone'=>'152 1219',  
					)); 

			Advertiser::create(array(
					'id'=>'132',
					'business_name'=>'Dra. Patricia Muñiz Sandoval',   
					'phone'=>'',
					'mobile_phone'=>'C 415 103 5401',  
					)); 

			Advertiser::create(array(
					'id'=>'133',
					'business_name'=>'Dulcería Goretti',   
					'phone'=>'152 4905',  
					)); 

			Advertiser::create(array(
					'id'=>'134',
					'business_name'=>'Dulcería Polifiesta',   
					'phone'=>'154 8663',  
					)); 

			Advertiser::create(array(
					'id'=>'135',
					'business_name'=>'Dupont',   
					'phone'=>'152 3987',  
					)); 

			Advertiser::create(array(
					'id'=>'136',
					'business_name'=>'EARTHchitecture',   
					'phone'=>'',
					'mobile_phone'=>'C 415 119 4684',  
					)); 

			Advertiser::create(array(
					'id'=>'137',
					'business_name'=>'La Bella Italia', 
					'address'=>'Portal Guadalupe 12',  
					'phone'=>'154 6758',  
					)); 

			Advertiser::create(array(
					'id'=>'138',
					'business_name'=>'Restaurante El Correo', 
					'address'=>'Correo 23, Centro',  
					'phone'=>'152 4951',  
					)); 

			Advertiser::create(array(
					'id'=>'139',
					'business_name'=>'El Fogón',   
					'phone'=>'152 1586',  
					)); 

			Advertiser::create(array(
					'id'=>'140',
					'business_name'=>'El Grito',   
					'phone'=>'121 0156',  
					)); 

			Advertiser::create(array(
					'id'=>'141',
					'business_name'=>'El Kiosco Verde',   
					'phone'=>'',
					'mobile_phone'=>'(461) 118 0255',  
					)); 

			Advertiser::create(array(
					'id'=>'142',
					'business_name'=>'El May',   
					'phone'=>'150 7184',  
					)); 

			Advertiser::create(array(
					'id'=>'143',
					'business_name'=>'El Palacio de los Azulejos',   
					'phone'=>'152 3964',  
					)); 

			Advertiser::create(array(
					'id'=>'144',
					'business_name'=>'Florería el Paraíso',   
					'phone'=>'152 3944',  
					)); 

			Advertiser::create(array(
					'id'=>'145',
					'business_name'=>'El Pegaso Restaurante Bar', 
					'address'=>'Corregidora 6', 
					'email'=>'elpegasomx@gmail.com', 
					'phone'=>'154 7611',
					'phone2'=>'152 1351',  
					)); 

			Advertiser::create(array(
					'id'=>'146',
					'business_name'=>'El Petit Four',   
					'phone'=>'154 4010',  
					)); 

			Advertiser::create(array(
					'id'=>'147',
					'business_name'=>'El Progreso, El Arte Hecho Mueble',  
					'email'=>'hpmuebles@yahoo.com.mx', 
					'phone'=>'120 0788',  
					'web_page'=>'www.manuelpadron.com', 
					)); 

			Advertiser::create(array(
					'id'=>'148',
					'business_name'=>'El Quinto Sol',   
					'phone'=>'152 1608',  
					)); 

			Advertiser::create(array(
					'id'=>'149',
					'business_name'=>'El Sindicato Casa de las Artes Escénicas',   
					'phone'=>'152 0131',  
					)); 

			Advertiser::create(array(
					'id'=>'150',
					'business_name'=>'El Sol del Bajío',   
					'phone'=>'121 1743',  
					)); 

			Advertiser::create(array(
					'id'=>'151',
					'business_name'=>'El Ten ten pie',   
					'phone'=>'152 7189',  
					)); 

			Advertiser::create(array(
					'id'=>'152',
					'business_name'=>'El Tomato',   
					'phone'=>'154 6390',  
					)); 

			Advertiser::create(array(
					'id'=>'153',
					'business_name'=>'El Tumbagón (dulces típicos)',   
					'phone'=>'152 3883',  
					)); 

			Advertiser::create(array(
					'id'=>'154',
					'business_name'=>'El Volcán',   
					'phone'=>'152 1482',  
					)); 

			Advertiser::create(array(
					'id'=>'155',
					'business_name'=>'Enlaces Terrestres Nacionales (ETN)',   
					'phone'=>'152 6407',  
					)); 

			Advertiser::create(array(
					'id'=>'156',
					'business_name'=>'Enlaces Turísticos Integrados
',   
					'phone'=>'154 9797',  
					)); 

			Advertiser::create(array(
					'id'=>'157',
					'business_name'=>'Erik Zavala Photographer', 
					'address'=>'Ancha de San Antonio 20, Int. 19', 
					'email'=>'erikzavala@me.com', 
					'phone'=>'154 9161',
					'mobile_phone'=>'C 415 103 3313',  
					'web_page'=>'www.erikzavalaphoto.com', 
					)); 

			Advertiser::create(array(
					'id'=>'158',
					'business_name'=>'Escondido Place',   
					'phone'=>'185 2022',  
					)); 

			Advertiser::create(array(
					'id'=>'159',
					'business_name'=>'Especialidades Médicas San Miguel',   
					'phone'=>'152 4981',  
					)); 

			Advertiser::create(array(
					'id'=>'160',
					'business_name'=>'Estética Expertos Kerastase',   
					'phone'=>'154 7023',  
					)); 

			Advertiser::create(array(
					'id'=>'161',
					'business_name'=>'Esteto Clínica María Ofelia', 
					'address'=>'Zacateros 50, Centro',  
					'phone'=>'152 2935',  
					)); 

			Advertiser::create(array(
					'id'=>'162',
					'business_name'=>'Estrella Blanca',   
					'phone'=>'152 2237',  
					)); 

			Advertiser::create(array(
					'id'=>'163',
					'business_name'=>'Estudio Edward Swift',   
					'phone'=>'150 6480',  
					)); 

			Advertiser::create(array(
					'id'=>'164',
					'business_name'=>'EVOS',   
					'phone'=>'152 8097',  
					)); 

			Advertiser::create(array(
					'id'=>'165',
					'business_name'=>'Exportadora Sanfra',   
					'phone'=>'152 7205',  
					)); 

			Advertiser::create(array(
					'id'=>'166',
					'business_name'=>'Exter Plus',   
					'phone'=>'154 6191',  
					)); 

			Advertiser::create(array(
					'id'=>'167',
					'business_name'=>'Fábrica La Aurora (Centro de Arte y Diseño)', 
					'address'=>'Calzada de la Aurora s/n,  Aurora', 
					'email'=>'laaurora04@yahoo.com.mx', 
					'phone'=>'152 1312',
					'phone2'=>'152 1012',  
					'web_page'=>'www.fabricalaurora.com', 
					)); 

			Advertiser::create(array(
					'id'=>'168',
					'business_name'=>'Farmacia Dermatológica San Lucas', 
					'address'=>'Hernández Macias 85 B, Centro', 
					'email'=>'dermalucas@yahoo.com.mx', 
					'phone'=>'152 6076',
					'mobile_phone'=>'C 415 109 7898',  
					)); 

			Advertiser::create(array(
					'id'=>'169',
					'business_name'=>'Farmacia Guadalajara',   
					'phone'=>'154 9047',  
					)); 

			Advertiser::create(array(
					'id'=>'170',
					'business_name'=>'Farmacia Guanajuato',   
					'phone'=>'152 6090',  
					)); 

			Advertiser::create(array(
					'id'=>'171',
					'business_name'=>'Farmacia ISSEG',   
					'phone'=>'152 0938',  
					)); 

			Advertiser::create(array(
					'id'=>'172',
					'business_name'=>'Farmacia San Francisco de Asís',   
					'phone'=>'152 8142',  
					)); 

			Advertiser::create(array(
					'id'=>'173',
					'business_name'=>'Feed the Hungry',  
					'email'=>'contact@feedthehungrysma.org', 
					'phone'=>'152 2402',  
					'web_page'=>'www.feedthehungrysma.org', 
					)); 

			Advertiser::create(array(
					'id'=>'174',
					'business_name'=>'Ferre Plomería',   
					'phone'=>'152 0605',  
					)); 

			Advertiser::create(array(
					'id'=>'175',
					'business_name'=>'Fertilawn',   
					'phone'=>'152 8701',  
					'web_page'=>'www.fertilawn.com.mx', 
					)); 

			Advertiser::create(array(
					'id'=>'176',
					'business_name'=>'Flecha Amarilla / Primera Plus',   
					'phone'=>'152 0084',  
					)); 

			Advertiser::create(array(
					'id'=>'177',
					'business_name'=>'Flexi',   
					'phone'=>'154 9136',  
					)); 

			Advertiser::create(array(
					'id'=>'178',
					'business_name'=>'Florería Lupita', 
					'address'=>'Itzquinapan 3 Fracc. Itzquinapan', 
					'email'=>'florerialupitasma@hotmail.com', 
					'phone'=>'152 1635',
					'phone2'=>'120 37 30',
					'mobile_phone'=>'C 415 153 6091',  
					'mobile_phone2'=>'C 415 153 6545', 
					)); 

			Advertiser::create(array(
					'id'=>'179',
					'business_name'=>'Florería Mary',   
					'phone'=>'',
					'mobile_phone'=>'C 415 153 6633',  
					)); 

			Advertiser::create(array(
					'id'=>'180',
					'business_name'=>'Floriade',   
					'phone'=>'154 4881',  
					)); 

			Advertiser::create(array(
					'id'=>'181',
					'business_name'=>'Forever Jeans',   
					'phone'=>'110 3076',  
					)); 

			Advertiser::create(array(
					'id'=>'182',
					'business_name'=>'Foto Estudio San Carlos',   
					'phone'=>'152 6437',  
					)); 

			Advertiser::create(array(
					'id'=>'183',
					'business_name'=>'Frutas y Verduras Yenny',   
					'phone'=>'154 6010',  
					)); 

			Advertiser::create(array(
					'id'=>'184',
					'business_name'=>'Frutería Gil',   
					'phone'=>'154 9135',  
					)); 

			Advertiser::create(array(
					'id'=>'185',
					'business_name'=>'Frutería Sefe',   
					'phone'=>'154 8002',  
					)); 

			Advertiser::create(array(
					'id'=>'186',
					'business_name'=>'Galería Afalgar',   
					'phone'=>'152 6599',  
					)); 

			Advertiser::create(array(
					'id'=>'187',
					'business_name'=>'Galería Izamal',   
					'phone'=>'154 5409',  
					)); 

			Advertiser::create(array(
					'id'=>'188',
					'business_name'=>'Galería Mariposa',   
					'phone'=>'152 4488',  
					)); 

			Advertiser::create(array(
					'id'=>'189',
					'business_name'=>'Gas Express Nieto', 
					'address'=>'Carr. San Miguel-Los Rodríguez Km. 5.56', 
					'email'=>'pedidossma@gasexpressnieto.com.mx', 
					'phone'=>'152 7777',
					'phone2'=>'152 6691',
					'mobile_phone'=>'152 0228',  
					)); 

			Advertiser::create(array(
					'id'=>'190',
					'business_name'=>'Gas Providencia',   
					'phone'=>'',
					'mobile_phone'=>'(418) 182 6023',  
					)); 

			Advertiser::create(array(
					'id'=>'191',
					'business_name'=>'Gerencia Central de Autobuses',   
					'phone'=>'152 2206',  
					)); 

			Advertiser::create(array(
					'id'=>'192',
					'business_name'=>'Grupo Idea', 
					'address'=>'Avenida Ancha de San Antonio 20. Centro Int. 5',  
					'phone'=>'152 7072',  
					)); 

			Advertiser::create(array(
					'id'=>'193',
					'business_name'=>'Girasol Boutique',   
					'phone'=>'152 2734',  
					)); 

			Advertiser::create(array(
					'id'=>'194',
					'business_name'=>'González Sierra y Asociados',   
					'phone'=>'110 2230',  
					)); 

			Advertiser::create(array(
					'id'=>'195',
					'business_name'=>'Grúas En Renta',   
					'phone'=>'',
					'mobile_phone'=>'(464) 641 1869',  
					)); 

			Advertiser::create(array(
					'id'=>'196',
					'business_name'=>'Grúas Garita',   
					'phone'=>'152 2298',  
					)); 

			Advertiser::create(array(
					'id'=>'197',
					'business_name'=>'Grúas Mercadillo',   
					'phone'=>'150 0060',  
					)); 

			Advertiser::create(array(
					'id'=>'198',
					'business_name'=>'Grúas y Enlaces, S.A. de C.V.',   
					'phone'=>'152 0660',  
					)); 

			Advertiser::create(array(
					'id'=>'199',
					'business_name'=>'Grupo 24 horas',   
					'phone'=>'152 6522',  
					)); 

			Advertiser::create(array(
					'id'=>'200',
					'business_name'=>'Grupo Imagen',   
					'phone'=>'152 8000',  
					)); 

			Advertiser::create(array(
					'id'=>'201',
					'business_name'=>'Grupo PERC',   
					'phone'=>'154 8499',  
					)); 

			Advertiser::create(array(
					'id'=>'202',
					'business_name'=>'Grupo Vega',   
					'phone'=>'152 4458',  
					)); 

			Advertiser::create(array(
					'id'=>'203',
					'business_name'=>'Guajuye',   
					'phone'=>'152 7030',  
					)); 

			Advertiser::create(array(
					'id'=>'204',
					'business_name'=>'H2O Soluciones Integrales para Sistemas de Agua',   
					'phone'=>'01 800 830 9501',  
					)); 

			Advertiser::create(array(
					'id'=>'205',
					'business_name'=>'Hacienda de las Flores', 
					'address'=>'Hospicio 16, Centro', 
					'email'=>'info@haciendadelasflores.com', 
					'phone'=>'152 1888',
					'phone2'=>'152 1859',  
					'web_page'=>'www.haciendadelasflores.com', 
					)); 

			Advertiser::create(array(
					'id'=>'206',
					'business_name'=>'Hacienda Purísima de Jalpa',   
					'phone'=>'',
					'mobile_phone'=>'(442) 290 3122',  
					)); 

			Advertiser::create(array(
					'id'=>'207',
					'business_name'=>'Hagamos muebles',   
					'phone'=>'',
					'mobile_phone'=>'(442) 217 2521',  
					)); 

			Advertiser::create(array(
					'id'=>'208',
					'business_name'=>'Hecho en México',   
					'phone'=>'154 6383',  
					)); 

			Advertiser::create(array(
					'id'=>'209',
					'business_name'=>'Helados Santa Clara',   
					'phone'=>'152 3558',  
					)); 

			Advertiser::create(array(
					'id'=>'210',
					'business_name'=>'Herrería México',  
					'email'=>'crtrz@hotmail.com', 
					'phone'=>'154 5940',  
					)); 

			Advertiser::create(array(
					'id'=>'211',
					'business_name'=>'Herrería Rosas',   
					'phone'=>'152 0603',  
					)); 

			Advertiser::create(array(
					'id'=>'212',
					'business_name'=>'Herrería San Martín',   
					'phone'=>'152 5274',  
					)); 

			Advertiser::create(array(
					'id'=>'213',
					'business_name'=>'Home Destinations', 
					'address'=>'Garita 2, Centro', 
					'email'=>'marthahernandez08@gmail.com', 
					'email2'=>'info@homedestinations.com.mx', 
					'phone'=>'154 7662',
					'phone2'=>'152 0328',
					'mobile_phone'=>'C 415 105 6911',  
					'web_page'=>'www.homedestinations.com.mx', 
					)); 

			Advertiser::create(array(
					'id'=>'214',
					'business_name'=>'Honda San Miguel', 
					'address'=>'Salida a Celaya Km. 1, 97', 
					'email'=>'hondasanmiguel@prodigy.net.mx', 
					'phone'=>'152 1080',
					'phone2'=>'152 7100',  
					'web_page'=>'www.honda.com.mx/motos', 
					)); 

			Advertiser::create(array(
					'id'=>'215',
					'business_name'=>'Hospice San Miguel',   
					'phone'=>'152 6620',  
					)); 

			Advertiser::create(array(
					'id'=>'216',
					'business_name'=>'Hospital de C.A.S.A.',   
					'phone'=>'152 6181',  
					)); 

			Advertiser::create(array(
					'id'=>'217',
					'business_name'=>'Hotel Hacienda Taboada', 
					'address'=>'Carretera a Dolores Hidalgo, Desviación Km. 8 Rancho Taboada', 
					'email'=>'ventas@hotelhaciendataboada.com', 
					'phone'=>'152 9250',  
					'web_page'=>'www.taboada.com.mx', 
					)); 

			Advertiser::create(array(
					'id'=>'218',
					'business_name'=>'Hotel Real de Minas San Miguel de Allende',  
					'email'=>'info@realdeminas.com', 
					'email2'=>'ventas@realdeminas.com', 
					'phone'=>'01 800 466 5800',  
					'web_page'=>'www.realdeminas.com', 
					)); 

			Advertiser::create(array(
					'id'=>'219',
					'business_name'=>'Hotel Imperio de Ángeles',   
					'phone'=>'152 9300',  
					'web_page'=>'www.imperiodeangeles.com.mx', 
					)); 

			Advertiser::create(array(
					'id'=>'220',
					'business_name'=>'Hotel la Mansión del Bosque.',   
					'phone'=>'152 0277',  
					)); 

			Advertiser::create(array(
					'id'=>'221',
					'business_name'=>'Hotel Posada Carmina',   
					'phone'=>'152 8888',  
					)); 

			Advertiser::create(array(
					'id'=>'222',
					'business_name'=>'Vista Hermosa Taboada', 
					'address'=>'Cuna de Allende 11, Centro', 
					'email'=>'ventas@taboada.com.mx', 
					'phone'=>'152 0078',  
					'web_page'=>'www.taboada.com.mx', 
					)); 

			Advertiser::create(array(
					'id'=>'223',
					'business_name'=>'Hoyo Trece  Residencial',  
					'email'=>'info@hoyotrece.com', 
					'phone'=>'152 0945',  
					'web_page'=>'www.hoyotrece.com', 
					)); 

			Advertiser::create(array(
					'id'=>'224',
					'business_name'=>'HSBC',   
					'phone'=>'152 6299',  
					)); 

			Advertiser::create(array(
					'id'=>'225',
					'business_name'=>'Idea Grupo Constructor',   
					'phone'=>'152 6639',  
					)); 

			Advertiser::create(array(
					'id'=>'226',
					'business_name'=>'Ilumina San Miguel',   
					'phone'=>'154 7643',  
					)); 

			Advertiser::create(array(
					'id'=>'227',
					'business_name'=>'Imperquimia San Miguel',   
					'phone'=>'154 4303',  
					'web_page'=>'imperquimia_sanmiguel@hotmail.com', 
					)); 

			Advertiser::create(array(
					'id'=>'228',
					'business_name'=>'Imprenta Camar',   
					'phone'=>'152 2367',  
					)); 

			Advertiser::create(array(
					'id'=>'229',
					'business_name'=>'Inbursa',   
					'phone'=>'110 3127',  
					)); 

			Advertiser::create(array(
					'id'=>'230',
					'business_name'=>'Inhumaciones López', 
					'address'=>'Mesones 45, Centro', 
					'email'=>'inhumaciones.lopez@gmail.com', 
					'email2'=>'inhumacioneslopez@hotmail.com', 
					'phone'=>'152 0208',  
					)); 

			Advertiser::create(array(
					'id'=>'231',
					'business_name'=>'Insh‘ala',   
					'phone'=>'152 8355',  
					'web_page'=>'www.inshalaimports.com', 
					)); 

			Advertiser::create(array(
					'id'=>'232',
					'business_name'=>'Inst. Ma. Del Refugio Aguilar',   
					'phone'=>'154 8018',  
					)); 

			Advertiser::create(array(
					'id'=>'233',
					'business_name'=>'Instituto Bilingüe Milenio',   
					'phone'=>'154 6460',  
					)); 

			Advertiser::create(array(
					'id'=>'234',
					'business_name'=>'Instituto Habla Hispana',   
					'phone'=>'152 0713',  
					)); 

			Advertiser::create(array(
					'id'=>'235',
					'business_name'=>'Instituto Latinoamericano Bilingüe',   
					'phone'=>'152 1748',  
					)); 

			Advertiser::create(array(
					'id'=>'236',
					'business_name'=>'Interceramic-Simply the best',   
					'phone'=>'154 6420',  
					'web_page'=>'www.interceramic.com', 
					)); 

			Advertiser::create(array(
					'id'=>'237',
					'business_name'=>'Interini',   
					'phone'=>'152 3780',  
					)); 

			Advertiser::create(array(
					'id'=>'238',
					'business_name'=>'Janis McDonald‘s Private Gym',   
					'phone'=>'152 0457',  
					)); 

			Advertiser::create(array(
					'id'=>'239',
					'business_name'=>'Jardines Nueva Vida', 
					'address'=>'Libramiento Manuel Zavala 100, San Antonio', 
					'email'=>'jardinesnuevavida@yahoo.com', 
					'phone'=>'152 6648',
					'phone2'=>'154 7166',
					'mobile_phone'=>'C 415 153 3810',  
					)); 

			Advertiser::create(array(
					'id'=>'240',
					'business_name'=>'Jasmine',   
					'phone'=>'152 7973',  
					)); 

			Advertiser::create(array(
					'id'=>'241',
					'business_name'=>'José Luis Rodríguez',   
					'phone'=>'152 3134',  
					)); 

			Advertiser::create(array(
					'id'=>'242',
					'business_name'=>'José Vasconcelos',   
					'phone'=>'152 1869',  
					)); 

			Advertiser::create(array(
					'id'=>'243',
					'business_name'=>'Juan Ezcurdia Studio Gallery',   
					'phone'=>'154 8286',  
					)); 

			Advertiser::create(array(
					'id'=>'244',
					'business_name'=>'Julián Cartas',   
					'phone'=>'152 0079',  
					)); 

			Advertiser::create(array(
					'id'=>'245',
					'business_name'=>'La Aurora, Oxígeno Médico',   
					'phone'=>'152 2414',  
					)); 

			Advertiser::create(array(
					'id'=>'246',
					'business_name'=>'La Calaca',   
					'phone'=>'152 3954',  
					)); 

			Advertiser::create(array(
					'id'=>'247',
					'business_name'=>'La Casita Feliz', 
					'address'=>'Guadiana 15 B Fracc. Guadiana', 
					'email'=>'lacasitafelizpasteleria@gmail.com', 
					'phone'=>'152 6555',  
					)); 

			Advertiser::create(array(
					'id'=>'248',
					'business_name'=>'La Cocina',   
					'phone'=>'152 5807',  
					)); 

			Advertiser::create(array(
					'id'=>'249',
					'business_name'=>'La Colmena',   
					'phone'=>'152 1422',  
					)); 

			Advertiser::create(array(
					'id'=>'250',
					'business_name'=>'La Conexión', 
					'address'=>'Aldama 3, Centro', 
					'email'=>'laconex2001@yahoo.com', 
					'phone'=>'152 1599',
					'phone2'=>'F 152 1687',  
					'web_page'=>'www.laconexion.com.mx', 
					)); 

			Advertiser::create(array(
					'id'=>'251',
					'business_name'=>'La Deriva Librería',   
					'phone'=>'154 5076',  
					)); 

			Advertiser::create(array(
					'id'=>'252',
					'business_name'=>'La Divina',   
					'phone'=>'154 4190',  
					)); 

			Advertiser::create(array(
					'id'=>'253',
					'business_name'=>'La Esmeralda',   
					'phone'=>'01 800 110 7369',  
					)); 

			Advertiser::create(array(
					'id'=>'254',
					'business_name'=>'La Espiga',   
					'phone'=>'154 5177',  
					)); 

			Advertiser::create(array(
					'id'=>'255',
					'business_name'=>'La Europea',   
					'phone'=>'152 2003',  
					)); 

			Advertiser::create(array(
					'id'=>'256',
					'business_name'=>'La gruta',   
					'phone'=>'185 2162',  
					)); 

			Advertiser::create(array(
					'id'=>'257',
					'business_name'=>'La Mesa Grande, Panadería + Café',   
					'phone'=>'154 0838',  
					'web_page'=>'www.lamesagrande.com', 
					)); 

			Advertiser::create(array(
					'id'=>'258',
					'business_name'=>'La Parisina',   
					'phone'=>'110 3115',  
					)); 

			Advertiser::create(array(
					'id'=>'259',
					'business_name'=>'La Sirena Gorda',   
					'phone'=>'110 0007',  
					)); 

			Advertiser::create(array(
					'id'=>'260',
					'business_name'=>'La Victoriana', 
					'address'=>'Hernández Macías 72, Int. 1.', 
					'email'=>'lavictoriana07@gmail.com', 
					'phone'=>'152 6903',
					'phone2'=>'152 7003',
					'mobile_phone'=>'01 800 465 800',  
					'web_page'=>'www.lavictoriana.com', 
					)); 

			Advertiser::create(array(
					'id'=>'261',
					'business_name'=>'La Virundela',   
					'phone'=>'152 4400',  
					)); 

			Advertiser::create(array(
					'id'=>'262',
					'business_name'=>'Laboratorio Biomédica',   
					'phone'=>'152 5217',  
					)); 

			Advertiser::create(array(
					'id'=>'263',
					'business_name'=>'Lagundi', 
					'address'=>'Umaran 17', 
					'email'=>'lagunsm@hotmail.com', 
					'phone'=>'152 0830',  
					)); 

			Advertiser::create(array(
					'id'=>'264',
					'business_name'=>'Laja Spa',   
					'phone'=>'152 7040',  
					)); 

			Advertiser::create(array(
					'id'=>'265',
					'business_name'=>'Cerrajería Lara',   
					'phone'=>'152 2686',  
					)); 

			Advertiser::create(array(
					'id'=>'266',
					'business_name'=>'Instituto Fray Bartolomé de Las Casas',   
					'phone'=>'152 1374',  
					)); 

			Advertiser::create(array(
					'id'=>'267',
					'business_name'=>'Lavinia‘s',   
					'phone'=>'154 5344',  
					)); 

			Advertiser::create(array(
					'id'=>'268',
					'business_name'=>'Libertad Servicios Financieros',   
					'phone'=>'152 3904',  
					)); 

			Advertiser::create(array(
					'id'=>'269',
					'business_name'=>'Lic. César Muñiz Rodríguez', 
					'address'=>'Av. Allende 6 San Antonio', 
					'email'=>'bjmuniz@hotmail.com', 
					'phone'=>'152 0985',
					'mobile_phone'=>'C 415 101 0150',  
					)); 

			Advertiser::create(array(
					'id'=>'270',
					'business_name'=>'LifePath Center',   
					'phone'=>'154 8465',  
					)); 

			Advertiser::create(array(
					'id'=>'271',
					'business_name'=>'Lisa Attridge',   
					'phone'=>'C 415 109 6972',  
					)); 

			Advertiser::create(array(
					'id'=>'272',
					'business_name'=>'Liverpool San Miguel',   
					'phone'=>'150 3000',  
					)); 

			Advertiser::create(array(
					'id'=>'273',
					'business_name'=>'London Style',   
					'phone'=>'120 4888',  
					)); 

			Advertiser::create(array(
					'id'=>'274',
					'business_name'=>'Los Rehiletes',   
					'phone'=>'152 1307',  
					)); 

			Advertiser::create(array(
					'id'=>'275',
					'business_name'=>'Los Senderos',  
					'email'=>'info@los-senderos.com', 
					'phone'=>'152 8411',  
					'web_page'=>'www.los-senderos.com', 
					)); 

			Advertiser::create(array(
					'id'=>'276',
					'business_name'=>'Pro 1one Lubricantes del Norte', 
					'address'=>'Libramiento Manuel Zavala Zavala Km. 1.9', 
					'email'=>'lubdelnorte@hotmail.com', 
					'phone'=>'152 2587',
					'phone2'=>'185 8419',
					'mobile_phone'=>'152 8891',  
					)); 

			Advertiser::create(array(
					'id'=>'277',
					'business_name'=>'Luna de Queso', 
					'address'=>'Salida a Celaya 51', 
					'email'=>'maper2000@yahoo.com', 
					'phone'=>'154 8122',  
					)); 

			Advertiser::create(array(
					'id'=>'278',
					'business_name'=>'Maderería Flores',   
					'phone'=>'152 1148',  
					)); 

			Advertiser::create(array(
					'id'=>'279',
					'business_name'=>'Maderería La Glorieta',   
					'phone'=>'120 3700',  
					)); 

			Advertiser::create(array(
					'id'=>'280',
					'business_name'=>'Manuel Suaste',   
					'phone'=>'152 0005',  
					)); 

			Advertiser::create(array(
					'id'=>'281',
					'business_name'=>'Vidrieria La Muñeca', 
					'address'=>'Farolito 30, Guadalupe', 
					'email'=>'vimusa@vidrieria.com.mx', 
					'email2'=>'atencion@vidrieria.com.mx', 
					'phone'=>'154 4980',
					'phone2'=>'154 4990',
					'mobile_phone'=>'120 4210',  
					'web_page'=>'www.vidrieria.com.mx', 
					)); 

			Advertiser::create(array(
					'id'=>'282',
					'business_name'=>'Marabu',   
					'phone'=>'152 1300',  
					)); 

			Advertiser::create(array(
					'id'=>'283',
					'business_name'=>'Maraye Materiales para empaque',   
					'phone'=>'152 6199',  
					)); 

			Advertiser::create(array(
					'id'=>'284',
					'business_name'=>'Mary Rapp',   
					'phone'=>'154 9259',  
					)); 

			Advertiser::create(array(
					'id'=>'285',
					'business_name'=>'Materiales para la Construcción Talego Hernández',   
					'phone'=>'120 2082',  
					)); 

			Advertiser::create(array(
					'id'=>'286',
					'business_name'=>'Matilda',   
					'phone'=>'152 1015',  
					)); 

			Advertiser::create(array(
					'id'=>'287',
					'business_name'=>'MAXEh Concreto Ecológico Terracreto',   
					'phone'=>'152 2541',  
					)); 

			Advertiser::create(array(
					'id'=>'288',
					'business_name'=>'Mc Donald‘s',   
					'phone'=>'110 3012',  
					)); 

			Advertiser::create(array(
					'id'=>'289',
					'business_name'=>'Meditation Center of San Miguel',   
					'phone'=>'152 0536',  
					)); 

			Advertiser::create(array(
					'id'=>'290',
					'business_name'=>'Melanie Nance',   
					'phone'=>'154 9230',  
					)); 

			Advertiser::create(array(
					'id'=>'291',
					'business_name'=>'Mexart',   
					'phone'=>'152 8900',  
					)); 

			Advertiser::create(array(
					'id'=>'292',
					'business_name'=>'Mint',   
					'phone'=>'152 1958',  
					)); 

			Advertiser::create(array(
					'id'=>'293',
					'business_name'=>'Mixta', 
					'address'=>'Pila Seca 3, Centro', 
					'email'=>'info@mixtasanmiguel.com', 
					'phone'=>'152 7343',  
					'web_page'=>'www.mixtasanmiguel.com', 
					)); 

			Advertiser::create(array(
					'id'=>'294',
					'business_name'=>'Cinemex',   
					'phone'=>'01 800 710 8888',  
					)); 

			Advertiser::create(array(
					'id'=>'295',
					'business_name'=>'Monex Grupo Financiero',   
					'phone'=>'154 9996',  
					)); 

			Advertiser::create(array(
					'id'=>'296',
					'business_name'=>'Motopartes, Refaccionaria y Taller',   
					'phone'=>'154 5127',  
					)); 

			Advertiser::create(array(
					'id'=>'297',
					'business_name'=>'Museo Casa de Allende',   
					'phone'=>'152 2499',  
					)); 

			Advertiser::create(array(
					'id'=>'298',
					'business_name'=>'My Rental in San Miguel',  
					'email'=>'rentalsinsanmiguel@gmail.com', 
					'phone'=>'152 6752',
					'phone2'=>'US 956 205 0433',  
					)); 

			Advertiser::create(array(
					'id'=>'299',
					'business_name'=>'Namuh',   
					'phone'=>'154 8080',  
					)); 

			Advertiser::create(array(
					'id'=>'300',
					'business_name'=>'Nirvana Restaurant & Retreat', 
					'address'=>'Antigua vía del ferrocarril 21 Casa 2, Cortijo', 
					'email'=>'ventas@hotelnirvana.mx', 
					'phone'=>'185 2194',
					'phone2'=>'185 2195',  
					'web_page'=>'www.hotelnirvana.mx', 
					)); 

			Advertiser::create(array(
					'id'=>'301',
					'business_name'=>'Noel',   
					'phone'=>'155 8066',  
					)); 

			Advertiser::create(array(
					'id'=>'302',
					'business_name'=>'Notaría 1',   
					'phone'=>'152 0753',  
					)); 

			Advertiser::create(array(
					'id'=>'303',
					'business_name'=>'Notaría 10',   
					'phone'=>'152 0801',  
					)); 

			Advertiser::create(array(
					'id'=>'304',
					'business_name'=>'Notaría 4',   
					'phone'=>'152 3298',  
					)); 

			Advertiser::create(array(
					'id'=>'305',
					'business_name'=>'Notaría 12',   
					'phone'=>'152 1242',  
					)); 

			Advertiser::create(array(
					'id'=>'306',
					'business_name'=>'Notaría 14',   
					'phone'=>'152 6333',  
					)); 

			Advertiser::create(array(
					'id'=>'307',
					'business_name'=>'Notaría 2',   
					'phone'=>'152 2416',  
					)); 

			Advertiser::create(array(
					'id'=>'308',
					'business_name'=>'Notaría Publica 3', 
					'address'=>'Canal 134, Centro', 
					'email'=>'abrilrubio@yahoo.com', 
					'email2'=>'rubionot@hotmail.com', 
					'phone'=>'152 0543',
					'phone2'=>'154 7296',  
					)); 

			Advertiser::create(array(
					'id'=>'309',
					'business_name'=>'Notaría 11', 
					'address'=>'Umaran 18, Centro', 
					'email'=>'notaria11.mmartinez@gmail.com', 
					'phone'=>'152 0348',  
					)); 

			Advertiser::create(array(
					'id'=>'310',
					'business_name'=>'Notaría 5',   
					'phone'=>'152 0301',  
					)); 

			Advertiser::create(array(
					'id'=>'311',
					'business_name'=>'Notaría 6',   
					'phone'=>'152 0587',  
					)); 

			Advertiser::create(array(
					'id'=>'312',
					'business_name'=>'Notaría 7',   
					'phone'=>'152 1451',  
					)); 

			Advertiser::create(array(
					'id'=>'313',
					'business_name'=>'Notaría 8',   
					'phone'=>'152 0801',  
					)); 

			Advertiser::create(array(
					'id'=>'314',
					'business_name'=>'Office Depot',   
					'phone'=>'120 4749',  
					)); 

			Advertiser::create(array(
					'id'=>'315',
					'business_name'=>'Óptica Víctor Hugo Medina Villa',   
					'phone'=>'152 6151',  
					)); 

			Advertiser::create(array(
					'id'=>'316',
					'business_name'=>'Opticare',   
					'phone'=>'154 7575',  
					)); 

			Advertiser::create(array(
					'id'=>'317',
					'business_name'=>'Origen',   
					'phone'=>'154 6224',  
					)); 

			Advertiser::create(array(
					'id'=>'318',
					'business_name'=>'Oxígeno Medicinal del Bajío',   
					'phone'=>'01 800 702 2663',  
					)); 

			Advertiser::create(array(
					'id'=>'319',
					'business_name'=>'Pahusa',   
					'phone'=>'152 0240',  
					)); 

			Advertiser::create(array(
					'id'=>'320',
					'business_name'=>'Panadería La Buena Vida', 
					'address'=>'Hernández Macías 72, Int. 5', 
					'email'=>'info@panlabuenavida.com', 
					'phone'=>'152 2211',  
					)); 

			Advertiser::create(array(
					'id'=>'321',
					'business_name'=>'Papelería D‘Sol',   
					'phone'=>'152 2948',  
					)); 

			Advertiser::create(array(
					'id'=>'322',
					'business_name'=>'Parabrisas Autovidrios Chávez',   
					'phone'=>'150 7075',  
					)); 

			Advertiser::create(array(
					'id'=>'323',
					'business_name'=>'Pastelería San Sebastián',   
					'phone'=>'152 3976',  
					)); 

			Advertiser::create(array(
					'id'=>'324',
					'business_name'=>'Patronato Pro Niños . Historic Walking Tour', 
					'address'=>'Av. Reforma 75 Fracc. Ignacio Ramírez', 
					'email'=>'info@patronatoproninos.org', 
					'phone'=>'152 7796',  
					'web_page'=>'www.patronatoproninos.org', 
					)); 

			Advertiser::create(array(
					'id'=>'325',
					'business_name'=>'Periódico Correo',   
					'phone'=>'152 6853',  
					)); 

			Advertiser::create(array(
					'id'=>'326',
					'business_name'=>'Pet Hotel San Miguel',   
					'phone'=>'121 0891',  
					'web_page'=>'www.pethotelsanmiguel.com', 
					)); 

			Advertiser::create(array(
					'id'=>'327',
					'business_name'=>'Pet-Vet', 
					'address'=>'Stirling Dickinson 27 San Antonio',  
					'phone'=>'152 4535',
					'phone2'=>'C 415 149 2675',
					'mobile_phone'=>'C 415 109 2468',  
					'mobile_phone2'=>'152 4276', 
					'nextel'=>'ID 52*304846*2',  
					)); 

			Advertiser::create(array(
					'id'=>'328',
					'business_name'=>'Pilkington',   
					'phone'=>'152 4113',  
					)); 

			Advertiser::create(array(
					'id'=>'329',
					'business_name'=>'Pinturas Doal', 
					'address'=>'Salida a Celaya 26, Int. 1, Calzada la Estación 10', 
					'email'=>'pinturasdoalsma@gmail.com', 
					'email2'=>'pinturasdoalsma@gmail.com', 
					'phone'=>'154 7599',
					'phone2'=>'185 8054',
					'mobile_phone'=>'C 415 101 0164',  
					)); 

			Advertiser::create(array(
					'id'=>'330',
					'business_name'=>'Piscinas San Miguel', 
					'address'=>'Salida a Queretaro 177', 
					'email'=>'sanmiguelpools@yahoo.com.mx', 
					'email2'=>'picinas.sanmiguel@yahoo.com', 
					'phone'=>'154 4508',
					'mobile_phone'=>'C 415 153 3581',  
					'web_page'=>'www.picinassanmiguel.com', 
					)); 

			Advertiser::create(array(
					'id'=>'331',
					'business_name'=>'Plaza 4 Quince',   
					'phone'=>'152 1640',  
					)); 

			Advertiser::create(array(
					'id'=>'332',
					'business_name'=>'Plaza La Luciérnaga',  
					'email'=>'mercadotecnia@plazalaLuciérnaga.com.mx', 
					'phone'=>'120 4727',  
					'web_page'=>'www.plazalaLuciérnaga.com.mx', 
					)); 

			Advertiser::create(array(
					'id'=>'333',
					'business_name'=>'Rosticería D‘ Daniel', 
					'address'=>'Av. Guadalupe 16',  
					'phone'=>'',
					'phone2'=>'C 415 113 3059',
					'mobile_phone'=>'C 415 100 0002',  
					)); 

			Advertiser::create(array(
					'id'=>'334',
					'business_name'=>'Pollo Feliz',   
					'phone'=>'154 5762',  
					)); 

			Advertiser::create(array(
					'id'=>'335',
					'business_name'=>'Posada Corazón B&B',   
					'phone'=>'152 0182',  
					)); 

			Advertiser::create(array(
					'id'=>'336',
					'business_name'=>'Prenda Plus',   
					'phone'=>'152 0156',  
					)); 

			Advertiser::create(array(
					'id'=>'337',
					'business_name'=>'Presto Lana',   
					'phone'=>'154 8890',  
					)); 

			Advertiser::create(array(
					'id'=>'338',
					'business_name'=>'Productos Remo',   
					'phone'=>'152 0453',  
					)); 

			Advertiser::create(array(
					'id'=>'339',
					'business_name'=>'Prolimp del Centro',   
					'phone'=>'',
					'mobile_phone'=>'(442) 220 8035',  
					)); 

			Advertiser::create(array(
					'id'=>'340',
					'business_name'=>'PROMAC',   
					'phone'=>'152 5201',  
					)); 

			Advertiser::create(array(
					'id'=>'341',
					'business_name'=>'Pronto Pizza',   
					'phone'=>'152 4066',  
					)); 

			Advertiser::create(array(
					'id'=>'342',
					'business_name'=>'Pueblo Viejo',   
					'phone'=>'152 4977',  
					)); 

			Advertiser::create(array(
					'id'=>'343',
					'business_name'=>'Qué Torta',   
					'phone'=>'154 5880',  
					)); 

			Advertiser::create(array(
					'id'=>'344',
					'business_name'=>'Rachelle Schaft',   
					'phone'=>'120 0802',  
					)); 

			Advertiser::create(array(
					'id'=>'345',
					'business_name'=>'Radio San Miguel S.A.',   
					'phone'=>'152 0227',  
					'web_page'=>'www.sqsanmiguel.com', 
					)); 

			Advertiser::create(array(
					'id'=>'346',
					'business_name'=>'Radio Taxi San Miguelito',   
					'phone'=>'152 0395',  
					'web_page'=>'www.rtsanmiguelito.com.mx', 
					)); 

			Advertiser::create(array(
					'id'=>'347',
					'business_name'=>'Rancho La Trinidad',   
					'phone'=>'',
					'mobile_phone'=>'C 415 111 7894',  
					)); 

			Advertiser::create(array(
					'id'=>'348',
					'business_name'=>'Real Estate San Miguel', 
					'address'=>'Cuadrante Esq. Aldama', 
					'email'=>'info@sanmiguel-mx.com', 
					'phone'=>'152 2284',
					'phone2'=>'152 7377',  
					'web_page'=>'www.sanmiguel-mx.com', 
					)); 

			Advertiser::create(array(
					'id'=>'349',
					'business_name'=>'Recreo',   
					'phone'=>'154 4820',  
					)); 

			Advertiser::create(array(
					'id'=>'350',
					'business_name'=>'Chamonix', 
					'address'=>'Sollano 17, Centro',  
					'phone'=>'154 8363',  
					)); 

			Advertiser::create(array(
					'id'=>'351',
					'business_name'=>'Rosewood',   
					'phone'=>'152 9700',  
					)); 

			Advertiser::create(array(
					'id'=>'352',
					'business_name'=>'Salón de eventos Faraón',   
					'phone'=>'185 2109',  
					)); 

			Advertiser::create(array(
					'id'=>'353',
					'business_name'=>'Salvador Patlán',   
					'phone'=>'152 7915',  
					)); 

			Advertiser::create(array(
					'id'=>'354',
					'business_name'=>'San Miguel Audio',   
					'phone'=>'152 7759',  
					)); 

			Advertiser::create(array(
					'id'=>'355',
					'business_name'=>'San Miguel Patio Decor', 
					'address'=>'Calzada La Aurora Local 3A-1',  
					'email2'=>'info@sanmiguelpatiodecor.com.mx', 
					'phone'=>'150 7392',  
					'web_page'=>'www.sanmiguelpatiodecor.com.mx', 
					)); 

			Advertiser::create(array(
					'id'=>'356',
					'business_name'=>'San Miguel Weddings',   
					'phone'=>'154 8121',  
					)); 

			Advertiser::create(array(
					'id'=>'357',
					'business_name'=>'Sanatorio de Nuestra Señora de la Salud',   
					'phone'=>'152 0430',  
					)); 

			Advertiser::create(array(
					'id'=>'358',
					'business_name'=>'Santa Teresita Botica',   
					'phone'=>'152 0147',  
					)); 

			Advertiser::create(array(
					'id'=>'359',
					'business_name'=>'Santander',   
					'phone'=>'152 2334',  
					)); 

			Advertiser::create(array(
					'id'=>'360',
					'business_name'=>'Club Deportivo Santo Domingo',   
					'phone'=>'154 7545',  
					)); 

			Advertiser::create(array(
					'id'=>'361',
					'business_name'=>'Santuario Hogar Guadalupano',   
					'phone'=>'152 5082',  
					)); 

			Advertiser::create(array(
					'id'=>'362',
					'business_name'=>'Satellite TV México',   
					'phone'=>'120 0200',  
					)); 

			Advertiser::create(array(
					'id'=>'363',
					'business_name'=>'Sazón',   
					'phone'=>'154 7671',  
					)); 

			Advertiser::create(array(
					'id'=>'364',
					'business_name'=>'Scotiabank',   
					'phone'=>'152 9120',  
					)); 

			Advertiser::create(array(
					'id'=>'365',
					'business_name'=>'Servicio Postal Mexicano',   
					'phone'=>'152 0089',  
					)); 

			Advertiser::create(array(
					'id'=>'366',
					'business_name'=>'Siatel Puertas Eléctricas',  
					'email'=>'puertaselect_carlossma@hotmail.com', 
					'phone'=>'122 0759',
					'phone2'=>'152 7266',
					'mobile_phone'=>'C 415 153 9264',  
					'nextel'=>'ID 52*15*9835',  
					)); 

			Advertiser::create(array(
					'id'=>'367',
					'business_name'=>'Sicilia in Bocca', 
					'address'=>'Salida Real a Querétaro 91', 
					'email'=>'sicilia_in_bocca@hotmail.com', 
					'email2'=>'sicilia_in_bocca@hotmail.com', 
					'phone'=>'152 0406',
					'mobile_phone'=>'C 415 100 7261',  
					)); 

			Advertiser::create(array(
					'id'=>'368',
					'business_name'=>'Sid Abraham',   
					'phone'=>'152 4193',  
					)); 

			Advertiser::create(array(
					'id'=>'369',
					'business_name'=>'Sierra Nevada',   
					'phone'=>'152 7040',  
					)); 

			Advertiser::create(array(
					'id'=>'370',
					'business_name'=>'Sisal',   
					'phone'=>'154 6323',  
					)); 

			Advertiser::create(array(
					'id'=>'371',
					'business_name'=>'Smart Tennis Academy',   
					'phone'=>'',
					'mobile_phone'=>'C 415 113 4019',  
					)); 

			Advertiser::create(array(
					'id'=>'372',
					'business_name'=>'Sociedad Protectora de Animales',   
					'phone'=>'152 6124',  
					)); 

			Advertiser::create(array(
					'id'=>'373',
					'business_name'=>'Solar San Miguel',   
					'phone'=>'154 6397',  
					)); 

			Advertiser::create(array(
					'id'=>'374',
					'business_name'=>'Solar Solutions',   
					'phone'=>'154 4915',  
					)); 

			Advertiser::create(array(
					'id'=>'375',
					'business_name'=>'SoniGas', 
					'address'=>'Camino a la Venta s/n El Salitre, Dolores', 
					'email'=>'dolores.operacion@sonigas.com.mx', 
					'phone'=>'418 182 6360',
					'phone2'=>'415 154 5055',
					'mobile_phone'=>'C 418 187 5134',  
					'mobile_phone2'=>'01800 503 8687', 
					'web_page'=>'www.gruposoni.com.mx', 
					)); 

			Advertiser::create(array(
					'id'=>'376',
					'business_name'=>'Soriana',   
					'phone'=>'120 4711',  
					)); 

			Advertiser::create(array(
					'id'=>'377',
					'business_name'=>'Spa Matilda',   
					'phone'=>'152 1015',  
					)); 

			Advertiser::create(array(
					'id'=>'378',
					'business_name'=>'St. Michael‘s Canine Center',   
					'phone'=>'154 4182',  
					)); 

			Advertiser::create(array(
					'id'=>'379',
					'business_name'=>'Starbucks',   
					'phone'=>'154 9895',  
					)); 

			Advertiser::create(array(
					'id'=>'380',
					'business_name'=>'Studio Pilates',   
					'phone'=>'152 4169',  
					)); 

			Advertiser::create(array(
					'id'=>'381',
					'business_name'=>'Sua Jewerly',   
					'phone'=>'152 1716',  
					)); 

			Advertiser::create(array(
					'id'=>'382',
					'business_name'=>'Tacos Don Félix',   
					'phone'=>'152 5719',  
					)); 

			Advertiser::create(array(
					'id'=>'383',
					'business_name'=>'Auto Cristales en San Miguel', 
					'address'=>'Carretera San Miguel a Celaya Km 1, Allende', 
					'email'=>'tallermendez_10@hotmail.com', 
					'phone'=>'152 4113',  
					)); 

			Advertiser::create(array(
					'id'=>'384',
					'business_name'=>'Tapicentro San Miguel',   
					'phone'=>'152 5324',  
					)); 

			Advertiser::create(array(
					'id'=>'385',
					'business_name'=>'Tapicería Jiménez',   
					'phone'=>'152 3876',  
					)); 

			Advertiser::create(array(
					'id'=>'386',
					'business_name'=>'Tato & Lore',   
					'phone'=>'154 7063',  
					)); 

			Advertiser::create(array(
					'id'=>'387',
					'business_name'=>'Taxitel‘s',   
					'phone'=>'154 0737',  
					)); 

			Advertiser::create(array(
					'id'=>'388',
					'business_name'=>'Teatro Ángela Peralta',   
					'phone'=>'152 2200',  
					)); 

			Advertiser::create(array(
					'id'=>'389',
					'business_name'=>'Tele vega',   
					'phone'=>'152 0719',  
					)); 

			Advertiser::create(array(
					'id'=>'390',
					'business_name'=>'Telmex',   
					'phone'=>'152 5222',  
					)); 

			Advertiser::create(array(
					'id'=>'391',
					'business_name'=>'Tennis San Miguel',   
					'phone'=>'152 1905',  
					)); 

			Advertiser::create(array(
					'id'=>'392',
					'business_name'=>'Terra', 
					'address'=>'Hospicio 37', 
					'email'=>'terra1@alfonsoalarcon.com', 
					'phone'=>'152 7754',  
					'web_page'=>'www.terralandscapedesing.com', 
					)); 

			Advertiser::create(array(
					'id'=>'393',
					'business_name'=>'Tesoros',   
					'phone'=>'154 4838',  
					)); 

			Advertiser::create(array(
					'id'=>'394',
					'business_name'=>'Teya Flaster',   
					'phone'=>'152 1776',  
					)); 

			Advertiser::create(array(
					'id'=>'395',
					'business_name'=>'The Nail Lounge Salon & Spa', 
					'address'=>'Plaza la Luciernaga, Locales 14, 15, y 16', 
					'email'=>'the.nail.lounge@outlook.com', 
					'phone'=>'154 4321',  
					)); 

			Advertiser::create(array(
					'id'=>'396',
					'business_name'=>'The Spa',   
					'phone'=>'152 1302',  
					)); 

			Advertiser::create(array(
					'id'=>'397',
					'business_name'=>'Tintorería Franco',   
					'phone'=>'152 4362',  
					)); 

			Advertiser::create(array(
					'id'=>'398',
					'business_name'=>'Tintorería y Lavanderia La Pila', 
					'address'=>'Jesus 25, Centro', 
					'email'=>'tintorerialapila@hotmail.com', 
					'phone'=>'152 5810',  
					)); 

			Advertiser::create(array(
					'id'=>'399',
					'business_name'=>'Tío Lucas Restaurant Bar', 
					'address'=>'Mesones 103, Centro', 
					'email'=>'tiolucassma@hotmail.com', 
					'phone'=>'152 4996',  
					'web_page'=>'www.portalsanmiguel.com', 
					)); 

			Advertiser::create(array(
					'id'=>'400',
					'business_name'=>'Tortitlán',   
					'phone'=>'152 3376',  
					)); 

			Advertiser::create(array(
					'id'=>'401',
					'business_name'=>'Transportadora Turística Imperial', 
					'address'=>'San Francisco 1 Altos Int. 1, Centro', 
					'email'=>'transtur.imperial@gmail.com', 
					'phone'=>'154 5408',  
					'web_page'=>'www.operatur-imperial.com', 
					)); 

			Advertiser::create(array(
					'id'=>'402',
					'business_name'=>'Tres Muchachos',   
					'phone'=>'154 8940',  
					)); 

			Advertiser::create(array(
					'id'=>'403',
					'business_name'=>'Tucán',   
					'phone'=>'152 5917',  
					)); 

			Advertiser::create(array(
					'id'=>'404',
					'business_name'=>'Vía Orgánica',   
					'phone'=>'152 8042',  
					)); 

			Advertiser::create(array(
					'id'=>'405',
					'business_name'=>'Viajes San Miguel',   
					'phone'=>'152 2537',  
					)); 

			Advertiser::create(array(
					'id'=>'406',
					'business_name'=>'Viajes Vertiz American Express Travel Services', 
					'address'=>'Hidalgo 1 A, Centro', 
					'email'=>'info@viajesvertiz.com', 
					'phone'=>'152 1856',
					'phone2'=>'F 152 0499',  
					'web_page'=>'www.viajesvertiz.com', 
					)); 

			Advertiser::create(array(
					'id'=>'407',
					'business_name'=>'Vidriería Multividrios',   
					'phone'=>'154 0551',  
					)); 

			Advertiser::create(array(
					'id'=>'408',
					'business_name'=>'Vilar', 
					'address'=>'Recreo 5, Centro', 
					'email'=>'vilar@prodigy.net.mx', 
					'phone'=>'152 2658',
					'phone2'=>'152 3130',  
					)); 

			Advertiser::create(array(
					'id'=>'409',
					'business_name'=>'Vista Magna Residencial',  
					'email'=>'gcortes7080@yahoo.com.mx', 
					'email2'=>'fcifuentes.mdi@gmail.com', 
					'phone'=>'121 0849',  
					'web_page'=>'www.vistamagna.com.mx', 
					)); 

			Advertiser::create(array(
					'id'=>'410',
					'business_name'=>'Vivero Magueyes',   
					'phone'=>'154 8747',  
					)); 

			Advertiser::create(array(
					'id'=>'411',
					'business_name'=>'Voluntarios Profesionales de C.A.S.A.',   
					'phone'=>'154 6060',  
					)); 

			Advertiser::create(array(
					'id'=>'412',
					'business_name'=>'Water Resort Club', 
					'address'=>'Carretera a Dolores Hidalgo, Desviación Km. 8. Rancho Taboada', 
					'email'=>'ventas@taboada.com.mx', 
					'phone'=>'152 9250',  
					'web_page'=>'www.taboada.com.mx', 
					)); 

			Advertiser::create(array(
					'id'=>'413',
					'business_name'=>'William Martin',   
					'phone'=>'152 7190',  
					)); 

			Advertiser::create(array(
					'id'=>'414',
					'business_name'=>'Xochi Flores y Casa',   
					'phone'=>'152 0470',  
					)); 

			Advertiser::create(array(
					'id'=>'415',
					'business_name'=>'Yoga San Miguel',   
					'phone'=>'',
					'mobile_phone'=>'C 415 119 1808',  
					)); 

			Advertiser::create(array(
					'id'=>'416',
					'business_name'=>'Zeus',   
					'phone'=>'154 7412',  
					)); 

			Advertiser::create(array(
					'id'=>'417',
					'business_name'=>'Zirándaro Desarrollo Residencial & Golf', 
					'address'=>'Carretera Qro - SMA, km. 28',  
					'phone'=>'120 8150',  
					'web_page'=>'www.zirandaro.com.mx', 
					)); 

			Advertiser::create(array(
					'id'=>'418',
					'business_name'=>'Academia de Fotografía',   
					'phone'=>'152 1442',  
					)); 

			Advertiser::create(array(
					'id'=>'419',
					'business_name'=>'Al-Anon (AA)',   
					'phone'=>'152 5110',  
					)); 

			Advertiser::create(array(
					'id'=>'420',
					'business_name'=>'Avalúos y levantamientos topográficos',   
					'phone'=>'120 4828',  
					)); 

			Advertiser::create(array(
					'id'=>'421',
					'business_name'=>'Blancos Pily',   
					'phone'=>'152 2351',  
					)); 

			Advertiser::create(array(
					'id'=>'422',
					'business_name'=>'Calderoni Properties', 
					'address'=>'Zacateros 21, Centro', 
					'email'=>'ben@calderoniproperties.com', 
					'phone'=>'154 8164',  
					'web_page'=>'www.calderoniproperties.com', 
					)); 

			Advertiser::create(array(
					'id'=>'423',
					'business_name'=>'Cantadora',   
					'phone'=>'154 8302',  
					)); 

			Advertiser::create(array(
					'id'=>'424',
					'business_name'=>'Templo de la Purísima Concepción Las Monjas',   
					'phone'=>'152 0688',  
					)); 

			Advertiser::create(array(
					'id'=>'425',
					'business_name'=>'Casa García',   
					'phone'=>'',
					'mobile_phone'=>'C 415 100 1303',  
					)); 

			Advertiser::create(array(
					'id'=>'426',
					'business_name'=>'Cerámica Antique',   
					'phone'=>'154 9629',  
					)); 

			Advertiser::create(array(
					'id'=>'427',
					'business_name'=>'Naturopatía Veterinaria', 
					'address'=>'Guadiana 17 A, Guadiana', 
					'email'=>'rodgarmei@gmail.com', 
					'phone'=>'154 4555',  
					)); 

			Advertiser::create(array(
					'id'=>'428',
					'business_name'=>'Contadores Públicos Ángel y Rodríguez, S.C.',  
					'email'=>'jrtangel@hotmail.com', 
					'email2'=>'jjangel@telecablesma.mx', 
					'phone'=>'152 2119',  
					)); 

			Advertiser::create(array(
					'id'=>'429',
					'business_name'=>'Deportes Aces',   
					'phone'=>'152 2899',  
					)); 

			Advertiser::create(array(
					'id'=>'430',
					'business_name'=>'Dr. MonterrubioSantamaría Gabriel',   
					'phone'=>'152 1603',  
					)); 

			Advertiser::create(array(
					'id'=>'431',
					'business_name'=>'Electricistas García',   
					'phone'=>'154 6708',  
					)); 

			Advertiser::create(array(
					'id'=>'432',
					'business_name'=>'Enjoy',   
					'phone'=>'120 4797',  
					)); 

			Advertiser::create(array(
					'id'=>'433',
					'business_name'=>'GNC',   
					'phone'=>'120 0056',  
					)); 

			Advertiser::create(array(
					'id'=>'434',
					'business_name'=>'Helados Dolphy',   
					'phone'=>'152 2744',  
					)); 

			Advertiser::create(array(
					'id'=>'435',
					'business_name'=>'Hierro Comercial',   
					'phone'=>'152 1956',  
					)); 

			Advertiser::create(array(
					'id'=>'436',
					'business_name'=>'Intercam Servicios Financieros', 
					'address'=>'San Francisco 4, Centro', 
					'email'=>'mramirez@intercam.com.mx', 
					'phone'=>'154 6676',
					'phone2'=>'154 6660',
					'mobile_phone'=>'120 4837',  
					'web_page'=>'www.intercam.com.mx', 
					)); 

			Advertiser::create(array(
					'id'=>'437',
					'business_name'=>'Jardín Botánico',   
					'phone'=>'154 4715',  
					)); 

			Advertiser::create(array(
					'id'=>'438',
					'business_name'=>'La Burger', 
					'address'=>'Carr. San Miguel de Allende Dolores Hidalgo Km. 7.3', 
					'email'=>'walter@laburger.com.mx', 
					'phone'=>'',
					'mobile_phone'=>'C 415 114 0073',  
					'web_page'=>'www.laburger.com.mx', 
					)); 

			Advertiser::create(array(
					'id'=>'439',
					'business_name'=>'La Fuente',   
					'phone'=>'152 2641',  
					)); 

			Advertiser::create(array(
					'id'=>'440',
					'business_name'=>'La Nueva Generación',   
					'phone'=>'152 5289',  
					)); 

			Advertiser::create(array(
					'id'=>'441',
					'business_name'=>'La Puertecita',   
					'phone'=>'152 5011',  
					)); 

			Advertiser::create(array(
					'id'=>'442',
					'business_name'=>'Licea A. Mario',   
					'phone'=>'152 0016',  
					)); 

			Advertiser::create(array(
					'id'=>'443',
					'business_name'=>'MASE Mantenimiento y  Servicio Eléctrico',   
					'phone'=>'152 4824',  
					)); 

			Advertiser::create(array(
					'id'=>'444',
					'business_name'=>'Micro Dental de San Miguel', 
					'address'=>'San Francisco 35 Int. 6, Centro', 
					'email'=>'georgekarman1@hotmail.com', 
					'phone'=>'152 4966',
					'mobile_phone'=>'C 415 117 5784',  
					)); 

			Advertiser::create(array(
					'id'=>'445',
					'business_name'=>'Notaría Pública 9', 
					'address'=>'Hidalgo 35, Centro', 
					'email'=>'eugeniagarcia@notaria9sma.com.mx', 
					'phone'=>'152 0249',
					'phone2'=>'152 2088',  
					)); 

			Advertiser::create(array(
					'id'=>'446',
					'business_name'=>'Novedades Rossy',   
					'phone'=>'152 1258',  
					)); 

			Advertiser::create(array(
					'id'=>'447',
					'business_name'=>'Parroquia de Jesús Nazareno (Atotonilco)',   
					'phone'=>'185 2060',  
					)); 

			Advertiser::create(array(
					'id'=>'448',
					'business_name'=>'Parroquia de San Miguel Arcángel',   
					'phone'=>'152 0544',  
					)); 

			Advertiser::create(array(
					'id'=>'449',
					'business_name'=>'Patronato de Cuerpo de Bomberos Voluntarios de San Miguel de Allende',   
					'phone'=>'152 2888',  
					)); 

			Advertiser::create(array(
					'id'=>'450',
					'business_name'=>'Radio Shack',   
					'phone'=>'110 3259',  
					)); 

			Advertiser::create(array(
					'id'=>'451',
					'business_name'=>'Rancho Las Sabinas',   
					'phone'=>'',
					'mobile_phone'=>'C 415 151 2430',  
					)); 

			Advertiser::create(array(
					'id'=>'452',
					'business_name'=>'Refaccionaria Olvera',   
					'phone'=>'152 1536',  
					)); 

			Advertiser::create(array(
					'id'=>'453',
					'business_name'=>'Sección Amarilla',   
					'phone'=>'',
					'mobile_phone'=>'(461) 615 7744',  
					)); 

			Advertiser::create(array(
					'id'=>'454',
					'business_name'=>'Seguridad Privada Samsa', 
					'address'=>'28 de Abril Sur 44, San Antonio', 
					'email'=>'seg.priv.samsa@hotmail.com', 
					'phone'=>'152 5151',
					'mobile_phone'=>'C 415 125 2880',  
					'mobile_phone2'=>'C 415 125 6677', 
					'web_page'=>'www.seguridadsamsa.com', 
					)); 

			Advertiser::create(array(
					'id'=>'455',
					'business_name'=>'Smar Tennis',   
					'phone'=>'',
					'mobile_phone'=>'C 415 113 4019',  
					)); 

			Advertiser::create(array(
					'id'=>'456',
					'business_name'=>'Taller de Joyería Creativa',   
					'phone'=>'152 0929',  
					)); 

			Advertiser::create(array(
					'id'=>'457',
					'business_name'=>'Templo de San Francisco',   
					'phone'=>'152 0947',  
					)); 

			Advertiser::create(array(
					'id'=>'458',
					'business_name'=>'Templo del Oratorio',   
					'phone'=>'152 0521',  
					)); 

			Advertiser::create(array(
					'id'=>'459',
					'business_name'=>'The Spanish Guru',   
					'phone'=>'185 8093',  
					)); 

			Advertiser::create(array(
					'id'=>'460',
					'business_name'=>'Transportes San Miguel (Mudanzaz)',   
					'phone'=>'152 4670',  
					)); 

			Advertiser::create(array(
					'id'=>'461',
					'business_name'=>'Taquito Traducciones',   
					'phone'=>'',
					'mobile_phone'=>'C 415 109 2396',  
					)); 

			Advertiser::create(array(
					'id'=>'462',
					'business_name'=>'Materiales Vélez',   
					'phone'=>'152 1225',  
					)); 

			Advertiser::create(array(
					'id'=>'463',
					'business_name'=>'Corporativo A.M.',  
					'email'=>'josel_aguilera@hotmail.com', 
					'phone'=>'',
					'mobile_phone'=>'C 415 114 2691',  
					)); 

			Advertiser::create(array(
					'id'=>'464',
					'business_name'=>'Quálitas',   
					'phone'=>'185 8541',  
					)); 

			Advertiser::create(array(
					'id'=>'465',
					'business_name'=>'Despacho Topográfico Vázquez', 
					'address'=>'Calle Hidalgo 1 Int. 3, Centro', 
					'email'=>'ingmalev@gmail.com', 
					'phone'=>'154 7530',
					'mobile_phone'=>'C 415 151 8868',  
					'mobile_phone2'=>'C 415 109 2795', 
					)); 

			Advertiser::create(array(
					'id'=>'466',
					'business_name'=>'Ana Lilia Buen Día (Agave)',  
					'email'=>'analiliasma@yahoo.com', 
					'phone'=>'152 2180',  
					'web_page'=>'www.agavesanmiguel.com', 
					)); 

			Advertiser::create(array(
					'id'=>'467',
					'business_name'=>'Natura Corpus Massage Treatments',  
					'email'=>'naturacorpus@gmail.com', 
					'phone'=>'185 8569',  
					)); 

			Advertiser::create(array(
					'id'=>'468',
					'business_name'=>'Consorcio CAZA', 
					'address'=>'Carr. San Miguel de Allende Dolores Hidalgo Km. 7.3', 
					'email'=>'contacto@consorciocaza.com.mx', 
					'phone'=>'110 3246',
					'mobile_phone'=>'C 415 114 3666',  
					'mobile_phone2'=>'C 415 101 1795', 
					'nextel'=>'ID 62*172782*2',  
					'web_page'=>'www.consorciocaza.com.mx', 
					)); 

			Advertiser::create(array(
					'id'=>'469',
					'business_name'=>'La Isla', 
					'address'=>'Maria Greever 5, Guadalupe', 
					'email'=>'naturalygourmet@gmail.com', 
					'phone'=>'152 2202',
					'mobile_phone'=>'C 415 101 0061',  
					)); 

			Advertiser::create(array(
					'id'=>'470',
					'business_name'=>'La Mesa Residencial Club de Golf',   
					'phone'=>'152 3535',  
					)); 

			Advertiser::create(array(
					'id'=>'471',
					'business_name'=>'Dusty Puppies', 
					'address'=>'Guadiana 17 A, Guadiana', 
					'email'=>'dusty_puppies@yahoo.com', 
					'phone'=>'154 4555',  
					)); 

			Advertiser::create(array(
					'id'=>'472',
					'business_name'=>'The Restaurant', 
					'address'=>'Sollano 16, Centro',  
					'phone'=>'154 7877',  
					)); 

			Advertiser::create(array(
					'id'=>'473',
					'business_name'=>'Casas Coloniales', 
					'address'=>'Canal 36, Centro', 
					'email'=>'victoriavidargas@gmail.com', 
					'phone'=>'152 0286',  
					)); 

			Advertiser::create(array(
					'id'=>'474',
					'business_name'=>'Piedras de San Miguel', 
					'address'=>'Tanque 14, Atascadero', 
					'email'=>'info@piedrassanmiguel.com', 
					'phone'=>'154 9193',  
					'web_page'=>'www.piedrassanmiguel.com', 
					)); 

			Advertiser::create(array(
					'id'=>'475',
					'business_name'=>'Karmeta',   
					'phone'=>'154 4888',  
					)); 

			Advertiser::create(array(
					'id'=>'476',
					'business_name'=>'Dr. Alejandro Rodríguez Espínola, Dra . Flor Marina Alvarado Juárez', 
					'address'=>'Prol. Pila Seca 20 de Enero', 
					'email'=>'alex_spinrod@hotmail.com', 
					'email2'=>'florecita_13@hotmail.com', 
					'phone'=>'152 3311',
					'mobile_phone'=>'C 415 151 1348',  
					)); 

			Advertiser::create(array(
					'id'=>'477',
					'business_name'=>'Barros Tarascos',  
					'email'=>'barrostarascos@hotmail.com', 
					'phone'=>'154 6352',  
					)); 

			Advertiser::create(array(
					'id'=>'478',
					'business_name'=>'MX Restaurante Bar', 
					'address'=>'San Francisco 8, Centro', 
					'email'=>'arturoregaladoj@hotmail.com', 
					'phone'=>'110 2100',  
					'web_page'=>'www.mxrestaurante.mx', 
					)); 

			Advertiser::create(array(
					'id'=>'479',
					'business_name'=>'Martínez y Martínez Abogados', 
					'address'=>'Umarán 18, Centro', 
					'email'=>'martinezymartinez.abogados@gmail.com', 
					'phone'=>'152 0348',  
					)); 

			Advertiser::create(array(
					'id'=>'480',
					'business_name'=>'Arq. Ana Delia López',  
					'email'=>'anadelialopez@gmail.com', 
					'email2'=>'arq.anadelia@gmail.com', 
					'phone'=>'150 7221',
					'mobile_phone'=>'C 415 103 0366',  
					)); 

			Advertiser::create(array(
					'id'=>'481',
					'business_name'=>'Red Star Ironworks',   
					'phone'=>'150 6192',  
					)); 

			Advertiser::create(array(
					'id'=>'482',
					'business_name'=>'Creaciones Ortega',   
					'phone'=>'120 0800',  
					)); 

			Advertiser::create(array(
					'id'=>'483',
					'business_name'=>'La Casa del Diezmo',   
					'phone'=>'154 4034',  
					)); 

			Advertiser::create(array(
					'id'=>'484',
					'business_name'=>'Brad Guse San Miguel MGMT & Real Estate', 
					'address'=>'Hernández Macías 111, Centro', 
					'email'=>'bradguse@sanmiguel-mgmt.com', 
					'phone'=>'152 4416',
					'mobile_phone'=>'C 415 100 2338',  
					'mobile_phone2'=>'US 832 364 6307', 
					'web_page'=>'www.sanmiguel-mgmt.com', 
					)); 

			Advertiser::create(array(
					'id'=>'485',
					'business_name'=>'La Grotta Restaurant-Bar',   
					'phone'=>'152 4119',  
					)); 

			Advertiser::create(array(
					'id'=>'486',
					'business_name'=>'Grupo Ferretero Don Pedro', 
					'address'=>'Ancha de San Antonio 123 A, Centro', 
					'email'=>'ventas@grupodonpedro.com', 
					'phone'=>'152 1714',
					'phone2'=>'120 4353',
					'mobile_phone'=>'152 0160',  
					'web_page'=>'www.grupodonpedro.com', 
					)); 

			Advertiser::create(array(
					'id'=>'487',
					'business_name'=>'UMA by Sami',  
					'email'=>'info@umafashion.com.mx', 
					'email2'=>'sami@umafashion.com.mx', 
					'phone'=>'152 0888',  
					'web_page'=>'www.umafashion.com.mx', 
					)); 

			Advertiser::create(array(
					'id'=>'488',
					'business_name'=>'Bistro los Senderos',  
					'email'=>'info@bistrolossenderos.com', 
					'phone'=>'155 9594',  
					'web_page'=>'www.bistrolossenderos.com', 
					)); 

			Advertiser::create(array(
					'id'=>'489',
					'business_name'=>'Nissan Vegusa',   
					'phone'=>'110 2323',  
					)); 

			Advertiser::create(array(
					'id'=>'490',
					'business_name'=>'Casa de Los Olivos',   
					'phone'=>'152 0309',  
					)); 

			Advertiser::create(array(
					'id'=>'491',
					'business_name'=>'Dos Casas',   
					'phone'=>'154 4073',  
					)); 

			Advertiser::create(array(
					'id'=>'492',
					'business_name'=>'Galería Estudio / Dawit Maldonado',   
					'phone'=>'1545238',  
					)); 

			Advertiser::create(array(
					'id'=>'493',
					'business_name'=>'Casa Maxwell', 
					'address'=>'Insurgentes 29', 
					'email'=>'casamaxwell@gmail.com', 
					'phone'=>'152 0257',  
					)); 

			Advertiser::create(array(
					'id'=>'494',
					'business_name'=>'Skot Foreman Fine Art',   
					'phone'=>'154 9588',  
					'web_page'=>'www.skotforeman.com', 
					)); 

			Advertiser::create(array(
					'id'=>'495',
					'business_name'=>'Dr. Chris John Ramaglia Maxwell', 
					'address'=>'Pila Seca 53, Centro', 
					'email'=>'medico_2001@hotmail.com', 
					'phone'=>'152 2111',  
					)); 

			Advertiser::create(array(
					'id'=>'496',
					'business_name'=>'Tourist Transportation Allende',   
					'phone'=>'152 6305',  
					)); 

			Advertiser::create(array(
					'id'=>'497',
					'business_name'=>'Daltile',   
					'phone'=>'154 9601',  
					)); 

			Advertiser::create(array(
					'id'=>'498',
					'business_name'=>'Vitromex',  
					'email'=>'samaniego1717@gmail.com', 
					'phone'=>'154 9601',  
					'web_page'=>'www.vitromex.com.mx', 
					)); 

			Advertiser::create(array(
					'id'=>'499',
					'business_name'=>'Grupo Médico San Francisco',  
					'email'=>'contacto@cdsf.com.mx', 
					'phone'=>'',
					'mobile_phone'=>'(442) 248 2672',  
					)); 

			Advertiser::create(array(
					'id'=>'500',
					'business_name'=>'Galería Arte Contemporáneo', 
					'address'=>'Sollano 13, Centro',  
					'phone'=>'152 5742',
					'phone2'=>'152 6342',  
					)); 

			Advertiser::create(array(
					'id'=>'501',
					'business_name'=>'Megacable', 
					'address'=>'Salida a Celaya 95',  
					'phone'=>'121 0000',  
					)); 

			Advertiser::create(array(
					'id'=>'502',
					'business_name'=>'Edén Management', 
					'address'=>'Pila Seca 18 bis, Centro', 
					'email'=>'info@edenadministracion.com', 
					'phone'=>'152 3762',
					'phone2'=>'C 415 103 0112',
					'mobile_phone'=>'US 210 775 2899',  
					'mobile_phone2'=>'415 153 5413', 
					'web_page'=>'www.edenadministracion.com', 
					)); 

			Advertiser::create(array(
					'id'=>'503',
					'business_name'=>'Chamonix CASA', 
					'address'=>'Sollano 17, Centro',  
					'phone'=>'152 3657',  
					)); 

			Advertiser::create(array(
					'id'=>'504',
					'business_name'=>'CI Banco', 
					'address'=>'San Francisco 25, Centro',  
					'phone'=>'154 8797',
					'phone2'=>'152 4123',
					'mobile_phone'=>'152 5092',  
					'mobile_phone2'=>'154 7748', 
					)); 

			Advertiser::create(array(
					'id'=>'505',
					'business_name'=>'Canal 4', 
					'address'=>'Montitlán 17, Los Balcones', 
					'email'=>'San Miguel_tv@hotmail.com', 
					'phone'=>'154 7110',  
					'web_page'=>'www.xhgsmcanal4.es.tl', 
					)); 

			Advertiser::create(array(
					'id'=>'506',
					'business_name'=>'Salón Biella',   
					'phone'=>'152 0961',  
					)); 

			Advertiser::create(array(
					'id'=>'507',
					'business_name'=>'El Galerón Restaurant', 
					'address'=>'Carretera a Dolores Hidalgo Desviación Km. 8', 
					'email'=>'ventas@hotelhaciendataboada.com', 
					'phone'=>'152 9250',  
					)); 

			Advertiser::create(array(
					'id'=>'508',
					'business_name'=>'Hacienda Puerto de Sosa',  
					'email'=>'Puertodesosahacienda@gmail.com', 
					'phone'=>'',
					'mobile_phone'=>'C 415 114 0016',  
					'web_page'=>'www.puertodesosahacienda.com', 
					)); 

			Advertiser::create(array(
					'id'=>'509',
					'business_name'=>'J de J construcción en Tablaroca',  
					'email'=>'jdetablaroca@live.com.mx', 
					'phone'=>'',
					'mobile_phone'=>'C 415 103 2802',  
					)); 

			Advertiser::create(array(
					'id'=>'510',
					'business_name'=>'Alejandro Gutierrez LA CASA', 
					'address'=>'Cuna de Allende 17 Int.8, Centro', 
					'email'=>'lacasa@lacasasma.com', 
					'phone'=>'154 7657',
					'phone2'=>'154 7659',  
					'web_page'=>'www.lacasasma.com', 
					)); 

			Advertiser::create(array(
					'id'=>'511',
					'business_name'=>'Banquetes Marcela', 
					'address'=>'Juan de Dios Peza 37, Guadalupe 37. Col. Guadalupe', 
					'email'=>'banquetesmarcela@hotmail.com', 
					'phone'=>'152 2012',
					'phone2'=>'152 6507',
					'mobile_phone'=>'C 415 117 1091',  
					)); 

			Advertiser::create(array(
					'id'=>'512',
					'business_name'=>'Bienes Raíces Rosalba',  
					'email'=>'ventasrentas@hotmail.com', 
					'phone'=>'154 4472',  
					)); 

			Advertiser::create(array(
					'id'=>'513',
					'business_name'=>'Hank´s New Orleans Café',   
					'phone'=>'152 2645',  
					)); 

			Advertiser::create(array(
					'id'=>'514',
					'business_name'=>'Lic. Eugenia García Zavala', 
					'address'=>'Hidalgo 35, Centro', 
					'email'=>'eugeniagarcia@notaria9sma.com.mx', 
					'phone'=>'152 0249',
					'phone2'=>'152 2088',  
					'web_page'=>'www.notaria9sma.com.mx', 
					)); 

			Advertiser::create(array(
					'id'=>'515',
					'business_name'=>'Aktyn',   
					'phone'=>'',
					'mobile_phone'=>'C 415 101 0268',  
					)); 

			Advertiser::create(array(
					'id'=>'516',
					'business_name'=>'El Mesón Hotel', 
					'address'=>'Mesones 80',  
					'phone'=>'152 0580',
					'phone2'=>'152 2103',  
					)); 

			Advertiser::create(array(
					'id'=>'517',
					'business_name'=>'Centro Comercial Super Espino',   
					'phone'=>'152 1009',  
					)); 

			Advertiser::create(array(
					'id'=>'518',
					'business_name'=>'Mini Market',   
					'phone'=>'152 1905',  
					)); 

			Advertiser::create(array(
					'id'=>'519',
					'business_name'=>'Francisco J. Deanda',   
					'phone'=>'',
					'mobile_phone'=>'C 415 105 6068',  
					)); 

			Advertiser::create(array(
					'id'=>'520',
					'business_name'=>'Plásticos Castañeda',   
					'phone'=>'152 0966',  
					)); 

			Advertiser::create(array(
					'id'=>'521',
					'business_name'=>'Ma. del Pueblito Cervantes Colunga',   
					'phone'=>'152 1336',  
					)); 

			Advertiser::create(array(
					'id'=>'522',
					'business_name'=>'Centro Nueva Vida',   
					'phone'=>'120 4799',  
					)); 

			Advertiser::create(array(
					'id'=>'523',
					'business_name'=>'Tortillería Cielito Lindo',   
					'phone'=>'151 5004',
					'phone2'=>'C 415 114 4065',
					'mobile_phone'=>'C 415 151 24 19',  
					'mobile_phone2'=>'C 415 113 2474', 
					)); 

			Advertiser::create(array(
					'id'=>'524',
					'business_name'=>'Tortillería Contreras',   
					'phone'=>'154 7714',  
					)); 

			Advertiser::create(array(
					'id'=>'525',
					'business_name'=>'La Azotea', 
					'address'=>'Umaran 6, Centro',  
					'phone'=>'152 4977',  
					)); 

			Advertiser::create(array(
					'id'=>'526',
					'business_name'=>'Panadería San Sebastían',   
					'phone'=>'154 8974',  
					)); 

			Advertiser::create(array(
					'id'=>'527',
					'business_name'=>'Pastelería Repostería Fertrin',   
					'phone'=>'152 4424',  
					)); 

			Advertiser::create(array(
					'id'=>'528',
					'business_name'=>'Taller el Nigromante',   
					'phone'=>'',
					'mobile_phone'=>'C 415 107 1461',  
					)); 

			Advertiser::create(array(
					'id'=>'529',
					'business_name'=>'Magnoly',   
					'phone'=>'',
					'mobile_phone'=>'C 477 347 5004',  
					)); 

			Advertiser::create(array(
					'id'=>'530',
					'business_name'=>'Dulcifiesta Téllez',   
					'phone'=>'152 3223',  
					)); 

			Advertiser::create(array(
					'id'=>'531',
					'business_name'=>'Nutrisa',   
					'phone'=>'120 4820',  
					)); 

			Advertiser::create(array(
					'id'=>'532',
					'business_name'=>'Tutti Frutti',   
					'phone'=>'154 7275',  
					)); 

			Advertiser::create(array(
					'id'=>'533',
					'business_name'=>'Casa Peralta',   
					'phone'=>'152 0312',  
					)); 

			Advertiser::create(array(
					'id'=>'534',
					'business_name'=>'Orfebrería Vázquez',   
					'phone'=>'152 0761',  
					)); 

			Advertiser::create(array(
					'id'=>'535',
					'business_name'=>'Artículos para Joyeros Martín',   
					'phone'=>'152 5817',  
					)); 

			Advertiser::create(array(
					'id'=>'536',
					'business_name'=>'Fabricación y Reparación  de Joyería',   
					'phone'=>'152 6396',  
					)); 

			Advertiser::create(array(
					'id'=>'537',
					'business_name'=>'Platería San Miguel',   
					'phone'=>'152 1066',  
					)); 

			Advertiser::create(array(
					'id'=>'538',
					'business_name'=>'Génesis Tienda Naturista',   
					'phone'=>'152 2016',  
					)); 

			Advertiser::create(array(
					'id'=>'539',
					'business_name'=>'Briseño',   
					'phone'=>'152 0983',  
					)); 

			Advertiser::create(array(
					'id'=>'540',
					'business_name'=>'Silvia-Canadá',   
					'phone'=>'152 1109',  
					)); 

			Advertiser::create(array(
					'id'=>'541',
					'business_name'=>'Acupuntura Sophia',   
					'phone'=>'154 5570',  
					)); 

			Advertiser::create(array(
					'id'=>'542',
					'business_name'=>'Mercado de San Juan de Dios',   
					'phone'=>'152 2014',  
					)); 

			Advertiser::create(array(
					'id'=>'543',
					'business_name'=>'Escuadra Deportiva',   
					'phone'=>'152 8051',  
					)); 

			Advertiser::create(array(
					'id'=>'544',
					'business_name'=>'Grupo Industrial Export',   
					'phone'=>'152 4180',  
					)); 

			Advertiser::create(array(
					'id'=>'545',
					'business_name'=>'Radio TV San Rafael',   
					'phone'=>'152 3270',  
					)); 

			Advertiser::create(array(
					'id'=>'546',
					'business_name'=>'Materiales Juárez',   
					'phone'=>'152 0845',  
					)); 

			Advertiser::create(array(
					'id'=>'547',
					'business_name'=>'Casa de la Hoja Seca',   
					'phone'=>'152 2643',  
					)); 

			Advertiser::create(array(
					'id'=>'548',
					'business_name'=>'Tapas Express',   
					'phone'=>'',
					'mobile_phone'=>'C 415 115 9295',  
					)); 

			Advertiser::create(array(
					'id'=>'549',
					'business_name'=>'Universidad de Guanajuato  (Extensión SMA)',   
					'phone'=>'152 5383',  
					)); 

			Advertiser::create(array(
					'id'=>'550',
					'business_name'=>'Universidad de León',   
					'phone'=>'152 7972',  
					)); 

			Advertiser::create(array(
					'id'=>'551',
					'business_name'=>'Productora de Tortillas San Miguel',   
					'phone'=>'154 4930',  
					)); 

			Advertiser::create(array(
					'id'=>'552',
					'business_name'=>'Ferretería y Tlapalería Don Cuco',   
					'phone'=>'152 2214',  
					)); 

			Advertiser::create(array(
					'id'=>'553',
					'business_name'=>'Paso a Paso',   
					'phone'=>'154 9944',  
					)); 

			Advertiser::create(array(
					'id'=>'554',
					'business_name'=>'Estacionamiento El Cardo',   
					'phone'=>'150 7060',  
					)); 

			Advertiser::create(array(
					'id'=>'555',
					'business_name'=>'La Caldera (Sólo pensión)',   
					'phone'=>'152 1730',  
					)); 

			Advertiser::create(array(
					'id'=>'556',
					'business_name'=>'La Esquina (Museo del juguete mexicano)',   
					'phone'=>'152 2602',  
					)); 

			Advertiser::create(array(
					'id'=>'557',
					'business_name'=>'Pescadería Peinado',   
					'phone'=>'154 8489',  
					)); 

			Advertiser::create(array(
					'id'=>'558',
					'business_name'=>'Mercado Ignacio Ramírez',   
					'phone'=>'152 4781',  
					)); 

			Advertiser::create(array(
					'id'=>'559',
					'business_name'=>'Mercado de Artesanías',   
					'phone'=>'152 2844',  
					)); 

			Advertiser::create(array(
					'id'=>'560',
					'business_name'=>'Vinatería Pepe Llanos',   
					'phone'=>'152 0836',  
					)); 

			Advertiser::create(array(
					'id'=>'561',
					'business_name'=>'Vinos y Licores Don Quijote',   
					'phone'=>'152 3748',  
					)); 

			Advertiser::create(array(
					'id'=>'562',
					'business_name'=>'Cava Sautto', 
					'address'=>'Hernández Macias 59', 
					'email'=>'cavasautto@gmail.com', 
					'phone'=>'152 0076',  
					)); 

			Advertiser::create(array(
					'id'=>'563',
					'business_name'=>'Teatro Santa Ana',   
					'phone'=>'152 7305',  
					)); 

			Advertiser::create(array(
					'id'=>'564',
					'business_name'=>'El Petit Bar',   
					'phone'=>'152 3229',  
					)); 

			Advertiser::create(array(
					'id'=>'565',
					'business_name'=>'Dr. Héctor Camarena Reynoso',   
					'phone'=>'',
					'mobile_phone'=>'(461) 614 1410',  
					)); 

			Advertiser::create(array(
					'id'=>'566',
					'business_name'=>'Limerick Pub',   
					'phone'=>'154 8642',  
					)); 

			Advertiser::create(array(
					'id'=>'567',
					'business_name'=>'Cerrajería Génesis',   
					'phone'=>'120 2152',  
					)); 

			Advertiser::create(array(
					'id'=>'568',
					'business_name'=>'Gabinete Radiológico Dental',   
					'phone'=>'152 0613',  
					)); 

			Advertiser::create(array(
					'id'=>'569',
					'business_name'=>'The Shop',   
					'phone'=>'152 4481',  
					)); 

			Advertiser::create(array(
					'id'=>'570',
					'business_name'=>'Mueblería Queretana',   
					'phone'=>'154 5536',  
					)); 

			Advertiser::create(array(
					'id'=>'571',
					'business_name'=>'Los Burritos San Miguel',   
					'phone'=>'152 3222',  
					)); 

			Advertiser::create(array(
					'id'=>'572',
					'business_name'=>'Vulcanizadora Móvil',   
					'phone'=>'',
					'mobile_phone'=>'C 415 125 1107',  
					)); 

			Advertiser::create(array(
					'id'=>'573',
					'business_name'=>'Abarrotes Martín',   
					'phone'=>'121 0063',  
					)); 

			Advertiser::create(array(
					'id'=>'574',
					'business_name'=>'Comunidad Educativa Árbol de Vida',   
					'phone'=>'110 2040',  
					)); 

			Advertiser::create(array(
					'id'=>'575',
					'business_name'=>'C. Cecilia Jiménez Arredondo',   
					'phone'=>'152 4361',  
					)); 

			Advertiser::create(array(
					'id'=>'576',
					'business_name'=>'C. Eduardo González Bravo',   
					'phone'=>'152 4159',  
					)); 

			Advertiser::create(array(
					'id'=>'577',
					'business_name'=>'C. Javier Salazar Diosdado',   
					'phone'=>'152 5861',  
					)); 

			Advertiser::create(array(
					'id'=>'578',
					'business_name'=>'C. Jesús Espinoza Govea',   
					'phone'=>'152 5354',  
					)); 

			Advertiser::create(array(
					'id'=>'579',
					'business_name'=>'Rug Doctor 911 Professional Cleaning', 
					'address'=>'Calle Ignacio Allende 9A, San Rafael', 
					'email'=>'rugdoctor911@yahoo.com', 
					'phone'=>'121 2824',
					'mobile_phone'=>'C 415 107 4723',  
					'mobile_phone2'=>'USA 678 500 9253', 
					'web_page'=>'www.osayicreations.jimdo.com', 
					)); 

			Advertiser::create(array(
					'id'=>'580',
					'business_name'=>'Taxi Express',   
					'phone'=>'120 4443',
					'phone2'=>'122 1579',
					'mobile_phone'=>'ID. 92*11*18503',  
					'mobile_phone2'=>'C 415 111 5503', 
					)); 

			Advertiser::create(array(
					'id'=>'581',
					'business_name'=>'Emelia & Felisa', 
					'address'=>'Recreo 13. Centro',  
					'phone'=>'154 9463',  
					)); 

			Advertiser::create(array(
					'id'=>'582',
					'business_name'=>'Colchorama', 
					'address'=>'Plaza la Luciérnaga 165. Local 49',  
					'phone'=>'120 4789',
					'phone2'=>'152 1412',  
					)); 

			Advertiser::create(array(
					'id'=>'583',
					'business_name'=>'San Miguel Community Foundation', 
					'address'=>'Prol. Pila Seca 91 A', 
					'email'=>'info@sanmiguelcommunityfoundation.org', 
					'phone'=>'152 7447',
					'phone2'=>'152 5327',  
					'web_page'=>'www.sanmiguelcommunityfoundation.org', 
					)); 

			Advertiser::create(array(
					'id'=>'584',
					'business_name'=>'Casa Calderoni B&B', 
					'address'=>'Callejón del Pueblito 4 A, Centro', 
					'email'=>'ben@casacalderoni.com', 
					'phone'=>'154 6005',
					'phone2'=>'USA 713 955 6091',  
					'web_page'=>'www.casacalderoni.com', 
					)); 

			Advertiser::create(array(
					'id'=>'585',
					'business_name'=>'El Pato', 
					'address'=>'Margarito Ledezma 19, Guadalupe', 
					'email'=>'pato199@prodigy.net.mx', 
					'phone'=>'152 1543',  
					)); 

			Advertiser::create(array(
					'id'=>'586',
					'business_name'=>'Oficina de Traducciones Dobarganes', 
					'address'=>'Correo 25, Centro', 
					'email'=>'barbaradobarganes@hotmail.com', 
					'email2'=>'mtdobarganes@yahoo.com.mx', 
					'phone'=>'152 0151',
					'mobile_phone'=>'C 415 103 0458',  
					'mobile_phone2'=>'C 415 153 5047', 
					)); 

			Advertiser::create(array(
					'id'=>'587',
					'business_name'=>'Sunset Bar & Lounge', 
					'address'=>'Mesones 101, Centro.', 
					'email'=>'hcm1711@gmail.com', 
					'phone'=>'121 7070',  
					)); 

			Advertiser::create(array(
					'id'=>'588',
					'business_name'=>'Kathy Patterson & Oliver Caraco', 
					'address'=>'Jesus 2, Centro', 
					'email'=>'kathy@atenearealty.com', 
					'email2'=>'oliver@atenearealty.com', 
					'phone'=>'',
					'mobile_phone'=>'C 415 101 1848',  
					'mobile_phone2'=>'C 415 114 1794', 
					'web_page'=>'www.atenearealty.com', 
					)); 

			Advertiser::create(array(
					'id'=>'589',
					'business_name'=>'Quinta Loreto Hotel & Restaurant', 
					'address'=>'Loreto 15, Centro', 
					'email'=>'contacto@quintaloreto.com.mx', 
					'phone'=>'152 0042',
					'phone2'=>'F 152 1304',  
					'web_page'=>'www.quintaloreto.com.mx', 
					)); 

			Advertiser::create(array(
					'id'=>'590',
					'business_name'=>'Lavandería Monarca', 
					'address'=>'Salida a Qro. 177', 
					'email'=>'lavanderia_monarca@hotmail.com', 
					'phone'=>'152 3900',  
					)); 

			Advertiser::create(array(
					'id'=>'591',
					'business_name'=>'Casa Misión de San Miguel', 
					'address'=>'3ra Cerrada de Pila Seca, 17, Centro', 
					'email'=>'info@casamision.com', 
					'phone'=>'154 5889',
					'mobile_phone'=>'US 646 472 5937',  
					'web_page'=>'www.casamision.com', 
					)); 

			Advertiser::create(array(
					'id'=>'592',
					'business_name'=>'Patronato Pro Niños', 
					'address'=>'Ave. Reforma 75 C Fracc. Ignacio Ramírez', 
					'email'=>'info@patronatoproninos.org', 
					'phone'=>'152 7796',  
					'web_page'=>'www.patronatoproninos.org', 
					)); 

			Advertiser::create(array(
					'id'=>'593',
					'business_name'=>'Colonial Real Estate', 
					'address'=>'Correo 3, Centro.', 
					'email'=>'info@colonial-realestate.com', 
					'phone'=>'154 4971',
					'mobile_phone'=>'US 339 309 5639',  
					'web_page'=>'www.sanmiguelallenderealestate.com', 
					)); 

			Advertiser::create(array(
					'id'=>'594',
					'business_name'=>'Servicios de Emergencias, Eventos y Traslados San Miguel (SEETS)', 
					'address'=>'Santa Fé 5, Allende', 
					'email'=>'jsel_tum@hotmail.com', 
					'email2'=>'ruizfrias15@outlook.com', 
					'phone'=>'',
					'phone2'=>'C 415 151 3771',
					'mobile_phone'=>'C 415 103 7464',  
					'mobile_phone2'=>'C 415 122 0308', 
					'nextel'=>'ID 62*281649*9',  
					)); 

			Advertiser::create(array(
					'id'=>'595',
					'business_name'=>'Terapia Fisica Alternativa', 
					'address'=>'Ancha de San Antonio 20 Int. 2', 
					'email'=>'tfa.sanmiguel@gmail.com', 
					'phone'=>'122 0479',
					'phone2'=>'ID 52*945716*1',  
					)); 

			Advertiser::create(array(
					'id'=>'596',
					'business_name'=>'Casa Maricela‘s', 
					'address'=>'Jesús 41, Centro',  
					'phone'=>'152 6631',
					'phone2'=>'121 0456',
					'mobile_phone'=>'C 415 103 2197',  
					'mobile_phone2'=>'USA 925 951 8660', 
					)); 

			Advertiser::create(array(
					'id'=>'597',
					'business_name'=>'Carlos Sánchez',   
					'phone'=>'',
					'mobile_phone'=>'C 415 105 9652',  
					)); 

			Advertiser::create(array(
					'id'=>'598',
					'business_name'=>'Herb Wilson', 
					'address'=>'Mesones 14 Int. 3', 
					'email'=>'hwilson99@yahoo.com', 
					'phone'=>'',
					'mobile_phone'=>'C 415 114 6506',  
					'mobile_phone2'=>'US 702 421 0447', 
					)); 

			Advertiser::create(array(
					'id'=>'599',
					'business_name'=>'Papelería El Iris', 
					'address'=>'San Francisco 16, Centro',  
					'phone'=>'152 0738',  
					)); 

			Advertiser::create(array(
					'id'=>'600',
					'business_name'=>'Centro de Verificación Vehicular El Caracol', 
					'address'=>'Carretera a Querétaro 25 y 48',  
					'phone'=>'152 1133',
					'phone2'=>'152 4492',  
					)); 

			Advertiser::create(array(
					'id'=>'601',
					'business_name'=>'Gasolinería Servicio el Caracol', 
					'address'=>'KM 32 San Miguel a Querétaro',  
					'phone'=>'152 5511',
					'phone2'=>'152 2304',  
					)); 

			Advertiser::create(array(
					'id'=>'602',
					'business_name'=>'Carnicería la Paloma', 
					'address'=>'Colegio 25, Centro', 
					'email'=>'carneslapaloma@hotmail.com', 
					'phone'=>'152 1484',  
					)); 

			Advertiser::create(array(
					'id'=>'603',
					'business_name'=>'Intercam Servicios Financieros', 
					'address'=>'San Francisco 4, Centro', 
					'email'=>'mramirez@intercam.com.mx', 
					'phone'=>'154 6676',
					'phone2'=>'154 6660',
					'mobile_phone'=>'120 4837',  
					'web_page'=>'www.intercam.com.mx', 
					)); 

			Advertiser::create(array(
					'id'=>'604',
					'business_name'=>'Casa del Inquisidor', 
					'address'=>'Aldama 1. Centro', 
					'email'=>'aldamanumber1@gmail.com', 
					'phone'=>'154 6868',
					'mobile_phone'=>'C 415 109 3023',  
					'web_page'=>'www.lacasadelinquisidor.com', 
					)); 

			Advertiser::create(array(
					'id'=>'605',
					'business_name'=>'Dr. Marco Antonio Zamudio Carrillo', 
					'address'=>'Hospital Medica Tec 100 San Miguel', 
					'email'=>'drmarcozamudio1@hotmail.com', 
					'phone'=>'154 8000',
					'phone2'=>'154 0474',
					'mobile_phone'=>'C 442 226 0798',  
					)); 

			Advertiser::create(array(
					'id'=>'606',
					'business_name'=>'Balloon Adventures',   
					'phone'=>'',
					'mobile_phone'=>'C 415 114 2174',  
					)); 

			Advertiser::create(array(
					'id'=>'607',
					'business_name'=>'Fly San Miguel',   
					'phone'=>'',
					'mobile_phone'=>'C 415 125 6072',  
					)); 

			Advertiser::create(array(
					'id'=>'608',
					'business_name'=>'Seguridad Drago',   
					'phone'=>'152 2328',  
					)); 

			Advertiser::create(array(
					'id'=>'609',
					'business_name'=>'Pet Care Center',   
					'phone'=>'152 2901',  
					)); 

			Advertiser::create(array(
					'id'=>'610',
					'business_name'=>'Sebastián Zavala',   
					'phone'=>'154 4948',  
					)); 

			Advertiser::create(array(
					'id'=>'611',
					'business_name'=>'Pérgola',   
					'phone'=>'154 5595',  
					)); 

			Advertiser::create(array(
					'id'=>'612',
					'business_name'=>'Cielito Lindo',   
					'phone'=>'155 9547',  
					)); 

			Advertiser::create(array(
					'id'=>'613',
					'business_name'=>'Cumpanio',   
					'phone'=>'152 2984',  
					)); 

			Advertiser::create(array(
					'id'=>'614',
					'business_name'=>'The Beer Company San Miguel',   
					'phone'=>'154 4224',  
					)); 

			Advertiser::create(array(
					'id'=>'615',
					'business_name'=>'Materiales Tovar',   
					'phone'=>'152 8250',  
					)); 

			Advertiser::create(array(
					'id'=>'616',
					'business_name'=>'Comercial Mexicana',   
					'phone'=>'120 2051',  
					)); 

			Advertiser::create(array(
					'id'=>'617',
					'business_name'=>'Herrería Salazar',   
					'phone'=>'152 1254',  
					)); 

			Advertiser::create(array(
					'id'=>'618',
					'business_name'=>'Unan San Miguel Allende',   
					'phone'=>'152 5383',  
					)); 

			Advertiser::create(array(
					'id'=>'619',
					'business_name'=>'MultiEnvios', 
					'address'=>'Zacateros 54, Centro', 
					'email'=>'multienviossanmiguel@hotmail.com', 
					'phone'=>'150 0257',  
					'web_page'=>'www.multienvios.com.mx', 
					)); 

			Advertiser::create(array(
					'id'=>'620',
					'business_name'=>'Daniel Rivera (Clases)',   
					'phone'=>'',
					'mobile_phone'=>'C 415 103 5302',  
					)); 

			Advertiser::create(array(
					'id'=>'621',
					'business_name'=>'Grúas',   
					'phone'=>'150 7286',  
					)); 

			Advertiser::create(array(
					'id'=>'622',
					'business_name'=>'Grúas Ayuda',   
					'phone'=>'152 3713',  
					)); 

			Advertiser::create(array(
					'id'=>'623',
					'business_name'=>'Grúas Teniente',   
					'phone'=>'152 1456',  
					)); 

			Advertiser::create(array(
					'id'=>'624',
					'business_name'=>'Clínica Tec 100 San Miguel de Allende',
					'address'=>'Libramiento Jose Manuel Zavala Zavala PPKBZON 12',
					'email'=>'clinicasanmiguel@medicatec100.com.mx',     
					'phone'=>'152 2320',
					'phone2'=>'12 5900',  
					)); 

			Advertiser::create(array(
					'id'=>'625',
					'business_name'=>'Clínica Tec 100 San Miguel de Allende',  
					'address'=>'Libramiento Jose Manuel Zavala Zavala PPKBZON 12',
					'email'=>'clinicasanmiguel@medicatec100.com.mx',   
					'phone'=>'152 2320',
					'phone2'=>'12 5900',  
					)); 

			Advertiser::create(array(
					'id'=>'626',
					'business_name'=>'Clínica Tec 100 San Miguel de Allende',
					'address'=>'Libramiento Jose Manuel Zavala Zavala PPKBZON 12',
					'email'=>'clinicasanmiguel@medicatec100.com.mx',   
					'phone'=>'152 2320',
					'phone2'=>'12 5900',  
					)); 

			Advertiser::create(array(
					'id'=>'627',
					'business_name'=>'Clínica Tec 100 San Miguel de Allende', 
					'address'=>'Libramiento Jose Manuel Zavala Zavala PPKBZON 12',
					'email'=>'clinicasanmiguel@medicatec100.com.mx',  
					'phone'=>'152 2320',
					'phone2'=>'12 5900',  
					)); 

			Advertiser::create(array(
					'id'=>'628',
					'business_name'=>'Clínica Tec 100 San Miguel de Allende', 
					'address'=>'Libramiento Jose Manuel Zavala Zavala PPKBZON 12',
					'email'=>'clinicasanmiguel@medicatec100.com.mx',  
					'phone'=>'152 2320',
					'phone2'=>'12 5900',  
					)); 

			Advertiser::create(array(
					'id'=>'629',
					'business_name'=>'Los Bisquets',   
					'phone'=>'154 8038',  
					)); 

			Advertiser::create(array(
					'id'=>'630',
					'business_name'=>'La Conexión', 
					'address'=>'Libramiento a Dolores 103', 
					'email'=>'laconex2001@yahoo.com', 
					'phone'=>'152 150 0208',
					'mobile_phone'=>'F 117 0000',  
					'web_page'=>'www.laconexion.com.mx', 
					)); 

			Advertiser::create(array(
					'id'=>'631',
					'business_name'=>'Axa San Miguel', 
					'address'=>'Carr. salida a Celaya 1 Local 1M', 
					'email'=>'alejandro.garcia@ruizquezada.com', 
					'phone'=>'110 2199',  
					'web_page'=>'www.axa.com.mx', 
					)); 

			Advertiser::create(array(
					'id'=>'632',
					'business_name'=>'OE Oficinas Empresariales', 
					'address'=>'Ancha de San Antonio 20 Int. 2', 
					'email'=>'oeoficinasempresariales@gmail.com', 
					'phone'=>'152 2905',
					'phone2'=>'154 8330',  
					)); 

			Advertiser::create(array(
					'id'=>'633',
					'business_name'=>'Cactus03', 
					'address'=>'Ancha de San Antonio  20 Int. 19', 
					'email'=>'ggarcia@cactus03.com', 
					'phone'=>'154 9161',
					'mobile_phone'=>'C 415 153 4103',  
					'mobile_phone2'=>'C 415 103 3313', 
					'web_page'=>'www.cactus03.com', 
					)); 

			Advertiser::create(array(
					'id'=>'634',
					'business_name'=>'Corporativo Jurídico', 
					'address'=>'Sollano 23a, Centro, 1 de Mayo 44, Fracc. Ignacio Ramìrez',  
					'phone'=>'',
					'mobile_phone'=>'C 415 105 4272',  
					'mobile_phone2'=>'C 415 566 1503', 
					)); 

			Advertiser::create(array(
					'id'=>'635',
					'business_name'=>'Todo en Equipales', 
					'address'=>'Salida a Celaya 26', 
					'email'=>'equipalesdesanmiguel@gmail.com', 
					'phone'=>'185 8090',  
					'web_page'=>'Todo en equipales', 
					)); 

			Advertiser::create(array(
					'id'=>'636',
					'business_name'=>'Pezcadeldia Fishop', 
					'address'=>'Hernandez Macias 135', 
					'email'=>'lapezcadeldia@gmail.com', 
					'phone'=>'154 8498',  
					'mobile_phone2'=>'C 415 103 7698', 
					)); 

			Advertiser::create(array(
					'id'=>'637',
					'business_name'=>'MultiEnvios', 
					'address'=>'Zacateros 54 Centro', 
					'email'=>'multienviossanmiguel@hotmail.com', 
					'phone'=>'150 0257',  
					'web_page'=>'www.multienvios.com.mx', 
					)); 

			Advertiser::create(array(
					'id'=>'638',
					'business_name'=>'Alarmas Sirce', 
					'address'=>'Sollano 4, Centro', 
					'email'=>'sirce91@prodigy.net.mx', 
					'phone'=>'152 5819',
					'phone2'=>'152 6256',  
					'web_page'=>'www.sirce.com.mx', 
					)); 

			Advertiser::create(array(
					'id'=>'639',
					'business_name'=>'Marisquería Pacifico', 
					'address'=>'Carr. San Miguel - Celaya Km 3.5', 
					'email'=>'sancamaronsma14@gmail.com', 
					'phone'=>'',
					'mobile_phone'=>'C. 415 112 4395',  
					)); 

			Advertiser::create(array(
					'id'=>'640',
					'business_name'=>'Liliana Patlan', 
					'address'=>'Chorro 48, Centro', 
					'email'=>'maquillate.lp@gmail.com', 
					'phone'=>'',
					'mobile_phone'=>'C 415 111 4740',  
					)); 

			Advertiser::create(array(
					'id'=>'641',
					'business_name'=>'I ARCH', 
					'address'=>'Calzada de la Estación 149', 
					'email'=>'ventas@envolve.mx', 
					'phone'=>'120 05 57',
					'mobile_phone'=>'C 415 101 97 91',  
					'web_page'=>'www.envolve.mx', 
					)); 

			Advertiser::create(array(
					'id'=>'642',
					'business_name'=>'Dr. Jesus Herrrera', 
					'address'=>'Stirling Dickinson 27 D', 
					'email'=>'jmherrerardent@yahoo.com.mx', 
					'phone'=>'122 2881',
					'phone2'=>'100 0403',
					'mobile_phone'=>'C 461 140 6631',  
					)); 

			Advertiser::create(array(
					'id'=>'643',
					'business_name'=>'Estética Lorena', 
					'address'=>'Nemesio Diez 20, Centro', 
					'email'=>'lorena.lambarri@hotmail.com', 
					'phone'=>'',
					'mobile_phone'=>'C. 415 104 2862',  
					)); 

			Advertiser::create(array(
					'id'=>'644',
					'business_name'=>'Coral Beauty', 
					'address'=>'Calzada de la Luz 47, Centro', 
					'email'=>'coralbeautyacuari@gmail.com', 
					'phone'=>'',
					'mobile_phone'=>'C 415 101 1748',  
					'mobile_phone2'=>'C 415 109 1235', 
					)); 

			Advertiser::create(array(
					'id'=>'645',
					'business_name'=>'Malicha´s', 
					'address'=>'Colegio Militar Guadalupe', 
					'email'=>'rest.grill.na@gmail.com', 
					'phone'=>'121 4977',
					'mobile_phone'=>'415 153 4218',  
					)); 

			Advertiser::create(array(
					'id'=>'646',
					'business_name'=>'Verintort', 
					'address'=>'Plaza Conspiracion Local 13', 
					'email'=>'verintort@gmail.com', 
					'phone'=>'121 4398',
					'mobile_phone'=>'C 415 103 4175',  
					'web_page'=>'www.verintort.com', 
					)); 

			Advertiser::create(array(
					'id'=>'647',
					'business_name'=>'BesTur (Operadora Turística)', 
					'address'=>'Circuito Misión Allende Sur 30 Misión de la Estación', 
					'email'=>'otbestur@yahoo.com', 
					'email2'=>'reservaciones@besturmexico.com', 
					'phone'=>'121 2540',
					'mobile_phone'=>'C 415 103 5353',  
					)); 

			Advertiser::create(array(
					'id'=>'648',
					'business_name'=>'Verónica Alvarado Property Management & Relocation Services', 
					'address'=>'Pila Seca 18 bis, Centro', 
					'email'=>'info@veronica-alvarado.com', 
					'phone'=>'152 3762',
					'mobile_phone'=>'C 415 103 0112',  
					'web_page'=>'www.veronica-alvarado.com', 
					)); 

			Advertiser::create(array(
					'id'=>'649',
					'business_name'=>'Tours Mexico Colonial Fieldtrips', 
					'address'=>'Zacateros 23, Centro', 
					'email'=>'jaimeolalde@hotmail.com', 
					'phone'=>'152 5794',
					'mobile_phone'=>'C 415 109 4674',  
					)); 

			Advertiser::create(array(
					'id'=>'650',
					'business_name'=>'Arq. Rafael Alvarez', 
					'address'=>'Tenerias 1, Centro', 
					'email'=>'alvarezestudio.arq@gmail.com', 
					'phone'=>'121 7636',
					'mobile_phone'=>'C 415 113 5795',  
					)); 

			Advertiser::create(array(
					'id'=>'651',
					'business_name'=>'Chao Ban', 
					'address'=>'Pila Seca 16, Centro', 
					'email'=>'chaoban@gmail.com', 
					'phone'=>'152 1559',  
					)); 

			Advertiser::create(array(
					'id'=>'652',
					'business_name'=>'Dra. Halina Rubio Villegas', 
					'address'=>'Vicente Araiza 1 La Lejona 2a Secc', 
					'email'=>'aliruv@hotmail.com', 
					'phone'=>'152 1391',
					'mobile_phone'=>'C 415 124 1641',  
					)); 

			Advertiser::create(array(
					'id'=>'653',
					'business_name'=>'Mariscolandia Mar y Tierra', 
					'address'=>'Prol. Refugio 7 B, San Antonio', 
					'email'=>'jrmariscolandia@gmail.com', 
					'phone'=>'154 5727',
					'mobile_phone'=>'C 415 104 4521',  
					)); 

			Advertiser::create(array(
					'id'=>'654',
					'business_name'=>'CI Banco', 
					'address'=>'San Francisco 25, Centro',  
					'phone'=>'154 8797',
					'phone2'=>'152 4123',
					'mobile_phone'=>'152 5092',  
					'mobile_phone2'=>'154 7748', 
					)); 

			Advertiser::create(array(
					'id'=>'655',
					'business_name'=>'Podologo Guillermo Quiroz', 
					'address'=>'Insurgentes 71, Centro',  
					'phone'=>'122 3536',
					'mobile_phone'=>'C 415 109 7300',  
					)); 

			Advertiser::create(array(
					'id'=>'656',
					'business_name'=>'Dr. Arturo Barrera Bortoni', 
					'address'=>'Libramiento Jose Manuel Zavala PPKBZON 12-1', 
					'email'=>'dcajbb@hotmail.com', 
					'phone'=>'152 2233',
					'mobile_phone'=>'C 415 151 0110',  
					)); 

			Advertiser::create(array(
					'id'=>'657',
					'business_name'=>'Dental Age S.A de C.V.', 
					'address'=>'Lib. Jose Manuel Zavala 7, Centro', 
					'email'=>'drmac@dentalage.mx', 
					'phone'=>'185 8936',
					'phone2'=>'185 8181',  
					)); 

			Advertiser::create(array(
					'id'=>'658',
					'business_name'=>'Odonto-Imagen', 
					'address'=>'Libramiento Manuel Zavala PPKBZON 79', 
					'email'=>'hpatricioh10@gmail.com', 
					'phone'=>'154 8505',
					'mobile_phone'=>'C 415 103 0077',  
					'mobile_phone2'=>'C 415 153 5041', 
					)); 

			Advertiser::create(array(
					'id'=>'659',
					'business_name'=>'FrankFer', 
					'address'=>'Plaza Caracol Local 21 Planta alta', 
					'email'=>'ventas-frankfer@hotmail.com', 
					'phone'=>'154 8459',  
					)); 

			Advertiser::create(array(
					'id'=>'660',
					'business_name'=>'Dra. Janeth Jacqueline Possimg Hernandez', 
					'address'=>'Refugio Sur 7 San Antonio', 
					'email'=>'jany_1200@hotmail.com', 
					'phone'=>'',
					'mobile_phone'=>'C 415 107 9761',  
					)); 

			Advertiser::create(array(
					'id'=>'661',
					'business_name'=>'El Chapeton', 
					'address'=>'Libramiento Jose Manuel Zavala 137',  
					'phone'=>'154 0875',  
					'nextel'=>'ID 42*15*567432',  
					)); 

			Advertiser::create(array(
					'id'=>'662',
					'business_name'=>'Cosultoria Juridica', 
					'address'=>'Mesones 11 Altos', 
					'email'=>'consultoriajuridica26@gmail.com', 
					'phone'=>'154 8945',  
					)); 

			Advertiser::create(array(
					'id'=>'663',
					'business_name'=>'Royalty Real Estate', 
					'address'=>'Hacienda del molino 5 Fracc. Las Brisas', 
					'email'=>'Jorge@royalty-realestate.com', 
					'email2'=>'inmobiliaria.sma@gmail.com', 
					'phone'=>'110 2220',
					'phone2'=>'122 1848',
					'mobile_phone'=>'C 415 111 9409',  
					'mobile_phone2'=>'USA 956 205 0461', 
					'nextel'=>'ID 72*14*55016',  
					)); 

			Advertiser::create(array(
					'id'=>'664',
					'business_name'=>'La Bodega Steak & Pizza', 
					'address'=>'Carr. Guanajuato pasando 1 km vias del tren', 
					'email'=>'oism12@hotmail.com', 
					'phone'=>'150 7077',  
					)); 

			Advertiser::create(array(
					'id'=>'665',
					'business_name'=>'Consultoría y Defensa Jurídica', 
					'address'=>'Plaza San Antonio 4 Int. 3', 
					'email'=>'consultoriajuridicasma@gmail.com', 
					'phone'=>'',
					'mobile_phone'=>'C 415 115 9337',  
					'mobile_phone2'=>'C 415 151 4769', 
					)); 

			Advertiser::create(array(
					'id'=>'666',
					'business_name'=>'Optica Patria', 
					'address'=>'Av.1 de Mayo 13 Fracc. Ignacio Ramírez', 
					'email'=>'contacto@opticapatria.com', 
					'phone'=>'121 0066',
					'mobile_phone'=>'C 415 107 2282',  
					'web_page'=>'www.opticapatria.com', 
					)); 

			Advertiser::create(array(
					'id'=>'667',
					'business_name'=>'Hearing Aids of S.M.A',  
					'email'=>'centrodeaudicion1@hotmail.com', 
					'phone'=>'185 8365',
					'phone2'=>'152 2329',
					'mobile_phone'=>'C 4158 114 7868',  
					)); 

			Advertiser::create(array(
					'id'=>'668',
					'business_name'=>'Servicio Electrónico', 
					'address'=>'28 de Abril 28 A Norte, San Antonio', 
					'email'=>'jjatomo_sony@hotmail.es', 
					'phone'=>'121 3809',
					'mobile_phone'=>'C 415 102 7008',  
					)); 

			Advertiser::create(array(
					'id'=>'669',
					'business_name'=>'Camara Hiperbárica', 
					'address'=>'Libramiento Manuel Zavala 43', 
					'email'=>'szolezzi@lycos.com', 
					'phone'=>'152 7300',
					'mobile_phone'=>'C 415 153 4039',  
					)); 

			Advertiser::create(array(
					'id'=>'670',
					'business_name'=>'Los Arcangeles', 
					'address'=>'Salida Real a Queretaro 16',  
					'phone'=>'152 3335',  
					)); 

			Advertiser::create(array(
					'id'=>'671',
					'business_name'=>'Café de la Parroquia', 
					'address'=>'Jesus 11, Centro',  
					'phone'=>'152 31 61',  
					)); 

			Advertiser::create(array(
					'id'=>'672',
					'business_name'=>'Dental Medical Clinic', 
					'address'=>'Salida a Celaya 22', 
					'email'=>'clinicadoctoratejeda@yahoo.com.mx', 
					'phone'=>'152 2065',
					'phone2'=>'154 5152',
					'mobile_phone'=>'C 415 151 0167',  
					'web_page'=>'www.dentistasma.com', 
					)); 

			Advertiser::create(array(
					'id'=>'673',
					'business_name'=>'Alta Mecánica San Miguel', 
					'address'=>'Plan de Ayala 4  Luis Donaldo Colosio',  
					'phone'=>'',
					'mobile_phone'=>'C 415 112 4650',  
					)); 

			Advertiser::create(array(
					'id'=>'674',
					'business_name'=>'Atrium', 
					'address'=>'Fabrica La Aurora', 
					'email'=>'info@atriumcasaypatio.com', 
					'phone'=>'154 5590',  
					'web_page'=>'www.atriumcasaypatio.com', 
					)); 

			Advertiser::create(array(
					'id'=>'675',
					'business_name'=>'Dra. Karla Carter', 
					'address'=>'Sollano 62, Centro', 
					'email'=>'karlacarter@hotmail.com', 
					'phone'=>'',  
					'web_page'=>'www.smile.com.mx', 
					)); 

			Advertiser::create(array(
					'id'=>'676',
					'business_name'=>'Warren Hardy Spanish', 
					'address'=>'San Rafael 6, San Juan de Dios', 
					'email'=>'info@warrenhardy.com', 
					'phone'=>'154 4017',
					'phone2'=>'152 4728',
					'mobile_phone'=>'154 4905',  
					'web_page'=>'www.warrenhardy.com', 
					)); 

			Advertiser::create(array(
					'id'=>'677',
					'business_name'=>'Dra.Carla Archer', 
					'address'=>'Libramiento a Dolores 12 Clinica Tec 100', 
					'email'=>'carla_archer@yahoo.com', 
					'phone'=>'152 2329',
					'phone2'=>'152 2233',  
					)); 

			Advertiser::create(array(
					'id'=>'678',
					'business_name'=>'Ariadna Delsol', 
					'address'=>'Correo 3, Centro', 
					'email'=>'arikir21@gmail.com', 
					'phone'=>'154 4971',
					'mobile_phone'=>'C 415 103 0008',  
					'web_page'=>'www.sanmigueldeallenderealestate.com', 
					)); 

			Advertiser::create(array(
					'id'=>'679',
					'business_name'=>'Colibri Real Estate', 
					'address'=>'Codo 2, Centro', 
					'email'=>'susana.2@hotmail.com', 
					'phone'=>'121 1545',
					'mobile_phone'=>'C 415 566 4658',  
					'web_page'=>'www.sanmiguel-realty.com', 
					)); 

			Advertiser::create(array(
					'id'=>'680',
					'business_name'=>'El Salon', 
					'address'=>'Soledad 1',  
					'phone'=>'122 1792',
					'phone2'=>'122 1793',  
					'nextel'=>'ID 72*828070*1',  
					)); 

			Advertiser::create(array(
					'id'=>'681',
					'business_name'=>'Radio Taxi "Doña Mary"',   
					'phone'=>'152 4579',
					'phone2'=>'1223007',  
					'nextel'=>'ID 62*991057*2',  
					)); 

			Advertiser::create(array(
					'id'=>'682',
					'business_name'=>'Gorditas Don Ciro', 
					'address'=>'San Miguel-Dolores KM 55',  
					'phone'=>'',
					'mobile_phone'=>'415 113 3362',  
					)); 

			Advertiser::create(array(
					'id'=>'683',
					'business_name'=>'Notaría Pública 9', 
					'address'=>'Hidalgo 35, Centro', 
					'email'=>'eugeniagarcia@notaria9sma.com.mx', 
					'phone'=>'152 0249',
					'phone2'=>'152 2088',  
					)); 

			Advertiser::create(array(
					'id'=>'684',
					'business_name'=>'Lic. Eugenia García Zavala', 
					'address'=>'Hidalgo 35, Centro', 
					'email'=>'eugeniagarcia@notaria9sma.com.mx', 
					'phone'=>'152 0249',
					'phone2'=>'152 2088',  
					'web_page'=>'www.notaria9sma.com.mx', 
					)); 

			Advertiser::create(array(
					'id'=>'685',
					'business_name'=>'Numero 43',  
					'email'=>'no.43deli@gmail.com', 
					'phone'=>'121 2241',  
					)); 

			Advertiser::create(array(
					'id'=>'686',
					'business_name'=>'Bugambilia International Realty', 
					'address'=>'Correo 47, Centro', 
					'email'=>'sanmiguelhome@aol.com', 
					'phone'=>'152 1903',
					'mobile_phone'=>'C 415 124 3912',  
					'web_page'=>'www.bugambiliarealty.com', 
					)); 

			Advertiser::create(array(
					'id'=>'687',
					'business_name'=>'Codo 16', 
					'address'=>'Codo 16',  
					'phone'=>'152 2834',  
					)); 

			Advertiser::create(array(
					'id'=>'688',
					'business_name'=>'Rachel Horn Home', 
					'address'=>'Fabrica La Aurora Int. 3', 
					'email'=>'angeles@rachelhorn.com', 
					'phone'=>'154 8323',
					'mobile_phone'=>'310 295 0100',  
					'web_page'=>'www.rachelhorn.com', 
					)); 

			Advertiser::create(array(
					'id'=>'689',
					'business_name'=>'Despacho Juridico Contable', 
					'address'=>'Orizaba 48B San Antonio', 
					'email'=>'asefobarrera@gmail.com', 
					'phone'=>'154 9458',
					'phone2'=>'152 6978',  
					)); 

			Advertiser::create(array(
					'id'=>'690',
					'business_name'=>'Martha Baby Sitter',   
					'phone'=>'',
					'mobile_phone'=>'C 415 111 3591',  
					)); 

			Advertiser::create(array(
					'id'=>'691',
					'business_name'=>'Mozayik', 
					'address'=>'Zacateros 39', 
					'email'=>'erikhfigueroa@gmail.com', 
					'phone'=>'',
					'mobile_phone'=>'C 415 114 3560',  
					)); 

			Advertiser::create(array(
					'id'=>'692',
					'business_name'=>'Serenity Rentals & Mangement',  
					'email'=>'reservations@serenitysanmiguel.com', 
					'email2'=>'lauryzg@gmail.com', 
					'phone'=>'',
					'mobile_phone'=>'C 415 105 4458',  
					'mobile_phone2'=>'USA 970 300 4820', 
					)); 

			Advertiser::create(array(
					'id'=>'693',
					'business_name'=>'Bienes Raices Candela', 
					'address'=>'Recreo 13 A, Centro',  
					'phone'=>'152 1828',
					'phone2'=>'152 3579',
					'mobile_phone'=>'C 415 103 3834',  
					)); 

			Advertiser::create(array(
					'id'=>'694',
					'business_name'=>'Seco Soluciones', 
					'address'=>'Libramiento a Dolores 44 C, Ejido de Tirado', 
					'email'=>'ventasallende@imperseco.com.mx', 
					'phone'=>'121 00 99',  
					)); 

			Advertiser::create(array(
					'id'=>'695',
					'business_name'=>'Bistro ST MICHEL', 
					'address'=>'San Francisco 23, Centro', 
					'email'=>'stmichelbistro@gmail.com', 
					'phone'=>'121 9090',  
					)); 

			Advertiser::create(array(
					'id'=>'696',
					'business_name'=>'Select REAL ESTATE "Manolo Orta"', 
					'address'=>'Hidalgo 10, Centro', 
					'email'=>'manolorta@gmail.com', 
					'phone'=>'152 2322',
					'phone2'=>'154 9242',
					'mobile_phone'=>'USA (281) 914 4276',  
					'mobile_phone2'=>'C 415 117 3552', 
					'web_page'=>'www.selectrealestate.com.mx', 
					)); 

			Advertiser::create(array(
					'id'=>'697',
					'business_name'=>'Ulises Estética', 
					'address'=>'Santa Fe 1, Allende',  
					'phone'=>'121 0647',  
					)); 

			Advertiser::create(array(
					'id'=>'698',
					'business_name'=>'Terraza & Restaurante La Lúpita', 
					'address'=>'Hidalgo 5, Centro',  
					'phone'=>'',
					'phone2'=>'152 0308',  
					)); 

			Advertiser::create(array(
					'id'=>'699',
					'business_name'=>'Grupo Wespy', 
					'address'=>'Cardo 4, Centro', 
					'email'=>'info@sanmiguelexclusives.com', 
					'phone'=>'154 6332',
					'phone2'=>'120 0000',
					'mobile_phone'=>'415 153 3373',  
					'web_page'=>'www.sanmiguelexclusives.com', 
					)); 

			Advertiser::create(array(
					'id'=>'700',
					'business_name'=>'Estética Pavy', 
					'address'=>'16 de Septiembre, Allende', 
					'email'=>'esteticapavi@hotmail.com', 
					'phone'=>'154 4649',
					'mobile_phone'=>'415 113 6257',  
					)); 

			Advertiser::create(array(
					'id'=>'701',
					'business_name'=>'Cafe Negro', 
					'address'=>'Pila Seca 34, Centro', 
					'email'=>'cafenegro_sma@outlook.com', 
					'phone'=>'152 32 62',
					'mobile_phone'=>'415 566 4914',  
					)); 

			Advertiser::create(array(
					'id'=>'702',
					'business_name'=>'Arq. Felicia Alarcón de Leon',  
					'email'=>'fela_ad@hotmail.com', 
					'phone'=>'',
					'mobile_phone'=>'415 115 4837',  
					)); 

			Advertiser::create(array(
					'id'=>'703',
					'business_name'=>'Frutas y Verduras Lopez', 
					'address'=>'Volanteros 5, Centro', 
					'email'=>'frute_lopez@outlook.es', 
					'phone'=>'124 3487',
					'mobile_phone'=>'C 415 566 0240',  
					)); 

			Advertiser::create(array(
					'id'=>'704',
					'business_name'=>'San Miguel Consultants', 
					'address'=>'Zacateros 81 Ancha de San Antonio 20 Int 19', 
					'email'=>'gabrielachalela@gmail.com', 
					'phone'=>'152 0305',
					'mobile_phone'=>'C 415 107 9977',  
					)); 

			Advertiser::create(array(
					'id'=>'705',
					'business_name'=>'Consultorio Médico Especializado en Psiquiatría', 
					'address'=>'Libramiento Jose Manuel Zavala PPKBZON 12', 
					'email'=>'argentumz@outlook.com', 
					'phone'=>'152 2233',
					'phone2'=>'152 2329',  
					)); 

			Advertiser::create(array(
					'id'=>'706',
					'business_name'=>'Dra. Maria Eugenia Beltran T.', 
					'address'=>'Calzada de la Presa 43', 
					'email'=>'drabeltran@hotmail.com', 
					'phone'=>'152 2694',
					'mobile_phone'=>'C 415 566 4621',  
					)); 

			Advertiser::create(array(
					'id'=>'707',
					'business_name'=>'Casa Tierra Negra', 
					'address'=>'Zacateros 81', 
					'email'=>'casatierranegra@gmail.com', 
					'phone'=>'152 0305',  
					'web_page'=>'www.casatierra.com', 
					)); 

			Advertiser::create(array(
					'id'=>'708',
					'business_name'=>'Kokoxcalli', 
					'address'=>'Libramiento Jose Manuel Zavala Zavala 79', 
					'email'=>'physio.sma@gmail.com', 
					'phone'=>'150 7660',  
					)); 

			Advertiser::create(array(
					'id'=>'709',
					'business_name'=>'Hotel Villa Mirasol', 
					'address'=>'Pila Seca 35', 
					'email'=>'reservaciones@villamirasolhotel.com', 
					'phone'=>'152 8057',
					'phone2'=>'152 6685',  
					'web_page'=>'www.villamirasolhotel.com', 
					)); 

			Advertiser::create(array(
					'id'=>'710',
					'business_name'=>'Sushi Gami', 
					'address'=>'Salida a Celaya frente a 1er Gas, sobre Ancha de San Antonio', 
					'email'=>'ocgami@gmail.com', 
					'phone'=>'',
					'mobile_phone'=>'C 415 109 1199',  
					)); 

			Advertiser::create(array(
					'id'=>'711',
					'business_name'=>'Hansen´s Bar & Grill', 
					'address'=>'Calzada de La Aurora 12', 
					'email'=>'hansen´sbargrill@yahoo.com', 
					'phone'=>'152 8203',  
					)); 

			Advertiser::create(array(
					'id'=>'712',
					'business_name'=>'Garrison & Garrison Books / Spanish 205', 
					'address'=>'Hidalgo 26, Centro', 
					'email'=>'garrison@hotmail.com', 
					'phone'=>'',
					'mobile_phone'=>'C 415 109 7387',  
					'web_page'=>'www.stuffmexicanslikeblog.wrodpress.com', 
					)); 

			Advertiser::create(array(
					'id'=>'713',
					'business_name'=>'13 Cielos Cocina  Mestiza de Autor', 
					'address'=>'Correo 34, Centro', 
					'email'=>'13cielosgte@gmail.com', 
					'phone'=>'152 0053',  
					)); 

			Advertiser::create(array(
					'id'=>'714',
					'business_name'=>'Eye Center', 
					'address'=>'Libramiento Manuel Zavala 160 Plaza del Angel, Local 112', 
					'email'=>'margacam2@gmail.com', 
					'phone'=>'152 8489',  
					)); 

			Advertiser::create(array(
					'id'=>'715',
					'business_name'=>'D´Luxe Hair Salon & Spa', 
					'address'=>'San Francisco 51, Centro',  
					'phone'=>'121 3899',
					'mobile_phone'=>'C 415 107 6014',  
					)); 

			Advertiser::create(array(
					'id'=>'716',
					'business_name'=>'Mojarro Bienes Raices', 
					'address'=>'Hernandez Macias 76 A', 
					'email'=>'info@mojarro.org', 
					'email2'=>'mojarro_realty@hotmail.com', 
					'phone'=>'152 2388',
					'phone2'=>'154 1001',
					'mobile_phone'=>'152 3167',  
					)); 

			Advertiser::create(array(
					'id'=>'717',
					'business_name'=>'Katiana Mojarro Rodriguez', 
					'address'=>'Hernandez Macias 76 A, Centro', 
					'email'=>'info@mojarro.org', 
					'phone'=>'',  
					'web_page'=>'www.mojarro.org', 
					)); 

			Advertiser::create(array(
					'id'=>'718',
					'business_name'=>'Hotel Suites Dalí', 
					'address'=>'Homobono 23 A, Centro', 
					'email'=>'info@suitesdali.com', 
					'phone'=>'',
					'mobile_phone'=>'C 415 154 5637',  
					'web_page'=>'www.suitesdali.com', 
					)); 

			Advertiser::create(array(
					'id'=>'719',
					'business_name'=>'Javier Mojarro Rodriguez', 
					'address'=>'Aldama 48', 
					'email'=>'javier.mojarro@gmail.com', 
					'phone'=>'152 3767',
					'phone2'=>'152 2388',  
					'web_page'=>'www.mojarro.org', 
					)); 

			Advertiser::create(array(
					'id'=>'720',
					'business_name'=>'Choco Cake',   
					'phone'=>'',
					'mobile_phone'=>'C 415 107 6674',  
					)); 

			Advertiser::create(array(
					'id'=>'721',
					'business_name'=>'Odontología Restauradora Integral y Prótesis Bucal', 
					'address'=>'San Francisco 51 B', 
					'email'=>'dra.hernandezcortes@gmail.com', 
					'phone'=>'',
					'mobile_phone'=>'C 415 117 21 83',  
					)); 

			Advertiser::create(array(
					'id'=>'722',
					'business_name'=>'Axa San Miguel Management', 
					'address'=>'Hernández Macias 111', 
					'email'=>'graciela@sanmiguel-mgmt.com', 
					'phone'=>'152 4416',
					'phone2'=>'152 0187',  
					'web_page'=>'www.sanmiguel-mgmt.com', 
					)); 

			Advertiser::create(array(
					'id'=>'723',
					'business_name'=>'Blocker Peggy San Miguel Management', 
					'address'=>'Hernández Macias 111', 
					'email'=>'ventas@sanmiguel-mgmt.com', 
					'phone'=>'1552 44 16',
					'phone2'=>'152 0187',
					'mobile_phone'=>'US 832 364 6307',  
					'web_page'=>'www.sanmiguel-mgmt.com', 
					)); 

			Advertiser::create(array(
					'id'=>'724',
					'business_name'=>'Lily´s Sabor a México', 
					'address'=>'Recreo 8 B', 
					'email'=>'lilyz.rest@gmail.com', 
					'phone'=>'152 6009',  
					)); 

			Advertiser::create(array(
					'id'=>'725',
					'business_name'=>'Medica Santa Carmen', 
					'address'=>'Libramiento Jose Manuel Zavala 160 Plaza El Angel', 
					'email'=>'info@santacarmen.mx', 
					'phone'=>'120 6333',
					'mobile_phone'=>'US 210 587 7980',  
					'web_page'=>'www.medicasantacarmen.com', 
					)); 

			Advertiser::create(array(
					'id'=>'726',
					'business_name'=>'RYSE', 
					'address'=>'Plaza 4Quince Carr. A Celaya 3 Local 1', 
					'email'=>'laura_yañez@ryse.com.mx', 
					'phone'=>'120 2106',  
					'web_page'=>'www.ryse.com.mx', 
					)); 

			Advertiser::create(array(
					'id'=>'727',
					'business_name'=>'San Miguel Patio Decor', 
					'address'=>'Calzada La Aurora Local 3A-1', 
					'email'=>'ventas@sanmiguelpatiodecor.com.mx', 
					'phone'=>'150 73 92',
					'phone2'=>'122 1012',  
					'nextel'=>'ID 72*15*17031',  
					'web_page'=>'www.sanmiguelpatiodecor.com.mx', 
					)); 

			Advertiser::create(array(
					'id'=>'728',
					'business_name'=>'Restaurante Do Brazil Brazza', 
					'address'=>'Carr. San Miguel - Celaya 3  Plaza 4Quince Local 14,15,16',  
					'phone'=>'120 2015',
					'phone2'=>'120 2030',  
					)); 

			Advertiser::create(array(
					'id'=>'729',
					'business_name'=>'Lino Sanabria', 
					'address'=>'Antonio Vivero 2 Fracc. Insurgentes',  
					'phone'=>'152 5622',
					'mobile_phone'=>'C 415 101 0439',  
					)); 

			Advertiser::create(array(
					'id'=>'730',
					'business_name'=>'INSHALA', 
					'address'=>'Aldama 30, Centro', 
					'email'=>'carol@inshalaimports.com', 
					'phone'=>'152 8355',
					'mobile_phone'=>'C 415 111 9855',  
					'web_page'=>'www.inshalaimports.com', 
					)); 

			Advertiser::create(array(
					'id'=>'731',
					'business_name'=>'El Palomar Hotel', 
					'address'=>'San Francisco 57',  
					'phone'=>'152 0656',  
					)); 

			Advertiser::create(array(
					'id'=>'732',
					'business_name'=>'Hotel Casa Correo', 
					'address'=>'Correo 38, Centro',  
					'phone'=>'152 0209',  
					)); 

			Advertiser::create(array(
					'id'=>'733',
					'business_name'=>'San Miguel de Allende Hotel.com', 
					'address'=>'Portal Allende 8',  
					'phone'=>'152 8889',  
					)); 

			Advertiser::create(array(
					'id'=>'734',
					'business_name'=>'Casa Luna', 
					'address'=>'Quebrada 117', 
					'email'=>'reservaciones@casaluna.com', 
					'phone'=>'152 1117',  
					'web_page'=>'www.sanmigueldeallendehotel.com', 
					)); 

			Advertiser::create(array(
					'id'=>'735',
					'business_name'=>'Hotel del Portal', 
					'address'=>'Portal Allende 8', 
					'email'=>'reservaciones@hoteldelportalsanmiguel.com', 
					'phone'=>'152 8889',  
					'web_page'=>'www.sanmigueldeallendehotel.com', 
					)); 

			Advertiser::create(array(
					'id'=>'736',
					'business_name'=>'Hotel Los Agaves', 
					'address'=>'Ancha de San Antonio 13 A', 
					'email'=>'reservaciones@losagaveshotel.com', 
					'phone'=>'152 2916',  
					'web_page'=>'www.sanmigueldeallendehotel.com', 
					)); 

			Advertiser::create(array(
					'id'=>'737',
					'business_name'=>'Kukuruku Green Concept Hotel', 
					'address'=>'Pila Seca 11', 
					'email'=>'reservaciones@kukuruku.com.mx', 
					'phone'=>'154 4006',  
					'web_page'=>'www.sanmigueldeallendehotel.com', 
					)); 

			Advertiser::create(array(
					'id'=>'738',
					'business_name'=>'Casa Linda Hotel Boutique', 
					'address'=>'Mesones 101', 
					'email'=>'reservaciones@hotelcasalinda.com', 
					'phone'=>'152 1054',
					'phone2'=>'154 4007',  
					'web_page'=>'www.sanmigueldeallendehotels.com', 
					)); 

			Advertiser::create(array(
					'id'=>'739',
					'business_name'=>'Casa Quetzal / Hotel Boutique', 
					'address'=>'Hospicio 34', 
					'email'=>'reservaciones@casaquetzal.com', 
					'phone'=>'152 0501',
					'phone2'=>'152 4961',  
					'web_page'=>'www.sanmigueldeallendehotel.com', 
					)); 

			Advertiser::create(array(
					'id'=>'740',
					'business_name'=>'Muelle 13', 
					'address'=>'Stirling Dickinson 15  San Antonio',  
					'phone'=>'',
					'mobile_phone'=>'C 415 107 8931',  
					)); 

			Advertiser::create(array(
					'id'=>'741',
					'business_name'=>'Arq. Olga Adriana Hernández Flores', 
					'address'=>'Calle Bellavista 4 Fracc. Bella Vista', 
					'email'=>'arquyrest@hotmail.com', 
					'phone'=>'152 3983',  
					)); 

			Advertiser::create(array(
					'id'=>'742',
					'business_name'=>'Grupo Torres Carrera', 
					'address'=>'Jacaranda 19, Jardines II',  
					'phone'=>'154 7221',  
					)); 

			Advertiser::create(array(
					'id'=>'743',
					'business_name'=>'Chef Prats Banquetes',  
					'email'=>'chefprats@hotmail.com', 
					'phone'=>'',
					'mobile_phone'=>'C 415 105 6635',  
					)); 

			Advertiser::create(array(
					'id'=>'744',
					'business_name'=>'La Burger Express', 
					'address'=>'Jesus 5', 
					'email'=>'walter@laburger.com.mx', 
					'phone'=>'154 7730',  
					'web_page'=>'www.laburger.com.mx', 
					)); 

			Advertiser::create(array(
					'id'=>'745',
					'business_name'=>'Sagesse',   
					'phone'=>'',
					'mobile_phone'=>'C 415 112 3645',  
					)); 

			Advertiser::create(array(
					'id'=>'746',
					'business_name'=>'Grupo Dental Nieto',  
					'email'=>'grupodentalnieto@gmail.com', 
					'phone'=>'154 7500',  
					)); 

			Advertiser::create(array(
					'id'=>'747',
					'business_name'=>'Eduardo Arias Architect', 
					'address'=>'La Escondida 1 Fracc. Los Frailes', 
					'email'=>'eduardoarias@telmexmail.co', 
					'email2'=>'architecteduardoarias.mx', 
					'phone'=>'',
					'mobile_phone'=>'C 415 153 5204',  
					'web_page'=>'www.eduardoarias.mx', 
					)); 

			Advertiser::create(array(
					'id'=>'748',
					'business_name'=>'Casa de Europa en México', 
					'address'=>'San Francisco 23, Centro', 
					'email'=>'info@casadeeuropa.org.mx', 
					'phone'=>'121 9090',  
					)); 

			Advertiser::create(array(
					'id'=>'749',
					'business_name'=>'Liceo de la Lengua', 
					'address'=>'Callejon del Pueblito 5', 
					'email'=>'liceodelalengua@yahoo.com', 
					'phone'=>'121 25 35',  
					'web_page'=>'www.liceodelalengua.com', 
					)); 

			Advertiser::create(array(
					'id'=>'750',
					'business_name'=>'Buenos Aires Bistro', 
					'address'=>'Mesones 62, Centro',  
					'phone'=>'154 6390',  
					)); 

			Advertiser::create(array(
					'id'=>'751',
					'business_name'=>'Patria Sur Cocina de Barrio', 
					'address'=>'Blvd. Bernardo Quintana 5410 Local 2 A Plaza Boulevares', 
					'email'=>'nacho.patriasur@gmail.com', 
					'phone'=>'',  
					'web_page'=>'www.patriasur.com.mx', 
					)); 

			Advertiser::create(array(
					'id'=>'752',
					'business_name'=>'Colibri Real Estate', 
					'address'=>'Codo 2, Centro', 
					'email'=>'susana.2@hotmail.com', 
					'phone'=>'121 1545',
					'mobile_phone'=>'C 415 566 4658',  
					'web_page'=>'www.sanmiguel-realty.com', 
					)); 

			Advertiser::create(array(
					'id'=>'753',
					'business_name'=>'Academia Chauvet', 
					'address'=>'Ancha de San Antonio 15 dentro del Hotel Posada La Aldea',  
					'phone'=>'',
					'mobile_phone'=>'C 415 103 0614',  
					)); 

			Advertiser::create(array(
					'id'=>'754',
					'business_name'=>'Sugar Spa', 
					'address'=>'Calle de La Gracia 4 Barrio de La Aldea', 
					'email'=>'susichau8@hotmail.com', 
					'phone'=>'152 0021',  
					)); 

			Advertiser::create(array(
					'id'=>'755',
					'business_name'=>'Central Mexicana de Servicios Generales de Alcohólicos Anónimos A.C', 
					'address'=>'Estrellita 14, Guadalupe',  
					'phone'=>'',
					'mobile_phone'=>'C 415 151 8313',  
					'mobile_phone2'=>'C 415 153 3791', 
					)); 

			Advertiser::create(array(
					'id'=>'756',
					'business_name'=>'Lic. Ian Taylor Clement', 
					'address'=>'Pila Seca 3, Centro', 
					'email'=>'elementattorney@gmail.com', 
					'phone'=>'127 0894',  
					'nextel'=>'ID 42*665137*2',  
					'web_page'=>'www.elementattorney.com', 
					)); 

			Advertiser::create(array(
					'id'=>'757',
					'business_name'=>'Clinicos de San Miguel', 
					'address'=>'Salida a Celaya 27', 
					'email'=>'alexlg73@@hotmail.com', 
					'phone'=>'154 6380',  
					'web_page'=>'www.clinicosdesanmiguel.com', 
					)); 

			Advertiser::create(array(
					'id'=>'758',
					'business_name'=>'Central de Alarmas Sirce', 
					'address'=>'Sollano 4. Centro', 
					'email'=>'sirce91@prodigy.net.mx', 
					'phone'=>'152 5819',
					'phone2'=>'152 6256',  
					'web_page'=>'www.sirce.com.mx', 
					)); 

			Advertiser::create(array(
					'id'=>'759',
					'business_name'=>'La Biblioteca de San Miguel de Allende A.C.', 
					'address'=>'Insurgentes 25, Relox 50 A', 
					'email'=>'bpsmac@prodigy.net.mx', 
					'phone'=>'152 0293',
					'phone2'=>'152 7305',
					'mobile_phone'=>'152 7305',  
					'mobile_phone2'=>'152 7048', 
					'nextel'=>'152 3770',  
					'web_page'=>'www.bibliotecasma.com', 
					)); 

			Advertiser::create(array(
					'id'=>'760',
					'business_name'=>'Zagar Construcciones S.C',   
					'phone'=>'154 4948',
					'phone2'=>'152 1912',
					'mobile_phone'=>'C 415 151 0838',  
					)); 

			Advertiser::create(array(
					'id'=>'761',
					'business_name'=>'Blocker Peggy San Miguel Management', 
					'address'=>'Hernández Macias 111', 
					'email'=>'ventas@sanmiguel-mgmt.com', 
					'phone'=>'1552 44 16',
					'phone2'=>'152 0187',
					'mobile_phone'=>'US 832 364 6307',  
					'web_page'=>'www.sanmiguel-mgmt.com', 
					)); 

			Advertiser::create(array(
					'id'=>'762',
					'business_name'=>'San Miguel Consultants', 
					'address'=>'Zacateros 81 Ancha de San Antonio 20 Int 19', 
					'email'=>'gabrielachalela@gmail.com', 
					'phone'=>'152 0305',
					'mobile_phone'=>'C 415 107 9977',  
					)); 

	}
}