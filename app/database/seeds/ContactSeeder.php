<?php
class ContactSeeder extends Seeder{

	public function run(){

		DB::table('contact')->delete();

		Contact::create(array(
							'id'=>'1',
							'address'=>'Ancha de san Antonio 20, Local 5 San Miguel de Allende, Gto. Mexico',
							'phones'=>'(415) 152 7072, 154 4951', 
							'email'=>'info@juarde.com.mx', 
					)); 
	}
}
