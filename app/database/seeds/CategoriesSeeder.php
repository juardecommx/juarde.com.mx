<?php
class CategoriesSeeder extends Seeder{

	public function run(){

		DB::table('categories')->delete();
		
		Category::create(array(
					'id'=>'1',
					'category'=>'Accessories', 
					)); 
		Category::create(array(
					'id'=>'2',
					'category'=>'Accountants', 
					)); 
		Category::create(array(
					'id'=>'3',
					'category'=>'Accountants (Auditing)', 
					)); 
		Category::create(array(
					'id'=>'4',
					'category'=>'Accountants (Bilingual)', 
					)); 
		Category::create(array(
					'id'=>'5',
					'category'=>'Acupuncture', 
					)); 
		Category::create(array(
					'id'=>'6',
					'category'=>'Adult Shops', 
					)); 
		Category::create(array(
					'id'=>'7',
					'category'=>'Adventure Tourism (Motorcycle Tours & Rentals)', 
					)); 
		Category::create(array(
					'id'=>'8',
					'category'=>'Advertising [see also Design, Graphic, Web Publicity]', 
					)); 
		Category::create(array(
					'id'=>'9',
					'category'=>'Aerial Photography', 
					)); 
		Category::create(array(
					'id'=>'10',
					'category'=>'Air Conditioning', 
					)); 
		Category::create(array(
					'id'=>'11',
					'category'=>'Airport Transportation [see also Drivers for Hire]', 
					)); 
		Category::create(array(
					'id'=>'12',
					'category'=>'Alarm Systems', 
					)); 
		Category::create(array(
					'id'=>'13',
					'category'=>'Alcoholism Treatment', 
					)); 
		Category::create(array(
					'id'=>'14',
					'category'=>'Alternative Medicine', 
					)); 
		Category::create(array(
					'id'=>'15',
					'category'=>'Aluminum Doors & Windows [see also also Ironwork]', 
					)); 
		Category::create(array(
					'id'=>'16',
					'category'=>'Ambulance Services', 
					)); 
		Category::create(array(
					'id'=>'17',
					'category'=>'Animal Hospitals [see also Veterinarians]', 
					)); 
		Category::create(array(
					'id'=>'18',
					'category'=>'Antiques', 
					)); 
		Category::create(array(
					'id'=>'19',
					'category'=>'Appraisal Services', 
					)); 
		Category::create(array(
					'id'=>'20',
					'category'=>'Aquariums', 
					)); 
		Category::create(array(
					'id'=>'21',
					'category'=>'Architects & Designers', 
					)); 
		Category::create(array(
					'id'=>'22',
					'category'=>'Architectural Restoration', 
					)); 
		Category::create(array(
					'id'=>'23',
					'category'=>'Aromatherapy', 
					)); 
		Category::create(array(
					'id'=>'24',
					'category'=>'Art', 
					)); 
		Category::create(array(
					'id'=>'25',
					'category'=>'Art & Decoration', 
					)); 
		Category::create(array(
					'id'=>'26',
					'category'=>'Art (Contemporary)', 
					)); 
		Category::create(array(
					'id'=>'27',
					'category'=>'Art (Folk)', 
					)); 
		Category::create(array(
					'id'=>'28',
					'category'=>'Art (Paintings)', 
					)); 
		Category::create(array(
					'id'=>'29',
					'category'=>'Art (Popular)', 
					)); 
		Category::create(array(
					'id'=>'30',
					'category'=>'Art Classes', 
					)); 
		Category::create(array(
					'id'=>'31',
					'category'=>'Art Galleries [see also Art]', 
					)); 
		Category::create(array(
					'id'=>'32',
					'category'=>'Art Studios', 
					)); 
		Category::create(array(
					'id'=>'33',
					'category'=>'Art Supplies', 
					)); 
		Category::create(array(
					'id'=>'34',
					'category'=>'Assisted Living', 
					)); 
		Category::create(array(
					'id'=>'35',
					'category'=>'Attorneys [see also Lawyers]', 
					)); 
		Category::create(array(
					'id'=>'36',
					'category'=>'Audio Systems', 
					)); 
		Category::create(array(
					'id'=>'37',
					'category'=>'Auto Body Work & Collision Repair', 
					)); 
		Category::create(array(
					'id'=>'38',
					'category'=>'Auto Electric', 
					)); 
		Category::create(array(
					'id'=>'39',
					'category'=>'Automobile (Supplies & Service)', 
					)); 
		Category::create(array(
					'id'=>'40',
					'category'=>'Automobile Insurance', 
					)); 
		Category::create(array(
					'id'=>'41',
					'category'=>'Automobile Mechanics', 
					)); 
		Category::create(array(
					'id'=>'42',
					'category'=>'Automotive Maintenance', 
					)); 
		Category::create(array(
					'id'=>'43',
					'category'=>'Baby Products', 
					)); 
		Category::create(array(
					'id'=>'45',
					'category'=>'Bakeries', 
					)); 
		Category::create(array(
					'id'=>'44',
					'category'=>'Bakeries', 
					)); 
		Category::create(array(
					'id'=>'46',
					'category'=>'Banks', 
					)); 
		Category::create(array(
					'id'=>'47',
					'category'=>'Banquet & Event Venues', 
					)); 
		Category::create(array(
					'id'=>'48',
					'category'=>'Barber Shops', 
					)); 
		Category::create(array(
					'id'=>'49',
					'category'=>'Bars', 
					)); 
		Category::create(array(
					'id'=>'50',
					'category'=>'Bars (Tapas & Wine)', 
					)); 
		Category::create(array(
					'id'=>'51',
					'category'=>'Bath & Body Care', 
					)); 
		Category::create(array(
					'id'=>'52',
					'category'=>'Bathroom Fixtures', 
					)); 
		Category::create(array(
					'id'=>'53',
					'category'=>'Bazaars', 
					)); 
		Category::create(array(
					'id'=>'54',
					'category'=>'Beauty Products', 
					)); 
		Category::create(array(
					'id'=>'55',
					'category'=>'Beauty Salons [see also Hair Stylists]', 
					)); 
		Category::create(array(
					'id'=>'56',
					'category'=>'Bed & Breakfast', 
					)); 
		Category::create(array(
					'id'=>'57',
					'category'=>'Bedding & Linens', 
					)); 
		Category::create(array(
					'id'=>'58',
					'category'=>'Beer', 
					)); 
		Category::create(array(
					'id'=>'59',
					'category'=>'Bicycle Accessories', 
					)); 
		Category::create(array(
					'id'=>'60',
					'category'=>'Bicycle Rentals & Sales', 
					)); 
		Category::create(array(
					'id'=>'61',
					'category'=>'Bird Watching', 
					)); 
		Category::create(array(
					'id'=>'62',
					'category'=>'Blood Banks', 
					)); 
		Category::create(array(
					'id'=>'63',
					'category'=>'Books', 
					)); 
		Category::create(array(
					'id'=>'64',
					'category'=>'Boots', 
					)); 
		Category::create(array(
					'id'=>'65',
					'category'=>'Boutiques', 
					)); 
		Category::create(array(
					'id'=>'66',
					'category'=>'Brakes & Suspensions', 
					)); 
		Category::create(array(
					'id'=>'67',
					'category'=>'Burial Services', 
					)); 
		Category::create(array(
					'id'=>'68',
					'category'=>'Bus Lines', 
					)); 
		Category::create(array(
					'id'=>'69',
					'category'=>'Butchers', 
					)); 
		Category::create(array(
					'id'=>'70',
					'category'=>'Cable TV', 
					)); 
		Category::create(array(
					'id'=>'71',
					'category'=>'Cafés [see also Restaurants (Cafés)]', 
					)); 
		Category::create(array(
					'id'=>'72',
					'category'=>'Camping', 
					)); 
		Category::create(array(
					'id'=>'73',
					'category'=>'Candles', 
					)); 
		Category::create(array(
					'id'=>'74',
					'category'=>'Candy Stores', 
					)); 
		Category::create(array(
					'id'=>'75',
					'category'=>'Car Dealers [see also Used Car Dealers]', 
					)); 
		Category::create(array(
					'id'=>'76',
					'category'=>'Car Wash', 
					)); 
		Category::create(array(
					'id'=>'77',
					'category'=>'Carpenters & Ebanists', 
					)); 
		Category::create(array(
					'id'=>'78',
					'category'=>'Carpet Cleaning', 
					)); 
		Category::create(array(
					'id'=>'79',
					'category'=>'Carpets & Rugs', 
					)); 
		Category::create(array(
					'id'=>'80',
					'category'=>'Catering', 
					)); 
		Category::create(array(
					'id'=>'81',
					'category'=>'Ceramic Tile', 
					)); 
		Category::create(array(
					'id'=>'82',
					'category'=>'Ceramics', 
					)); 
		Category::create(array(
					'id'=>'83',
					'category'=>'Charitable Organizations', 
					)); 
		Category::create(array(
					'id'=>'84',
					'category'=>'Cheese', 
					)); 
		Category::create(array(
					'id'=>'85',
					'category'=>'Chiropractors', 
					)); 
		Category::create(array(
					'id'=>'86',
					'category'=>'Chocolate Stores', 
					)); 
		Category::create(array(
					'id'=>'87',
					'category'=>'Churches', 
					)); 
		Category::create(array(
					'id'=>'88',
					'category'=>'Classes (Schools)', 
					)); 
		Category::create(array(
					'id'=>'89',
					'category'=>'Classes (Workshops)', 
					)); 
		Category::create(array(
					'id'=>'90',
					'category'=>'Cleaning Products', 
					)); 
		Category::create(array(
					'id'=>'91',
					'category'=>'Clothing', 
					)); 
		Category::create(array(
					'id'=>'92',
					'category'=>'Computers (Sales & Maintenance)', 
					)); 
		Category::create(array(
					'id'=>'94',
					'category'=>'Conference Rooms', 
					)); 
		Category::create(array(
					'id'=>'93',
					'category'=>'Conference Rooms', 
					)); 
		Category::create(array(
					'id'=>'95',
					'category'=>'Consignment Shops', 
					)); 
		Category::create(array(
					'id'=>'96',
					'category'=>'Construction', 
					)); 
		Category::create(array(
					'id'=>'97',
					'category'=>'Construction (Finishing)', 
					)); 
		Category::create(array(
					'id'=>'98',
					'category'=>'Construction Materials', 
					)); 
		Category::create(array(
					'id'=>'99',
					'category'=>'Construction Tiles', 
					)); 
		Category::create(array(
					'id'=>'100',
					'category'=>'Convenience Stores', 
					)); 
		Category::create(array(
					'id'=>'101',
					'category'=>'Cooking Classes', 
					)); 
		Category::create(array(
					'id'=>'102',
					'category'=>'Courier Services', 
					)); 
		Category::create(array(
					'id'=>'103',
					'category'=>'Craft Supplies', 
					)); 
		Category::create(array(
					'id'=>'104',
					'category'=>'Cremation Services', 
					)); 
		Category::create(array(
					'id'=>'105',
					'category'=>'Currency Exchange', 
					)); 
		Category::create(array(
					'id'=>'106',
					'category'=>'Curtains', 
					)); 
		Category::create(array(
					'id'=>'107',
					'category'=>'Dairy Products', 
					)); 
		Category::create(array(
					'id'=>'108',
					'category'=>'Dance Classes', 
					)); 
		Category::create(array(
					'id'=>'109',
					'category'=>'Delicatessen', 
					)); 
		Category::create(array(
					'id'=>'110',
					'category'=>'Delivery Services', 
					)); 
		Category::create(array(
					'id'=>'112',
					'category'=>'Dental Cleaning & Bleaching', 
					)); 
		Category::create(array(
					'id'=>'111',
					'category'=>'Dental Cleaning & Bleaching', 
					)); 
		Category::create(array(
					'id'=>'113',
					'category'=>'Dental Implants', 
					)); 
		Category::create(array(
					'id'=>'114',
					'category'=>'Dentists', 
					)); 
		Category::create(array(
					'id'=>'115',
					'category'=>'Department Stores', 
					)); 
		Category::create(array(
					'id'=>'116',
					'category'=>'Depilation', 
					)); 
		Category::create(array(
					'id'=>'117',
					'category'=>'Dermatological Pharmacies', 
					)); 
		Category::create(array(
					'id'=>'118',
					'category'=>'Dermatologists', 
					)); 
		Category::create(array(
					'id'=>'119',
					'category'=>'Design & Decor [see also Interior Design & Decor]', 
					)); 
		Category::create(array(
					'id'=>'120',
					'category'=>'Dog Grooming', 
					)); 
		Category::create(array(
					'id'=>'121',
					'category'=>'Dog Obedience Training', 
					)); 
		Category::create(array(
					'id'=>'122',
					'category'=>'Drivers for Hire', 
					)); 
		Category::create(array(
					'id'=>'123',
					'category'=>'Drugstores', 
					)); 
		Category::create(array(
					'id'=>'124',
					'category'=>'Drugstores (Delivery Service)', 
					)); 
		Category::create(array(
					'id'=>'125',
					'category'=>'Dry Cleaners', 
					)); 
		Category::create(array(
					'id'=>'126',
					'category'=>'Drywalling', 
					)); 
		Category::create(array(
					'id'=>'127',
					'category'=>'Ecotechnology Housing', 
					)); 
		Category::create(array(
					'id'=>'128',
					'category'=>'Electric Doors', 
					)); 
		Category::create(array(
					'id'=>'129',
					'category'=>'Electrical Appliance Repair', 
					)); 
		Category::create(array(
					'id'=>'130',
					'category'=>'Electrical Appliances', 
					)); 
		Category::create(array(
					'id'=>'131',
					'category'=>'Electrical Supplies', 
					)); 
		Category::create(array(
					'id'=>'132',
					'category'=>'Electricians', 
					)); 
		Category::create(array(
					'id'=>'133',
					'category'=>'Electrified Fences', 
					)); 
		Category::create(array(
					'id'=>'134',
					'category'=>'Electronics', 
					)); 
		Category::create(array(
					'id'=>'135',
					'category'=>'Emergency Alert Systems', 
					)); 
		Category::create(array(
					'id'=>'136',
					'category'=>'Emissions Control Centers', 
					)); 
		Category::create(array(
					'id'=>'137',
					'category'=>'Endocrinologists', 
					)); 
		Category::create(array(
					'id'=>'138',
					'category'=>'Endodontists', 
					)); 
		Category::create(array(
					'id'=>'139',
					'category'=>'Event Halls [see also Banquet & Event Venues]', 
					)); 
		Category::create(array(
					'id'=>'140',
					'category'=>'Event Planning', 
					)); 
		Category::create(array(
					'id'=>'141',
					'category'=>'Excursions / Hiking', 
					)); 
		Category::create(array(
					'id'=>'142',
					'category'=>'Fabrics & Textiles', 
					)); 
		Category::create(array(
					'id'=>'143',
					'category'=>'Facials', 
					)); 
		Category::create(array(
					'id'=>'144',
					'category'=>'Fans (Ceiling)', 
					)); 
		Category::create(array(
					'id'=>'145',
					'category'=>'Farms (Organic)', 
					)); 
		Category::create(array(
					'id'=>'146',
					'category'=>'Fashion', 
					)); 
		Category::create(array(
					'id'=>'147',
					'category'=>'Fast Food', 
					)); 
		Category::create(array(
					'id'=>'148',
					'category'=>'Fences & Wire Mesh', 
					)); 
		Category::create(array(
					'id'=>'149',
					'category'=>'Fertilization', 
					)); 
		Category::create(array(
					'id'=>'150',
					'category'=>'Financial Planning', 
					)); 
		Category::create(array(
					'id'=>'151',
					'category'=>'Financial Services', 
					)); 
		Category::create(array(
					'id'=>'152',
					'category'=>'Fish & Seafood', 
					)); 
		Category::create(array(
					'id'=>'153',
					'category'=>'Fitness', 
					)); 
		Category::create(array(
					'id'=>'154',
					'category'=>'Flooring', 
					)); 
		Category::create(array(
					'id'=>'155',
					'category'=>'Flower Shops', 
					)); 
		Category::create(array(
					'id'=>'156',
					'category'=>'Forage Suppliers', 
					)); 
		Category::create(array(
					'id'=>'157',
					'category'=>'Framing', 
					)); 
		Category::create(array(
					'id'=>'158',
					'category'=>'Fruits & Vegetables', 
					)); 
		Category::create(array(
					'id'=>'159',
					'category'=>'Funeral Homes [see also Funeral Services]', 
					)); 
		Category::create(array(
					'id'=>'160',
					'category'=>'Funeral Insurance', 
					)); 
		Category::create(array(
					'id'=>'161',
					'category'=>'Funeral Services', 
					)); 
		Category::create(array(
					'id'=>'162',
					'category'=>'Furniture', 
					)); 
		Category::create(array(
					'id'=>'163',
					'category'=>'Furniture (Outdoor)', 
					)); 
		Category::create(array(
					'id'=>'164',
					'category'=>'Furniture Fabrication', 
					)); 
		Category::create(array(
					'id'=>'165',
					'category'=>'Furniture Rentals', 
					)); 
		Category::create(array(
					'id'=>'166',
					'category'=>'Garden Decor', 
					)); 
		Category::create(array(
					'id'=>'167',
					'category'=>'Garden Supplies [see also Garden Decor]', 
					)); 
		Category::create(array(
					'id'=>'168',
					'category'=>'Gas LP', 
					)); 
		Category::create(array(
					'id'=>'169',
					'category'=>'Gas Stations', 
					)); 
		Category::create(array(
					'id'=>'170',
					'category'=>'Gifts', 
					)); 
		Category::create(array(
					'id'=>'171',
					'category'=>'Glass (Handblown)', 
					)); 
		Category::create(array(
					'id'=>'172',
					'category'=>'Glassworks', 
					)); 
		Category::create(array(
					'id'=>'173',
					'category'=>'Goldsmiths & Silversmiths', 
					)); 
		Category::create(array(
					'id'=>'174',
					'category'=>'Golf', 
					)); 
		Category::create(array(
					'id'=>'175',
					'category'=>'Golf Cart Maintenance', 
					)); 
		Category::create(array(
					'id'=>'176',
					'category'=>'Gourmet Stores', 
					)); 
		Category::create(array(
					'id'=>'177',
					'category'=>'Graden Design & Construction', 
					)); 
		Category::create(array(
					'id'=>'178',
					'category'=>'Graphic Design', 
					)); 
		Category::create(array(
					'id'=>'179',
					'category'=>'Grocery Stores', 
					)); 
		Category::create(array(
					'id'=>'180',
					'category'=>'Guest Houses', 
					)); 
		Category::create(array(
					'id'=>'181',
					'category'=>'Gyms', 
					)); 
		Category::create(array(
					'id'=>'182',
					'category'=>'Hair Stylists', 
					)); 
		Category::create(array(
					'id'=>'183',
					'category'=>'Handcrafts', 
					)); 
		Category::create(array(
					'id'=>'185',
					'category'=>'Hardware Stores', 
					)); 
		Category::create(array(
					'id'=>'184',
					'category'=>'Hardware Stores', 
					)); 
		Category::create(array(
					'id'=>'186',
					'category'=>'Health Care [see also Assisted Living, Physicians, Dentists, etc.]', 
					)); 
		Category::create(array(
					'id'=>'187',
					'category'=>'Health Centers', 
					)); 
		Category::create(array(
					'id'=>'188',
					'category'=>'Hearing Aids', 
					)); 
		Category::create(array(
					'id'=>'189',
					'category'=>'Heating', 
					)); 
		Category::create(array(
					'id'=>'190',
					'category'=>'Home Accessories', 
					)); 
		Category::create(array(
					'id'=>'191',
					'category'=>'Home Building & Improvement', 
					)); 
		Category::create(array(
					'id'=>'192',
					'category'=>'Home Decor & Furnishings', 
					)); 
		Category::create(array(
					'id'=>'193',
					'category'=>'Home Maintenance', 
					)); 
		Category::create(array(
					'id'=>'194',
					'category'=>'Horse Boarding', 
					)); 
		Category::create(array(
					'id'=>'195',
					'category'=>'Horse Training', 
					)); 
		Category::create(array(
					'id'=>'196',
					'category'=>'Horseback Riding Lessons', 
					)); 
		Category::create(array(
					'id'=>'198',
					'category'=>'Hospitals & Clinics', 
					)); 
		Category::create(array(
					'id'=>'197',
					'category'=>'Hospitals & Clinics', 
					)); 
		Category::create(array(
					'id'=>'199',
					'category'=>'Hostals', 
					)); 
		Category::create(array(
					'id'=>'200',
					'category'=>'Hotels', 
					)); 
		Category::create(array(
					'id'=>'201',
					'category'=>'Hotels Boutique', 
					)); 
		Category::create(array(
					'id'=>'202',
					'category'=>'Ice Cream', 
					)); 
		Category::create(array(
					'id'=>'203',
					'category'=>'Immigration Services', 
					)); 
		Category::create(array(
					'id'=>'204',
					'category'=>'Imports / Exports', 
					)); 
		Category::create(array(
					'id'=>'205',
					'category'=>'Insurance', 
					)); 
		Category::create(array(
					'id'=>'206',
					'category'=>'Insurance (Homeowners)', 
					)); 
		Category::create(array(
					'id'=>'207',
					'category'=>'Insurance (Life)', 
					)); 
		Category::create(array(
					'id'=>'208',
					'category'=>'Insurance (Medical)', 
					)); 
		Category::create(array(
					'id'=>'209',
					'category'=>'Interior Design & Decor', 
					)); 
		Category::create(array(
					'id'=>'210',
					'category'=>'Internal Medicine', 
					)); 
		Category::create(array(
					'id'=>'211',
					'category'=>'Internet Providers', 
					)); 
		Category::create(array(
					'id'=>'212',
					'category'=>'Internet Service (Cafés)', 
					)); 
		Category::create(array(
					'id'=>'213',
					'category'=>'Investments [see also Banks]', 
					)); 
		Category::create(array(
					'id'=>'214',
					'category'=>'Ironwork', 
					)); 
		Category::create(array(
					'id'=>'215',
					'category'=>'Irrigation Systems', 
					)); 
		Category::create(array(
					'id'=>'216',
					'category'=>'Jewelry (Studios & Stores)', 
					)); 
		Category::create(array(
					'id'=>'217',
					'category'=>'Jewelry Repair', 
					)); 
		Category::create(array(
					'id'=>'218',
					'category'=>'Kitchen Accessories', 
					)); 
		Category::create(array(
					'id'=>'219',
					'category'=>'Kitchens', 
					)); 
		Category::create(array(
					'id'=>'220',
					'category'=>'Laboratories', 
					)); 
		Category::create(array(
					'id'=>'221',
					'category'=>'Lamps', 
					)); 
		Category::create(array(
					'id'=>'222',
					'category'=>'Landscaping', 
					)); 
		Category::create(array(
					'id'=>'223',
					'category'=>'Language Schools', 
					)); 
		Category::create(array(
					'id'=>'224',
					'category'=>'Laser (Dental)', 
					)); 
		Category::create(array(
					'id'=>'225',
					'category'=>'Laundries', 
					)); 
		Category::create(array(
					'id'=>'226',
					'category'=>'Leather Goods', 
					)); 
		Category::create(array(
					'id'=>'227',
					'category'=>'Legal Consulting', 
					)); 
		Category::create(array(
					'id'=>'228',
					'category'=>'Libraries', 
					)); 
		Category::create(array(
					'id'=>'229',
					'category'=>'Lighting', 
					)); 
		Category::create(array(
					'id'=>'230',
					'category'=>'Live Music', 
					)); 
		Category::create(array(
					'id'=>'231',
					'category'=>'Locksmiths', 
					)); 
		Category::create(array(
					'id'=>'232',
					'category'=>'Lumber', 
					)); 
		Category::create(array(
					'id'=>'233',
					'category'=>'Magazines', 
					)); 
		Category::create(array(
					'id'=>'234',
					'category'=>'Mail / Message Centers', 
					)); 
		Category::create(array(
					'id'=>'235',
					'category'=>'Manicure & Pedicure', 
					)); 
		Category::create(array(
					'id'=>'236',
					'category'=>'Markets', 
					)); 
		Category::create(array(
					'id'=>'237',
					'category'=>'Massage', 
					)); 
		Category::create(array(
					'id'=>'238',
					'category'=>'Maxillofacial Surgery', 
					)); 
		Category::create(array(
					'id'=>'239',
					'category'=>'Medical Emergencies', 
					)); 
		Category::create(array(
					'id'=>'240',
					'category'=>'Meditation', 
					)); 
		Category::create(array(
					'id'=>'241',
					'category'=>'Mortgages', 
					)); 
		Category::create(array(
					'id'=>'242',
					'category'=>'Motorcycle Rentals', 
					)); 
		Category::create(array(
					'id'=>'243',
					'category'=>'Motorcycle Sales', 
					)); 
		Category::create(array(
					'id'=>'244',
					'category'=>'Motorcycles (Parts & Service)', 
					)); 
		Category::create(array(
					'id'=>'245',
					'category'=>'Movie Theaters', 
					)); 
		Category::create(array(
					'id'=>'246',
					'category'=>'Moving Companies', 
					)); 
		Category::create(array(
					'id'=>'247',
					'category'=>'Municipal Services', 
					)); 
		Category::create(array(
					'id'=>'248',
					'category'=>'Museums', 
					)); 
		Category::create(array(
					'id'=>'249',
					'category'=>'Musicians', 
					)); 
		Category::create(array(
					'id'=>'250',
					'category'=>'Nannies', 
					)); 
		Category::create(array(
					'id'=>'251',
					'category'=>'Natural Food Stores', 
					)); 
		Category::create(array(
					'id'=>'252',
					'category'=>'Natural Products [see also Natural Food Stores]', 
					)); 
		Category::create(array(
					'id'=>'253',
					'category'=>'Newspapers', 
					)); 
		Category::create(array(
					'id'=>'254',
					'category'=>'Night Clubs', 
					)); 
		Category::create(array(
					'id'=>'255',
					'category'=>'Notary Public', 
					)); 
		Category::create(array(
					'id'=>'256',
					'category'=>'Nurseries', 
					)); 
		Category::create(array(
					'id'=>'257',
					'category'=>'Nurses', 
					)); 
		Category::create(array(
					'id'=>'258',
					'category'=>'Office Supplies', 
					)); 
		Category::create(array(
					'id'=>'259',
					'category'=>'Online Business Management', 
					)); 
		Category::create(array(
					'id'=>'260',
					'category'=>'Opticians', 
					)); 
		Category::create(array(
					'id'=>'261',
					'category'=>'Oral Surgery', 
					)); 
		Category::create(array(
					'id'=>'262',
					'category'=>'Organic Products', 
					)); 
		Category::create(array(
					'id'=>'263',
					'category'=>'Orthodontists', 
					)); 
		Category::create(array(
					'id'=>'264',
					'category'=>'Oxygen', 
					)); 
		Category::create(array(
					'id'=>'265',
					'category'=>'Packaging Material', 
					)); 
		Category::create(array(
					'id'=>'266',
					'category'=>'Packing & Shipping', 
					)); 
		Category::create(array(
					'id'=>'267',
					'category'=>'Paint', 
					)); 
		Category::create(array(
					'id'=>'268',
					'category'=>'Parking (Overnight / Monthly)', 
					)); 
		Category::create(array(
					'id'=>'269',
					'category'=>'Parking Lots', 
					)); 
		Category::create(array(
					'id'=>'270',
					'category'=>'Party Planning', 
					)); 
		Category::create(array(
					'id'=>'271',
					'category'=>'Party Planning (Children)', 
					)); 
		Category::create(array(
					'id'=>'272',
					'category'=>'Party Rentals', 
					)); 
		Category::create(array(
					'id'=>'273',
					'category'=>'Party Supplies', 
					)); 
		Category::create(array(
					'id'=>'274',
					'category'=>'Pastries', 
					)); 
		Category::create(array(
					'id'=>'275',
					'category'=>'Pawn Shops', 
					)); 
		Category::create(array(
					'id'=>'276',
					'category'=>'Perfumes', 
					)); 
		Category::create(array(
					'id'=>'277',
					'category'=>'Personal Training', 
					)); 
		Category::create(array(
					'id'=>'278',
					'category'=>'Pest Control', 
					)); 
		Category::create(array(
					'id'=>'279',
					'category'=>'Pet Boarding', 
					)); 
		Category::create(array(
					'id'=>'280',
					'category'=>'Pet Care', 
					)); 
		Category::create(array(
					'id'=>'282',
					'category'=>'Pet Food', 
					)); 
		Category::create(array(
					'id'=>'281',
					'category'=>'Pet Food', 
					)); 
		Category::create(array(
					'id'=>'283',
					'category'=>'Pet Grooming', 
					)); 
		Category::create(array(
					'id'=>'284',
					'category'=>'Pet Shops', 
					)); 
		Category::create(array(
					'id'=>'285',
					'category'=>'Pet Supplies', 
					)); 
		Category::create(array(
					'id'=>'286',
					'category'=>'Pets', 
					)); 
		Category::create(array(
					'id'=>'287',
					'category'=>'Photo Studios', 
					)); 
		Category::create(array(
					'id'=>'288',
					'category'=>'Photographers', 
					)); 
		Category::create(array(
					'id'=>'289',
					'category'=>'Photography Classes', 
					)); 
		Category::create(array(
					'id'=>'290',
					'category'=>'Physical Therapy', 
					)); 
		Category::create(array(
					'id'=>'291',
					'category'=>'Physicians', 
					)); 
		Category::create(array(
					'id'=>'292',
					'category'=>'Physicians (Cardiopulmonary)', 
					)); 
		Category::create(array(
					'id'=>'293',
					'category'=>'Physicians (Endocrinologists Gynecologists)', 
					)); 
		Category::create(array(
					'id'=>'294',
					'category'=>'Physicians (Family)', 
					)); 
		Category::create(array(
					'id'=>'295',
					'category'=>'Physicians (Gynecologists)', 
					)); 
		Category::create(array(
					'id'=>'296',
					'category'=>'Physicians (Plastic Surgeons)', 
					)); 
		Category::create(array(
					'id'=>'297',
					'category'=>'Physicians (Psychiatrists)', 
					)); 
		Category::create(array(
					'id'=>'298',
					'category'=>'Physicians (Urologists Oncologists)', 
					)); 
		Category::create(array(
					'id'=>'299',
					'category'=>'Pilates', 
					)); 
		Category::create(array(
					'id'=>'300',
					'category'=>'Pizza [see also Restaurants (Pizza)]', 
					)); 
		Category::create(array(
					'id'=>'301',
					'category'=>'Plastic Surgery', 
					)); 
		Category::create(array(
					'id'=>'302',
					'category'=>'Plumbers', 
					)); 
		Category::create(array(
					'id'=>'303',
					'category'=>'Plumbing Supplies', 
					)); 
		Category::create(array(
					'id'=>'304',
					'category'=>'Pool Accessories', 
					)); 
		Category::create(array(
					'id'=>'305',
					'category'=>'Pool Heating', 
					)); 
		Category::create(array(
					'id'=>'306',
					'category'=>'Pool Maintenance', 
					)); 
		Category::create(array(
					'id'=>'307',
					'category'=>'Printers', 
					)); 
		Category::create(array(
					'id'=>'309',
					'category'=>'Private Security Service Providers', 
					)); 
		Category::create(array(
					'id'=>'308',
					'category'=>'Private Security Service Providers', 
					)); 
		Category::create(array(
					'id'=>'310',
					'category'=>'Psychotherapists', 
					)); 
		Category::create(array(
					'id'=>'311',
					'category'=>'Purification Systems', 
					)); 
		Category::create(array(
					'id'=>'312',
					'category'=>'Purified Water Providers', 
					)); 
		Category::create(array(
					'id'=>'313',
					'category'=>'Quarry / Stone', 
					)); 
		Category::create(array(
					'id'=>'314',
					'category'=>'Radio Stations', 
					)); 
		Category::create(array(
					'id'=>'315',
					'category'=>'Real Estate Development', 
					)); 
		Category::create(array(
					'id'=>'316',
					'category'=>'Real Estate Management', 
					)); 
		Category::create(array(
					'id'=>'317',
					'category'=>'Real Estate Rentals', 
					)); 
		Category::create(array(
					'id'=>'318',
					'category'=>'Real Estate Sales', 
					)); 
		Category::create(array(
					'id'=>'319',
					'category'=>'Recreational Pools [see also Sports Clubs]', 
					)); 
		Category::create(array(
					'id'=>'320',
					'category'=>'Rehabilitation (Assisted Living)', 
					)); 
		Category::create(array(
					'id'=>'321',
					'category'=>'Rentals (Tram)', 
					)); 
		Category::create(array(
					'id'=>'322',
					'category'=>'Rentals (Videos)', 
					)); 
		Category::create(array(
					'id'=>'323',
					'category'=>'Restaurant Bars', 
					)); 
		Category::create(array(
					'id'=>'324',
					'category'=>'Restaurants', 
					)); 
		Category::create(array(
					'id'=>'325',
					'category'=>'Restaurants (Argentinian)', 
					)); 
		Category::create(array(
					'id'=>'326',
					'category'=>'Restaurants (Burgers)', 
					)); 
		Category::create(array(
					'id'=>'327',
					'category'=>'Restaurants (Cafés)', 
					)); 
		Category::create(array(
					'id'=>'328',
					'category'=>'Restaurants (Crepes)', 
					)); 
		Category::create(array(
					'id'=>'329',
					'category'=>'Restaurants (Diners)', 
					)); 
		Category::create(array(
					'id'=>'330',
					'category'=>'Restaurants (Fast Food)', 
					)); 
		Category::create(array(
					'id'=>'331',
					'category'=>'Restaurants (French)', 
					)); 
		Category::create(array(
					'id'=>'332',
					'category'=>'Restaurants (International)', 
					)); 
		Category::create(array(
					'id'=>'333',
					'category'=>'Restaurants (Italian)', 
					)); 
		Category::create(array(
					'id'=>'334',
					'category'=>'Restaurants (Mexican)', 
					)); 
		Category::create(array(
					'id'=>'335',
					'category'=>'Restaurants (Organic)', 
					)); 
		Category::create(array(
					'id'=>'336',
					'category'=>'Restaurants (Pizza)', 
					)); 
		Category::create(array(
					'id'=>'337',
					'category'=>'Restaurants (Roasted Chicken)', 
					)); 
		Category::create(array(
					'id'=>'338',
					'category'=>'Restaurants (Seafood)', 
					)); 
		Category::create(array(
					'id'=>'339',
					'category'=>'Restaurants (Swedish)', 
					)); 
		Category::create(array(
					'id'=>'340',
					'category'=>'Restaurants (Tacos)', 
					)); 
		Category::create(array(
					'id'=>'341',
					'category'=>'Restaurants (Yucateca Food)', 
					)); 
		Category::create(array(
					'id'=>'342',
					'category'=>'Riding Clubs', 
					)); 
		Category::create(array(
					'id'=>'343',
					'category'=>'Roasted Chicken', 
					)); 
		Category::create(array(
					'id'=>'345',
					'category'=>'Satellite Television & Internet', 
					)); 
		Category::create(array(
					'id'=>'344',
					'category'=>'Satellite Television & Internet', 
					)); 
		Category::create(array(
					'id'=>'346',
					'category'=>'Savings & Loan [see also Banks]', 
					)); 
		Category::create(array(
					'id'=>'347',
					'category'=>'Schools', 
					)); 
		Category::create(array(
					'id'=>'348',
					'category'=>'Schools (Elementary)', 
					)); 
		Category::create(array(
					'id'=>'349',
					'category'=>'Schools (High School)', 
					)); 
		Category::create(array(
					'id'=>'350',
					'category'=>'Schools (Junior High)', 
					)); 
		Category::create(array(
					'id'=>'351',
					'category'=>'Schools (Kindergarten)', 
					)); 
		Category::create(array(
					'id'=>'352',
					'category'=>'Sculpture [see also Art Galleries]', 
					)); 
		Category::create(array(
					'id'=>'353',
					'category'=>'Shoe Repair & Cleaning', 
					)); 
		Category::create(array(
					'id'=>'354',
					'category'=>'Shoes', 
					)); 
		Category::create(array(
					'id'=>'355',
					'category'=>'Shopping', 
					)); 
		Category::create(array(
					'id'=>'356',
					'category'=>'Shopping Malls', 
					)); 
		Category::create(array(
					'id'=>'357',
					'category'=>'Silver Shops', 
					)); 
		Category::create(array(
					'id'=>'358',
					'category'=>'Social Media', 
					)); 
		Category::create(array(
					'id'=>'359',
					'category'=>'Solar Heating [see also Solar Systems]', 
					)); 
		Category::create(array(
					'id'=>'360',
					'category'=>'Solar Systems', 
					)); 
		Category::create(array(
					'id'=>'361',
					'category'=>'Sound Systems (Sales & Rentals)', 
					)); 
		Category::create(array(
					'id'=>'362',
					'category'=>'Spanish Classes', 
					)); 
		Category::create(array(
					'id'=>'363',
					'category'=>'Spas', 
					)); 
		Category::create(array(
					'id'=>'364',
					'category'=>'Sporting Goods', 
					)); 
		Category::create(array(
					'id'=>'365',
					'category'=>'Sports', 
					)); 
		Category::create(array(
					'id'=>'366',
					'category'=>'Sports Bars', 
					)); 
		Category::create(array(
					'id'=>'367',
					'category'=>'Sports Clubs', 
					)); 
		Category::create(array(
					'id'=>'368',
					'category'=>'Stationery [see also Office Supplies]', 
					)); 
		Category::create(array(
					'id'=>'369',
					'category'=>'Stock Exchange', 
					)); 
		Category::create(array(
					'id'=>'370',
					'category'=>'Storage Facilities', 
					)); 
		Category::create(array(
					'id'=>'371',
					'category'=>'Sustainable Architecture', 
					)); 
		Category::create(array(
					'id'=>'372',
					'category'=>'Swimming Pools', 
					)); 
		Category::create(array(
					'id'=>'373',
					'category'=>'Tailors', 
					)); 
		Category::create(array(
					'id'=>'374',
					'category'=>'Talavera', 
					)); 
		Category::create(array(
					'id'=>'375',
					'category'=>'Taquerías [see also Restaurants (Tacos)]', 
					)); 
		Category::create(array(
					'id'=>'376',
					'category'=>'Tax Consultancy Services', 
					)); 
		Category::create(array(
					'id'=>'377',
					'category'=>'Tax Preparation', 
					)); 
		Category::create(array(
					'id'=>'378',
					'category'=>'Taxis', 
					)); 
		Category::create(array(
					'id'=>'379',
					'category'=>'Telephone Companies', 
					)); 
		Category::create(array(
					'id'=>'380',
					'category'=>'Telephone Systems', 
					)); 
		Category::create(array(
					'id'=>'381',
					'category'=>'Tennis', 
					)); 
		Category::create(array(
					'id'=>'382',
					'category'=>'Theaters', 
					)); 
		Category::create(array(
					'id'=>'383',
					'category'=>'Therapy', 
					)); 
		Category::create(array(
					'id'=>'384',
					'category'=>'Thermal Waters', 
					)); 
		Category::create(array(
					'id'=>'385',
					'category'=>'Tires', 
					)); 
		Category::create(array(
					'id'=>'386',
					'category'=>'Title Services [see also Notary Public]', 
					)); 
		Category::create(array(
					'id'=>'387',
					'category'=>'Tortilla Shops', 
					)); 
		Category::create(array(
					'id'=>'388',
					'category'=>'Tours', 
					)); 
		Category::create(array(
					'id'=>'389',
					'category'=>'Towing', 
					)); 
		Category::create(array(
					'id'=>'390',
					'category'=>'Toy Stores', 
					)); 
		Category::create(array(
					'id'=>'391',
					'category'=>'Translators', 
					)); 
		Category::create(array(
					'id'=>'392',
					'category'=>'Transportation', 
					)); 
		Category::create(array(
					'id'=>'393',
					'category'=>'Transportation Rentals', 
					)); 
		Category::create(array(
					'id'=>'394',
					'category'=>'Travel Agencies', 
					)); 
		Category::create(array(
					'id'=>'395',
					'category'=>'TV Channels', 
					)); 
		Category::create(array(
					'id'=>'396',
					'category'=>'Universities', 
					)); 
		Category::create(array(
					'id'=>'397',
					'category'=>'Upholstery', 
					)); 
		Category::create(array(
					'id'=>'398',
					'category'=>'Used Car Dealers', 
					)); 
		Category::create(array(
					'id'=>'399',
					'category'=>'Vacation Packages', 
					)); 
		Category::create(array(
					'id'=>'400',
					'category'=>'Vacation Rentals', 
					)); 
		Category::create(array(
					'id'=>'401',
					'category'=>'Veterinarians', 
					)); 
		Category::create(array(
					'id'=>'402',
					'category'=>'Video & Audio Transfer Services', 
					)); 
		Category::create(array(
					'id'=>'403',
					'category'=>'Video Games', 
					)); 
		Category::create(array(
					'id'=>'404',
					'category'=>'Viewing & Visitation Rooms', 
					)); 
		Category::create(array(
					'id'=>'405',
					'category'=>'Vitamins', 
					)); 
		Category::create(array(
					'id'=>'406',
					'category'=>'Volunteer Offering', 
					)); 
		Category::create(array(
					'id'=>'407',
					'category'=>'Watch Sales & Repair', 
					)); 
		Category::create(array(
					'id'=>'408',
					'category'=>'Water Heaters', 
					)); 
		Category::create(array(
					'id'=>'409',
					'category'=>'Water Purification Systems', 
					)); 
		Category::create(array(
					'id'=>'410',
					'category'=>'Waterproofing', 
					)); 
		Category::create(array(
					'id'=>'411',
					'category'=>'Website Design', 
					)); 
		Category::create(array(
					'id'=>'413',
					'category'=>'Wedding Planning & Services', 
					)); 
		Category::create(array(
					'id'=>'412',
					'category'=>'Wedding Planning & Services', 
					)); 
		Category::create(array(
					'id'=>'414',
					'category'=>'Window Covering', 
					)); 
		Category::create(array(
					'id'=>'415',
					'category'=>'Window Treatment', 
					)); 
		Category::create(array(
					'id'=>'416',
					'category'=>'Windshields', 
					)); 
		Category::create(array(
					'id'=>'417',
					'category'=>'Wines & Liquors', 
					)); 
		Category::create(array(
					'id'=>'418',
					'category'=>'Yoga Classes', 
					)); 																															
	}
}