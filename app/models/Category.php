<?php
class Category extends Eloquent {
	 protected $table = 'categories';

	 public function advertisement() {
        return $this->hasOne('Advertisement');
    }

	 public function categoria() {
        return $this->belongs_to('Categoria');
    }
}
?>