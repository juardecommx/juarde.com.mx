<?php
class Advertisement extends Eloquent {
	 protected $table = 'advertisements';

	public function advertiser() {
        return $this->belongsTo('Advertiser');
    }

    public function category() {
        return $this->belongsTo('Category');
    }
}
?>