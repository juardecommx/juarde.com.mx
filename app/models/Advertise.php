<?php
class Advertiser extends Eloquent {
	 protected $table = 'advertisers';

	 public function advertisement() {
        return $this->hasMany('Advertisement');
    }
}
?>