<?php

class CategoriesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{

    //$categories = DB::table('categories')->orderBy('category','ASC')->paginate(11);
    $categories = DB::table('categories')->orderBy('category','ASC')->get();

    return View::make('english.categories')
    ->with(compact('categories'));
	}


	public function getIndex2()
	{
    $categorias = DB::table('categorias')->orderBy('categoria','ASC')->get();

    return View::make('spanish.categorias')
    ->with(compact('categorias'));
	}


	public function getList()
	{
		$categories = DB::table('categories')
		->join('categorias', 'categorias.id', '=', 'categories.id')
		->orderBy('category','ASC')->get();

    	return View::make('admin.categories.index')
    	->with(compact('categories'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function anyCreate()
	{
		return View::make('admin.categories.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function anyStore()
	{
		$category=new Category;
		$category->category=Input::get('category');

        if ($category->save()) {
        	$categorySaved = true;
        }

        $id_temp = DB::table('categories')->where('category',Input::get('category'))
        ->orderBy('created_at', 'desc')->first();
        	
    $categoria=new Categoria;
		$categoria->id=$id_temp->id;
		$categoria->categoria=Input::get('categoria');
		if ($categoria->save()) {
			$categoriaSaved = true;
		}

		if ($categorySaved and $categoriaSaved) {
			Session::flash('message', "Category '$category->category' - '$categoria->categoria' successfully created");
			Session::flash('class', 'success');
		} else {
			Session::flash('message', 'An error has occurred');
			Session::flash('class', 'danger');
		}

		return Redirect::to('admin/categories/list');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function anyShow($id=null)
	{
		$category = category::find($id);
		$categoria = categoria::find($id);

		return View::make('admin.categories.show')
		->with('category', $category)
		->with('categoria', $categoria);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function anyEdit($id=null)
	{
		$category = Category::find($id);
		$categoria = Categoria::find($id);
		
		$data = array(
							'success' => true,
							'id' => $category->id,
							// their respective names
							'category' => $category->category,
							'categoria' => $categoria->categoria
						);
		
		return Response::json($data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function anyUpdate($id)
	{
		$category = category::find($id);
		$categoria = categoria::find($id);

		$categoria->categoria=Input::get('categoria');
		$category->category=Input::get('category');

		if ($category->save() and $categoria->save()) {
			Session::flash('message', "Category '$category->category' - '$categoria->categoria' successfully updated");
			Session::flash('class', 'success');
		} else {
			Session::flash('message', 'An error has occurred');
			Session::flash('class', 'danger');
		}

		return Redirect::to('admin/categories/list');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function anyDestroy($id)
	{
		$category = category::find($id);
		$categoria = categoria::find($id);

		if ($categoria->delete() and $category->delete()) {
			Session::flash('message', "Category '$category->category' - '$categoria->categoria' successfully deleted");
			Session::flash('class', 'warning');
		} else {
			Session::flash('message', 'An error has occurred');
			Session::flash('class', 'danger');
		}

		return Redirect::to('admin/categories/list');
	}


}
