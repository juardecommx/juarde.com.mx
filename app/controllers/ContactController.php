<?php

class ContactController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		$contact = Contact::find(1);
		return View::make('english.contact')
		->with(compact('contact'));
	}

	public function getIndex2()
	{
		$contact = Contact::find(1);
		return View::make('spanish.contacto')
		->with(compact('contact'));
	}


	public function getList()
	{
		$contact = Contact::find(1);
    	return View::make('admin.contact.index')
    	->with(compact('contact'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function anyUpdate()
	{
		$contact = Contact::find(1);

		$contact->address=Input::get('address');
		$contact->phones=Input::get('phones');
		$contact->email=Input::get('email');

		if ($contact->save() and $contact->save()) {
			Session::flash('message', "Contacto successfully updated");
			Session::flash('class', 'success');
		} else {
			Session::flash('message', 'An error has occurred');
			Session::flash('class', 'danger');
		}

		return Redirect::to('admin/contact/list');
		
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
