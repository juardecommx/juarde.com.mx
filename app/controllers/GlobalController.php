<?php

class GlobalController extends BaseController {

public static function spanishLinks()
	{
		$links=array('pages'=>'paginas',
			'advertisements'=>'buscar',
			'categories'=>'categorias',
			'events'=>'eventos',
			'residents'=>'residentes',
			'home'=>'inicio','/'=>'inicio',
			'show'=>'ver',
			'contact'=>'contacto',
			'send'=>'enviar',
			'detail'=>'detalle');
		return $links;
	}

	public static function englishLinks()
	{
		$links=array('paginas'=>'pages',
			'buscar'=>'advertisements',
			'categorias'=>'categories',
			'eventos'=>'events',
			'residentes'=>'residents',
			'inicio'=>'home',
			'ver'=>'show',
			'contacto'=>'contact',
			'enviar'=>'send',
			'detalle'=>'detail');
		return $links;
	}

	public static function saveGetEnglish($links)
	{
		$link=$links[Request::path()];

		if(Request::path()=='advertisements'){
			$temp_get='?search='.$_GET['search'];
			$link=$link . $temp_get;
		}else{
			$temp_get='';
		}
					
		return $link;
	}

	public static function saveGetSpanish($links)
	{
		$link=$links[Request::path()];

		if(Request::path()=='buscar'){
			$temp_get='?search='.$_GET['search'];
			$link=$link . $temp_get;
		}else{
			$temp_get='';
		}

		return $link;
	}

	public static function fechas()
		{
			$meses=array(
						'01'=>'Enero',
						'02'=>'Febrero',
						'03'=>'Marzo',
						'04'=>'Abril',
						'05'=>'Mayo',
						'06'=>'Junio',
						'07'=>'Julio',
						'08'=>'Agosto',
						'09'=>'Septiembre',
						'10'=>'Octubre',
						'11'=>'Noviembre',
						'12'=>'Diciembre');
					
			return $meses;
		}

	public static function dates(){

		
		$months=array(
						'01'=>'January',
						'02'=>'February',
						'03'=>'March',
						'04'=>'April',
						'05'=>'May',
						'06'=>'June',
						'07'=>'July',
						'08'=>'August',
						'09'=>'September',
						'10'=>'October',
						'11'=>'November',
						'12'=>'December');
		return $months;
	}

}
