<?php

class ResidentsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		//$residents = DB::table('residents')->orderBy('last_name1','ASC')->paginate(13);
		$residents = DB::table('residents')->orderBy('last_name1','ASC')->get();

	    return View::make('english.residents')
	    ->with(compact('residents'));
	}

	public function getIndex2()
	{
		//$residents = DB::table('residents')->orderBy('last_name1','ASC')->paginate(13);
		$residents = DB::table('residents')->orderBy('last_name1','ASC')->get();

	    return View::make('spanish.residentes')
	    ->with(compact('residents'));
	}


	public function getList()
	{
		$residents = DB::table('residents')->orderBy('last_name1','ASC')->get();
    	return View::make('admin.residents.index')->with(compact('residents'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getCreate()
	{
		return View::make('admin.residents.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function anyStore()
	{

		$resident=new Resident;		

		$resident->first_name1=Input::get('first_name1');
		$resident->last_name1=strtoupper(Input::get('last_name1'));
		$resident->first_name2=Input::get('first_name2');

		if(null != Input::get('last_name2')){
			$resident->last_name2="& ".strtoupper(Input::get('last_name2'));
		}else{
			$resident->last_name2=strtoupper(Input::get('last_name2'));
		}

		$resident->street_address=Input::get('street_address');
		$resident->apdo=Input::get('apdo');
		$resident->tel=Input::get('tel');
		$resident->tel2=Input::get('tel2');
		$resident->fax=Input::get('fax');
		$resident->voip_server=Input::get('voip_server');
		$resident->email=Input::get('email');
		$resident->facebook=Input::get('facebook');
		$resident->twitter=Input::get('twitter');

		if ($resident->save()) {
			$full_name = $resident->last_name1 . ' ' . $resident->first_name1;
			if(null != $resident->last_name2){
				$full_name = $full_name . ' ' . $resident->last_name2;
				if(null != $resident->first_name2){
					$full_name = $full_name . ' ' . $resident->first_name2;
				}
			}
			Session::flash('message', "Resident '$full_name' successfully created");
			Session::flash('class', 'success');
		} else {
			Session::flash('message', 'An error has occurred');
			Session::flash('class', 'danger');
		}

		return Redirect::to('admin/residents/list');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getShow()
	{
		$resident = Resident::find($_GET['id']);
		return Response::json($resident);
		//return View::make('english.residentDetail')->with(compact('resident'));
	}


	public function getVer()
	{
		$resident = Resident::find($_GET['id']);
		return Response::json($resident);
		//return View::make('spanish.detalleResidente')->with(compact('resident'));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function anyEdit($id)
	{
		$resident = Resident::find($id);

		return Response::json($resident);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function anyUpdate($id)
	{
		$resident = Resident::find($id);

		$resident->first_name1=Input::get('first_name1');
		$resident->last_name1=strtoupper(Input::get('last_name1'));
		$resident->first_name2=Input::get('first_name2');

		if(null != Input::get('last_name2')){
			$resident->last_name2="& ".strtoupper(Input::get('last_name2'));
		}else{
			$resident->last_name2=strtoupper(Input::get('last_name2'));
		}

		$resident->street_address=Input::get('street_address');
		$resident->apdo=Input::get('apdo');
		$resident->tel=Input::get('tel');
		$resident->tel2=Input::get('tel2');
		$resident->fax=Input::get('fax');
		$resident->voip_server=Input::get('voip_server');
		$resident->email=Input::get('email');
		$resident->facebook=Input::get('facebook');
		$resident->twitter=Input::get('twitter');

		if ($resident->save()) {
			$full_name = $resident->last_name1 . ' ' . $resident->first_name1;
			if(null != $resident->last_name2){
				$full_name = $full_name . ' ' . $resident->last_name2;
				if(null != $resident->first_name2){
					$full_name = $full_name . ' ' . $resident->first_name2;
				}
			}
			Session::flash('message', "Resident '$full_name' successfully updated");
			Session::flash('class', 'success');
		} else {
			Session::flash('message', 'An error has occurred');
			Session::flash('class', 'danger');
		}

		return Redirect::to('admin/residents/list');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function anyDestroy($id)
	{
		$resident = Resident::find($id);

		if ($resident->delete()) {
			$full_name = $resident->last_name1 . ' ' . $resident->first_name1;
			if(null != $resident->last_name2){
				$full_name = $full_name . ' ' . $resident->last_name2;
				if(null != $resident->first_name2){
					$full_name = $full_name . ' ' . $resident->first_name2;
				}
			}
			Session::flash('message', "Resident '$full_name' successfully deleted");
			Session::flash('class', 'warning');
		} else {
			Session::flash('message', 'An error has occurred');
			Session::flash('class', 'danger');
		}

		return Redirect::to('admin/residents/list');
	}


}
