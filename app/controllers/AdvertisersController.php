<?php

class AdvertisersController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		$query=DB::table('advertisements')->get;
		return View::make('english.advertisers')
    	->with(compact('query'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getDetail()
	{
		$advertiser = Advertiser::find($_GET['id']);
		// Cheap hack to add the image attribute to $advertiser object.
		// This should be part of the model!
		$imageURL = public_path().'/assets/thumbs/'.$advertiser->id.'.jpg';
		if(file_exists($imageURL)){
			$advertiser->image = $imageURL;
		}
		
		return Response::json($advertiser);
	}

	public function getDetalle()
	{
		$advertiser = Advertiser::find($_GET['id']);
		// Cheap hack to add the image attribute to $advertiser object.
		// This should be part of the model!
		$imageURL = public_path().'/assets/thumbs/'.$advertiser->id.'.jpg';
		if(file_exists($imageURL)){
			$advertiser->image = $imageURL;
		}
		
		return Response::json($advertiser);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
