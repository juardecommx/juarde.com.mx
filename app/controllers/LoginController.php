<?php
class LoginController extends BaseController{

	

	public function DoLogin(){
		$rules=array(
			'name'=>'required',
			'password' => 'required'
		);

		$validator=Validator::make(Input::all(),$rules);

		if($validator->fails()){
			return Redirect::to('home')
				->withErrors($validator)
				->withInput(Input::except('password'));
			}else{

				$userdata=array(
					'name'=>Input::get('name'),
					'password'=>Input::get('password')
					);

				if(Auth::attempt($userdata)){
					return Redirect::to('admin');
				}else{
					return Redirect::to('home');
				}
			}
		}

	public function DoLogout() {
		Auth::logout();
		return Redirect::to('home');
	}

}
