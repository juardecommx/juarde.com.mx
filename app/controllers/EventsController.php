<?php

class EventsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		 $events = DB::table('events')->orderBy('date','ASC')
    	->get();

	    return View::make('english.events')
	    ->with(compact('events'));
	}

	public function getIndex2()
	{
		 $eventos = DB::table('eventos')->orderBy('fecha','ASC')
    	->get();

	    return View::make('spanish.eventos')
	    ->with(compact('eventos'));
	}


	public function getList()
	{
		$events = DB::table('events')->orderBy('date','ASC')
		->join('eventos', 'events.id', '=', 'eventos.id')
    	->get();
    	return View::make('admin.events.index')
    	->with(compact('events'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getCreate()
	{
		return View::make('admin.events.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function anyStore()
	{
		$event=new Ev;
		$event->date=Input::get('date');
		$event->description=Input::get('event');
		
		if ($event->save()) {
			$eventSaved = true;	
		}

		$id_temp = DB::table('events')->where('description',Input::get('event'))
        ->orderBy('created_at', 'desc')->first();

		$evento=new Evento;
		$evento->id=$id_temp->id;
		$evento->fecha=Input::get('date');
		$evento->descripcion=Input::get('evento');
		if ($evento->save()) {
			$eventoSaved = true;
		}

		if ($eventSaved and $eventoSaved) {
			Session::flash('message', "Event '$event->description' - '$evento->descripcion' successfully created");
			Session::flash('class', 'success');
		} else {
			Session::flash('message', 'An error has occurred');
			Session::flash('class', 'danger');
		}

		return Redirect::to('admin/events/list');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function anyShow($id=null)
	{
		$event= Ev::find($id);
		$evento = Evento::find($id);

		return View::make('admin.events.show')
		->with('event', $evento)
		->with('event', $event);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function anyEdit($id=null)
	{
		$event = Ev::find($id);
		$evento = Evento::find($id);

		//return View::make('admin.events.edit')
		//->with('event', $event)
		//->with('evento', $evento);
		$data = array(
									'success' => true,
									'id' => $event->id,
									'event' => $event->description,
									'evento' => $evento->descripcion,
									'date' => $event->date
								);
		return Response::json($data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function anyUpdate($id)
	{
		$event = Ev::find($id);
		$evento = Evento::find($id);

		$event->date=Input::get('date');
		$event->description=Input::get('event');

		$evento->fecha=Input::get('date');
		$evento->descripcion=Input::get('evento');

		if ($event->save() and $evento->save()) {
			Session::flash('message', "Event '$event->description' - '$evento->descripcion' successfully updated");
			Session::flash('class', 'success');
		} else {
			Session::flash('message', 'An error has occurred');
			Session::flash('class', 'danger');
		}

		return Redirect::to('admin/events/list');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function anyDestroy($id)
	{
		$event = Ev::find($id);
		$evento = Evento::find($id);

		if ($evento->delete() and $event->delete()) {
			Session::flash('message', "Event '$event->description' - '$evento->descripcion' successfully deleted");
			Session::flash('class', 'warning');
		} else {
			Session::flash('message', 'An error has occurred');
			Session::flash('class', 'danger');
		}

		return Redirect::to('admin/events/list');
	}


}
