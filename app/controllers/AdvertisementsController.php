<?php

class AdvertisementsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function advertisersQuery(){
		$query=DB::table('advertisers')->orderBy('business_name')->get();
	    return $query;
	}

	public function query(){
		$search=Input::get('search');

		$query=DB::table('advertisements')
	    ->join('advertisers', 'advertisers.id', '=', 'advertisements.advertiser_id')
	    ->join('categories', 'categories.id', '=', 'advertisements.category_id')
	    ->join('categorias', 'categorias.id', '=', 'categories.id')
	    ->whereRaw("MATCH(business_name,web_page,address,email,phone) AGAINST(? IN BOOLEAN MODE) 
	    	or MATCH(categoria) AGAINST(? IN BOOLEAN MODE) 
	    	or MATCH(category) AGAINST(? IN BOOLEAN MODE)
	    	or business_name LIKE ?
	    	or category LIKE ?
	    	or categoria LIKE ?", 
	    	array($search,$search,$search,"%$search%","%$search%","%$search%"));

	    $results=$query->groupBy('business_name')
	    ->orderByRaw("MATCH(business_name,web_page,address,email,phone) AGAINST(? IN BOOLEAN MODE) + MATCH(categoria) AGAINST(? IN BOOLEAN MODE) + MATCH(category) AGAINST(? IN BOOLEAN MODE) DESC", array($search,$search,$search))
	    ->orderBy('business_name')
	    ->get();

	    $advertisements = $query
	    ->groupBy('business_name')
	    ->orderByRaw("MATCH(business_name,web_page,address,email,phone) AGAINST(? IN BOOLEAN MODE) + MATCH(categoria) AGAINST(? IN BOOLEAN MODE) + MATCH(category) AGAINST(? IN BOOLEAN MODE) DESC", array($search,$search,$search))
	    ->orderBy('business_name')
	    ->paginate(8);

	    $pagination=$query->paginate(8);

	    return array('advertisements'=>$advertisements,'search'=>$search,'results'=>$results,'pagination'=>$pagination);
	}

	public function getIndex(){
		$query=$this->query();
	    return View::make('english.search')
	    ->with(compact('query'));
	}

	public function getIndex2(){
		$query=$this->query();
	    return View::make('spanish.buscar')
	    ->with(compact('query'));
	}

	public function getPages(){
		$records=$this->advertisersQuery();
		return View::make('english.advertisers')
    	->with(compact('records'));
	}

	public function getPages2(){
		$records=$this->advertisersQuery();
		return View::make('spanish.paginas')
    	->with(compact('records'));
	}


	public function getList()
	{
		$query=$this->advertisersQuery();
		$categoryOptions = DB::table('categories')->orderBy('category', 'asc')->lists('category','id');
		//creo la lista de los anuncios de paginas amarillas
    	return View::make('admin.pages.index', array('categoryOptions' => $categoryOptions))
    	->with(compact('query'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getCreate()
	{
		//redirecciono al formularop de create
		$categoryOptions = DB::table('categories')->orderBy('category', 'asc')->lists('category','id');
		return View::make('admin.pages.create',array('categoryOptions' => $categoryOptions));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function anyStore()
	{

		//creeo un anunciante y lo instancio
		$advertiser=new Advertiser;
		$advertiser->business_name=Input::get('business_name');
		$advertiser->address=Input::get('address');
		$advertiser->email=Input::get('email');
		$advertiser->phone=Input::get('phone');
		$advertiser->phone2=Input::get('phone2');
		$advertiser->mobile_phone=Input::get('mobile_phone');
		$advertiser->mobile_phone2=Input::get('mobile_phone2');
		$advertiser->nextel=Input::get('nextel');
		$advertiser->web_page=Input::get('web_page');
		$advertiser->facebook=Input::get('facebook');
		$advertiser->twitter=Input::get('twitter');
		$advertiser->other=Input::get('other');


		//compruebo que se haya guardado correctamente el anunciante
		if ($advertiser->save()) {
			Session::flash('message', "Advertiser '$advertiser->business_name' successfully created");
			Session::flash('class', 'success');
		} else {
			Session::flash('message', 'An error has occurred');
			Session::flash('class', 'danger');
		}
        
        //obtengo el id del anunciante para usarlo en sus anuncios
        $id_temp = DB::table('advertisers')->where('business_name',Input::get('business_name'))
        ->orderBy('created_at', 'desc')->first();


        //compruebo que categorias estan habilitadas para darlos de alta.
		if(null!=Input::get('categoria1')){
			$advertisement=new Advertisement;
			$advertisement->category_id=Input::get('categoria1');
			$advertisement->advertiser_id=$id_temp->id;
			$advertisement->save();
		}
		if(null!=Input::get('categoria2')){
			$advertisement=new Advertisement;
			$advertisement->category_id=Input::get('categoria2');
			$advertisement->advertiser_id=$id_temp->id;
			$advertisement->save();
		}
		if(null!=Input::get('categoria3')){
			$advertisement=new Advertisement;
			$advertisement->category_id=Input::get('categoria3');
			$advertisement->advertiser_id=$id_temp->id;
			$advertisement->save();
		}
		if(null!=Input::get('categoria4')){
			$advertisement=new Advertisement;
			$advertisement->category_id=Input::get('categoria4');
			$advertisement->advertiser_id=$id_temp->id;
			$advertisement->save();
		}
		if(null!=Input::get('categoria5')){
			$advertisement=new Advertisement;
			$advertisement->category_id=Input::get('categoria5');
			$advertisement->advertiser_id=$id_temp->id;
			$advertisement->save();
		}

		$resize=$this->resize($id_temp->id);
		//echo $id_temp->id;

		//redirecciono a yelow pages
		return Redirect::to('admin/pages/list');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getShow($id=null)
	{

		$advertiser = Advertiser::find($id);
		$advertisement = Advertisement::find($id);
		$category = category::find($id);
		$categoria = categoria::find($id);

		return View::make('admin.pages.show')
		->with('advertiser', $advertiser)
		->with('advertisement', $advertisement)
		->with('category', $category)
		->with('categoria', $categoria);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function anyEdit($id)
	{
		//$categoryOptions = DB::table('categories')->orderBy('category', 'asc')->lists('category','id');
		$advertiser = Advertiser::find($id);
		//$category =  DB::table('advertisements')->where('advertiser_id',$id)->get();

		//return array('categoryOptions'=>$categoryOptions,'advertiser'=>$advertiser,'category'=>$category);
		$imageURL = public_path().'/assets/thumbs/'.$advertiser->id.'.jpg';
		if(file_exists($imageURL)){
			$advertiser->image = $imageURL;
		}
		return Response::json($advertiser);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function anyUpdate($id)
	{
		$advertiser = Advertiser::find($id);

		$advertiser->business_name=Input::get('business_name');
		$advertiser->address=Input::get('address');
		$advertiser->email=Input::get('email');
		$advertiser->phone=Input::get('phone');
		$advertiser->phone2=Input::get('phone2');
		$advertiser->mobile_phone=Input::get('mobile_phone');
		$advertiser->mobile_phone2=Input::get('mobile_phone2');
		$advertiser->nextel=Input::get('nextel');
		$advertiser->web_page=Input::get('web_page');
		$advertiser->facebook=Input::get('facebook');
		$advertiser->twitter=Input::get('twitter');
		$advertiser->other=Input::get('other');


		//compruebo que se haya guardado correctamente el anunciante
		if ($advertiser->save()) {
			Session::flash('message', "Advertiser '$advertiser->business_name' successfully updated");
			Session::flash('class', 'success');
		} else {
			Session::flash('message', 'An error has occurred');
			Session::flash('class', 'danger');
		}
        
        //compruebo que categorias estan habilitadas para darlos de alta.
        if(null!=Input::get('hide1')){
        $srch_category=Input::get('hide1');
		$category_tmp=Advertisement::find($srch_category);
		$category_tmp->category_id=Input::get('categoria1');
		$category_tmp->save();
		}else{
			if(null==Input::get('hide1') and Input::get('categoria1')!= null){
				$advertisement=new Advertisement;
				$advertisement->category_id=Input::get('categoria1');
				$advertisement->advertiser_id=$id;
				$advertisement->save();
			}
		}


		if(null!=Input::get('hide2')){
        $srch_category=Input::get('hide2');
		$category_tmp=Advertisement::find($srch_category);
		$category_tmp->category_id=Input::get('categoria2');
		$category_tmp->save();
		}else{
			if(null==Input::get('hide2') and Input::get('categoria2')!= null){
				$advertisement=new Advertisement;
				$advertisement->category_id=Input::get('categoria2');
				$advertisement->advertiser_id=$id;
				$advertisement->save();
			}
		}

		if(null!=Input::get('hide3')){
        $srch_category=Input::get('hide3');
		$category_tmp=Advertisement::find($srch_category);
		$category_tmp->category_id=Input::get('categoria3');
		$category_tmp->save();
		}else{
			if(null==Input::get('hide3') and Input::get('categoria3')!= null){
				$advertisement=new Advertisement;
				$advertisement->category_id=Input::get('categoria3');
				$advertisement->advertiser_id=$id;
				$advertisement->save();
			}
		}

		if(null!=Input::get('hide4')){
        $srch_category=Input::get('hide4');
		$category_tmp=Advertisement::find($srch_category);
		$category_tmp->category_id=Input::get('categoria4');
		$category_tmp->save();
		}else{
			if(null==Input::get('hide1') and Input::get('categoria4')!= null){
				$advertisement=new Advertisement;
				$advertisement->category_id=Input::get('categoria4');
				$advertisement->advertiser_id=$id;
				$advertisement->save();
			}
		}

		if(null!=Input::get('hide5')){
        $srch_category=Input::get('hide5');
		$category_tmp=Advertisement::find($srch_category);
		$category_tmp->category_id=Input::get('categoria5');
		$category_tmp->save();
		}else{
			if(null==Input::get('hide5') and Input::get('categoria5')!= null){
				$advertisement=new Advertisement;
				$advertisement->category_id=Input::get('categoria5');
				$advertisement->advertiser_id=$id;
				$advertisement->save();
			}
		}

		$resize=$this->resize($id);
		//redirecciono a yelow pages
		return Redirect::to('admin/pages/list');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function anyDestroy($id)
	{
		//elimino las categorias a las que pertenece el anunciante
		DB::table('advertisements')->where('advertiser_id', $id)->delete();

		//busco al anunciante
		$advertiser = Advertiser::find($id);

		//elimino al anunciante
		if ($advertiser->delete()) {
			Session::flash('message', "Advertiser '$advertiser->business_name' successfully deleted");
			Session::flash('class', 'warning');
		} else {
			Session::flash('message', 'An error has occurred');
			Session::flash('class', 'danger');
		}

		//elimino las imagenes de ese anunciante
		@unlink(public_path().'/assets/thumbs/'.$id.".jpg");
		@unlink(public_path().'/assets/anuncios/'.$id.".jpg");

		//redierecciono a la lista
		return Redirect::to('admin/pages/list');
	}


	function resize($id_temp){
		
		$file=Input::file('image');
		$id=$id_temp;
		if($file!=null){

			/* Medidas de los Thumbnails */
			$width=100; 
			$height=100;

			/* Medidas de los Anuncioss */
			$width2=400; 
			$height2=400;

	        /* Obtemenos las dimensiones originales*/
	        list($w, $h) = getimagesize($_FILES['image']['tmp_name']);

			/* Calculamos el promedio de la dimension */
			$x_ratio = $width / $w;
			$y_ratio = $height / $h;

			$x_ratio2 = $width2 / $w;
			$y_ratio2 = $height2 / $h;

			if( ($w <= $width) && ($h <= $height) ){//Si ancho
			$w_final = $w;
			$h_final = $h;

			$w_final2 = $w;
			$h_final2 = $h;
			}
			/*
			* si proporcion horizontal*alto mayor que el alto maximo,
			* alto final es alto por la proporcion horizontal
			* es decir, le quitamos al ancho, la misma proporcion que
			* le quitamos al alto
			*
			*/
			elseif (($x_ratio * $h) < $height){
			$h_final = ceil($x_ratio * $h);
			$w_final = $width;
			$h_final2 = ceil($x_ratio2 * $h);
			$w_final2 = $width2;
			}
			/*
			* Igual que antes pero a la inversa
			*/
			else{
			$w_final = ceil($y_ratio * $w);
			$h_final = $height;
			$w_final2 = ceil($y_ratio2 * $w);
			$h_final2 = $height2;
			}


			/* Nombramos el archivo */
	    		if($id==null){
	    			$filename=$file->getClientOriginalName();
	    		}else{
	    			$filename=$id.".".$file->getClientOriginalExtension();	
	    		}

		  	/* Nombre del nuevo archivo */
		  	$path = public_path().'/assets/thumbs/'.$filename;
		  	$path2= public_path().'/assets/anuncios/'.$filename;

	 	 	/* Se leen los datos binarios del archivo */
	  		$imgString = file_get_contents($_FILES['image']['tmp_name']);
	  		
	  		/* Se crea la imagen a partir de los datos binarios */
	  		$image = imagecreatefromstring($imgString);
		 	$tmp = imagecreatetruecolor($w_final, $h_final);
	  		imagecopyresampled($tmp, $image,
	    	0, 0,
	    	0, 0,
	    	$w_final, $h_final,
	    	$w, $h);

	    	$image2 = imagecreatefromstring($imgString);
		 	$tmp2 = imagecreatetruecolor($w_final2, $h_final2);
	  		imagecopyresampled($tmp2, $image2,
	    	0, 0,
	    	0, 0,
	    	$w_final2, $h_final2,
	    	$w, $h);


	  		/* Guardamos la imagen y el thumbnail */
	  		switch ($_FILES['image']['type']) {
	    		case 'image/jpeg':
	      		imagejpeg($tmp, $path, 100);
	      		imagejpeg($tmp2, $path2, 100);
	      		break;
	    	case 'image/png':
	      		imagepng($tmp, $path, 0);
	      		imagepng($tmp2, $path2, 100);
	      		break;
	    	case 'image/gif':
	      		imagegif($tmp, $path);
	      		imagegif($tmp2, $path2, 100);
	      		break;
	    	default:
	      		exit;
	      		break;
	  		}

	  		/* limpiamos memoria :) */
	  		imagedestroy($image);
	  		imagedestroy($tmp);
	  		imagedestroy($image2);
	  		imagedestroy($tmp2);
		}
	}

	public function categoryOption() {
  		$categoryOptions = Category::lists('category', 'id');
    	return array($categoryOptions->id=>$categoryOptions->category);
	}

	// Get the categories of an advertiser
	public function anyCategories($id=null) {
		$categories =  DB::table('advertisements')->where('advertiser_id',$id)->get();
		return Response::json($categories);
	}
		
}
