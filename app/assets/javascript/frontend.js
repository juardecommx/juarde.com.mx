//Custom JS for the frontend
$(function(){

	// ListNavs
	$("#categories-listnav, #residents-listnav, #pages-listnav").listnav({
		initLetter: 'a',
		includeOther: true,
		includeNums: false,
		noMatchText: 'There are no matching entries.'
	});
	$("#categorias-listnav, #residentes-listnav, #paginas-listnav").listnav({
		initLetter: 'a',
		allText: 'Todos',
		includeOther: true,
		includeNums: false,
		noMatchText: 'No hay registros que coincidan.'
	});

	// Popover Login
	$('#login').popover({
		html: true,
		placement: 'bottom',
		container: 'body',
		content: function(){
			return $('#popover-login').html();
		}
	});


	//
	//== Residentes Modal
	//
	$('#residentes-listnav').on('click', '.ver', function(){
		var id = $(this).attr('id');
		var url = baseURL + '/' + 'ver?id=' + id;
		$.get(url, function(json){
			//since we're relying on append let's empty our containers first
			$('#residentes-name').empty();
			$('tbody').empty();
			$('#residentes-name').append(json.last_name1);
			$('#residentes-name').append(' '+json.first_name1);
			$('#residentes-name').append(' '+json.last_name2);
			$('#residentes-name').append(' '+json.first_name2);
			if(json.street_address){
				$('tbody').append('<tr><td><strong>Dirección:</strong></td>' + '<td><i class="fa fa-lg fa-map-marker fa-fw"></i>' + json.street_address + '</td>');
			}
			if(json.apdo){
				$('tbody').append('<tr><td><strong>Apartado Postal:</strong></td>' + '<td><i class="fa fa-lg fa-envelope-square fa-fw"></i>' + json.apdo + '</td>');
			}
			if(json.tel){
				$('tbody').append('<tr><td><strong>Teléfono:</strong></td>' + '<td><i class="fa fa-lg fa-phone fa-fw"></i>' + json.tel + '</td>');
			}
			if(json.tel2){
				$('tbody').append('<tr><td><strong>Teléfono 2:</strong></td>' + '<td><i class="fa fa-lg fa-phone fa-fw"></i>' + json.tel2 + '<br>');
			}
			if(json.fax){
				$('tbody').append('<tr><td><strong>Fax:</strong></td>' + '<td><i class="fa fa-lg fa-fax fa-fw"></i>' + json.fax + '</td');
			}
			if(json.voip_server){
				$('tbody').append('<tr><td><strong>VoIP:</strong></td>' + '<td><i class="fa fa-lg fa-phone-square fa-fw"></i>' + json.voip_server + '</td>');
			}
			if(json.email){
				$('tbody').append('<tr><td><strong>Email:</strong></td>' + '<td><i class="fa fa-lg fa-envelope fa-fw"></i>' + json.email + '</td>');
			}
			if(json.facebook){
				$('tbody').append('<tr><td><strong>Facebook:</strong></td>' + '<td><i class="fa fa-lg fa-facebook fa-fw"></i>' + json.facebook + '</td>');
			}
			if(json.twitter){
				$('tbody').append('<tr><td><strong>Twitter:</strong></td>' + '<td><i class="fa fa-lg fa-twitter fa-fw"></i>' + json.twitter + '</td>');
			}
		});
	});


	//
	//== Residents Modal
	//
	$('#residents-listnav').on('click', '.show', function(){
		var id = $(this).attr('id');
		var url = baseURL + '/' + 'show?id=' + id;
		$.get(url, function(json){
			//since we're relying on append let's empty our containers first
			$('#residents-name').empty();
			$('tbody').empty();
			$('#residents-name').append(json.last_name1);
			$('#residents-name').append(' '+json.first_name1);
			$('#residents-name').append(' '+json.last_name2);
			$('#residents-name').append(' '+json.first_name2);
			if(json.street_address){
				$('tbody').append('<tr><td><strong>Address:</strong></td>' + '<td><i class="fa fa-lg fa-map-marker fa-fw"></i>' + json.street_address + '</td>');
			}
			if(json.apdo){
				$('tbody').append('<tr><td><strong>PO Box:</strong></td>' + '<td><i class="fa fa-lg fa-envelope-square fa-fw"></i>' + json.apdo + '</td>');
			}
			if(json.tel){
				$('tbody').append('<tr><td><strong>Phone:</strong></td>' + '<td><i class="fa fa-lg fa-phone fa-fw"></i>' + json.tel + '</td>');
			}
			if(json.tel2){
				$('tbody').append('<tr><td><strong>Phone 2:</strong></td>' + '<td><i class="fa fa-lg fa-phone fa-fw"></i>' + json.tel2 + '<br>');
			}
			if(json.fax){
				$('tbody').append('<tr><td><strong>Fax:</strong></td>' + '<td><i class="fa fa-lg fa-fax fa-fw"></i>' + json.fax + '</td');
			}
			if(json.voip_server){
				$('tbody').append('<tr><td><strong>VoIP:</strong></td>' + '<td><i class="fa fa-lg fa-phone-square fa-fw"></i>' + json.voip_server + '</td>');
			}
			if(json.email){
				$('tbody').append('<tr><td><strong>Email:</strong></td>' + '<td><i class="fa fa-lg fa-envelope fa-fw"></i>' + json.email + '</td>');
			}
			if(json.facebook){
				$('tbody').append('<tr><td><strong>Facebook:</strong></td>' + '<td><i class="fa fa-lg fa-facebook fa-fw"></i>' + json.facebook + '</td>');
			}
			if(json.twitter){
				$('tbody').append('<tr><td><strong>Twitter:</strong></td>' + '<td><i class="fa fa-lg fa-twitter fa-fw"></i>' + json.twitter + '</td>');
			}
		});
	});

	//
	//== Pages Modal
	//
	$('#modal-pages').on('show.bs.modal', function (event){
		var link = $(event.relatedTarget); // link that triggered the event
		var id = link.attr('id');
		var url = baseURL + '/' + 'detail?id=' + id;
		//since we're relying on append let's empty our containers first
		$('h4.modal-title').empty();
		$('#pages-image').attr('src', '');
		$('tbody').empty();
		$.get(url, function(json){
			$('h4.modal-title').append(json.business_name);
			if(json.address){
				$('tbody').append('<tr><td><strong>Address:</strong></td>' + '<td><i class="fa fa-lg fa-map-marker fa-fw"></i>' + json.address + '</td></tr>');
			}
			if(json.phone){
				$('tbody').append('<tr><td><strong>Phone:</strong></td>' + '<td><i class="fa fa-lg fa-phone fa-fw"></i>' + json.phone + '</td></tr>');
			}
			if(json.phone2){
				$('tbody').append('<tr><td><strong>Phone 2:</strong></td>' + '<td><i class="fa fa-lg fa-phone fa-fw"></i>' + json.phone2 + '</td></tr>');
			}
			if(json.mobile_phone){
				$('tbody').append('<tr><td><strong>Cell Phone:</strong></td>' + '<td><i class="fa fa-lg fa-mobile fa-fw"></i>' + json.mobile_phone + '</td></tr>');
			}
			if(json.mobile_phone2){
				$('tbody').append('<tr><td><strong>Cell Phone 2:</strong></td>' + '<td><i class="fa fa-lg fa-mobile fa-fw"></i>' + json.mobile_phone2 + '</td></tr>');
			}
			if(json.nextel){
				$('tbody').append('<tr><td><strong>Nextel:</strong></td>' + '<td><i class="fa fa-lg fa-mobile fa-fw"></i>' + json.nextel + '</td></tr>');
			}
			if(json.email){
				$('tbody').append('<tr><td><strong>Email:</strong></td>' + '<td><i class="fa fa-lg fa-envelope fa-fw"></i><a href="mailto:' + json.email + '">' + json.email + '</a></td></tr>');
			}
			if(json.email){
				$('tbody').append('<tr><td><strong>Email 2:</strong></td>' + '<td><i class="fa fa-lg fa-envelope fa-fw"></i><a href="mailto:' + json.email2 + '">' + json.email2 + '</a></td></tr>');
			}
			if(json.web_page){
				$('tbody').append('<tr><td><strong>Website:</strong></td>' + '<td><i class="fa fa-lg fa-external-link-square fa-fw"></i><a target="_blank" href="http://' + json.web_page + '">' + json.web_page + '</a></td></tr>');
			}
			if(json.facebook){
				$('tbody').append('<tr><td><strong>Facebook:</strong></td>' + '<td><i class="fa fa-lg fa-facebook fa-fw"></i><a target="_blank" href="https://facebook.com/' + json.facebook + '">' + json.facebook + '</a></td></tr>');
			}
			if(json.twitter){
				$('tbody').append('<tr><td><strong>Twitter:</strong></td>' + '<td><i class="fa fa-lg fa-twitter fa-fw"></i><a target="_blank" href="https://twitter.com/' + json.twitter + '">' + json.twitter + '</a></td></tr>');
			}
			if(json.other){
				$('tbody').append('<tr><td><strong>Other:</strong></td>' + '<td>'+ json.other + '</td></tr>');
			}
			if(json.image){
				var imageURL = baseURL + '/assets/anuncios/' + id + '.jpg';
				$('#pages-image').attr('src', imageURL);
			}
		});
	});

	//
	//== Paginas Modal
	//
	$('#modal-paginas').on('show.bs.modal', function (event){
		var link = $(event.relatedTarget); // link that triggered the event
		var id = link.attr('id');
		var url = baseURL + '/' + 'detalle?id=' + id;
		//since we're relying on append let's empty our containers first
		$('h4.modal-title').empty();
		$('#paginas-image').attr('src', '');
		$('tbody').empty();
		$.get(url, function(json){
			$('h4.modal-title').append(json.business_name);
			if(json.address){
				$('tbody').append('<tr><td><strong>Dirección:</strong></td>' + '<td><i class="fa fa-lg fa-map-marker fa-fw"></i>' + json.address + '</td></tr>');
			}
			if(json.phone){
				$('tbody').append('<tr><td><strong>Teléfono:</strong></td>' + '<td><i class="fa fa-lg fa-phone fa-fw"></i>' + json.phone + '</td></tr>');
			}
			if(json.phone2){
				$('tbody').append('<tr><td><strong>Teléfono 2:</strong></td>' + '<td><i class="fa fa-lg fa-phone fa-fw"></i>' + json.phone2 + '</td></tr>');
			}
			if(json.mobile_phone){
				$('tbody').append('<tr><td><strong>Celular:</strong></td>' + '<td><i class="fa fa-lg fa-mobile fa-fw"></i>' + json.mobile_phone + '</td></tr>');
			}
			if(json.mobile_phone2){
				$('tbody').append('<tr><td><strong>Celular 2:</strong></td>' + '<td><i class="fa fa-lg fa-mobile fa-fw"></i>' + json.mobile_phone2 + '</td></tr>');
			}
			if(json.nextel){
				$('tbody').append('<tr><td><strong>Nextel:</strong></td>' + '<td><i class="fa fa-lg fa-mobile fa-fw"></i>' + json.nextel + '</td></tr>');
			}
			if(json.email){
				$('tbody').append('<tr><td><strong>Email:</strong></td>' + '<td><i class="fa fa-lg fa-envelope fa-fw"></i><a href="mailto:' + json.email + '">' + json.email + '</a></td></tr>');
			}
			if(json.email2){
				$('tbody').append('<tr><td><strong>Email 2:</strong></td>' + '<td><i class="fa fa-lg fa-envelope fa-fw"></i><a href="mailto:' + json.email2 + '">' + json.email2 + '</a></td></tr>');
			}
			if(json.web_page){
				$('tbody').append('<tr><td><strong>Sitio Web:</strong></td>' + '<td><i class="fa fa-lg fa-external-link-square fa-fw"></i><a target="_blank" href="http://' + json.web_page + '">' + json.web_page + '</a></td></tr>');
			}
			if(json.facebook){
				$('tbody').append('<tr><td><strong>Facebook:</strong></td>' + '<td><i class="fa fa-lg fa-facebook fa-fw"></i><a target="_blank" href="https://facebook.com/' + json.facebook + '">' + json.facebook + '</a></td></tr>');
			}
			if(json.twitter){
				$('tbody').append('<tr><td><strong>Twitter:</strong></td>' + '<td><i class="fa fa-lg fa-twitter fa-fw"></i><a target="_blank" href="https://twitter.com/' + json.twitter + '">' + json.twitter + '</a></td></tr>');
			}
			if(json.other){
				$('tbody').append('<tr><td><strong>Otro:</strong></td>' + '<td>'+ json.other + '</td></tr>');
			}
			if(json.image){
				var imageURL = baseURL + '/assets/anuncios/' + id + '.jpg';
				$('#paginas-image').attr('src', imageURL);
			}
		});
	});

	//
	//== Buscar Modal
	//
	$('#buscar-accordion').on('click', '.show', function(){
		var id = $(this).attr('id');
		var url = baseURL + '/' + 'detalle?id=' + id;
		$.get(url, function(json){
			//since we're relying on append let's empty our containers first
			$('h4.modal-title').empty();
			$('#buscar-image').attr('src', '');
			$('.modal tbody').empty();
			$('h4.modal-title').append(json.business_name);
			if(json.address){
				$('.modal tbody').append('<tr><td><strong>Dirección:</strong></td>' + '<td><i class="fa fa-lg fa-map-marker fa-fw"></i>' + json.address + '</td></tr>');
			}
			if(json.phone){
				$('.modal tbody').append('<tr><td><strong>Teléfono:</strong></td>' + '<td><i class="fa fa-lg fa-phone fa-fw"></i>' + json.phone + '</td></tr>');
			}
			if(json.phone2){
				$('.modal tbody').append('<tr><td><strong>Teléfono 2:</strong></td>' + '<td><i class="fa fa-lg fa-phone fa-fw"></i>' + json.phone2 + '</td></tr>');
			}
			if(json.mobile_phone){
				$('.modal tbody').append('<tr><td><strong>Celular:</strong></td>' + '<td><i class="fa fa-lg fa-mobile fa-fw"></i>' + json.mobile_phone + '</td></tr>');
			}
			if(json.mobile_phone2){
				$('.modal tbody').append('<tr><td><strong>Celular 2:</strong></td>' + '<td><i class="fa fa-lg fa-mobile fa-fw"></i>' + json.mobile_phone2 + '</td></tr>');
			}
			if(json.nextel){
				$('.modal tbody').append('<tr><td><strong>Nextel:</strong></td>' + '<td><i class="fa fa-lg fa-mobile fa-fw"></i>' + json.nextel + '</td></tr>');
			}
			if(json.email){
				$('.modal tbody').append('<tr><td><strong>Email:</strong></td>' + '<td><i class="fa fa-lg fa-envelope fa-fw"></i><a href="mailto:' + json.email + '">' + json.email + '</a></td></tr>');
			}
			if(json.email2){
				$('.modal tbody').append('<tr><td><strong>Email 2:</strong></td>' + '<td><i class="fa fa-lg fa-envelope fa-fw"></i><a href="mailto:' + json.email2 + '">' + json.email2 + '</a></td></tr>');
			}
			if(json.web_page){
				$('.modal tbody').append('<tr><td><strong>Sitio Web:</strong></td>' + '<td><i class="fa fa-lg fa-external-link-square fa-fw"></i><a target="_blank" href="http://' + json.web_page + '">' + json.web_page + '</a></td></tr>');
			}
			if(json.facebook){
				$('.modal tbody').append('<tr><td><strong>Facebook:</strong></td>' + '<td><i class="fa fa-lg fa-facebook fa-fw"></i><a target="_blank" href="https://facebook.com/' + json.facebook + '">' + json.facebook + '</a></td></tr>');
			}
			if(json.twitter){
				$('.modal tbody').append('<tr><td><strong>Twitter:</strong></td>' + '<td><i class="fa fa-lg fa-twitter fa-fw"></i><a target="_blank" href="https://twitter.com/' + json.twitter + '">' + json.twitter + '</a></td></tr>');
			}
			if(json.other){
				$('.modal tbody').append('<tr><td><strong>Otro:</strong></td>' + '<td>'+ json.other + '</td></tr>');
			}
			if(json.image){
				var imageURL = baseURL + '/assets/anuncios/' + id + '.jpg';
				$('#buscar-image').attr('src', imageURL);
			}
		});
	});

	//
	//== Search Modal
	//
	$('#search-accordion').on('click', '.show', function(){
		var id = $(this).attr('id');
		var url = baseURL + '/' + 'detail?id=' + id;
		$.get(url, function(json){
			//since we're relying on append let's empty our containers first
			$('h4.modal-title').empty();
			$('#search-image').attr('src', '');
			$('.modal tbody').empty();
			$('h4.modal-title').append(json.business_name);
			if(json.address){
				$('.modal tbody').append('<tr><td><strong>Address:</strong></td>' + '<td><i class="fa fa-lg fa-map-marker fa-fw"></i>' + json.address + '</td></tr>');
			}
			if(json.phone){
				$('.modal tbody').append('<tr><td><strong>Phone:</strong></td>' + '<td><i class="fa fa-lg fa-phone fa-fw"></i>' + json.phone + '</td></tr>');
			}
			if(json.phone2){
				$('.modal tbody').append('<tr><td><strong>Phone 2:</strong></td>' + '<td><i class="fa fa-lg fa-phone fa-fw"></i>' + json.phone2 + '</td></tr>');
			}
			if(json.mobile_phone){
				$('.modal tbody').append('<tr><td><strong>Cell Phone:</strong></td>' + '<td><i class="fa fa-lg fa-mobile fa-fw"></i>' + json.mobile_phone + '</td></tr>');
			}
			if(json.mobile_phone2){
				$('.modal tbody').append('<tr><td><strong>Cell Phone 2:</strong></td>' + '<td><i class="fa fa-lg fa-mobile fa-fw"></i>' + json.mobile_phone2 + '</td></tr>');
			}
			if(json.nextel){
				$('.modal tbody').append('<tr><td><strong>Nextel:</strong></td>' + '<td><i class="fa fa-lg fa-mobile fa-fw"></i>' + json.nextel + '</td></tr>');
			}
			if(json.email){
				$('.modal tbody').append('<tr><td><strong>Email:</strong></td>' + '<td><i class="fa fa-lg fa-envelope fa-fw"></i><a href="mailto:' + json.email + '">' + json.email + '</a></td></tr>');
			}
			if(json.email2){
				$('.modal tbody').append('<tr><td><strong>Email 2:</strong></td>' + '<td><i class="fa fa-lg fa-envelope fa-fw"></i><a href="mailto:' + json.email2 + '">' + json.email2 + '</a></td></tr>');
			}
			if(json.web_page){
				$('.modal tbody').append('<tr><td><strong>Web Site:</strong></td>' + '<td><i class="fa fa-lg fa-external-link-square fa-fw"></i><a target="_blank" href="http://' + json.web_page + '">' + json.web_page + '</a></td></tr>');
			}
			if(json.facebook){
				$('.modal tbody').append('<tr><td><strong>Facebook:</strong></td>' + '<td><i class="fa fa-lg fa-facebook fa-fw"></i><a target="_blank" href="https://facebook.com/' + json.facebook + '">' + json.facebook + '</a></td></tr>');
			}
			if(json.twitter){
				$('.modal tbody').append('<tr><td><strong>Twitter:</strong></td>' + '<td><i class="fa fa-lg fa-twitter fa-fw"></i><a target="_blank" href="https://twitter.com/' + json.twitter + '">' + json.twitter + '</a></td></tr>');
			}
			if(json.other){
				$('.modal tbody').append('<tr><td><strong>Other:</strong></td>' + '<td>'+ json.other + '</td></tr>');
			}
			if(json.image){
				var imageURL = baseURL + '/assets/anuncios/' + id + '.jpg';
				$('#search-image').attr('src', imageURL);
			}
		});
	});

}); //Jquery is loaded