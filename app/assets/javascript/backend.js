//Custom JS for the backend
$(function(){

	// Automatically close alerts
	// window.setTimeout(function() {
 //    $(".alert").slideUp(500, function(){
 //        $(this).alert('close'); 
 //    });
	// }, 5000);

  // Initialize all confirmations on a page
  $('[data-toggle=confirmation]').confirmation({
  	container: 'body',
  	singleton: true,
  	popout: true,
  	btnOkIcon: 'fa fa-check',
  	btnCancelIcon: 'fa fa-times'
  });

  //File Input
  $('input[type=file]').bootstrapFileInput();

	//
	//== Datatables
	//
	
	// Categories
	$('#table-categories').dataTable({
		stateSave: true,
		responsive: true,
		dom: "<'row'<'col-sm-1'<'#btn-new-categories'>><'col-sm-5'l><'col-sm-6'f>>" +
				"<'row'<'col-sm-12'tr>>" +
				"<'row'<'col-sm-6'i><'col-sm-6'p>>",
		"columnDefs": [{ "orderable": false, "targets": 2 }]
	});
	
	//Categories custom button
	$('#btn-new-categories').html('<a href="#modal-new" data-toggle="modal" data-target="#modal-new"class="btn btn-primary btn-sm" id="table-btn-new"><i class="fa fa-plus-square fa-fw"></i>New</a>');

	// Categories Edit
	$('#table-categories').on('click', '.edit', function(){
		// the data action of the categories controller expects the id
		var id = $(this).attr('id');
		var url = 'edit' + '/' + id;
		//var data = $('#val').serialize();
		// $.post(url, data, function(json){
		$.post(url, function(json){
			if(json.success) {
				$('#form-edit input[name=category]').val(json.category);
				$('#form-edit input[name=categoria]').val(json.categoria);
				// baseURL is created in master layout with the "URL::to('/')" helper
				var newAction = baseURL + '/' + 'admin/categories/update' + '/' + id;
				$('#form-edit').attr('action', newAction);
			}
		});
	});

	// Residents
	$('#table-residents').dataTable({
		stateSave: true,
		responsive: true,
		dom: "<'row'<'col-sm-1'<'#btn-new-residents'>><'col-sm-5'l><'col-sm-6'f>>" +
				"<'row'<'col-sm-12'tr>>" +
				"<'row'<'col-sm-6'i><'col-sm-6'p>>",
		"columnDefs": [{ "orderable": false, "targets": 1 }]
	});

	//Residents custom button
	$('#btn-new-residents').html('<a href="#modal-new" data-toggle="modal" data-target="#modal-new"class="btn btn-primary btn-sm" id="table-btn-new"><i class="fa fa-plus-square fa-fw"></i>New</a>');

	// Residents Edit
	$('#table-residents').on('click', '.edit', function(){
		var id = $(this).attr('id');
		var url = 'edit' + '/' + id;
		$.post(url, function(json){
			$('#form-edit input[name=last_name1]').val(json.last_name1);
			$('#form-edit input[name=first_name1]').val(json.first_name1);
			$('#form-edit input[name=last_name2]').val(json.last_name2);
			$('#form-edit input[name=first_name2]').val(json.first_name2);
			$('#form-edit input[name=street_address]').val(json.street_address);
			$('#form-edit input[name=apdo]').val(json.apdo);
			$('#form-edit input[name=tel]').val(json.tel);
			$('#form-edit input[name=tel2]').val(json.tel2);
			$('#form-edit input[name=fax]').val(json.fax);
			$('#form-edit input[name=voip_server]').val(json.voip_server);
			$('#form-edit input[name=email]').val(json.email);
			$('#form-edit input[name=facebook]').val(json.facebook);
			$('#form-edit input[name=twitter]').val(json.twitter);
			var newAction = baseURL + '/' + 'admin/residents/update' + '/' + id;
			$('#form-edit').attr('action', newAction);
		});
	});

	// Pages
	$('#table-pages').dataTable({
		stateSave: true,
		responsive: true,
		dom: "<'row'<'col-sm-1'<'#btn-new-pages'>><'col-sm-5'l><'col-sm-6'f>>" +
				"<'row'<'col-sm-12'tr>>" +
				"<'row'<'col-sm-6'i><'col-sm-6'p>>",
		"columnDefs": [{ "orderable": false, "targets": 1 }]
	});

	// Pages custom button
	$('#btn-new-pages').html('<a href="#modal-new" data-toggle="modal" data-target="#modal-new"class="btn btn-primary btn-sm" id="table-btn-new"><i class="fa fa-plus-square fa-fw"></i>New</a>');

	// Pages new button empties the file name span
	$('#btn-new-pages').on('click', function(){
		// var imageNew = $('#form-new #image-new');
		// imageNew.replaceWith(imageNew = imageNew.clone(true));
		$('#form-new span.file-input-name').empty();
	})

	// Pages Edit
	$('#table-pages').on('click', '.edit', function(){
		var id = $(this).attr('id');
		var editURL = 'edit' + '/' + id;
		var categoriesURL = 'categories' + '/' + id;
		$.post(editURL, function(json){
			$('#form-edit input[name=business_name]').val(json.business_name);
			$('#form-edit input[name=address]').val(json.address);
			$('#form-edit input[name=email]').val(json.email);
			$('#form-edit input[name=email2]').val(json.email2);
			$('#form-edit input[name=phone]').val(json.phone);
			$('#form-edit input[name=phone2]').val(json.phone2);
			$('#form-edit input[name=mobile_phone]').val(json.mobile_phone);
			$('#form-edit input[name=mobile_phone2]').val(json.mobile_phone2);
			$('#form-edit input[name=nextel]').val(json.nextel);
			$('#form-edit input[name=web_page]').val(json.web_page);
			$('#form-edit input[name=facebook]').val(json.facebook);
			$('#form-edit input[name=twitter]').val(json.twitter);
			$('#form-edit input[name=other]').val(json.other);
			//Clone the input[file]
			//var imageEdit = $('#form-edit #image-edit');
			//imageEdit.replaceWith(imageEdit = imageEdit.clone(true));
			//And empty the file name span
			$('#form-edit span.file-input-name').empty();
			$.post(categoriesURL, function(data){
				for (var i = 0; i < data.length; i++) {
					$('#form-edit #category'+ (i+1) + ' option').first().removeAttr('selected');
					$('#form-edit #category'+ (i+1) + ' option[value=' + data[i].category_id + ']').prop('selected', true);
					$('#form-edit input[name=hide'+ (i+1) +']').attr('value', data[i].id);
				};
			})
			var newAction = baseURL + '/' + 'admin/pages/update' + '/' + id;
			$('#form-edit').attr('action', newAction);
		});
	});

	// Events
	$('#table-events').dataTable({
		stateSave: true,
		responsive: true,
		dom: "<'row'<'col-sm-1'<'#btn-new-events'>><'col-sm-5'l><'col-sm-6'f>>" +
				"<'row'<'col-sm-12'tr>>" +
				"<'row'<'col-sm-6'i><'col-sm-6'p>>",
		"columnDefs": [{ "orderable": false, "targets": 3 }],
		"order": [ 2, 'asc' ]
	});

	// Events custom button
	$('#btn-new-events').html('<a href="#modal-new" data-toggle="modal" data-target="#modal-new"class="btn btn-primary btn-sm" id="table-btn-new"><i class="fa fa-plus-square fa-fw"></i>New</a>');

	// Events Edit
	$('#table-events').on('click', '.edit', function(){
		var id = $(this).attr('id');
		var url = 'edit' + '/' + id;
		$.post(url, function(json){
			if(json.success) {
				var newAction = baseURL + '/' + 'admin/events/update' + '/' + id;
				$('#form-edit').attr('action', newAction);
				$('#form-edit input[name=event]').val(json.event);
				$('#form-edit input[name=evento]').val(json.evento);
				$('#form-edit input[name=date]').val(json.date);
			}
		});
	});

}); //Jquery is loaded