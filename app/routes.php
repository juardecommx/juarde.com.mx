<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('english.index');
});

Route::get('home', function()
{
	return View::make('english.index');
});

Route::get('inicio', function()
{
	return View::make('spanish.index');
});


//Admin
Route::controller('admin/contact', 'ContactController');
Route::controller('admin/events', 'EventsController');
Route::controller('admin/pages', 'AdvertisementsController');
Route::controller('admin/residents', 'ResidentsController');
Route::controller('admin/categories', 'CategoriesController');
Route::get('admin','AdminController@getIndex');

//Public Spanish
Route::get('buscar','AdvertisementsController@getIndex2');
Route::get('categorias','CategoriesController@getIndex2');
Route::get('eventos','EventsController@getIndex2');
Route::get('ver','ResidentsController@getVer');
Route::get('detalle','AdvertisersController@getDetalle');
Route::get('contacto','ContactController@getIndex2');
Route::get('residentes','ResidentsController@getIndex2');
Route::get('paginas', 'AdvertisementsController@getPages2');

//Public English
Route::get('advertisements','AdvertisementsController@getIndex');
Route::get('residents','ResidentsController@getIndex');
Route::get('show','ResidentsController@getShow');
Route::get('detail','AdvertisersController@getDetail');
Route::get('contact','ContactController@getIndex');
Route::get('contact/send','ContactController@anySend');
Route::get('categories','CategoriesController@getIndex');
Route::get('events','EventsController@getIndex');
Route::get('pages', 'AdvertisementsController@getPages');


//Login
//Route::get('login',array('uses'=>'LoginController@ShowLogin'));
Route::post('login',array('uses'=>'LoginController@DoLogin'));
Route::get('logout', array('uses' => 'LoginController@DoLogout'));

Route::when('admin', 'auth');
Route::when('admin/*', 'auth');



//Script de envio de email
Route::any('send', function()
{
	$cadena_buscada   = 'domperidone';
	$posicion_coincidencia = strpos(Input::get('content'), $cadena_buscada);
	if ($posicion_coincidencia === false) {

		if ($_POST['formValidator']=="") {
		
	    $data=array(
					'name'=>Input::get('name'),
					'email'=>Input::get('email'),
					'content'=>Input::get('content')
				);
			$toEmail='info@juarde.com.mx';
			$fromName='Juarde.com.mx';

			Mail::send('emails.mail',$data,function($confirm) use ($fromName,$toEmail){
				$confirm->to($toEmail,$fromName);
				$confirm->from($toEmail,$fromName);
				$confirm->subject('Mail del sitio Juarde.com.mx');
			});
		}
	}
	return Redirect::to('contact');
});

Route::any('enviar', function()
{
	$cadena_buscada   = 'domperidone';
	$posicion_coincidencia = strpos(Input::get('content'), $cadena_buscada);
	if ($posicion_coincidencia === false) {

		if ($_POST['formValidator']=="") {

	    $data=array(
					'name'=>Input::get('name'),
					'email'=>Input::get('email'),
					'content'=>Input::get('content')
				);
			$toEmail='info@juarde.com.mx';
			$fromName='Juarde.com.mx';

			Mail::send('emails.mail',$data,function($confirm) use ($fromName,$toEmail){
				$confirm->to($toEmail,$fromName);
				$confirm->from($toEmail,$fromName);
				$confirm->subject('Mail del sitio Juarde.com.mx');
			});
		}
	}
		return Redirect::to('contacto');
});